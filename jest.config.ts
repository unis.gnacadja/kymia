import type { Config } from 'jest';

const config: Config = {
    displayName: 'Teleconsulation',
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    globalSetup: 'jest-preset-angular/global-setup',
    projects: [
        '<rootDir>/projects/idermato',
        '<rootDir>/projects/kymia',
        '<rootDir>/projects/lib'
    ],
    collectCoverage: true,
    collectCoverageFrom: ['src/app/**/*.ts', '!src/app/modules', '**/*.ts'],
    coveragePathIgnorePatterns: [
        'node_modules',
        'test-config',
        'interfaces',
        'jestGlobalMocks.ts',
        '.module.ts',
        '.validator.ts',
        '<rootDir>/src/app/polyfill.ts',
        '<rootDir>/src/app/modules',
        '.mock.ts',
        'constants.ts',
        'jest.config.ts',
        'enum.ts'
    ],
    reporters: [
        ['default', { outputDirectory: 'coverage', outputName: 'junit.xml' }]
    ],
    testResultsProcessor: 'jest-sonar-reporter'
};

export default config;
