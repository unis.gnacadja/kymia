module.exports = {
  "singleQuote": true,
  "endOfLine": "lf",
  "jsxBracketSameLine": false,
  "printWidth": 80,
  "tabWidth": 4,
  "trailingComma": "none",
  "semi": true,
  "arrowParens": "always",
  "importOrder": ["^@core/(.*)$", "^@server/(.*)$", "^@ui/(.*)$", "^[./]"],
  "importOrderSeparation": true,
  "importOrderSortSpecifiers": true,
  "importOrderParserPlugins": ["typescript", "decorators-legacy"],
  "plugins": [require.resolve("@trivago/prettier-plugin-sort-imports")]
}
