/* eslint-disable @typescript-eslint/naming-convention */
import type { Config } from 'jest';

const config: Config = {
    displayName: 'Kymia',
    verbose: true,
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    globalSetup: 'jest-preset-angular/global-setup',
    rootDir: './',
    roots: ['<rootDir>'],
    modulePaths: ['<rootDir>'],
    moduleNameMapper: {
        '^@lib/(.*)$': '<rootDir>/../lib/$1',
        '^@idermato/(.*)$': '<rootDir>/../idermato/$1'
    },
    collectCoverage: true,
    collectCoverageFrom: [
        'src/app/**/*.ts',
        '!src/app/modules',
        '!src/app/mock'
    ],
    coveragePathIgnorePatterns: [
        'node_modules',
        'test-config',
        'interfaces',
        'jestGlobalMocks.ts',
        '.module.ts',
        '<rootDir>/src/app/polyfill.ts',
        '<rootDir>/src/app/modules',
        '<rootDir>/src/app/models',
        'jest.config.ts',
        'main.ts',
        'polyfills.ts',
        'environments/*',
        'zone-flags.ts',
        'api.interceptor.ts',
        '.mock.json',
        'mock'
    ],
    coverageDirectory: './coverage',
    coverageReporters: ['clover', 'json', 'lcov', ['text', { skipFull: true }]],
    reporters: [
        ['default', { outputDirectory: 'coverage', outputName: 'junit.xml' }]
    ],
    testResultsProcessor: 'jest-sonar-reporter'
};

export default config;
