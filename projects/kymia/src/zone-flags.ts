/* eslint-disable no-unused-vars */
/**
 * Prevents Angular change detection from
 * running with certain Web Component callbacks
 */
const obj = window as any;
let { ZONE_DISABLE_CUSTOMELEMENTS: ZONE_DISABLE_CUSTOMELEMENTS } = obj;
ZONE_DISABLE_CUSTOMELEMENTS = true;
