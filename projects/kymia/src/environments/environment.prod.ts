export const environment = {
    production: true,
    sentryTrace: 0.4,
    introKey: 'intro-has-seen',
    baseUrl: 'https://teleconsulting.betatest.kymia.tech',
    standalone: 'https://api.dev.dermato.rintio.com',
    openStreetapi: 'https://nominatim.openstreetmap.org',
    emobpayGateway: 'https://emobpay.rintio.com/',
    user: {
        avatar: 'https://ionicframework.com/docs/img/demos/avatar.svg'
    }
};
