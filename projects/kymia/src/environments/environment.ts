// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    sentryTrace: 1.0,
    introKey: 'intro-has-seen',
    baseUrl: 'https://api.dev.dermato.rintio.com',
    standalone: 'https://api.dev.dermato.rintio.com',
    openStreetapi: 'https://nominatim.openstreetmap.org',
    emobpayGateway: 'https://emobpay.rintio.com/',
    user: {
        avatar: 'https://ionicframework.com/docs/img/demos/avatar.svg'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
//  * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
