import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
import * as Sentry from '@sentry/angular-ivy';
import { BrowserTracing } from '@sentry/tracing';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
if (environment.production) {
    enableProdMode();
}

// monitoring app
Sentry.init({
    dsn: 'https://6bdced0fdd9e4f07966770f2f5f4cad8@o4504813681377280.ingest.sentry.io/4504813684391936',
    integrations: [new BrowserTracing()],
    tracesSampleRate: environment.sentryTrace
});
platformBrowserDynamic().bootstrapModule(AppModule);

defineCustomElements(window);
