export enum OrderBy {
    asc = 'asc',
    desc = 'desc'
}

export const conditions: string[] = [
    'L’application n’est pas responsable de la qualité des soins du médecin',
    'L’application n’est pas responsable des informations que vous enregistrez',
    'L’application n’est pas responsable de votre choix de médecin',
    'Les paiements ou justificatifs de paiements doivent être enregistrés dans l’application',
    'Au cas où le rendez vous avec le médecin serait annulé vous pouvez choisir un autre rendez vous',
    'Les médecins sont notés suivants vos avis et commentaires'
];

export const reasons: string[] = [
    'La rapidité pour trouver un bon Médecin',
    'La facilité pour prendre un rendez vous',
    'La gestion simplifiée de votre planning de consultations'
];

export const statusMessages: Record<string, string> = {
    terminated: 'Terminée',
    payed: 'Payé',
    annuled: 'Annulée',
    confirmByDoctor: 'Confirmée',
    waiting: 'En attente'
};

export const messages: Record<string, string> = {
    errorAuth:
        "Oups ! Votre mot de passe ou votre adresse email n'est pas correct",
    errorEmail: "L'adresse e-mail est invalide ou non renseignée",
    congratulation: 'Félicitations',
    welcome:
        'Bienvenue dans Kymia, vous êtes désormais abonné à la meilleure base de données de médecins pour vous consulter au plus tôt.',
    start: 'Commencer',
    login: 'Se connecter',
    passwordReseted: 'Mot de passe réinitialisé',
    passwordSuccessfullyReseted: `Votre mot de passe a été modifié avec succès.
            Suivez les instructions contenues dans le mail qui vous a été envoyé pour vous connecter.`,
    findDoctor: 'Trouvez un praticien pour une téléconsultation',
    notifyAvailability: 'Rendez-vous pris avec succès',
    noDescription: 'Veuillez renseigner le symptôme',
    noFile: 'Veuillez sélectionner un fichier',
    noAppointment: `Vous n'avez pas de rendez-vous actuellement`,
    consultationInPending:
        "Le rendez-vous ne peut-être confirmé par le médecin qu'après paiement des frais de consultation.",
    fileAccepted: 'Fichier chargé avec succès',
    paymentMessage: 'Envoyer un justificatif de paiement',
    errorFile:
        'Le fichier n’a pas pu être chargé, vérifiez le format (JPG ou PDF) et la taille limitée à 1 Mo',
    noAvailability: 'Aucune disponibilité trouvée',
    succefullyUpdated: 'Modifié avec succès',
    sendError: "Erreur d'envoi",
    existMail: 'Cet email existe déjà',
    phoneError: 'Le champ doit contenir entre 6 et 16 chiffres',
    mustContainLetters: 'Le champ doit contenir des lettres',
    errorFileSymptom:
        'Le format du fichier sélectionné n’est pas pris en charge'
};
