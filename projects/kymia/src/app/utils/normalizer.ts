import { normalizeDoctor } from '@lib/normalizer';
import { Calendar, Consultation } from '../models';

export const normalizeCalendar: (calendar: any) => Calendar = (
    calendar: any
) => ({
    available: calendar.available,
    availableDate: calendar.availableDate.split('/').reverse().join('/'),
    availableDateEnd: calendar.availableDateEnd.slice(0, 5),
    availableDateStart: calendar.availableDateStart.slice(0, 5),
    id: calendar.id,
    ...(calendar.doctor && {
        doctor: normalizeDoctor(calendar.doctor)
    })
});

export const normalizeConsultation: (consultation: any) => Consultation = (
    consultation: any
) => ({
    id: consultation.id,
    ...(consultation.calendar && {
        calendar: normalizeCalendar(consultation.calendar)
    }),
    ...(consultation.images && { images: consultation.images }),
    ...(consultation.problemDescription && {
        problemDescription: consultation.problemDescription
    }),
    ...(consultation.status && { status: consultation.status }),
    ...(consultation.callUrl && { callUrl: consultation.callUrl }),
    ...(consultation.descriptionClinique && {
        clinicalDescription: consultation.descriptionClinique
    }),
    ...(consultation.explorationParaclinique && {
        paraclinicalExploration: consultation.explorationParaclinique
    }),
    ...(consultation.traitement && { treatment: consultation.traitement }),
    ...(consultation.evolution && { evolution: consultation.evolution }),
    ...(consultation.diagnostique && { diagnosis: consultation.diagnostique }),
    ...(consultation.transaction && { transaction: consultation.transaction }),
    ...(consultation.ordonnaceUrl && {
        ordonnaceUrl: consultation.ordonnaceUrl
    })
});
