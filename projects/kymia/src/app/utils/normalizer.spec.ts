import { environment } from 'src/environments/environment';
import { ConsultationMock } from '../mock';
import { normalizeConsultation } from './normalizer';

it('should normalize consultation data', () => {
    const consultationWithPhoto = {
        ...ConsultationMock.terminated,
        calendar: {
            ...ConsultationMock.terminated.calendar,
            doctor: {
                ...ConsultationMock.terminated.calendar.doctor,
                photo: 'example.png'
            }
        }
    };
    expect(
        normalizeConsultation(consultationWithPhoto).calendar.doctor.photo
    ).toEqual('example.png');
});

it('should normalize doctor data with default environment as doctor picture when doctor has no photo', () => {
    expect(
        normalizeConsultation(ConsultationMock.terminated).calendar.doctor.photo
    ).toBe(environment.user.avatar);
});
