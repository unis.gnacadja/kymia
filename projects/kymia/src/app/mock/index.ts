import { normalizeDoctor } from '@lib/normalizer';
import { BehaviorSubject, of } from 'rxjs';
import { Consultation, Location } from '../models';
import { normalizeConsultation } from '../utils/normalizer';
import * as ActivityMock from './activity.mock.json';
import * as MockCities from './cities.json';
import * as ConsultationMock from './consultation.mock.json';
import * as CountriesMock from './countries.mock.json';
import * as CriteriaMock from './criteria.mock.json';
import * as Dates from './dates.json';
import * as DoctorMock from './doctor.mock.json';
import * as Doctors from './listDoctors.json';
import * as MockPhoto from './photo.json';
import * as PlaceMock from './place.mock.json';
import * as SpecialityMock from './speciality.mock.json';
import * as MockUser from './user.json';

const mockReturnValue = <T>(value: T) => jest.fn().mockReturnValue(of(value));

export const normalizedDoctor = normalizeDoctor(DoctorMock);

export const doctorServiceMock = {
    list: Array(10).fill(normalizedDoctor),
    search: jest.fn().mockReturnValue(
        of({
            content: [DoctorMock]
        })
    ),
    getDoctor: jest
        .fn()
        .mockReturnValue(of(ConsultationMock.waiting.calendar.doctor)),
    getAvailableDates: jest.fn().mockReturnValue(of({ content: Dates })),
    getDate: jest.fn().mockReturnValue(of({ content: Dates }))
};

export const consultationList: Record<string, Consultation> = {
    waiting: normalizeConsultation(ConsultationMock.waiting),
    confirmed: normalizeConsultation(ConsultationMock.confirmed),
    canceled: normalizeConsultation(ConsultationMock.canceled),
    terminated: normalizeConsultation(ConsultationMock.terminated),
    payed: normalizeConsultation(ConsultationMock.payed)
};

export const consultationServiceMock = {
    list: Object.values(consultationList),
    registerConsultation: jest
        .fn()
        .mockReturnValue(of(ConsultationMock.waiting)),
    getPatientSchedule: jest
        .fn()
        .mockReturnValue(of({ content: [ConsultationMock.waiting] })),
    registerSchedulePaiement: jest
        .fn()
        .mockReturnValue(of(ConsultationMock.payed)),
    cancelSchedule: mockReturnValue({}),
    getConsultation: mockReturnValue(ConsultationMock.canceled)
};

export const commonServiceMock = {
    getActivities: jest.fn().mockReturnValue(of({ content: ActivityMock })),
    reverseLocationMap: jest.fn().mockReturnValue(
        of({
            address: {
                id: Doctors.content[0].city.id,
                city: Doctors.content[0].city.name,
                country: Doctors.content[0].city.country
            }
        })
    ),
    domains: ActivityMock
};

export const userServiceMock: {
    registerUser: jest.Mock;
    resetPassword: jest.Mock;
    getUserInfo: jest.Mock;
    getPatient: jest.Mock;
    updateProfilUser: jest.Mock;
    setUser: jest.Mock;
    updateLocation: jest.Mock;
    user: any;
    user$: BehaviorSubject<any>;
    location: BehaviorSubject<Location>;
} = {
    registerUser: mockReturnValue(MockUser.simple),
    resetPassword: mockReturnValue({}),
    getUserInfo: jest.fn().mockReturnValue(MockUser.simple),
    getPatient: mockReturnValue(MockUser.completeProfile),
    updateProfilUser: mockReturnValue({}),
    setUser: jest.fn(),
    updateLocation: jest.fn(),
    user: MockUser.simple,
    user$: new BehaviorSubject(MockUser.completeProfile),
    location: new BehaviorSubject({ id: '', city: '', country: '' })
};

export const citiesMock = [
    Doctors.content[0].city,
    Doctors.content[1].city,
    Doctors.content[2].city,
    Doctors.content[3].city
];

export const locationServiceMock = {
    getCountries: mockReturnValue({ content: CountriesMock }),
    searchCity: mockReturnValue({ content: citiesMock })
};

export const routerMock: { navigate: jest.Mock; navigateByUrl: jest.Mock } = {
    navigate: jest.fn(),
    navigateByUrl: jest.fn()
};

export const navControllerMock = {
    navigateForward: jest.fn(),
    navigate: jest.fn()
};

export const popoverControllerMock = {
    create: jest.fn().mockResolvedValue({
        present: jest.fn()
    }),
    present: jest.fn(),
    dismiss: jest.fn()
};

export {
    MockUser,
    ConsultationMock,
    CountriesMock,
    Dates,
    Doctors,
    ActivityMock,
    PlaceMock,
    DoctorMock,
    CriteriaMock,
    MockCities,
    MockPhoto,
    SpecialityMock
};
