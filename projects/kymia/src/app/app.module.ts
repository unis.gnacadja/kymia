import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import * as fr from '@angular/common/locales/fr';
import {
    CUSTOM_ELEMENTS_SCHEMA,
    ErrorHandler,
    LOCALE_ID,
    NgModule
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage-angular';
import * as Sentry from '@sentry/angular-ivy';
import { CommonService } from 'src/app/services/common.service';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { LocationService } from 'src/app/services/location/location.service';
import { StockageService } from 'src/app/services/stockage.service';
import { UserService } from 'src/app/services/user.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppreciationComponent } from './components/appreciation/appreciation.component';
import { CancelScheduleComponent } from './components/cancel-schedule/cancel-schedule.component';
import { SharedModule } from './components/shared/shared.module';
import { ApiInterceptor } from './services/api.interceptor';
@NgModule({
    declarations: [
        AppComponent,
        AppreciationComponent,
        CancelScheduleComponent
    ],
    entryComponents: [],
    imports: [
        IonicStorageModule.forRoot({
            name: '__Kymia' /// setting local database name
        }),
        BrowserModule,
        ReactiveFormsModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        SharedModule
    ],
    providers: [
        UserService,
        DoctorService,
        DatePipe,
        CommonService,
        LocationService,
        DoctorService,
        StockageService,
        ConsultationsService,
        { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: LOCALE_ID, useValue: 'fr' },
        {
            provide: ErrorHandler,
            useValue: Sentry.createErrorHandler({
                showDialog: true
            })
        },
        {
            provide: Sentry.TraceService,
            deps: [Router]
        }
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
    constructor() {
        registerLocaleData(fr.default);
    }
}
