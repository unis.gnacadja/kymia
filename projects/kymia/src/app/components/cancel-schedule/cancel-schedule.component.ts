import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import {
    NavController,
    PopoverController,
    ToastController
} from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Consultation } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { normalizeConsultation } from 'src/app/utils/normalizer';

@Component({
    selector: 'app-cancel-schedule',
    templateUrl: './cancel-schedule.component.html',
    styleUrls: ['./cancel-schedule.component.scss']
})
export class CancelScheduleComponent implements OnInit {
    @Input() id: string;
    commentaire: string = '';
    loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    formBuilder: FormBuilder;
    nav: NavController;
    editable: boolean = true;
    constructor(
        public consultationService: ConsultationsService,
        public popoverCtrl: PopoverController,
        public userservice: UserService,
        private router: Router,
        public toastCtrl: ToastController,
        public loadingService: LoadingService,
        public userService: UserService,
        public notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.consultationService.getConsultation(this.id).subscribe({
            next: (data: Consultation) => {
                if (ConsultationStatus.annuled === data.status) {
                    this.editable = false;
                    this.toastCtrl
                        .create({
                            message:
                                'Vous ne pouvez plus annuler cette consultation',
                            duration: 2000,
                            color: 'danger'
                        })
                        .then((toast) => {
                            toast.present();
                            this.popoverCtrl.dismiss();
                        });
                }
            },
            error: () => {
                this.editable = false;
                this.toastCtrl
                    .create({
                        message:
                            'Une erreur est survenue pendant la vérification de la consultation, veuillez réessayer',
                        duration: 2000,
                        color: 'danger'
                    })
                    .then((toast) => {
                        toast.present();
                        this.popoverCtrl.dismiss();
                    });
            }
        });
    }

    async onSubmit(): Promise<void> {
        if (this.commentaire.trim()) {
            await this.loadingService.present(helperMessages.loading);

            this.consultationService
                .cancelSchedule(this.id, this.commentaire.trim())
                .pipe(
                    finalize(() => {
                        this.loadingService.dismiss();
                    })
                )
                .subscribe({
                    next: () => {
                        this.consultationService
                            .getPatientSchedule(this.userService.user.id)
                            .subscribe((data: any) => {
                                this.consultationService.list =
                                    data.content.map(normalizeConsultation);
                            });
                        this.router.navigate(['/tabs/meetings']);
                        this.popoverCtrl.dismiss();
                    },
                    error: () => {
                        this.notifyService.presentToast();
                    }
                });
        }
    }

    quit(): void {
        this.popoverCtrl.dismiss();
    }
}
