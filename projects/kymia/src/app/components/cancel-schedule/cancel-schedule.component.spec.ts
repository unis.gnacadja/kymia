import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import {
    IonicModule,
    PopoverController,
    ToastController
} from '@ionic/angular';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import {
    ConsultationMock,
    consultationServiceMock,
    userServiceMock
} from 'src/app/mock';
import { HomePage } from 'src/app/pages/home/home.page';
import { MeetingsPage } from 'src/app/pages/meetings/meetings.page';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { CancelScheduleComponent } from './cancel-schedule.component';

const routes: Routes = [
    { path: 'tabs/meetings', component: MeetingsPage },
    { path: '', component: HomePage }
];

class MockPopoverController {
    dismiss() {
        return of();
    }
}

const DEFAULT_ACTION = {
    create: (args) =>
        Promise.resolve({
            present: () => Promise.resolve()
        }),
    dismiss: () => Promise.resolve()
};

describe('CancelScheduleComponent', () => {
    let component: CancelScheduleComponent;
    let fixture: ComponentFixture<CancelScheduleComponent>;
    let popoverCtrl: MockPopoverController = new MockPopoverController();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [CancelScheduleComponent],
            imports: [
                IonicModule.forRoot(),
                HttpClientTestingModule,
                RouterTestingModule,
                RouterModule.forRoot(routes),
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                UserService,
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useValue: {}
                },
                { provide: APP_BASE_HREF, useValue: '' },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                {
                    provide: ToastController,
                    useValue: DEFAULT_ACTION
                },
                {
                    provide: PopoverController,
                    useValue: popoverCtrl
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(CancelScheduleComponent);
        component = fixture.componentInstance;
        component.id = ConsultationMock.waiting.id;
        fixture.detectChanges();
    }));

    it('it should be check if consultation can be cancelled', fakeAsync(() => {
        jest.spyOn(consultationServiceMock, 'getConsultation');
        component.ngOnInit();
        tick();
        expect(component.editable).toBe(false);
    }));

    it('it should handle API error and display an error message', fakeAsync(() => {
        consultationServiceMock.getConsultation.mockReturnValue(
            throwError({ status: 500 })
        );
        component.ngOnInit();
        tick();
        expect(component.editable).toBe(false);
    }));

    it('should not cancel if commentaire is empty', fakeAsync(() => {
        component.commentaire = 'Indisponible';
        component.onSubmit();
        tick();
        expect(consultationServiceMock.cancelSchedule).toHaveBeenCalled();
    }));

    it('should handle error when cancelSchedule returns error', fakeAsync(() => {
        consultationServiceMock.cancelSchedule.mockReturnValue(
            throwError({ status: 500 })
        );
        component.commentaire = 'Indisponible';
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should dismiss the popop', () => {
        jest.spyOn(popoverCtrl, 'dismiss');
        component.quit();
        expect(popoverCtrl.dismiss).toHaveBeenCalled();
    });
});
