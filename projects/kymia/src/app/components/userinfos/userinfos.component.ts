import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
@Component({
    selector: 'app-userinfos',
    templateUrl: './userinfos.component.html',
    styleUrls: ['./userinfos.component.scss']
})
export class UserinfosComponent implements OnInit, OnDestroy {
    private destroy$ = new Subject<void>();
    username: string;
    constructor(private userService: UserService) {}

    ngOnInit() {
        this.userService.user$.pipe(takeUntil(this.destroy$)).subscribe(() => {
            this.username = `${this.userService.user.firstname} ${this.userService.user.name}`;
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
