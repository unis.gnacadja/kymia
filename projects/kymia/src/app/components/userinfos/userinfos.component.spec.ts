import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { userServiceMock } from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { UserinfosComponent } from './userinfos.component';

describe('UserinfosComponent', () => {
    let component: UserinfosComponent;
    let fixture: ComponentFixture<UserinfosComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [UserinfosComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(UserinfosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should displays the user's name", () => {
        const username = fixture.nativeElement.querySelector(
            '[data-test-id="username"]'
        );
        expect(username.textContent.trim()).toBe(
            `${userServiceMock.user.firstname} ${userServiceMock.user.name}`
        );
    });
});
