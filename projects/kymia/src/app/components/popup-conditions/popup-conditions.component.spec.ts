import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, PopoverController } from '@ionic/angular';
import { of } from 'rxjs';
import { PopupConditionsComponent } from './popup-conditions.component';

class MockPopoverController {
    dismiss() {
        return of();
    }
}

describe('PopupConditionsComponent', () => {
    let component: PopupConditionsComponent;
    let fixture: ComponentFixture<PopupConditionsComponent>;
    let popoverCtrl: MockPopoverController = new MockPopoverController();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [PopupConditionsComponent],
            imports: [IonicModule.forRoot()],
            providers: [
                {
                    provide: PopoverController,
                    useValue: popoverCtrl
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(PopupConditionsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be mounted', () => {
        expect(component).toBeTruthy();
    });
    it('should dismiss the popop', () => {
        jest.spyOn(popoverCtrl, 'dismiss');
        component.quit();
        expect(popoverCtrl.dismiss).toHaveBeenCalled();
    });
});
