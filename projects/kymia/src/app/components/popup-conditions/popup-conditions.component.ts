import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { conditions } from 'src/app/utils/constants';

@Component({
    selector: 'app-popup-conditions',
    templateUrl: './popup-conditions.component.html',
    styleUrls: ['./popup-conditions.component.scss']
})
export class PopupConditionsComponent {
    conditions: string[] = conditions;

    constructor(public popoverController: PopoverController) {}

    quit(): void {
        this.popoverController.dismiss();
    }
}
