import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessagePipe, NormalizeHour } from '@idermato/src/app/pipes';
import { IonicModule } from '@ionic/angular';
import { ConsultationStatus } from '@lib/enum';
import { ConsultationMock, normalizedDoctor } from 'src/app/mock';
import { ConsultationComponent } from './consultation.component';

describe('ConsultationComponent', () => {
    let component: ConsultationComponent;
    let fixture: ComponentFixture<ConsultationComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ConsultationComponent, MessagePipe],
            imports: [IonicModule.forRoot()],
            providers: [NormalizeHour],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        });

        fixture = TestBed.createComponent(ConsultationComponent);
        component = fixture.componentInstance;
        component.item = {
            ...ConsultationMock.waiting,
            calendar: {
                ...ConsultationMock.waiting.calendar,
                doctor: normalizedDoctor,
                availableDate: '2022/08/10'
            }
        };
        fixture.detectChanges();
    });

    it('should call showVideoCam on ngOnChanges', () => {
        const showVideoCamSpy = jest.spyOn(component, 'showVideoCam');
        component.ngOnChanges({
            item: new SimpleChange(null, component.item, true)
        });
        expect(showVideoCamSpy).toHaveBeenCalled();
    });

    it('Should start meeting', () => {
        window.open = jest.fn();
        component.item.callUrl = ConsultationMock.terminated.callUrl;
        component.startZoom();
        expect(window.open).toHaveBeenCalledWith(
            ConsultationMock.terminated.callUrl,
            '_system'
        );
    });

    it('should display video consultation icon when consultation is confirmed and availableDateStart is 10 minutes before the current date time', () => {
        component.item.status = ConsultationStatus.payed;
        component.item.calendar.availableDate = '2025/05/23';
        component.item.calendar.availableDateStart = '16:05:00';
        jest.useFakeTimers().setSystemTime(new Date('2025-05-23T16:00:00'));
        component.ngOnChanges({
            item: new SimpleChange(null, component.item, true)
        });
        fixture.detectChanges();
        const videoConsultationIcon = fixture.nativeElement.querySelector(
            '[data-test-id="video-consultation"]'
        );
        expect(videoConsultationIcon).not.toBeNull();
    });

    it('should automatically display video consultation icon when time is more than 15 minutes before the current date time', () => {
        component.item.status = ConsultationStatus.payed;
        component.item.calendar.availableDate = '2025/05/23';
        component.item.calendar.availableDateStart = '16:25:00';
        jest.useFakeTimers().setSystemTime(new Date('2025-05-23T16:00:00'));
        component.ngOnChanges({
            item: new SimpleChange(null, component.item, true)
        });
        jest.advanceTimersByTime(25 * 60 * 1000);
        fixture.detectChanges();
        const videoConsultationIcon = fixture.nativeElement.querySelector(
            '[data-test-id="video-consultation"]'
        );
        expect(videoConsultationIcon).not.toBeNull();
    });

    it('should clear timer on ngOnDestroy', () => {
        const clearTimeoutSpy = jest.spyOn(window, 'clearTimeout');
        component.timer = setTimeout(() => {}, 1000);
        component.ngOnDestroy();
        expect(clearTimeoutSpy).toHaveBeenCalledWith(component.timer);
    });
});
