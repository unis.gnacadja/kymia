import {
    Component,
    Input,
    OnChanges,
    OnDestroy,
    SimpleChanges
} from '@angular/core';
import { NormalizeHour } from '@idermato/src/app/pipes';
import { ConsultationStatus } from '@lib/enum';
import { Consultation } from 'src/app/models';
import { statusMessages } from 'src/app/utils/constants';

@Component({
    selector: 'app-consultation',
    templateUrl: './consultation.component.html',
    styleUrls: ['./consultation.component.scss'],
    providers: [NormalizeHour]
})
export class ConsultationComponent implements OnChanges, OnDestroy {
    @Input() item: Consultation;
    statusMessages: Record<string, string> = statusMessages;
    ConsultationStatus = ConsultationStatus;
    showVideo: boolean = false;
    available: boolean = false;
    timer: ReturnType<typeof setTimeout>;
    availableDateStart: Date;

    constructor(public normalizeHour: NormalizeHour) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.item && changes.item) {
            this.showVideoCam();
        }
    }

    /**
     * enable the video cam when the consultation meets the requirements
     */
    showVideoCam(): void {
        this.availableDateStart = this.normalizeHour.transform(
            new Date(this.item.calendar.availableDate),
            this.item.calendar.availableDateStart
        );

        const tenMinutesMs = 10 * 60 * 1000;
        const diff = this.timeDiffFromNow(this.availableDateStart);

        this.available = diff <= tenMinutesMs && diff >= 0;

        const eligible = this.item.status === ConsultationStatus.payed;

        if (!this.available && eligible && diff > tenMinutesMs) {
            this.timer = setTimeout(() => {
                this.showVideo =
                    this.timeDiffFromNow(this.availableDateStart) <=
                    tenMinutesMs;
            }, diff);
        }
        this.showVideo = this.available && eligible;
    }

    /**
     * get time difference between a date and now
     *
     * @param date The date to compare
     * @returns the time difference between the date and the current time in millisecond
     */

    timeDiffFromNow(date: Date) {
        return date.getTime() - new Date().getTime();
    }

    ngOnDestroy(): void {
        clearTimeout(this.timer);
    }

    startZoom() {
        window.open(this.item.callUrl, '_system');
    }
}
