import {
    Component,
    ContentChildren,
    ElementRef,
    Input,
    QueryList,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
    @ViewChild('popover') popover;
    @Input() title: string;
    @Input() displayUserInfos: boolean = true;

    @ContentChildren('[options]', { read: ElementRef }) options:
        | QueryList<ElementRef>
        | undefined;

    constructor(public router: Router) {}
}
