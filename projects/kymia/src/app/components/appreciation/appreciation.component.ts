import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import {
    LoadingController,
    NavController,
    PopoverController
} from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-appreciation',
    templateUrl: './appreciation.component.html',
    styleUrls: ['./appreciation.component.scss']
})
export class AppreciationComponent implements OnInit {
    @Input() id: string;
    commentaire: string;
    loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    formBuilder: FormBuilder;
    nav: NavController;
    note: number;
    constructor(
        public consultationService: ConsultationsService,
        public popoverCtrl: PopoverController,
        public userservice: UserService,
        private router: Router,
        public loadingCtrl: LoadingController,
        private injector: Injector
    ) {
        this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
        this.nav = this.injector.get<NavController>(NavController);
    }

    ngOnInit(): void {
        this.note = 0;
    }

    onSubmit(): void {
        const terminateConsultationDto = {
            comment: this.commentaire,
            note: this.note
        };

        this.consultationService
            .setScheduleComments(this.id, terminateConsultationDto)
            .subscribe({
                next: () => {
                    this.loading.next(false);
                },
                error: () => {}
            });

        this.router.navigate(['/tabs/meetings']);
        this.popoverCtrl.dismiss();
    }

    notes(x: number): void {
        this.note = x === 1 && this.note === 1 ? 0 : x;
    }

    quit(): void {
        this.router.navigateByUrl('/consultation');
        this.popoverCtrl.dismiss();
    }
}
