import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';
import { IonicModule, PopoverController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { of, throwError } from 'rxjs';
import { DetailConsultationPage } from 'src/app/pages/detail-consultation/detail-consultation.page';
import { HomePage } from 'src/app/pages/home/home.page';
import { MeetingsPage } from 'src/app/pages/meetings/meetings.page';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { AppreciationComponent } from './appreciation.component';

const routes: Routes = [
    { path: 'tabs/meetings', component: MeetingsPage },
    { path: 'consultation', component: DetailConsultationPage },
    { path: '', component: HomePage }
];

class MockConsultationService {
    setScheduleComments(id, commentNote) {
        return of({});
    }
}

describe('AppreciationComponent', () => {
    let component: AppreciationComponent;
    let fixture: ComponentFixture<AppreciationComponent>;
    let consultationService: MockConsultationService =
        new MockConsultationService();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [AppreciationComponent],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot(routes),
                HttpClientTestingModule,
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                UserService,
                {
                    provide: ConsultationsService,
                    useValue: consultationService
                },
                {
                    provide: ActivatedRoute,
                    useValue: {}
                },
                { provide: APP_BASE_HREF, useValue: '' },
                {
                    provide: PopoverController,
                    useValue: {
                        dismiss: () => Promise.resolve()
                    }
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(AppreciationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be mounted', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnInit: note is 0', () => {
        component.ngOnInit();
        expect(component.note).toBe(0);
    });

    describe('onSubmit ', () => {
        it('should be able to comment', fakeAsync(() => {
            component.commentaire = 'Je recommande ce médécin';
            component.note = 5;
            jest.spyOn(consultationService, 'setScheduleComments');
            component.onSubmit();
            tick();
            expect(consultationService.setScheduleComments).toHaveBeenCalled();
        }));

        it('should go to meeting list', fakeAsync(() => {
            component.commentaire = 'Je recommande ce médécin';
            component.note = 5;
            jest.spyOn(consultationService, 'setScheduleComments');
            component.onSubmit();
            tick();
            expect(window.location.pathname).toBe('/tabs/meetings');
        }));

        it('should handle error response', fakeAsync(() => {
            jest.spyOn(
                consultationService,
                'setScheduleComments'
            ).mockReturnValue(
                throwError(() => new Error(helperMessages.errorOccurred))
            );

            component.onSubmit();
            tick();
            expect(consultationService.setScheduleComments).toHaveBeenCalled();
        }));
    });

    describe('function notes', () => {
        it('should set note to 0 if note = 1', () => {
            component.note = 1;
            component.notes(1);
            expect(component.note).toBe(0);
        });

        it('should set note to given value', () => {
            component.note = 1;
            component.notes(3);
            expect(component.note).toBe(3);
        });
    });

    it('function quit should navigate to consultation', fakeAsync(() => {
        component.quit();
        tick();
        expect(window.location.pathname).toBe('/consultation');
    }));
});
