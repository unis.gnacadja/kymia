import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA, Input } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { of, throwError } from 'rxjs';
import { Dates, DoctorMock, doctorServiceMock } from 'src/app/mock';
import { DoctorService } from 'src/app/services/doctor.service';
import { DoctorScheduleComponent } from './doctor-schedule.component';

jest.mock('ionic2-calendar/calendar', () => ({
    CalendarComponent: class MockNavComponent {
        calendarMode: string;
    }
}));

@Component({
    selector: 'calendar',
    template: '',
    providers: [
        {
            provide: CalendarComponent,
            useClass: MockCalendarComponent
        }
    ]
})
class MockCalendarComponent {
    @Input() calendarMode: string;
    @Input() markDisabled: (date: Date) => boolean;
    @Input() currentDate: string;
    @Input() formatDayHeader: string;
    @Input() step: string;
    @Input() onCurrentDateChanged: jest.Mock;
    @Input() showEventDetail: boolean;
    slidePrev = jest.fn();
    slideNext = jest.fn();
}

describe('DoctorScheduleComponent', () => {
    let component: DoctorScheduleComponent;
    let fixture: ComponentFixture<DoctorScheduleComponent>;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DoctorScheduleComponent, MockCalendarComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: DoctorService,
                    useValue: doctorServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            imports: [IonicModule.forRoot(), HttpClientTestingModule]
        }).compileComponents();

        fixture = TestBed.createComponent(DoctorScheduleComponent);
        component = fixture.componentInstance;
        component.doctorId = DoctorMock.id;
        fixture.detectChanges();
    }));

    it('should get the doctor availabilities', () => {
        expect(component.availableDates.length).toBe(Dates.length);
    });

    describe('markDisabled', () => {
        it('should return false for dates in availableDates', () => {
            component.availableDates.forEach((date) => {
                expect(component.markDisabled(date.day)).toBe(false);
            });
        });

        it('should return true for dates not in availableDates', () => {
            component.availableDates = [
                { day: new Date('2024/08/02'), calendars: [] }
            ];
            expect(component.markDisabled(new Date('2024/08/01'))).toBe(true);
        });
    });

    it('should update availableHours correctly when updateHours is called', fakeAsync(() => {
        component.availableDates = [
            { day: new Date('2024/08/01'), calendars: Dates[0].calendars }
        ];
        component.updateHours(new Date('2024/08/01'));
        tick();
        expect(component.calendars).toEqual(Dates[0].calendars);
    }));

    it('should update the doctor when updateCalendar is called', () => {
        const event = new CustomEvent('ionChange', {
            detail: { value: Dates[0].calendars[0].availableDateStart }
        });
        component.updateCalendar(event);
        expect(component.doctor.nextCalendar).toEqual(Dates[0].calendars[0]);
    });

    it('should slide to previous item when previous is called', () => {
        jest.spyOn(component.calendarComponent, 'slidePrev');
        component.previous();
        expect(component.calendarComponent.slidePrev).toHaveBeenCalled();
    });

    it('should slide to next item when next is called', () => {
        jest.spyOn(component.calendarComponent, 'slideNext');
        component.next();
        expect(component.calendarComponent.slideNext).toHaveBeenCalled();
    });

    it('should present the toast when there is no availability', () => {
        doctorServiceMock.getAvailableDates.mockReturnValue(
            of({ content: [] })
        );
        component.getAvailability().then(() => {
            expect(notifyServiceMock.presentToast).toHaveBeenCalled();
        });
    });

    it('should present the toast when there is an error', async () => {
        doctorServiceMock.getAvailableDates.mockReturnValue(
            throwError(new Error())
        );
        component.getAvailability().then(() => {
            expect(notifyServiceMock.presentToast).toHaveBeenCalled();
        });
    });
});
