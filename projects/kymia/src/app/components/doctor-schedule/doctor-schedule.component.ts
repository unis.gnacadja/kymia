import {
    Component,
    Input,
    OnInit,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { helperMessages } from '@lib/constants';
import { CalendarComponent, CalendarMode } from 'ionic2-calendar/calendar';
import { finalize } from 'rxjs/operators';
import { Calendar, Doctor } from 'src/app/models';
import { DoctorService } from 'src/app/services/doctor.service';
import { messages } from 'src/app/utils/constants';
import { normalizeCalendar } from 'src/app/utils/normalizer';

@Component({
    selector: 'app-doctor-schedule',
    templateUrl: './doctor-schedule.component.html',
    styleUrls: ['./doctor-schedule.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DoctorScheduleComponent implements OnInit {
    @Input() doctorId: string;
    doctor: Doctor;
    displayDate: Date;
    minDate: Date;
    calendar = {
        mode: 'month' as CalendarMode,
        formatDayHeader: 'EEEEE',
        step: 30,
        currentDate: new Date()
    };
    availableDates: {
        day: Date;
        calendars: Calendar[];
    }[] = [];
    calendars: Calendar[] = [];
    selectedHour: string;
    disabledPrevious = true;

    calendarComponent: CalendarComponent;

    @ViewChild('calendarComponent') set content(content: CalendarComponent) {
        if (content) {
            this.calendarComponent = content;
        }
    }

    constructor(
        private doctorService: DoctorService,
        private loadingService: LoadingService,
        private notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.doctor = this.doctorService.list.find(
            (doctor) => doctor.id === this.doctorId
        );

        this.minDate = new Date(this.doctor.nextCalendar.availableDate);

        this.displayDate = new Date(this.doctor.nextCalendar.availableDate);
        this.displayDate.setDate(1);

        this.getAvailability();
    }

    /**
     * Marks the disabled dates
     *
     * @param date - The date to be marked.
     * @returns True if the date is disabled, false otherwise.
     */
    markDisabled = (date: Date): boolean => {
        const dateToCheck = new Date(date);
        dateToCheck.setHours(0, 0, 0, 0);
        return this.availableDates.every(
            (availableDate) =>
                availableDate.day.getTime() !== dateToCheck.getTime()
        );
    };

    /**
     * Update the hours
     *
     * @param date - The date to be updated.
     */
    updateHours(date: Date): void {
        if (this.availableDates.length > 0) {
            date.setHours(0, 0, 0, 0);
            const dates = this.availableDates.find(
                (availableDate) =>
                    availableDate.day.getTime() === date.getTime()
            );
            if (dates) {
                this.calendars = dates.calendars;
                this.selectedHour = this.calendars[0].availableDateStart;
                setTimeout(() => {
                    this.doctor.nextCalendar = this.calendars[0];
                }, 0);
            }
        }
    }

    /**
     * Update the calendar
     *
     * @param event - The event to be updated.
     */
    updateCalendar(event: Event): void {
        this.selectedHour = (event as CustomEvent).detail.value;
        this.doctor.nextCalendar = this.calendars.find(
            (calendar) => calendar.availableDateStart === this.selectedHour
        );
    }

    /**
     * Move to the previous month
     */
    async previous(): Promise<void> {
        this.displayDate = new Date(
            this.displayDate.setMonth(this.displayDate.getMonth() - 1)
        );
        this.calendarComponent.slidePrev();
        this.getAvailability();
    }

    /**
     * Move to the next month
     */
    async next(): Promise<void> {
        this.displayDate = new Date(
            this.displayDate.setMonth(this.displayDate.getMonth() + 1)
        );
        this.calendarComponent.slideNext();
        this.getAvailability();
    }

    /**
     * Get doctor availabilities
     */
    async getAvailability(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);
        this.disabledPrevious = false;
        this.doctorService
            .getAvailableDates(
                this.doctor.id,
                this.displayDate.getFullYear(),
                this.displayDate.getMonth() + 1
            )
            .pipe(
                finalize(() => {
                    this.disabledPrevious =
                        this.displayDate.getMonth() <=
                            this.minDate.getMonth() &&
                        this.displayDate.getFullYear() ===
                            this.minDate.getFullYear();
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (res: any) => {
                    this.availableDates = res.content.map((content) => ({
                        day: new Date(
                            content.availableDate.split('/').reverse().join('/')
                        ),
                        calendars: content.calendars.map(normalizeCalendar)
                    }));
                    if (this.availableDates.length === 0) {
                        this.notifyService.presentToast({
                            message: messages.noAvailability
                        });
                    } else {
                        this.calendars = this.availableDates[0].calendars;
                        this.calendar.currentDate = this.availableDates[0].day;
                    }
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }
}
