import { registerLocaleData } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import localeFr from '@angular/common/locales/fr';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MessagePipe } from '@idermato/src/app/pipes';
import { IonicModule } from '@ionic/angular';
import { ShortenPipe } from 'ngx-pipes';
import { of } from 'rxjs';
import { ConsultationMock, normalizedDoctor } from 'src/app/mock';
import { AppointmentComponent } from './appointment.component';
registerLocaleData(localeFr);

describe('AppointmentComponent', () => {
    let component: AppointmentComponent;
    let fixture: ComponentFixture<AppointmentComponent>;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [AppointmentComponent, ShortenPipe, MessagePipe],
            imports: [
                IonicModule.forRoot(),
                HttpClientTestingModule,
                RouterTestingModule
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: LOCALE_ID, useValue: 'fr-FR' },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParams: of({
                            consultationId: ConsultationMock.waiting.id
                        })
                    }
                }
            ]
        });
        fixture = TestBed.createComponent(AppointmentComponent);
        component = fixture.componentInstance;
        component.item = {
            ...ConsultationMock.confirmed,
            calendar: {
                ...ConsultationMock.waiting.calendar,
                doctor: normalizedDoctor,
                availableDate: '10/08/2022'
            }
        };
    }));

    it('should have enabled button', () => {
        jest.useFakeTimers().setSystemTime(new Date('2023-06-05T16:00:00'));
        component.item.calendar.availableDate = '2024/06/06';
        component.ngOnInit();
        expect(component.disabled).toBe(true);
    });
});
