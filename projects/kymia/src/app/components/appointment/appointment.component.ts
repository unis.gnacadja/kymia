import { Component, Input, OnInit } from '@angular/core';
import { ConsultationStatus } from '@lib/enum';
import { Consultation } from 'src/app/models';
import { statusMessages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
    @Input() item: Consultation;
    showCam: boolean = false;
    disabled: boolean = false;
    defaultAvatar: string = environment.user.avatar;
    ConsultationStatus = ConsultationStatus;
    statusMessages: Record<string, string> = statusMessages;

    constructor() {}

    ngOnInit(): void {
        this.disabled =
            new Date(this.item.calendar.availableDate).getTime() >
            new Date().getTime();
        this.showCam =
            this.item.status === ConsultationStatus.waiting ||
            this.item.status === ConsultationStatus.payed ||
            this.item.status === ConsultationStatus.confirmByDoctor;
    }
}
