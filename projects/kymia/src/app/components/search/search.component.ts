import {
    Component,
    DoCheck,
    EventEmitter,
    Input,
    OnInit,
    Output
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { Geolocation, Position } from '@capacitor/geolocation';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { helperMessages } from '@lib/constants';
import { finalize } from 'rxjs/operators';
import { Criteria, Location } from 'src/app/models';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';

@Component({
    selector: 'app-search-engine',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, DoCheck {
    @Input() mode: 'LINK' | 'SEARCH' = 'SEARCH';
    @Input() target: string;
    @Input() advanced: boolean = true;
    @Input() title: string = messages.findDoctor;
    @Output() setCriteria = new EventEmitter<Criteria>();
    searchTerm: FormGroup;
    location: Location;
    latitude: number = 0;
    longitude: number = 0;
    country: string;

    constructor(
        private readonly commonService: CommonService,
        private readonly router: Router,
        public readonly userService: UserService,
        private readonly formBuilder: FormBuilder,
        private readonly loadingService: LoadingService
    ) {}

    async ngOnInit(): Promise<void> {
        this.searchTerm = this.formBuilder.group({
            search: ['']
        });

        this.userService.location.subscribe(async (data) => {
            this.location = data;
        });
        if (Capacitor.isNativePlatform()) {
            const geoGranted = await this.checkGrants();
            if (geoGranted) {
                const geoPosition = await Geolocation.getCurrentPosition();
                this.getLocation(geoPosition);
            }
        }
    }

    /**
     * Get the position of the user
     */
    private async getLocation(position: Position) {
        await this.loadingService.present(helperMessages.loading);
        this.commonService
            .reverseLocationMap({
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
            })
            .pipe(
                finalize(async () => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (result: any) => {
                    const res = {
                        id: result.address.id,
                        city: result.address.city,
                        country: result.address.country
                    };
                    this.location = res;
                    this.userService.updateLocation(res);
                },
                error: () => {}
            });
    }

    /**
     * Navigate to the target
     */
    onEnter() {
        if (this.mode === 'LINK') {
            this.router.navigate([this.target]);
        }
    }

    /**
     * Check the grants
     */
    private async checkGrants(): Promise<boolean> {
        const grants = await Geolocation.checkPermissions();
        return grants.location === 'granted';
    }

    /**
     * Finds the criteria
     */
    find(): void {
        this.setCriteria.emit({
            item: this.searchTerm.value.search,
            ...(this.location && { cityId: this.location.id })
        });
    }

    /**
     * send the criteria when the search term is empty
     */
    ngDoCheck() {
        if (this.searchTerm.value.search.length === 0 && !this.advanced) {
            this.find();
        }
    }
}
