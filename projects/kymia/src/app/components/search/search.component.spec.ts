import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Capacitor } from '@capacitor/core';
import { Geolocation } from '@capacitor/geolocation';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock } from '@lib/mocks';
import { commonServiceMock, userServiceMock } from 'src/app/mock';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { SearchComponent } from './search.component';

const permissions = { location: 'granted', coarseLocation: 'granted' };

jest.mock('@capacitor/geolocation', () => {
    return {
        Geolocation: {
            checkPermissions: () => Promise.resolve(permissions),
            getCurrentPosition: jest.fn().mockResolvedValue({
                coords: {
                    latitude: 1,
                    longitude: 1
                }
            }),
            requestPermissions: () => Promise.resolve(permissions)
        }
    };
});
jest.mock('@capacitor/core');

describe('SearchComponent', () => {
    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;
    let router: Router;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SearchComponent],
            imports: [IonicModule.forRoot(), RouterTestingModule],
            providers: [
                { provide: CommonService, useValue: commonServiceMock },
                { provide: UserService, useValue: userServiceMock },
                { provide: LoadingService, useValue: loadingServiceMock },
                FormBuilder
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        router = TestBed.inject(Router);
        fixture.detectChanges();
    }));

    it('should call get location when geolocation permissions are granted', async () => {
        Capacitor.isNativePlatform = jest.fn().mockReturnValue(true);
        Geolocation.checkPermissions = jest
            .fn()
            .mockResolvedValue({ location: 'granted' });

        await component.ngOnInit();

        expect(Geolocation.getCurrentPosition).toHaveBeenCalled();
    });

    it('should call router navigate on onEnter if mode is LINK', () => {
        jest.spyOn(router, 'navigate').mockReturnValue(null);
        component.mode = 'LINK';
        component.target = '/test';
        component.onEnter();
        expect(router.navigate).toHaveBeenCalledWith(['/test']);
    });

    it('should emit criteria on find', () => {
        jest.spyOn(component.setCriteria, 'emit');
        component.searchTerm.setValue({ search: 'Cardiologist' });
        component.find();
        expect(component.setCriteria.emit).toHaveBeenCalledWith({
            item: 'Cardiologist',
            cityId: component.location.id
        });
    });

    it('should display the whole list when the search term is empty', () => {
        const setCriteriaSpy = jest.spyOn(component.setCriteria, 'emit');
        component.advanced = false;
        component.searchTerm.setValue({ search: '' });
        component.ngDoCheck();
        expect(setCriteriaSpy).toHaveBeenCalledWith({
            item: '',
            cityId: component.location.id
        });
    });
});
