import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { IonicModule, NavController } from '@ionic/angular';
import { ShortenPipe } from 'ngx-pipes';
import {
    MockUser,
    normalizedDoctor,
    routerMock,
    userServiceMock
} from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { DoctorComponent } from './doctor-list-item.component';

describe('NewDocotorCardComponent', () => {
    let component: DoctorComponent;
    let fixture: ComponentFixture<DoctorComponent>;

    userServiceMock.user = MockUser.completeProfile;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DoctorComponent, ShortenPipe],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: Router,
                    useValue: routerMock
                },
                {
                    provide: NavController,
                    useValue: {}
                }
            ],
            imports: [IonicModule.forRoot(), HttpClientTestingModule]
        }).compileComponents();

        fixture = TestBed.createComponent(DoctorComponent);
        component = fixture.componentInstance;
        component.doctor = normalizedDoctor;
        fixture.detectChanges();
    }));

    it('should toggle the schedule on click on toggleSchedule', () => {
        const showSchedule = component.showSchedule;
        component.toggleSchedule();
        expect(component.showSchedule).toBe(!showSchedule);
    });

    it('should go to the taking page on click on goToTaking', () => {
        component.goToTaking();
        expect(routerMock.navigate).toHaveBeenCalledWith(['/taking'], {
            queryParams: { doctorId: component.doctor.id }
        });
    });
});
