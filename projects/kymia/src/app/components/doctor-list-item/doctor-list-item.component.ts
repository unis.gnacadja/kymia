import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Doctor } from 'src/app/models/doctor.model';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-doctor-list-item',
    templateUrl: './doctor-list-item.component.html',
    styleUrls: ['./doctor-list-item.component.scss']
})
export class DoctorComponent implements OnInit {
    @Input() doctor: Doctor;
    completeProfile: boolean;
    address: string;
    defaultAvatar: string = environment.user.avatar;
    showSchedule: boolean = false;

    constructor(
        private readonly userService: UserService,
        private readonly router: Router
    ) {}

    ngOnInit(): void {
        this.completeProfile = this.userService.user.completeProfile;
        this.address = `${this.doctor.city.name} ${this.doctor.city.country.name}`;
    }

    /**
     * Toggle the schedule
     */
    toggleSchedule(): void {
        this.showSchedule = !this.showSchedule;
    }

    /**
     * Go to the book an appointment page
     */
    goToTaking(): void {
        this.router.navigate(['/taking'], {
            queryParams: { doctorId: this.doctor.id }
        });
        this.showSchedule = false;
    }
}
