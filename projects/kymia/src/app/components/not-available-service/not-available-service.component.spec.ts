import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule, PopoverController } from '@ionic/angular';
import { NotAvailableServiceComponent } from './not-available-service.component';

class MockRouter {
    constructor() {}
    navigate() {
        return {};
    }
}

describe('NotAvailableServiceComponent', () => {
    let component: NotAvailableServiceComponent;
    let fixture: ComponentFixture<NotAvailableServiceComponent>;
    let routerSpy = new MockRouter();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [NotAvailableServiceComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: Router,
                    useValue: routerSpy
                },
                {
                    provide: PopoverController,
                    useValue: {
                        create: () =>
                            Promise.resolve({
                                present: () => Promise.resolve()
                            }),
                        dismiss: () => Promise.resolve()
                    }
                }
            ],
            imports: [IonicModule.forRoot(), RouterTestingModule]
        }).compileComponents();

        fixture = TestBed.createComponent(NotAvailableServiceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be mounted', () => {
        expect(component).toBeTruthy();
    });

    it('should route to leave the current page', fakeAsync(() => {
        jest.spyOn(routerSpy, 'navigate');
        component.quit();
        tick();
        expect(routerSpy.navigate).toHaveBeenCalledWith(['/login']);
    }));
});
