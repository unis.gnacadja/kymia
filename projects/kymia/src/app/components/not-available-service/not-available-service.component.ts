import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
    selector: 'app-not-available-service',
    templateUrl: './not-available-service.component.html',
    styleUrls: ['./not-available-service.component.scss']
})
export class NotAvailableServiceComponent {
    constructor(
        public popoverController: PopoverController,
        private router: Router
    ) {}

    quit(): void {
        this.popoverController.dismiss();
        this.router.navigate(['/login']);
    }
}
