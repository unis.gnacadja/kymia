import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppImgComponent } from '@idermato/src/app/components/app-img/app-img.component';
import { ButtonComponent } from '@idermato/src/app/components/button/button.component';
import { PhoneIndicatorComponent } from '@idermato/src/app/components/forms/fields/phone-indicator/phone-indicator.component';
import { ImgListComponent } from '@idermato/src/app/components/img-list/img-list.component';
import { NotificationComponent } from '@idermato/src/app/components/notification/notification.component';
import { TabsComponent } from '@idermato/src/app/components/tabs/tabs.component';
import { MessagePipe } from '@idermato/src/app/pipes';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { IonicModule } from '@ionic/angular';
import { CalendarComponent, NgCalendarModule } from 'ionic2-calendar';
import { FileUploadModule } from 'ng2-file-upload';
import { NgPipesModule } from 'ngx-pipes';
import { FilterBy, LabelPluralPipe } from 'src/app/pipe';
import { FilterPipe } from '../../../app/modules/autocomplete-lib/src/lib/autocomplete/highlight.pipe';
import { AppointmentComponent } from '../appointment/appointment.component';
import { CardComponent } from '../card/card.component';
import { ConsultationComponent } from '../consultation/consultation.component';
import { DoctorComponent } from '../doctor-list-item/doctor-list-item.component';
import { DoctorScheduleComponent } from '../doctor-schedule/doctor-schedule.component';
import { DoctorlistComponent } from '../doctorlist/doctorlist.component';
import { HeaderComponent } from '../header/header.component';
import { NotAvailableServiceComponent } from '../not-available-service/not-available-service.component';
import { PopupConditionsComponent } from '../popup-conditions/popup-conditions.component';
import { SearchComponent } from '../search/search.component';
import { UserinfosComponent } from '../userinfos/userinfos.component';
import { AutocompleteLibModule } from './../../modules/autocomplete-lib/src/lib/autocomplete-lib.module';
@NgModule({
    declarations: [
        NotAvailableServiceComponent,
        ConsultationComponent,
        FilterPipe,
        DoctorComponent,
        SearchComponent,
        DoctorScheduleComponent,
        DoctorlistComponent,
        UserinfosComponent,
        HeaderComponent,
        PopupConditionsComponent,
        LabelPluralPipe,
        AppImgComponent,
        ButtonComponent,
        MessagePipe,
        NotificationComponent,
        CardComponent,
        ImgListComponent,
        AppointmentComponent,
        FilterBy,
        TabsComponent,
        PhoneIndicatorComponent
    ],
    exports: [
        NotAvailableServiceComponent,
        ConsultationComponent,
        FilterPipe,
        DoctorComponent,
        SearchComponent,
        DoctorScheduleComponent,
        DoctorlistComponent,
        UserinfosComponent,
        HeaderComponent,
        PopupConditionsComponent,
        LabelPluralPipe,
        AppImgComponent,
        ButtonComponent,
        NotificationComponent,
        CardComponent,
        ReactiveFormsModule,
        ImgListComponent,
        AppointmentComponent,
        FilterBy,
        TabsComponent,
        CalendarComponent,
        PhoneIndicatorComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        AutocompleteLibModule,
        RouterModule,
        NgPipesModule,
        FileUploadModule,
        ReactiveFormsModule,
        NgCalendarModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [MessagePipe, LoadingService, FilterBy]
})
export class SharedModule {}
