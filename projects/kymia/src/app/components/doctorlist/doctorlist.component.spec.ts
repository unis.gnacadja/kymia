import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import localeFr from '@angular/common/locales/fr';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, SimpleChange } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { throwError } from 'rxjs';
import {
    CriteriaMock,
    doctorServiceMock,
    normalizedDoctor
} from 'src/app/mock';
import { DoctorService } from 'src/app/services/doctor.service';
import { UserService } from 'src/app/services/user.service';
import { DoctorlistComponent } from './doctorlist.component';

registerLocaleData(localeFr);

describe('DoctorlistComponent', () => {
    let component: DoctorlistComponent;
    let fixture: ComponentFixture<DoctorlistComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DoctorlistComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            providers: [
                UserService,
                DatePipe,
                {
                    provide: DoctorService,
                    useValue: doctorServiceMock
                },
                { provide: LOCALE_ID, useValue: 'fr-FR' }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DoctorlistComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should search doctors with the criteria when the search criteria changes', () => {
        const newCriteria = {
            item: '',
            location: {
                name: 'Abomey',
                longitude: 7.1904,
                latitude: 1.99
            },
            size: 3
        };
        component.ngOnChanges({
            criteria: new SimpleChange(null, newCriteria, true)
        });
        fixture.detectChanges();
        expect(doctorServiceMock.search).toHaveBeenCalledWith(newCriteria);
    });

    it('should track doctor list by id', () => {
        expect(component.tracklistDoctor(0, normalizedDoctor)).toBe(
            normalizedDoctor.id
        );
    });
    it('should handle error response', fakeAsync(() => {
        jest.spyOn(doctorServiceMock, 'search').mockReturnValue(
            throwError(() => new Error(helperMessages.errorOccurred))
        );

        component.find(CriteriaMock);
        tick();
        expect(doctorServiceMock.search).toHaveBeenCalled();
    }));
});
