import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { cardMedia } from '@lib/constants';
import { normalizeDoctor } from '@lib/normalizer';
import { finalize } from 'rxjs/operators';
import { Criteria, Doctor, SearchDoctorResponse } from 'src/app/models';
import { DoctorService } from 'src/app/services/doctor.service';
import { reasons } from 'src/app/utils/constants';
@Component({
    selector: 'app-doctorlist',
    templateUrl: './doctorlist.component.html',
    styleUrls: ['./doctorlist.component.scss']
})
export class DoctorlistComponent implements OnChanges {
    @Input() criteria: Criteria;
    @Input() link: string = '#';
    list: Doctor[] = [];
    searching: boolean = false;
    loader: boolean = false;
    reasons: string[] = reasons;
    cardMedia: string = cardMedia;

    constructor(private readonly doctorService: DoctorService) {}

    ngOnChanges(changes: SimpleChanges) {
        // enable @Input changes detection
        if (changes.criteria.previousValue !== changes.criteria.currentValue) {
            this.find(changes.criteria.currentValue as Criteria);
        }
    }

    /**
     * Find doctor by criteria
     *
     * @param filter
     */

    find(filter: Criteria): void {
        this.searching = true;
        this.loader = true;
        this.doctorService
            .search(filter)
            .pipe(
                finalize(() => {
                    this.loader = false;
                })
            )
            .subscribe({
                next: (response: SearchDoctorResponse) => {
                    this.list = this.doctorService.list =
                        response.content.map(normalizeDoctor);
                },
                error: () => {}
            });
    }

    /**
     * Tracking doctor list by id
     *
     * @param _index
     * @param doctor
     * @returns doctor id
     */
    tracklistDoctor(_index: number, doctor: Doctor) {
        return doctor.id;
    }
}
