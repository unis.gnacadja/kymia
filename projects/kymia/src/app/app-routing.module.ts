import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IntroGuard } from 'src/app/guards/intro.guard';
import { AuthGuard } from './guards/auth.guard';

registerLocaleData(localeFr);
const routes: Routes = [
    {
        path: 'tabs',
        children: [
            {
                path: 'home',
                loadChildren: () =>
                    import('../app/pages/home/home.module').then(
                        (m) => m.HomePageModule
                    ),
                canLoad: [IntroGuard]
            },
            {
                path: 'meetings',
                loadChildren: () =>
                    import('../app/pages/meetings/meetings.module').then(
                        (m) => m.MeetingsPageModule
                    )
            },
            {
                path: 'setting',
                loadChildren: () =>
                    import('../app/pages/setting/setting.module').then(
                        (m) => m.SettingPageModule
                    ),
                canLoad: [IntroGuard]
            },
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            }
        ],
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'intro',
        loadChildren: () =>
            import('../app/pages/intro/intro.module').then(
                (m) => m.IntroPageModule
            )
    },
    {
        path: 'login',
        loadChildren: () =>
            import('../app/pages/login/login.module').then(
                (m) => m.LoginPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'register',
        loadChildren: () =>
            import('./pages/register/register.module').then(
                (m) => m.RegisterPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'pwdforget',
        loadChildren: () =>
            import('../app/pages/pwdforget/pwdforget.module').then(
                (m) => m.PwdforgetPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'newpwd',
        loadChildren: () =>
            import('../app/pages/newpwd/newpwd.module').then(
                (m) => m.NewpwdPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'register-success',
        loadChildren: () =>
            import('./pages/register-success/register-success.module').then(
                (m) => m.RegisterSuccessPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'newpwdsuccess',
        loadChildren: () =>
            import('./pages/newpwdsuccess/newpwdsuccess.module').then(
                (m) => m.NewpwdsuccessPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'taking',
        loadChildren: () =>
            import('./pages/appointment-taking/appointment-taking.module').then(
                (m) => m.AppointmentTakingPageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'validation',
        loadChildren: () =>
            import('./pages/doublevalidation/doublevalidation.module').then(
                (m) => m.DoublevalidationPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'specialities',
        loadChildren: () =>
            import('./pages/specialities/specialities.module').then(
                (m) => m.SpecialitiesPageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },

    {
        path: 'specialists',
        loadChildren: () =>
            import('./pages/specialists/specialists.module').then(
                (m) => m.SpecialistsPageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },

    {
        path: 'profile',
        loadChildren: () =>
            import('./pages/profile/profile.module').then(
                (m) => m.ProfilePageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: '',
        redirectTo: 'tabs/home',
        pathMatch: 'full'
    },
    {
        path: 'search',
        loadChildren: () =>
            import('./pages/search/search.module').then(
                (m) => m.DoctorAppointmentChoicePageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'consultation',
        loadChildren: () =>
            import(
                './pages/detail-consultation/detail-consultation.module'
            ).then((m) => m.DetailConsultationPageModule),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'location',
        loadChildren: () =>
            import('./pages/location/location.module').then(
                (m) => m.LocationPageModule
            ),
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'fr-FR' }],
    exports: [RouterModule]
})
export class AppRoutingModule {}
