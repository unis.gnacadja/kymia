import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router, RouterModule, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Storage } from '@ionic/storage-angular';
import { environment } from 'src/environments/environment';
import { HomePage } from '../pages/home/home.page';
import { IntroPage } from '../pages/intro/intro.page';
import { UserService } from '../services/user.service';
import { IntroGuard } from './intro.guard';

const routes: Routes = [
    { path: 'intro', component: IntroPage },
    { path: '', component: HomePage },
    {
        path: 'register',
        loadChildren: () =>
            import('../pages/register/register.module').then(
                (m) => m.RegisterPageModule
            ),
        canLoad: [IntroGuard]
    }
];

describe('IntroGuard', () => {
    let guard: IntroGuard;
    let storageSpy: Storage = new Storage();
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                RouterModule.forRoot(routes)
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: {
                        isLogged: true
                    }
                },
                {
                    provide: Storage,
                    useValue: storageSpy
                },
                {
                    provide: ActivatedRoute,
                    useValue: {}
                },
                { provide: APP_BASE_HREF, useValue: '' }
            ]
        });
        guard = TestBed.inject(IntroGuard);
        storageSpy.create();
        router = TestBed.inject(Router);
        jest.spyOn(router, 'navigate');
    });

    it('should be created', () => {
        expect(guard).toBeTruthy();
    });

    describe('TA1: If intro key is set to true', () => {
        it('TA1-1: should return true', async () => {
            storageSpy.set(environment.introKey, true);
            let result = await guard.canLoad();
            expect(result).toBe(true);
        });
    });

    describe('TA2: if introduction key is set to fals', () => {
        it('TA2-1: should return false page', async () => {
            storageSpy.set(environment.introKey, false);
            let result = await guard.canLoad();
            expect(result).toBe(false);
        });
    });

    describe('TA3: if introduction key is not found in storage', () => {
        it('TA3-1: should return false', async () => {
            storageSpy.clear();
            let result = await guard.canLoad();
            expect(result).toBe(false);
        });

        it('TA3-3: should redirect to introduction page', fakeAsync(() => {
            storageSpy.clear();
            guard.canLoad();
            tick();
            expect(window.location.pathname).toBe('/intro');
        }));
    });
});
