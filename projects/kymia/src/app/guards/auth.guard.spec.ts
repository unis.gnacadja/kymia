import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { routerMock } from '../mock';
import { AuthGuard } from './auth.guard';

// Mock UserService with necessary properties and methods
const userServiceMock = {
    isLogged: false,
    canActivate: jest.fn().mockResolvedValue(true),
    canLoad: jest.fn().mockResolvedValue(false)
};

describe('AuthGuard', () => {
    let authGuard: AuthGuard;
    let router: Router;
    let userService: UserService;
    let result: boolean;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthGuard,
                { provide: Router, useValue: routerMock },
                { provide: UserService, useValue: userServiceMock }
            ]
        });

        authGuard = TestBed.inject(AuthGuard);
        router = TestBed.inject(Router);
        userService = TestBed.inject(UserService);
    });

    describe('when the user is not logged in', () => {
        it('should return false', async () => {
            result = await authGuard.canLoad();
            expect(result).toBe(false);
        });

        it('should redirect to /login', () => {
            expect(router.navigate).toHaveBeenCalledWith(['/login']);
        });
    });

    describe('when the user is logged in', () => {
        it('should return true', async () => {
            // Set the user as logged in
            userService.isLogged = true;
            routerMock.navigate.mockReset();

            result = await authGuard.canActivate();
            expect(result).toBe(true);
        });

        it('should not navigate to /login', () => {
            expect(router.navigate).not.toHaveBeenCalled();
        });
    });
});
