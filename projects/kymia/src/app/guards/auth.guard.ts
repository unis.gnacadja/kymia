import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {
    constructor(private router: Router, private user: UserService) {}

    async canLoad(): Promise<boolean> {
        return this.enableAcces();
    }

    async canActivate(): Promise<boolean> {
        return this.enableAcces();
    }

    enableAcces() {
        if (!this.user.isLogged) {
            this.router.navigate(['/login']);
        }
        return this.user.isLogged;
    }
}
