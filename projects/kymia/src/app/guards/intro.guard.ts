import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { StockageService } from '../services/stockage.service';

@Injectable({
    providedIn: 'root'
})
export class IntroGuard implements CanLoad {
    constructor(private router: Router, private stockage: StockageService) {}

    async canLoad(): Promise<any> {
        await this.stockage.init();
        const data = await this.stockage.get(environment.introKey);

        if (data !== true) {
            this.router.navigate(['/intro']);
        }
        return data === true;
    }
}
