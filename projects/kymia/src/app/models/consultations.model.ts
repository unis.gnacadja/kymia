import { Calendar } from './calendar.model';
import { Transaction } from './types/user';

export type Consultation = {
    calendar?: Calendar;
    id: string;
    images?: string[];
    problemDescription: string;
    status?: string;
    callUrl?: string;
    clinicalDescription?: string;
    paraclinicalExploration?: string;
    treatment?: string;
    evolution?: string;
    diagnosis?: string;
    transaction?: Transaction;
    ordonnaceUrl?: string;
};
