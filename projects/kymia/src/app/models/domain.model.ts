import { Doctor } from './doctor.model';

export type Domain = {
    id: string;
    name: string;
    icon: string;
    description: string;
    fonctionName: string;
    doctors?: Doctor[];
};
