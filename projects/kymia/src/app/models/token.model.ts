export type Token = {
    sub: string;
    rendezvousLimitTime: number;
    firstname: string;
    role: string;
    phone: string;
    photo: string;
    completeProfile: boolean;
    exp: number;
    userId: string;
    paymentLimitTime: number;
    confirmationLimitTime: number;
    lastname: string;
};
