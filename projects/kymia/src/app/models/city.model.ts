import { Country } from './country.model';

export type City = {
    id: string;
    name: string;
    longitude: number;
    latitude: number;
    country: Country;
};
