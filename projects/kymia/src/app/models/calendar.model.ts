import { Doctor } from './doctor.model';

export type Calendar = {
    available: boolean;
    availableDate: string;
    availableDateEnd: string;
    availableDateStart: string;
    id: string;
    doctor?: Doctor;
};
export type CalendarGroupByDate = {
    availableDate: string;
    calendars: Calendar[];
};
