import { City } from '../city.model';
import { Consultation } from '../consultations.model';

export type User = {
    birthdate?: string;
    city?: City;
    cityId?: string;
    consultations?: Consultation[];
    country?: string;
    createAt?: string;
    email: string;
    name: string;
    firstname: string;
    groupeSanguin?: string;
    id?: string;
    password?: string;
    phone: string;
    photo?: string;
    status?: string;
    type?: string;
    updateAt?: string;
    secteurActivite?: string;
    antecedents?: string;
    idLieuDeNaissance?: string;
    genre?: string;
    role?: string;
    completeProfile?: boolean;
    lieuDeNaissance?: City;
    transactions?: Transaction[];
};

export interface Transaction {
    id: string;
    billingOperator: string;
    price: number;
    paymentProof: string;
    transactionDate: string;
    consultation?: Consultation;
}
