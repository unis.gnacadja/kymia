export * from './calendar.model';
export * from './city.model';
export * from './consultations.model';
export * from './country.model';
export * from './doctor.model';
export * from './domain.model';
export * from './token.model';
export * from './types/user';

export type Criteria = {
    item: string;
    cityId?: string;
    domainId?: string;
    location?: { name?: string; longitude: string; latitude: string };
    size?: number;
};

export type Location = {
    id: string;
    city: string;
    country: string;
};
