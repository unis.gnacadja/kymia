import { Calendar } from './calendar.model';
import { City } from './city.model';
import { Domain } from './domain.model';
import { User } from './types/user';

export type Doctor = {
    banque?: string;
    city: City;
    domain: Domain;
    consultationPrice?: number;
    nbrConsult?: number;
    paymentNumbers?: string[];
    rib?: string;
    email: string;
    firstname: string;
    id: string;
    name: string;
    phone?: string;
    photo?: string;
    status: string;
    experienceYears: number;
    rate: number;
    commentCount: number;
    nextCalendar?: Calendar;
    nextMonth?: string;
    experience?: string;
    noteAndCommentResponses?: NoteAndCOmmentResponses[];
};

export type NoteAndCOmmentResponses = {
    comment: string;
    note: number;
    patient: User;
};

export type SearchDoctorResponse = {
    content: Doctor[];
    empty: boolean;
    first: boolean;
    last: boolean;
    numberPage?: number;
    numberOfElements: number;
    pageable: {
        offset: number;
        pageNumber: number;
        pageSize: number;
        paged: boolean;
        sort: {
            empty: boolean;
            sorted: boolean;
            unsorted: boolean;
        };
        unpaged: boolean;
    };
    size: number;
    sort: {
        empty: boolean;
        sorted: boolean;
        unsorted: boolean;
    };
    totalElements: number;
    totalPages: number;
};
