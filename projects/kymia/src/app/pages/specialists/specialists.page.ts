//YOUR MODIFICATIONS
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Criteria, Domain } from 'src/app/models';
import { CommonService } from 'src/app/services/common.service';

@Component({
    selector: 'app-specialists',
    templateUrl: './specialists.page.html',
    styleUrls: ['./specialists.page.scss']
})
export class SpecialistsPage implements OnInit {
    searchCriteria: Criteria;
    domain: Domain;

    constructor(
        private activatedRoute: ActivatedRoute,
        private commonService: CommonService
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.domain = this.commonService.domains.find(
                (item) => item.id === params['domainId']
            );
            this.searchCriteria = {
                item: null,
                domainId: this.domain.id,
                location: null
            };
        });
    }
}
