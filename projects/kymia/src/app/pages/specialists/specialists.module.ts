import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgPipesModule } from 'ngx-pipes';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SpecialistsPageRoutingModule } from './specialists-routing.module';
import { SpecialistsPage } from './specialists.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        SharedModule,
        FormsModule,
        IonicModule,
        NgPipesModule,
        SpecialistsPageRoutingModule
    ],
    declarations: [SpecialistsPage]
})
export class SpecialistsPageModule {}
