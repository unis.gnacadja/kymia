import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { mockActivatedRoute } from '@lib/mocks';
import { of } from 'rxjs';
import { ActivityMock, commonServiceMock } from 'src/app/mock';
import { CommonService } from 'src/app/services/common.service';
import { SpecialistsPage } from './specialists.page';

describe('SpecialistsPage', () => {
    let component: SpecialistsPage;
    let fixture: ComponentFixture<SpecialistsPage>;

    const searchDomain = {
        ...ActivityMock[0],
        id: 'search domain id'
    };

    commonServiceMock.domains = [...ActivityMock, searchDomain];
    mockActivatedRoute.queryParams = of({ domainId: searchDomain.id });

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SpecialistsPage],
            providers: [
                {
                    provide: CommonService,
                    useValue: commonServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useValue: mockActivatedRoute
                }
            ],
            imports: [IonicModule.forRoot()],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(SpecialistsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should get the domain from the activated route', () => {
        component.ngOnInit();
        expect(component.domain).toEqual(searchDomain);
    });
});
