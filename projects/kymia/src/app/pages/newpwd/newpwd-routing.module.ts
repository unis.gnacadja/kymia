import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewpwdPage } from './newpwd.page';

const routes: Routes = [
    {
        path: '',
        component: NewpwdPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewpwdPageRoutingModule {}
