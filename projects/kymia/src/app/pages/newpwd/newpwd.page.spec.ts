import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule, NavController, ToastController } from '@ionic/angular';
import { of, throwError } from 'rxjs';
import { MockUser } from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { NewpwdPage } from './newpwd.page';

class MockNavController {
    navigateForward(args) {
        return {};
    }
    navigateRoot(params) {
        return {};
    }
}
class MockRouter {
    navigate() {
        return {};
    }
}
class MockUserService {
    user = MockUser.completeProfile;
    newpwd() {
        return of(MockUser.loggedUser);
    }
}

const DEFAULT_ACTION = {
    create: (args) =>
        Promise.resolve({
            present: () => Promise.resolve()
        }),
    present: () => Promise.resolve()
};

describe('NewpwdPage', () => {
    let component: NewpwdPage;
    let fixture: ComponentFixture<NewpwdPage>;
    let navControllerSpy: MockNavController = new MockNavController();
    let routerSpy: MockRouter = new MockRouter();
    let userServiceSpy: MockUserService = new MockUserService();
    let toastCtrl = DEFAULT_ACTION;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [NewpwdPage],
            imports: [
                IonicModule.forRoot(),
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule,
                HttpClientTestingModule
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceSpy
                },
                {
                    provide: NavController,
                    useValue: navControllerSpy
                },
                {
                    provide: Router,
                    useValue: routerSpy
                },
                {
                    provide: ToastController,
                    useValue: toastCtrl
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(NewpwdPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    it('TA1: should have old password, new password, and confirmation fields in change password form', () => {
        const EXPECTED_KEYS = ['oldPassword', 'newPassword', 'confirmPassword'];
        expect(Object.keys(component.f)).toEqual(EXPECTED_KEYS);
    });

    describe('TA2: After successful submission', () => {
        beforeEach(fakeAsync(() => {
            jest.spyOn(component.nav, 'navigateRoot');
            component.newpwdForm.controls.oldPassword.setValue('Pa$$w0rd!');
            component.newpwdForm.controls.newPassword.setValue('Pa$$w0rd_1!');
            component.newpwdForm.controls.confirmPassword.setValue(
                'Pa$$w0rd_1!'
            );
            jest.spyOn(toastCtrl, 'create');
            component.onSubmit();
            tick();
        }));
        it('TA2-1:should display the popover', () => {
            expect(toastCtrl.create).toHaveBeenCalledWith({
                message: 'Modifié avec succès',

                duration: 2000,

                color: 'success'
            });
        });
        it('TA2-2: should redirect to the setting page upon submission', () => {
            jest.spyOn(routerSpy, 'navigate');
            component.goToSettings();
            expect(component.router.navigate).toHaveBeenCalledWith([
                'tabs/setting'
            ]);
        });
        it('TA2-3: should show error message for mismatched old password', fakeAsync(() => {
            const EXPECTED_ERROR =
                'Votre ancien mot de passe est incorrect. Veuillez réessayer.';
            jest.spyOn(userServiceSpy, 'newpwd').mockReturnValueOnce(
                throwError({
                    error: { message: EXPECTED_ERROR },
                    status: 14
                })
            );
            component.newpwdForm.controls.oldPassword.setValue('Pa$$w0rd!');
            component.newpwdForm.controls.newPassword.setValue('Pa$$w0rd_1!');
            component.newpwdForm.controls.confirmPassword.setValue(
                'Pa$$w0rd_1!'
            );
            component.onSubmit();
            expect(component.error.getValue()).toBe(EXPECTED_ERROR);
        }));
    });
    it('TA3: should display an error message when the two new passwords do not match', () => {
        component.newpwdForm.controls.newPassword.setValue('Pa$$w0rd_1!');
        component.newpwdForm.controls.confirmPassword.setValue('Pa$$w0rd_2!');
        const confirmPasswordError =
            component.newpwdForm.controls.confirmPassword.errors.mustMatch;
        expect(confirmPasswordError).toBe(true);
    });
    it('TA4: should display an error message when the password is less than eight characters long', () => {
        component.newpwdForm.controls.newPassword.setValue('Pa$$');
        expect(
            component.newpwdForm.controls.newPassword.hasError('minlength')
        ).toBe(true);
    });
});
