import { Component, Injector, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { cardMedia } from '@lib/constants';
import { mustMatch } from '@lib/MustMatch.validator';
import { BehaviorSubject } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-newpwd',

    templateUrl: './newpwd.page.html',

    styleUrls: ['./newpwd.page.scss']
})
export class NewpwdPage implements OnInit {
    cardMedia: string = cardMedia;
    newpwdForm: FormGroup;

    isValidEmail: BehaviorSubject<boolean>;

    loading: BehaviorSubject<boolean>;

    submit: BehaviorSubject<boolean>;

    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    message: string;

    constructor(
        private injector: Injector,

        private toastController: ToastController,

        public formBuilder: FormBuilder,

        public router: Router,

        public userService: UserService,
        public nav: NavController
    ) {}

    ngOnInit(): void {
        this.isValidEmail = new BehaviorSubject<boolean>(false);

        this.loading = new BehaviorSubject<boolean>(false);

        this.submit = new BehaviorSubject<boolean>(false);

        this.newpwdForm = this.formBuilder.group(
            {
                oldPassword: [
                    '',

                    Validators.compose([
                        Validators.minLength(8),
                        Validators.required
                    ])
                ],

                newPassword: [
                    '',

                    Validators.compose([
                        Validators.minLength(8),
                        Validators.required
                    ])
                ],

                confirmPassword: [
                    '',

                    Validators.compose([
                        Validators.minLength(8),
                        Validators.required
                    ])
                ]
            },

            {
                validator: mustMatch('newPassword', 'confirmPassword')
            }
        );
    }

    get f(): { [key: string]: AbstractControl } {
        return this.newpwdForm.controls;
    }

    /*

   * go to settings page

   */

    goToSettings(): void {
        this.router.navigate(['tabs/setting']);
    }

    onSubmit(): void {
        this.error.next(null);
        if (!this.submit.getValue()) {
            this.submit.next(true);
        }
        if (
            this.submit.getValue() &&
            !this.loading.getValue() &&
            this.newpwdForm.valid
        ) {
            this.loading.next(true);
            if (
                this.newpwdForm.get('oldPassword').valid &&
                this.newpwdForm.get('newPassword').valid &&
                this.newpwdForm.get('confirmPassword').valid
            ) {
                this.userService

                    .newpwd(this.userService.user.id, {
                        oldPassword: this.newpwdForm
                            .get('oldPassword')
                            .value.trim(),

                        newPassword: this.newpwdForm
                            .get('newPassword')
                            .value.trim()
                    })
                    .subscribe(
                        () => {
                            this.loading.next(false);
                            this.submit.next(false);
                            this.notify();
                            this.goToSettings();
                        },
                        (error) => {
                            this.loading.next(false);
                            this.submit.next(false);
                            this.error.next(error.error.message);
                            this.message = error.error.message;
                        }
                    );
            }
        }
    }

    async notify(): Promise<void> {
        const toast = await this.toastController.create({
            message: 'Modifié avec succès',

            duration: 2000,

            color: 'success'
        });

        toast.present();
    }
}
