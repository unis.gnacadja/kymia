import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NewpwdPageRoutingModule } from './newpwd-routing.module';
import { NewpwdPage } from './newpwd.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        NewpwdPageRoutingModule,
        ReactiveFormsModule
    ],
    providers: [],
    declarations: [NewpwdPage]
})
export class NewpwdPageModule {}
