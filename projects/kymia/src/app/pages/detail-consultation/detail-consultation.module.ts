import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NormalizeDate, NormalizeHour } from '@idermato/src/app/pipes';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DetailConsultationPageRoutingModule } from './detail-consultation-routing.module';
import { DetailConsultationPage } from './detail-consultation.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DetailConsultationPageRoutingModule,
        SharedModule
    ],
    declarations: [DetailConsultationPage],
    providers: [NormalizeDate, NormalizeHour]
})
export class DetailConsultationPageModule {}
