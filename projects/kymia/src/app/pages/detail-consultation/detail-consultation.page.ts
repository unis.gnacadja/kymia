import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { PopoverController } from '@ionic/angular';
import { acceptedFileTypes, helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { finalize } from 'rxjs/operators';
import { CancelScheduleComponent } from 'src/app/components/cancel-schedule/cancel-schedule.component';
import { ConsultationComponent } from 'src/app/components/consultation/consultation.component';
import { Consultation } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { normalizeConsultation } from 'src/app/utils/normalizer';

export enum ModalType {
    appreciation = 'appreciation',
    cancelSchedule = 'cancelSchedule'
}

@Component({
    selector: 'app-detail-consultation',
    templateUrl: './detail-consultation.page.html',
    styleUrls: ['./detail-consultation.page.scss']
})
export class DetailConsultationPage implements OnInit {
    paymentStatus = {
        waiting: ConsultationStatus.waiting,
        payed: ConsultationStatus.payed,
        pending: helperMessages.loading
    };
    currentPayementStatus: string;
    tap: boolean = false;
    consultation: Consultation;
    consultationTime: string;
    ConsultationStatus = ConsultationStatus;
    notifications: { type: string; message: string }[] = [];
    displayLink: boolean = false;
    files: string;
    consultationForm: FormGroup;
    phoneNumbers: string[] = [];
    acceptedFileTypes: string = acceptedFileTypes;
    errorMessage: string = '';
    selectedFile: File;
    description: string = messages.paymentMessage;
    consultationInPending: string = messages.consultationInPending;

    @ViewChild('consultationComponent')
    consultationComponent: ConsultationComponent;

    constructor(
        public router: Router,
        public popoverCtrl: PopoverController,
        public activatedRoute: ActivatedRoute,
        public consultationService: ConsultationsService,
        public formBuilder: FormBuilder,
        public loadingService: LoadingService,
        public notifyService: NotifyService,
        public userService: UserService
    ) {}

    async ngOnInit(): Promise<void> {
        this.consultationForm = this.formBuilder.group({
            file: ['']
        });

        this.activatedRoute.queryParams.subscribe((params) => {
            this.setConsultation(params['consultationId']);

            this.phoneNumbers = [
                this.consultation.calendar.doctor.phone,
                ...this.consultation.calendar.doctor.paymentNumbers
            ];

            const linkStatus: string[] = [
                ConsultationStatus.waiting,
                ConsultationStatus.payed,
                ConsultationStatus.confirmByDoctor
            ];

            this.displayLink = linkStatus.includes(this.consultation.status);

            if (
                this.consultation.status ===
                    ConsultationStatus.confirmByDoctor ||
                (this.consultation.status === ConsultationStatus.waiting &&
                    !this.consultation.transaction)
            ) {
                this.currentPayementStatus = this.paymentStatus.waiting;
            } else {
                this.currentPayementStatus = this.paymentStatus.payed;
            }

            this.files = this.consultation.images.length
                .toString()
                .padStart(2, '0');
        });
    }

    ionViewDidEnter(): void {
        this.tap = true;
        // set the consultation time in format hh:mm and substract 10 minutes
        const readyAvailableDateStart =
            this.consultationComponent.availableDateStart;
        readyAvailableDateStart.setMinutes(
            readyAvailableDateStart.getMinutes() - 10
        );
        this.consultationTime = readyAvailableDateStart.toLocaleTimeString(
            'fr-FR',
            { hour: '2-digit', minute: '2-digit' }
        );
    }

    async showModal(): Promise<void> {
        if (this.tap) {
            const popover = await this.popoverCtrl.create({
                component: CancelScheduleComponent,
                componentProps: { id: this.consultation.id },
                animated: true,
                showBackdrop: true
            });
            popover.present();
        }
    }

    /**
     * Select files
     *
     * @param event - The event triggered by selecting files
     */
    loadPrescription(event: Event): void {
        this.errorMessage = '';
        const input = event.target as HTMLInputElement;
        const file = input.files?.[0];

        if (file?.type && this.acceptedFileTypes.includes(file.type)) {
            this.selectedFile = file;
            this.description = file.name;
        } else {
            this.errorMessage = messages.errorFile;
            this.selectedFile = null;
            this.description = messages.paymentMessage;
        }
    }

    /**
     * Submit form
     */
    async onSubmit(): Promise<void> {
        if (this.selectedFile) {
            await this.loadingService.present(helperMessages.loading);

            const formData = new FormData();
            formData.append(
                'transactionJson',
                JSON.stringify({ billingOperator: 'ESPECES' })
            );
            formData.append('file', this.selectedFile);

            this.consultationService
                .registerSchedulePaiement(this.consultation.id, formData)
                .pipe(
                    finalize(() => {
                        this.loadingService.dismiss();
                    })
                )
                .subscribe({
                    next: () => {
                        this.consultationService
                            .getPatientSchedule(this.userService.user.id)
                            .subscribe((data: any) => {
                                this.consultationService.list =
                                    data.content.map(normalizeConsultation);
                                this.setConsultation(this.consultation.id);
                            });
                        this.currentPayementStatus = this.paymentStatus.payed;
                        this.notifyService.presentToast({
                            message: messages.fileAccepted,
                            duration: 2000,
                            color: State.success
                        });
                    },
                    error: () => {
                        this.notifyService.presentToast();
                    }
                });
        } else {
            this.currentPayementStatus = this.paymentStatus.waiting;
        }
    }

    /**
     * payed consultation
     */
    payNow(): void {
        this.currentPayementStatus = this.paymentStatus.pending;
    }

    /**
     * Set the consultation
     *
     * @param consultationId consultation id
     */
    setConsultation(consultationId) {
        this.consultation = this.consultationService.list.find(
            (item) => item.id === consultationId
        );
    }
}
