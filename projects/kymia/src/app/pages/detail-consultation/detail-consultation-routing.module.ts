import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailConsultationPage } from './detail-consultation.page';

const routes: Routes = [
    {
        path: '',
        component: DetailConsultationPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetailConsultationPageRoutingModule {}
