import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessagePipe } from '@idermato/src/app/pipes';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { IonicModule, PopoverController } from '@ionic/angular';
import { acceptedFileTypes, helperMessages } from '@lib/constants';
import {
    loadingServiceMock,
    mockActivatedRoute,
    notifyServiceMock
} from '@lib/mocks';
import { of, throwError } from 'rxjs';
import { CancelScheduleComponent } from 'src/app/components/cancel-schedule/cancel-schedule.component';
import { ConsultationComponent } from 'src/app/components/consultation/consultation.component';
import {
    consultationList,
    consultationServiceMock,
    popoverControllerMock,
    userServiceMock
} from 'src/app/mock';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { DetailConsultationPage } from './detail-consultation.page';

describe('DetailConsultationPage', () => {
    let component: DetailConsultationPage;
    let fixture: ComponentFixture<DetailConsultationPage>;

    const input = document.createElement('input');
    input.type = 'file';
    let file = new File(['test content'], 'test.png', {
        type: acceptedFileTypes
    });
    Object.defineProperty(input, 'files', { value: [file] });

    let event = new CustomEvent('change');
    Object.defineProperty(event, 'target', { value: input });

    const expectedConsultation = consultationList.waiting;

    mockActivatedRoute.queryParams = of({
        consultationId: expectedConsultation.id
    });

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailConsultationPage,
                ConsultationComponent,
                MessagePipe
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientModule],
            providers: [
                FormBuilder,
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },

                {
                    provide: PopoverController,
                    useValue: popoverControllerMock
                },
                {
                    provide: ActivatedRoute,
                    useValue: mockActivatedRoute
                },
                MessagePipe,
                { provide: LOCALE_ID, useValue: 'en-US' }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DetailConsultationPage);
        component = fixture.componentInstance;
        component.consultationComponent = {
            availableDateStart: new Date('2024-06-05T14:10:00')
        } as ConsultationComponent;
        fixture.detectChanges();
    }));

    it('should be able to obtain the same consultation as the one sent in parameter', () => {
        expect(component.consultation).toEqual(expectedConsultation);
    });

    it('should open the cancellation modal on click on the cancellation link', () => {
        const popoverControllerSpy = jest.spyOn(
            popoverControllerMock,
            'create'
        );
        component.ionViewDidEnter();
        const cancelButton = fixture.nativeElement.querySelector(
            '[data-testid="cancel-meeting"]'
        );
        cancelButton.click();

        expect(popoverControllerSpy).toHaveBeenCalledWith({
            component: CancelScheduleComponent,
            componentProps: { id: expectedConsultation.id },
            animated: true,
            showBackdrop: true
        });
    });

    it('should display the “pay now” button when payment has not yet been made', fakeAsync(() => {
        component.ngOnInit();

        const payNowButton = fixture.nativeElement.querySelector(
            '[data-testid="pay-now"]'
        );
        expect(payNowButton).not.toBeNull();
    }));

    it('should hide the “pay now” button when payment has already been made', fakeAsync(() => {
        mockActivatedRoute.queryParams = of({
            consultationId: consultationList.payed.id
        });
        component.ngOnInit();

        const payNowButton = fixture.nativeElement.querySelector(
            '[data-testid="pay-now"]'
        );
        expect(payNowButton).not.toBeNull();
    }));

    it('should close the consultation when the payment is successful', fakeAsync(() => {
        component.currentPayementStatus = component.paymentStatus.waiting;
        component.payNow();
        tick();
        expect(component.currentPayementStatus).toBe(
            component.paymentStatus.pending
        );
    }));

    it('should update fileName when a supported file type is selected', fakeAsync(() => {
        component.loadPrescription(event);
        tick();
        expect(component.description).toEqual(file.name);
    }));

    it('should display an error message when an unsupported file type is uploaded', fakeAsync(() => {
        file = new File(['test content'], 'test.exe', {
            type: 'application/x-msdownload'
        });
        Object.defineProperty(input, 'files', { value: [file] });
        component.errorMessage = '';

        component.loadPrescription(event);
        tick();

        expect(component.errorMessage).toEqual(messages.errorFile);
    }));

    it('should display an error message when no file is uploaded', fakeAsync(() => {
        Object.defineProperty(input, 'files', { value: null });
        component.loadPrescription(event);
        tick();
        expect(component.errorMessage).toBe(messages.errorFile);
    }));

    it('should change the current payment status to waiting when no file is selected', () => {
        component.selectedFile = null;
        component.onSubmit();
        expect(component.currentPayementStatus).toBe(
            component.paymentStatus.waiting
        );
    });

    it('should show error message when the file upload fails', fakeAsync(() => {
        component.selectedFile = file;
        component.onSubmit();
        jest.spyOn(
            consultationServiceMock,
            'registerSchedulePaiement'
        ).mockReturnValueOnce(
            throwError({
                status: 404,
                error: {
                    message: helperMessages.errorOccurred
                }
            })
        );
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should show success message when a valid file is submitted', fakeAsync(() => {
        component.selectedFile = file;
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.fileAccepted,
            duration: 2000,
            color: State.success
        });
    }));
});
