import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.page.html',
    styleUrls: ['./setting.page.scss']
})
export class SettingPage implements OnInit {
    user: User;
    defaultAvatar: string = environment.user.avatar;

    constructor(private userService: UserService) {}

    ngOnInit(): void {
        this.userService.user$.subscribe(() => {
            this.user = this.userService.getUserInfo();
        });
    }
}
