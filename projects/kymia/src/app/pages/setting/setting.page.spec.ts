import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { userServiceMock } from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { SettingPage } from './setting.page';

describe('SettingPage', () => {
    let component: SettingPage;
    let fixture: ComponentFixture<SettingPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SettingPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(SettingPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should get the user info', () => {
        expect(component.user).toBe(userServiceMock.user);
    });
});
