import { Component } from '@angular/core';
import { messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-newpwdsuccess',
    templateUrl: './newpwdsuccess.page.html',
    styleUrls: ['./newpwdsuccess.page.scss']
})
export class NewpwdsuccessPage {
    messages: Record<string, string> = messages;
    environment = environment;
}
