import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../components/shared/shared.module';
import { NewpwdsuccessPageRoutingModule } from './newpwdsuccess-routing.module';
import { NewpwdsuccessPage } from './newpwdsuccess.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NewpwdsuccessPageRoutingModule,
        SharedModule
    ],
    declarations: [NewpwdsuccessPage]
})
export class NewpwdsuccessPageModule {}
