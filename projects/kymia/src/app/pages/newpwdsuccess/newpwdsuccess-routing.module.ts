import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewpwdsuccessPage } from './newpwdsuccess.page';

const routes: Routes = [
    {
        path: '',
        component: NewpwdsuccessPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewpwdsuccessPageRoutingModule {}
