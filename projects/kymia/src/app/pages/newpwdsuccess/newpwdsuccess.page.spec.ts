import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { NewpwdsuccessPage } from './newpwdsuccess.page';
describe('NewpwdsuccessPage', () => {
    let component: NewpwdsuccessPage;
    let fixture: ComponentFixture<NewpwdsuccessPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [NewpwdsuccessPage],

            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(NewpwdsuccessPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be mounted', () => {
        expect(component).toBeTruthy();
    });
});
