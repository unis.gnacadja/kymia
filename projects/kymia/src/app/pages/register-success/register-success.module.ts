import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { RegisterSuccessPageRoutingModule } from './register-success-routing.module';
import { RegisterSuccessPage } from './register-success.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegisterSuccessPageRoutingModule,
        SharedModule
    ],
    declarations: [RegisterSuccessPage]
})
export class RegisterSuccessPageModule {}
