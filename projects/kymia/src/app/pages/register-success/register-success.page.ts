import { Component } from '@angular/core';
import { messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-register-success',
    templateUrl: './register-success.page.html',
    styleUrls: ['./register-success.page.scss']
})
export class RegisterSuccessPage {
    messages: Record<string, string> = messages;
    environment = environment;
}
