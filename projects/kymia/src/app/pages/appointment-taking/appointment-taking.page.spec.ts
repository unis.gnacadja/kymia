import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import localeFr from '@angular/common/locales/fr';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { IonicModule } from '@ionic/angular';
import { acceptedFileTypes, helperMessages } from '@lib/constants';
import { mockActivatedRoute, mockRouter, notifyServiceMock } from '@lib/mocks';
import fetch from 'node-fetch';
import { of, throwError } from 'rxjs';
import { consultationServiceMock, userServiceMock } from 'src/app/mock';
import { doctorServiceMock } from 'src/app/mock/index';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';
import { AppointmentTakingPage } from './appointment-taking.page';

registerLocaleData(localeFr);

describe('AppointmentTakingPage', () => {
    let component: AppointmentTakingPage;
    let fixture: ComponentFixture<AppointmentTakingPage>;
    mockActivatedRoute.queryParams = of({
        doctorId: doctorServiceMock.list[0].id
    });

    const input = document.createElement('input');
    input.type = 'file';
    let file;

    let event = new CustomEvent('change');
    Object.defineProperty(event, 'target', { value: input });

    global.URL.createObjectURL = jest.fn();

    beforeAll(waitForAsync(() => {
        globalThis.fetch = fetch;
        TestBed.configureTestingModule({
            declarations: [AppointmentTakingPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            providers: [
                DatePipe,
                FormBuilder,
                {
                    provide: DoctorService,
                    useValue: doctorServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                { provide: LOCALE_ID, useValue: 'fr-FR' },
                {
                    provide: Router,
                    useValue: mockRouter
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                { provide: ActivatedRoute, useValue: mockActivatedRoute }
            ]
        });
        TestBed.compileComponents();
        fixture = TestBed.createComponent(AppointmentTakingPage);
        component = fixture.componentInstance;
        file = new File(['test content'], 'test.png', {
            type: component.acceptedFileTypes
        });
        Object.defineProperty(input, 'files', { value: [file] });
        fixture.detectChanges();
    }));

    it('should update consultation by uploading prescription', async () => {
        global.URL.createObjectURL = jest.fn(() => environment.user.avatar);

        component.loadPrescription(event);
        expect(component.selectedFiles.length).toBe(1);
    });

    it('should notify when there are no selected files', fakeAsync(() => {
        component.problemDescription = 'Expected description';
        component.selectedFiles = [];
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.noFile
        });
    }));

    it('should display an error message if the file size exceeds 1MB', () => {
        // Create an invalid file with size > 1MB (2MB)
        const invalidFile = new File([new Array(2 * 1024 * 1024).fill('a').join('')], 'large.png', {
            type: 'image/png'
        });
        Object.defineProperty(input, 'files', { value: [invalidFile] });
        component.loadPrescription(event);

        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.errorFile
        });
    });

    it('should display an error message if the file type is not accepted', () => {
        const unsupportedFile = new File([new Array(500 * 1024).fill('a').join('')], 'unsupported.txt', {
            type: 'text/pdf'
        });
        Object.defineProperty(input, 'files', { value: [unsupportedFile] });
        
        component.loadPrescription(event);
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.errorFileSymptom
        });
    });

    it('should notify when there are no description', fakeAsync(() => {
        component.problemDescription = '';
        component.selectedFiles = [];
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.noDescription
        });
    }));

    it('should display notification after registering a consultation', fakeAsync(() => {
        component.problemDescription = 'Expected description';
        component.selectedFiles = [file];
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.notifyAvailability,
            duration: 2000,
            color: State.success
        });
    }));

    it('should display an error message when a consultation could not be saved', fakeAsync(() => {
        jest.spyOn(
            consultationServiceMock,
            'registerConsultation'
        ).mockReturnValueOnce(
            throwError({
                status: 404,
                error: {
                    message: helperMessages.errorOccurred
                }
            })
        );
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: helperMessages.errorOccurred
        });
    }));

    it('should enable the modification of the description when openDescription is called', () => {
        component.disable = true;
        component.openDescription();
        expect(component.disable).toBe(false);
    });
});
