import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentTakingPage } from './appointment-taking.page';

const routes: Routes = [
    {
        path: '',
        component: AppointmentTakingPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppointmentTakingPageRoutingModule {}
