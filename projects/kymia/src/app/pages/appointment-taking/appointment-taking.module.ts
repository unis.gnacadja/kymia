import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { FileUploadModule } from 'ng2-file-upload';
import { NgPipesModule } from 'ngx-pipes';
import { DefaultPicturePipe } from 'src/app/pipe';
import { SharedModule } from '../../components/shared/shared.module';
import { AppointmentTakingPageRoutingModule } from './appointment-taking-routing.module';
import { AppointmentTakingPage } from './appointment-taking.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AppointmentTakingPageRoutingModule,
        SharedModule,
        Ionic4DatepickerModule,
        FileUploadModule,
        NgPipesModule
    ],
    providers: [DatePipe],
    declarations: [AppointmentTakingPage, DefaultPicturePipe],
    exports: [DefaultPicturePipe]
})
export class AppointmentTakingPageModule {}
