import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { ConsultationStatus } from '@lib/enum';
import { Media } from '@lib/types';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Consultation, Doctor } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { normalizeConsultation } from 'src/app/utils/normalizer';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-appointment-taking',
    templateUrl: './appointment-taking.page.html',
    styleUrls: ['./appointment-taking.page.scss']
})
export class AppointmentTakingPage implements OnInit {
    problemDescription: string = '';
    doctor: Doctor;
    loading: BehaviorSubject<boolean>;
    item: Consultation;
    acceptedFileTypes: string = 'image/png,image/jpeg,image/jpg,image/webp';
    consultationForm: FormGroup;
    environment = environment;
    image: string;
    listMedia: Media[] = [];
    selectedFiles: File[] = [];
    disable: boolean = true;
    consultationStatus = ConsultationStatus;

    constructor(
        private activatedRoute: ActivatedRoute,
        public formBuilder: FormBuilder,
        public doctorService: DoctorService,
        public datePipe: DatePipe,
        public router: Router,
        public userService: UserService,
        public consultationService: ConsultationsService,
        public notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.consultationForm = this.formBuilder.group({
            file: ['']
        });
        this.loading = new BehaviorSubject<boolean>(false);
        this.activatedRoute.queryParams.subscribe((params) => {
            this.doctor = this.doctorService.list.find(
                (doctor) => doctor.id === params['doctorId']
            );
            this.item = {
                id: '',
                calendar: {
                    ...this.doctor.nextCalendar,
                    doctor: this.doctor
                },
                problemDescription: this.doctor.domain.description,
                status: this.consultationStatus.waiting
            };
        });
    }

    openDescription(): void {
        this.disable = false;
    }

    /**
     * Select files
     *
     * @param event - The event triggered by selecting files
     */
    loadPrescription(event: Event) {
        const input = event.target as HTMLInputElement;
        const maxSizeInBytes = 1 * 1024 * 1024;

        if (input.files) {
            Array.from(input.files).forEach((file) => {
                if (file.size <= maxSizeInBytes) {
                    // add file to the list
                    this.addFile(file);
                } else {
                    this.notifyService.presentToast({
                        message: messages.errorFile
                    });
                }
            });
        }
    }

    addFile(file: File): void {
        if (this.acceptedFileTypes.includes(file.type)) {
            this.image = URL.createObjectURL(file);

            this.listMedia.push({
                src: this.image,
                alt: this.doctor.domain.name,
                title: this.doctor.domain.name
            });
            this.selectedFiles.push(file);
        } else {
            this.notifyService.presentToast({
                message: messages.errorFileSymptom
            });
        }
    }

    /**
     * submit form
     */
    onSubmit(): void {
        if (this.problemDescription.trim()) {
            if (this.selectedFiles.length === 0) {
                this.notifyService.presentToast({
                    message: messages.noFile
                });
            } else {
                const formData = new FormData();
                // Add description to FormData
                formData.append(
                    'consultationJson',
                    JSON.stringify({
                        calendarId: this.doctor.nextCalendar.id,
                        patientId: this.userService.user.id,
                        problemDescription: this.problemDescription
                    })
                );
                // Adding files to FormData
                this.selectedFiles.forEach((file, index) => {
                    formData.append(`file_${index + 1}`, file);
                });
                this.loading.next(true);
                this.consultationService
                    .registerConsultation(formData)
                    .pipe(
                        finalize(() => {
                            this.loading.next(false);
                        })
                    )
                    .subscribe({
                        next: (res: any) => {
                            this.notifyService.presentToast({
                                message: messages.notifyAvailability,
                                duration: 2000,
                                color: State.success
                            });
                            this.consultationService
                                .getPatientSchedule(this.userService.user.id)
                                .subscribe((data: any) => {
                                    this.consultationService.list =
                                        data.content.map(normalizeConsultation);
                                    this.router.navigate(['/consultation'], {
                                        queryParams: {
                                            consultationId: res.id
                                        },
                                        replaceUrl: true
                                    });
                                });
                        },
                        error: (error) => {
                            this.notifyService.presentToast({
                                message: error.error.message
                            });
                        }
                    });
            }
        } else {
            this.notifyService.presentToast({
                message: messages.noDescription
            });
        }
    }
}
