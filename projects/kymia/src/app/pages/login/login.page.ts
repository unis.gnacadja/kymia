import { Component, Injector, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { NavController, PopoverController } from '@ionic/angular';
import { cardMedia } from '@lib/constants';
import jwtDecode from 'jwt-decode';
import { BehaviorSubject } from 'rxjs';
import { Token } from 'src/app/models';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { NotAvailableServiceComponent } from '../../components/not-available-service/not-available-service.component';

@Component({
    selector: 'app-login',

    templateUrl: './login.page.html',

    styleUrls: ['./login.page.scss']
})
export class LoginComponent implements OnInit {
    errorApi: boolean;

    emailMessage: string;

    loginForm: FormGroup;

    loading: BehaviorSubject<boolean>;

    submit: BehaviorSubject<boolean>;

    formBuilder: FormBuilder;

    nav: NavController;

    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    data: any;
    cardMedia = cardMedia;

    constructor(
        private injector: Injector,

        private loginService: UserService,

        public popoverController: PopoverController
    ) {
        this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);

        this.nav = this.injector.get<NavController>(NavController);
    }

    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);

        this.submit = new BehaviorSubject<boolean>(false);

        this.errorApi = false;
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],

            password: ['', Validators.required]
        });
    }

    get f(): { [key: string]: AbstractControl } {
        return this.loginForm.controls;
    }

    onSubmit(): void {
        this.error.next(null);

        this.emailMessage = '';

        this.loginForm.controls.email.setValue(
            this.loginForm.controls.email.value.trim()
        );

        this.loginForm.controls.password.setValue(
            this.loginForm.controls.password.value.trim()
        );

        if (!this.submit.getValue()) {
            this.submit.next(true);
        }

        if (
            this.submit.getValue() &&
            !this.loading.getValue() &&
            this.loginForm.get('email').valid &&
            this.loginForm.get('password').valid
        ) {
            this.loading.next(true);

            this.loginService

                .login({
                    email: this.loginForm.get('email').value.trim(),

                    password: this.loginForm.get('password').value.trim()
                })

                .subscribe(
                    (res: any) => {
                        this.loading.next(true);

                        this.submit.next(false);

                        this.loginService.isLogged = true;

                        this.loginService.token = res.token;
                        try {
                            const decodedToken: Token = jwtDecode(res.token);
                            this.loginService.user = {
                                ...decodedToken,
                                id: decodedToken.userId,
                                email: decodedToken.sub,
                                name: decodedToken.lastname
                            };
                        } catch (e) {}

                        this.nav.navigateRoot(['tabs/home']);
                    },

                    (errors) => {
                        this.loading.next(false);

                        this.submit.next(false);
                        if (errors.error?.error === 'PENDING_USER') {
                            this.nav.navigateRoot(['/validation']);
                        } else if (
                            errors.status === 502 ||
                            errors.status === 503 ||
                            errors.status === 0
                        ) {
                            this.presentPopover();

                            this.errorApi = true;
                        } else {
                            this.error.next(messages.errorAuth);

                            this.emailMessage = '';
                        }
                    }
                );
        } else {
            this.loading.next(false);

            this.emailMessage = messages.errorEmail;
        }
    }

    async presentPopover(): Promise<void> {
        const popover = await this.popoverController.create({
            component: NotAvailableServiceComponent,

            cssClass: 'new-class',

            translucent: true
        });

        await popover.present();
    }
}
