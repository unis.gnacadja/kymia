import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, NavController, PopoverController } from '@ionic/angular';
import { of, throwError } from 'rxjs';
import { NotAvailableServiceComponent } from 'src/app/components/not-available-service/not-available-service.component';
import { MockUser } from 'src/app/mock';
import { User } from 'src/app/models';
import { StockageService } from 'src/app/services/stockage.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { LoginComponent } from './login.page';

class MockNavController {
    navigate(params) {
        return {};
    }
    navigateRoot(params) {
        return {};
    }
}
class MockUserService {
    user: User;
    isLogged = false;
    token: string = '';
    login() {
        return of(MockUser.loggedUser);
    }
}

describe('KYMIA#86695mfth: should have primary color for color link', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let computeLink: HTMLElement;
    let userServiceSpy: MockUserService = new MockUserService();
    let navControllerSpy: MockNavController = new MockNavController();
    let popoverControllerSpy = {
        create: (args) =>
            Promise.resolve({
                present: () => Promise.resolve()
            }),
        dismiss: () => Promise.resolve()
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            providers: [
                {
                    provide: StockageService,
                    useValue: {
                        init: () => Promise.resolve(),
                        get: () => Promise.resolve(),
                        set: () => Promise.resolve()
                    }
                },
                {
                    provide: PopoverController,
                    useValue: popoverControllerSpy
                },
                { provide: UserService, useValue: userServiceSpy },
                { provide: NavController, useValue: navControllerSpy }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        computeLink = fixture.nativeElement.querySelector('#register');
        fixture.detectChanges();
    }));

    it('TA2: should have login form fields for email and password', () => {
        const EXPECTED_KEYS = ['email', 'password'];
        expect(Object.keys(component.f)).toEqual(EXPECTED_KEYS);
    });

    describe('TA5: After successful submission', () => {
        beforeAll(fakeAsync(() => {
            jest.spyOn(component.nav, 'navigateRoot');
            component.loginForm.controls.email.setValue('johndoe@gmail.com');
            component.loginForm.controls.password.setValue('Pa$$w0rd!');
            component.onSubmit();
        }));

        it('TA5-1: should redirect to the home page', () => {
            expect(component.nav.navigateRoot).toHaveBeenCalledWith([
                'tabs/home'
            ]);
        });

        it('TA5-2: should store received token', () => {
            expect(userServiceSpy.token).toBe(MockUser.loggedUser.token);
        });

        it('TA5-3: should deduce the name', () => {
            expect(userServiceSpy.user.name).toBe(
                MockUser.completeProfile.name
            );
        });

        it('TA5-4: should deduce the first name', () => {
            expect(userServiceSpy.user.firstname).toBe(
                MockUser.completeProfile.firstname
            );
        });

        it('TA5-5: should deduce the email', () => {
            expect(userServiceSpy.user.email).toBe(
                MockUser.completeProfile.email
            );
        });
    });

    it('TA6: should redirect to the validation page if the user has not yet confirmed', () => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                error: { error: 'PENDING_USER' },
                status: 403
            })
        );
        jest.spyOn(component.nav, 'navigateRoot');
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        expect(component.nav.navigateRoot).toHaveBeenCalledWith([
            '/validation'
        ]);
    });

    it('should redirect to the validation page if the user has not yet confirmed', () => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                status: 403
            })
        );
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        expect(component.error.getValue()).toBe(messages.errorAuth);
    });

    it('TA7: should display popup when service is unavailable', fakeAsync(() => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                error: { error: 'Server Error' },
                status: 502
            })
        );
        jest.spyOn(popoverControllerSpy, 'create');
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        tick();
        expect(popoverControllerSpy.create).toHaveBeenCalledWith({
            component: NotAvailableServiceComponent,
            cssClass: 'new-class',
            translucent: true
        });
    }));

    it('TA8: should display email-not-found error message upon submission', fakeAsync(() => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                error: { message: messages.errorAuth },
                status: 500
            })
        );
        jest.spyOn(popoverControllerSpy, 'create');
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        tick();
        expect(component.error.getValue()).toBe(messages.errorAuth);
    }));

    it('TA9: should display invalid-email error message upon submission', () => {
        jest.spyOn(popoverControllerSpy, 'create');
        component.loginForm.controls.email.setValue('mack,@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        expect(component.emailMessage).toBe(messages.errorEmail);
    });
});
