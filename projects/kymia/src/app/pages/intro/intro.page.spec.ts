import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NavController } from '@ionic/angular';
import { StockageService } from 'src/app/services/stockage.service';
import { IntroPage } from './intro.page';

class MockNavController {
    navigateRoot(args) {
        return {};
    }
}

describe('IntroPage', () => {
    let component: IntroPage;
    let fixture: ComponentFixture<IntroPage>;
    let navControllerSpy: MockNavController = new MockNavController();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [IntroPage],
            imports: [],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: StockageService,
                    useValue: {
                        set: () => Promise.resolve()
                    }
                },
                {
                    provide: NavController,
                    useValue: navControllerSpy
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(IntroPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('TA1: should redirect to login page on clicking "sign in" link', () => {
        jest.spyOn(navControllerSpy, 'navigateRoot');
        component.start();
        expect(navControllerSpy.navigateRoot).toHaveBeenCalledWith('/login');
    });
});
