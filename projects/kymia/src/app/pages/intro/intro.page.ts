import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { StockageService } from 'src/app/services/stockage.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-intro',

    templateUrl: './intro.page.html',

    styleUrls: ['./intro.page.scss']
})
export class IntroPage implements OnInit {
    @ViewChild('slideblock', { static: true }) slideblock: IonSlides;

    currentIndex: number;
    defaultAvatar = environment.user.avatar;
    slides: {
        title: string;
        image: string;
    }[];

    slideOpts: {
        initialSlide: number;
        speed: number;
        slidesPerView: number;
        autoplay: {
            delay: number;
            disableOnInteraction: boolean;
        };
    };

    constructor(
        private stockage: StockageService,
        public navCtrl: NavController
    ) {}

    ngOnInit(): void {
        this.currentIndex = 0;
        this.slideOpts = {
            initialSlide: 0,
            speed: 3000,
            slidesPerView: 1,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false
            }
        };
        this.slides = [
            {
                title: 'Trouvez des médecins selon vos critères',

                image: 'assets/img/doctors.png'
            },

            {
                title: 'Faites vous consulter partout et au plus tôt',

                image: 'assets/img/doctor.png'
            },

            {
                title: 'La solution de poche pour votre santé',
                image: 'assets/img/kymia.png'
            }
        ];
    }

    start(): void {
        this.stockage.set(environment.introKey, true);
        this.navCtrl.navigateRoot('/login');
    }
}
