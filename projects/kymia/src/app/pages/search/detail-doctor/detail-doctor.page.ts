import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConsultationStatus } from '@lib/enum';
import { Doctor } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-detail-doctor',
    templateUrl: './detail-doctor.page.html',
    styleUrls: ['./detail-doctor.page.scss']
})
export class DetailDoctorPage implements OnInit {
    doctor: Doctor;
    environment = environment;
    appointment: string;
    showSchedule: boolean = false;

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly consultationService: ConsultationsService,
        private readonly doctorService: DoctorService
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.doctor = this.doctorService.list.find(
                (item) => item.id === params['doctorId']
            );
        });
    }

    ionViewWillEnter(): void {
        // Display the number of waiting consultations
        const waitingConsultations = this.consultationService.list.filter(
            (booking) =>
                booking.status === ConsultationStatus.waiting &&
                new Date().getTime() <=
                    new Date(booking.calendar.availableDate).getTime() &&
                booking.calendar.doctor.id === this.doctor.id
        ).length;
        if (waitingConsultations > 0) {
            this.appointment = `Vous avez ${waitingConsultations
                .toString()
                .padStart(2, '0')} rendez-vous en attente avec ce praticien`;
        }
    }

    /**
     * Close the schedule
     */
    ionViewWillLeave(): void {
        this.showSchedule = false;
    }

    /**
     * Toggle the display of the schedule
     */
    toggleSchedule(): void {
        this.showSchedule = !this.showSchedule;
    }
}
