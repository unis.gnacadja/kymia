import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailDoctorPage } from './detail-doctor.page';

const routes: Routes = [
    {
        path: '',
        component: DetailDoctorPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetailDoctorPageRoutingModule {}
