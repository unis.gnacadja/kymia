import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DetailDoctorPageRoutingModule } from './detail-doctor-routing.module';
import { DetailDoctorPage } from './detail-doctor.page';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DetailDoctorPageRoutingModule,
        SharedModule
    ],
    declarations: [DetailDoctorPage]
})
export class DetailDoctorPageModule {}
