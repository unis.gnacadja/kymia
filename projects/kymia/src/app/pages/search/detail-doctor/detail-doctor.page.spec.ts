import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { normalizeDoctor } from '@lib/normalizer';
import { of } from 'rxjs';
import {
    consultationList,
    consultationServiceMock,
    DoctorMock,
    doctorServiceMock
} from 'src/app/mock';
import { Consultation } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { DetailDoctorPage } from './detail-doctor.page';

describe('DetailDoctorPage', () => {
    let component: DetailDoctorPage;
    let fixture: ComponentFixture<DetailDoctorPage>;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DetailDoctorPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientModule],
            providers: [
                { provide: DoctorService, useValue: doctorServiceMock },
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParams: of({ doctorId: DoctorMock.id })
                    }
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DetailDoctorPage);
        component = fixture.componentInstance;
        component.doctor = normalizeDoctor(DoctorMock);
        fixture.detectChanges();
    }));

    it('should display the number of waiting consultations when there is at least one', () => {
        jest.useFakeTimers().setSystemTime(new Date('2023-06-05T16:00:00'));
        const waitingConsultations: Consultation = {
            ...consultationList.waiting,
            calendar: {
                ...consultationList.waiting.calendar,
                availableDate: '2024/06/05'
            }
        };
        consultationServiceMock.list = [
            ...Array(2).fill(waitingConsultations),
            ...consultationServiceMock.list
        ];
        component.ionViewWillEnter();
        expect(component.appointment).toBe(
            'Vous avez 02 rendez-vous en attente avec ce praticien'
        );
    });

    it('should close schedule when ionViewWillLeave', () => {
        component.ionViewWillLeave();
        expect(component.showSchedule).toBe(false);
    });

    it('should toggle the schedule when toggleSchedule is called', () => {
        const showSchedule = component.showSchedule;
        component.toggleSchedule();
        expect(component.showSchedule).toBe(!showSchedule);
    });
});
