import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { CriteriaMock } from 'src/app/mock';
import { SearchPage } from 'src/app/pages/search/search.page';

describe('SearchPage', () => {
    let component: SearchPage;
    let fixture: ComponentFixture<SearchPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SearchPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(SearchPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should update search criteria when getCriteria is called', () => {
        component.getCriteria(CriteriaMock);
        expect(component.searchCriteria).toEqual(CriteriaMock);
    });

    it('should search again when ionViewDidLeave is called', () => {
        component.doctorlistComponent = {
            find: jest.fn()
        } as any;
        component.ionViewDidLeave();
        expect(component.doctorlistComponent.find).toHaveBeenCalled();
    });
});
