import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DoctorAppointmentChoicePageRoutingModule } from './search-routing.module';
import { SearchPage } from './search.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DoctorAppointmentChoicePageRoutingModule,
        Ng2SearchPipeModule,
        SharedModule
    ],
    declarations: [SearchPage]
})
export class DoctorAppointmentChoicePageModule {}
