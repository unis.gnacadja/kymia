import { Component, ViewChild } from '@angular/core';
import { DoctorlistComponent } from 'src/app/components/doctorlist/doctorlist.component';
import { Criteria } from 'src/app/models';

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss']
})
export class SearchPage {
    searchCriteria: Criteria;

    @ViewChild(DoctorlistComponent) doctorlistComponent: DoctorlistComponent;

    /**
     * Refresh the doctor list
     */
    ionViewDidLeave(): void {
        this.doctorlistComponent.find(this.searchCriteria);
    }

    /**
     * Update the search criteria
     *
     * @param payload: The criteria to be searched
     */
    getCriteria(payload: Criteria) {
        this.searchCriteria = payload;
    }
}
