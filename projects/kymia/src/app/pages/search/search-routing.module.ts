import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPage } from './search.page';

const routes: Routes = [
    {
        path: '',
        component: SearchPage
    },
    {
        path: 'detail-doctor',
        loadChildren: () =>
            import('./detail-doctor/detail-doctor.module').then(
                (m) => m.DetailDoctorPageModule
            )
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DoctorAppointmentChoicePageRoutingModule {}
