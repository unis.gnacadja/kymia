import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { AutocompleteLibModule } from '../../modules/autocomplete-lib/src/lib/autocomplete-lib.module';
import { ProfilPageRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        ProfilPageRoutingModule,
        AutocompleteLibModule,
        SharedModule
    ],
    providers: [],
    declarations: [ProfilePage]
})
export class ProfilePageModule {}
