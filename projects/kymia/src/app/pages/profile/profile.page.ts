import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { cardMedia, helperMessages } from '@lib/constants';
import { PhoneService } from '@lib/services/phone.service';
import { finalize } from 'rxjs/operators';
import { Domain, User } from 'src/app/models';
import { LocationService } from 'src/app/services/location/location.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { City } from '../../models/city.model';
import { Country } from '../../models/country.model';
@Component({
    selector: 'app-profil',

    templateUrl: './profile.page.html',

    styleUrls: ['./profile.page.scss']
})
export class ProfilePage implements OnInit {
    cardMedia: string = cardMedia;

    search: string = 'name';

    readonly: boolean = true;

    loading: boolean = false;

    submit: boolean = false;

    error: string = null;

    activities: Domain[] = [];

    countries: Country[];

    userCity: City;

    idLieuDeNaissance: string;

    cities: City[];

    selectedCity: City;

    image: any;

    profilForm: FormGroup;
    formBuilder: FormBuilder = new FormBuilder();

    constructor(
        private readonly loadingService: LoadingService,
        private readonly locationService: LocationService,
        private readonly userService: UserService,
        private readonly notifyService: NotifyService,
        public phoneService: PhoneService
    ) {}

    ngOnInit() {
        this.profilForm = this.formBuilder.group({
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            email: [
                '',
                Validators.compose([Validators.email, Validators.required])
            ],
            phone: [
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(16)
                ])
            ],
            country: ['', Validators.required],
            city: ['', Validators.required],
            groupeSanguin: ['', Validators.required],
            office: ['', Validators.required],
            adresse: ['', Validators.required],
            antecedents: ['', Validators.required],
            secteurActivite: ['', Validators.required],
            idLieuDeNaissance: ['', Validators.required]
        });
        this.detailsfetch();
    }

    /**
     * Fetches the patient data
     */
    async detailsfetch(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);
        this.userService
            .getPatient(this.userService.user.id)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: async (user: User) => {
                    this.getForm(user);
                    await this.getCountries();
                    this.userCity = user.city;
                    this.selectedCity = user.city;
                    this.userService.setUser(user);
                    this.image = user.photo;
                }
            });
    }

    /*
     * get countries
     */
    async getCountries(): Promise<void> {
        this.locationService.getCountries().subscribe({
            next: (res: any) => {
                this.countries = res.content.sort((a, b) =>
                    a.name.localeCompare(b.name)
                );
            }
        });
    }

    /*
     * get cities of country
     */
    getCities(countryId: string): void {
        if (countryId) {
            this.locationService.searchCity(countryId).subscribe({
                next: (res: any) => {
                    this.cities = res.content.sort((a, b) =>
                        a.name.localeCompare(b.name)
                    );
                }
            });
        }
    }

    /**
     * Selects a country and fetches the cities of the selected country
     *
     * @param country: Country: country to select
     */
    selectCountryEvent(country: Country) {
        this.getCities(country.id);
    }

    /**
     * Selects a city
     *
     * @param city: City: city to select
     */
    selectCityEvent(city: City): void {
        this.selectedCity = city;
    }

    /**
     * Clears the selected city
     */
    removeCity() {
        this.selectedCity = null;
    }

    /*
     *  get form data lieuDeNaissance
     */
    getForm(data: User) {
        this.profilForm.patchValue({
            firstname: data.firstname,
            lastname: data.name,
            phone: this.phoneService.getPhoneNumber(data.phone),
            groupeSanguin: data.groupeSanguin,
            antecedents: data.antecedents,
            secteurActivite: data.secteurActivite,
            email: data.email
        });

        if (data.city) {
            this.profilForm.patchValue({
                city: data.city.name,
                country: data.city.country.name
            });
        }
    }

    get f(): { [key: string]: AbstractControl } {
        return this.profilForm.controls;
    }

    /*
     * submit
     */
    onSubmit(): void {
        if (this.readonly) {
            this.readonly = false;
        } else {
            this.userCity = this.selectedCity;
            this.error = null;

            if (
                !this.submit &&
                !this.loading &&
                this.profilForm.controls.firstname.valid &&
                this.profilForm.controls.lastname.valid &&
                this.profilForm.controls.phone.valid
            ) {
                this.submit = true;
                this.readonly = true;
                this.loading = true;
                this.userService

                    .updateProfilUser(this.userService.user.id, {
                        email: this.profilForm.value.email,

                        cityId: this.selectedCity?.id?.toString(),

                        firstname: this.profilForm.value.firstname,

                        name: this.profilForm.value.lastname,

                        phone: `${this.phoneService.dialCode}${this.profilForm.value.phone}`,

                        groupeSanguin: this.profilForm.value.groupeSanguin,

                        genre: this.userService.user.genre,

                        antecedents: this.profilForm.value.antecedents,

                        secteurActivite: this.profilForm.value.secteurActivite,

                        idLieuDeNaissance: this.selectedCity?.id?.toString()
                    })
                    .pipe(
                        finalize(() => {
                            this.loading = false;
                            this.submit = false;
                        })
                    )
                    .subscribe({
                        next: (res: User) => {
                            this.userService.setUser(res);
                            this.readonly = true;
                            this.notifyService.presentToast({
                                message: messages.succefullyUpdated,
                                color: State.success
                            });
                        },
                        error: () => {
                            this.readonly = false;
                            this.notifyService.presentToast({
                                message: messages.sendError
                            });
                        }
                    });
            }
        }
    }

    /**
     * Check if the form is valid
     */
    check() {
        if (
            this.profilForm.controls.phone.hasError('minlength') ||
            this.profilForm.controls.phone.hasError('maxlength')
        ) {
            this.submit = true;
        }
    }

    /**
     * Check if the key pressed is valid
     *
     * @param event
     */
    keyPress(event: any) {
        const pattern = /[0-9+\- ]/;

        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
}
