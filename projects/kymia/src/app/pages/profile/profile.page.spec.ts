import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { citiesMock, locationServiceMock, userServiceMock } from 'src/app/mock';
import { LocationService } from 'src/app/services/location/location.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { ProfilePage } from './profile.page';

describe('ProfilePage', () => {
    let component: ProfilePage;
    let fixture: ComponentFixture<ProfilePage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ProfilePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: LocationService,
                    useValue: locationServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ProfilePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should update the list of towns', () => {
        component.selectCountryEvent(component.countries[1]);
        expect(component.cities).toEqual(citiesMock);
    });

    it("should update the user's city", () => {
        component.selectCityEvent(citiesMock[1]);
        expect(component.selectedCity).toBe(citiesMock[1]);
    });

    describe('On submit', () => {
        it('should set readonly to false when readonly is true', () => {
            component.readonly = true;
            component.onSubmit();
            expect(component.readonly).toBe(false);
        });

        it('should notify on successful form submission', fakeAsync(() => {
            component.readonly = false;
            component.onSubmit();
            tick();
            expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
                message: messages.succefullyUpdated,
                color: State.success
            });
        }));

        it('should notify on form submission failure', fakeAsync(() => {
            userServiceMock.updateProfilUser.mockReturnValue(
                throwError({ error: { status: 500 } })
            );
            component.readonly = false;
            component.selectedCity = null;
            component.onSubmit();
            tick();
            expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
                message: messages.sendError
            });
        }));
    });

    it('should not submit if phone number is too short', () => {
        component.submit = false;
        component.profilForm.controls.phone.setValue('1234567');
        component.check();
        expect(component.submit).toBe(true);
    });

    it('Should not submit if phone number is too long.', () => {
        component.submit = false;
        component.profilForm.controls.phone.setValue('12345678998765432');
        component.check();
        expect(component.submit).toBe(true);
    });

    it('should allow user to input numbers in phone field ', () => {
        let event: KeyboardEvent = new KeyboardEvent('keypress', {
            charCode: '6'.charCodeAt(0),
            keyCode: '6'.charCodeAt(0),
            bubbles: true,
            cancelable: true
        });
        jest.spyOn(event, 'preventDefault');
        component.keyPress(event);
        expect(event.preventDefault).not.toHaveBeenCalled();
    });

    it('should disallow user from inputting letters in phone field.', () => {
        const event: KeyboardEvent = new KeyboardEvent('keypress', {
            charCode: 'a'.charCodeAt(0),
            keyCode: 'a'.charCodeAt(0),
            bubbles: true,
            cancelable: true
        });
        jest.spyOn(event, 'preventDefault');
        component.keyPress(event);
        expect(event.preventDefault).toHaveBeenCalled();
    });

    it("should set the user's city to null when removeCity is called", () => {
        component.removeCity();
        expect(component.selectedCity).toBe(null);
    });
    it('should call removeCity when city is removed', () => {
        component.removeCity();
        expect(component.selectedCity).toBeNull();
    });
});
