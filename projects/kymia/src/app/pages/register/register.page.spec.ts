import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { IonicModule, NavController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import { MockUser, routerMock, userServiceMock } from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { RegisterComponent } from './register.page';

describe('RegisterPage', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [RegisterComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                IonicModule.forRoot(),
                HttpClientTestingModule,
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                { provide: Router, useValue: routerMock },
                { provide: NavController, useValue: {} },
                { provide: UserService, useValue: userServiceMock },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.registerForm.controls.email.setValue(MockUser.newUSer.email);
        component.registerForm.controls.password.setValue(
            MockUser.completeProfile.password
        );
        component.registerForm.controls.firstname.setValue(
            MockUser.newUSer.firstname
        );
        component.registerForm.controls.lastname.setValue(
            MockUser.completeProfile.name
        );
        component.registerForm.controls.phone.setValue(MockUser.newUSer.phone);
        component.registerForm.controls.acceptTerms.setValue(true);
    }));

    it('should display the terms of use when the Terms and Conditions link is clicked', () => {
        const buttonConditions = fixture.nativeElement.querySelector(
            "[data-testid='conditions']"
        );
        buttonConditions.click();
        fixture.detectChanges();
        expect(component.showConditions).toBe(true);
    });

    it('should redirect to the validation page upon successful registration', fakeAsync(() => {
        component.registerForm.controls.email.setValue(MockUser.newUSer.email);
        jest.spyOn(routerMock, 'navigate');
        jest.spyOn(userServiceMock, 'registerUser').mockReturnValue(
            of(MockUser.simple)
        );
        component.onSubmit();
        tick();
        expect(routerMock.navigate).toHaveBeenCalledWith(['/validation']);
    }));

    describe('toggleShow', () => {
        it('should display the password when it is hidden', () => {
            component.showPassword = false;
            component.toggleShow();
            expect(component.showPassword).toBe(true);
        });

        it('should hide the password when it is displayed', () => {
            component.showPassword = true;
            component.toggleShow();
            expect(component.showPassword).toBe(false);
        });
    });

    it('should display an error message when registration fails', fakeAsync(() => {
        const mockError = {
            error: { message: helperMessages.errorOccurred },
            status: 403
        };
        component.registerForm.controls.email.setValue(MockUser.newUSer.email);
        component.loading.next(false);
        component.submit.next(false);
        jest.spyOn(userServiceMock, 'registerUser').mockReturnValue(
            throwError(mockError)
        );

        component.onSubmit();
        tick();

        expect(component.error.getValue()).toBe(mockError.error.message);
    }));

    it('should display a notification if registration fails and the API doesn’t return an error', fakeAsync(() => {
        const errorResponse = {
            error: null
        };
        jest.spyOn(userServiceMock, 'registerUser').mockReturnValue(
            throwError(errorResponse)
        );
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith();
    }));
});
