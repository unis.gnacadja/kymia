import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { cardMedia } from '@lib/constants';
import { dataPattern } from '@lib/MustMatch.validator';
import { PhoneService } from '@lib/services/phone.service';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/internal/operators/finalize';
import { UserService as RegisterService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading: BehaviorSubject<boolean>;
    submit: BehaviorSubject<boolean>;
    error: BehaviorSubject<string>;
    showPassword: boolean;
    showConditions: boolean = false;
    phoneErrorMessage: string = '';
    cardMedia: string = cardMedia;
    messages: Record<string, string> = messages;

    /*****
     * Constructor
     ****/
    constructor(
        private readonly notifyService: NotifyService,
        private readonly formBuilder: FormBuilder,
        private readonly registerService: RegisterService,
        private readonly router: Router,
        private readonly phoneService: PhoneService
    ) {}

    /**
     * ngOnInit
     */
    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);
        this.submit = new BehaviorSubject<boolean>(false);
        this.error = new BehaviorSubject<string>(null);
        this.registerForm = this.formBuilder.group({
            email: [
                '',
                [
                    Validators.required,
                    Validators.email,
                    Validators.pattern(dataPattern.email)
                ]
            ],
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.pattern(dataPattern.password)
                ]
            ],
            firstname: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.pattern(dataPattern.name)
                ]
            ],
            lastname: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.pattern(dataPattern.name)
                ]
            ],
            phone: [
                '',
                [Validators.required, Validators.pattern(dataPattern.phone)]
            ],
            acceptTerms: [false, Validators.requiredTrue],
            birthdate: ['']
        });
    }

    get f(): { [key: string]: AbstractControl } {
        return this.registerForm.controls;
    }

    /*
     * @description: submit the register form
     */
    async onSubmit(): Promise<void> {
        this.registerForm.controls.email.setValue(
            this.registerForm.controls.email.value.trim()
        );
        this.registerForm.controls.password.setValue(
            this.registerForm.controls.password.value.trim()
        );
        this.error.next(null);
        if (!this.submit.getValue()) {
            this.submit.next(true);
        }

        if (!this.loading.getValue() && this.registerForm.valid) {
            this.loading.next(true);
            this.registerService
                .registerUser({
                    email: this.registerForm.value.email,
                    password: this.registerForm.value.password,
                    firstname: this.registerForm.value.firstname,
                    name: this.registerForm.value.lastname,
                    phone: `${this.phoneService.dialCode}${this.registerForm.value.phone}`
                })
                .pipe(
                    finalize(() => {
                        this.loading.next(false);
                        this.submit.next(false);
                    })
                )
                .subscribe({
                    next: () => {
                        localStorage.email = this.registerForm.value.email;
                        localStorage.password =
                            this.registerForm.value.password;
                        localStorage.type = 'Patient';
                        this.router.navigate(['/validation']);
                    },
                    error: (error) => {
                        if (error.status === 403) {
                            this.error.next(error.error.message);
                        } else {
                            this.notifyService.presentToast();
                        }
                    }
                });
        }
    }

    /**
     * Show or hide the conditions
     */
    openCondtions(): void {
        this.showConditions = !this.showConditions;
    }

    /**
     * Toggle the password visibility
     */
    toggleShow(): void {
        this.showPassword = !this.showPassword;
    }
}
