import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { IonicModule, NavController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { MockUser, routerMock, userServiceMock } from 'src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { PwdforgetPage } from './pwdforget.page';

describe('PwdforgetPage', () => {
    let component: PwdforgetPage;
    let fixture: ComponentFixture<PwdforgetPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [PwdforgetPage],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                { provide: Router, useValue: routerMock },
                { provide: NavController, useValue: {} }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(PwdforgetPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should detect errors in the email field', fakeAsync(() => {
        component.pwdforgetForm.controls.email.setValue('john@ gmail.com');
        component.onSubmit();
        tick();
        expect(component.f.email.errors).toEqual({ email: true });
    }));

    it('should display a success message after submission', fakeAsync(() => {
        component.pwdforgetForm.controls.email.setValue(
            MockUser.completeProfile.email
        );
        jest.spyOn(notifyServiceMock, 'presentToast');
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.succefullyUpdated,
            duration: 4000,
            color: State.success
        });
    }));

    it('should set errorApi to true when no error is received', fakeAsync(() => {
        const errorResponse = {
            error: null
        };
        component.pwdforgetForm.controls.email.setValue(
            MockUser.completeProfile.email
        );
        jest.spyOn(userServiceMock, 'resetPassword').mockReturnValue(
            throwError(errorResponse)
        );
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith();
    }));

    it('should receive 403 when email not found during submission', fakeAsync(() => {
        const errorResponse = {
            error: {
                status: 403,
                message: helperMessages.errorOccurred
            }
        };
        component.pwdforgetForm.controls.email.setValue(
            MockUser.completeProfile.email
        );
        jest.spyOn(userServiceMock, 'resetPassword').mockReturnValue(
            throwError(errorResponse)
        );
        component.onSubmit();
        tick();
        expect(component.errorMessage).toEqual(errorResponse.error.message);
    }));
});
