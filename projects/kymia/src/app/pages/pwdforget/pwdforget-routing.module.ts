import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PwdforgetPage } from './pwdforget.page';

const routes: Routes = [
    {
        path: '',
        component: PwdforgetPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PwdforgetPageRoutingModule {}
