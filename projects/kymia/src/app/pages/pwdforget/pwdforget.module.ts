import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { PwdforgetPageRoutingModule } from './pwdforget-routing.module';
import { PwdforgetPage } from './pwdforget.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        PwdforgetPageRoutingModule,
        ReactiveFormsModule
    ],
    providers: [],
    declarations: [PwdforgetPage]
})
export class PwdforgetPageModule {}
