import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { State } from '@idermato/src/app/utils/enum';
import { cardMedia } from '@lib/constants';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/internal/operators/finalize';
import { UserService as PwdforgetService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
@Component({
    selector: 'app-pwdforget',
    templateUrl: './pwdforget.page.html',
    styleUrls: ['./pwdforget.page.scss']
})
export class PwdforgetPage implements OnInit {
    pwdforgetForm: FormGroup;
    loading: BehaviorSubject<boolean>;
    submit: BehaviorSubject<boolean>;
    errorMessage: string;
    cardMedia = cardMedia;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public pwdforgetService: PwdforgetService,
        public notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);
        this.submit = new BehaviorSubject<boolean>(false);
        this.pwdforgetForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    get f(): { [key: string]: AbstractControl } {
        return this.pwdforgetForm.controls;
    }

    onSubmit(): void {
        if (!this.submit.getValue()) {
            this.submit.next(true);
        }
        if (
            this.submit.getValue() &&
            !this.loading.getValue() &&
            this.pwdforgetForm.get('email').valid
        ) {
            this.loading.next(true);
            this.pwdforgetService
                .resetPassword(this.pwdforgetForm.get('email').value)
                .pipe(
                    finalize(() => {
                        this.loading.next(false);
                        this.submit.next(false);
                    })
                )
                .subscribe({
                    next: () => {
                        this.notifyService.presentToast({
                            message: messages.succefullyUpdated,
                            duration: 4000,
                            color: State.success
                        });
                        this.router.navigate(['/newpwdsuccess']);
                    },
                    error: (error) => {
                        if (error.error?.message) {
                            this.errorMessage = error.error.message;
                        } else {
                            this.notifyService.presentToast();
                        }
                    }
                });
        }
    }
}
