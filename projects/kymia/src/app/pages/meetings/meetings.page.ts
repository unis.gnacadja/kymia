import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { finalize } from 'rxjs/operators';
import { Consultation } from 'src/app/models';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { messages, statusMessages } from 'src/app/utils/constants';
import { normalizeConsultation } from 'src/app/utils/normalizer';

type Tab = {
    value: string[];
    label: string;
};

const upcomingStatuses: string[] = [
    ConsultationStatus.payed,
    ConsultationStatus.confirmByDoctor,
    ConsultationStatus.waiting
];

@Component({
    selector: 'app-meetings',

    templateUrl: './meetings.page.html',

    styleUrls: ['./meetings.page.scss']
})
export class MeetingsPage implements OnInit {
    meetStatus = ConsultationStatus;
    terminateStatus = statusMessages.terminated;
    tabs: Tab[] = [
        {
            value: upcomingStatuses,
            label: helperMessages.next
        },
        {
            value: [ConsultationStatus.terminated],
            label: statusMessages.terminated
        },
        { value: [ConsultationStatus.annuled], label: statusMessages.annuled }
    ];

    upcomingStatuses: string[] = upcomingStatuses;

    list: Consultation[] = [];

    activeTab: string[];

    loader: boolean;

    completeProfile: boolean;

    constructor(
        public consultationsService: ConsultationsService,
        private userservice: UserService,
        public loadingService: LoadingService
    ) {}

    ngOnInit(): void {
        this.completeProfile = this.userservice.user.completeProfile;
        this.activeTab = this.tabs[0].value;
    }

    ionViewWillEnter(): void {
        this.detailsfetch();
    }

    /**
     * Fetches the consultations for the current user
     */
    async detailsfetch(): Promise<void> {
        this.loader = true;
        await this.loadingService.present(messages.loading);
        this.consultationsService
            .getPatientSchedule(this.userservice.user.id)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                    this.loader = false;
                })
            )
            .subscribe({
                next: (res: { content: Consultation[] }) => {
                    this.consultationsService.list = res.content.map(
                        normalizeConsultation
                    );
                }
            });
    }

    /**
     * Tracks the tab by value
     *
     * @param _index
     * @param tab
     */
    trackTab(_index: number, tab: Tab) {
        return tab.value;
    }

    /**
     * Tracking consultation List by Id
     *
     * @param _index
     * @param consultation
     */
    trackConsultation(_index: number, consultation: Consultation) {
        return consultation.id;
    }
}
