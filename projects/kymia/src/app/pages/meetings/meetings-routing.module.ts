import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeetingsPage } from './meetings.page';

const routes: Routes = [
    {
        path: '',
        component: MeetingsPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MeetingsPageRoutingModule {}
