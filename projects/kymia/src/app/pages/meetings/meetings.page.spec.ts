import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { IonicModule } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { loadingServiceMock } from '@lib/mocks';
import { of } from 'rxjs';
import {
    ConsultationMock,
    consultationServiceMock,
    userServiceMock
} from 'src/app/mock';
import { FilterBy } from 'src/app/pipe';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { UserService } from 'src/app/services/user.service';
import { MeetingsPage } from './meetings.page';

describe('MeetingPage', () => {
    let component: MeetingsPage;
    let fixture: ComponentFixture<MeetingsPage>;

    consultationServiceMock.getPatientSchedule.mockReturnValue(
        of({ content: Object.values(ConsultationMock) })
    );

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [MeetingsPage, FilterBy],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            providers: [
                Storage,
                FilterBy,
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                }
            ]
        });
        TestBed.compileComponents();
        fixture = TestBed.createComponent(MeetingsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should display upcoming Booking on view enter', fakeAsync(() => {
        jest.spyOn(component, 'ionViewWillEnter');
        component.ionViewWillEnter();
        tick();
        component.consultationsService.list.forEach((consultation) => {
            expect(component.upcomingStatuses.includes(consultation.status));
        });
    }));

    it('should track tab by value', () => {
        expect(component.trackTab(0, component.tabs[0])).toBe(
            component.tabs[0].value
        );
    });

    it('should track consultation by id', () => {
        expect(
            component.trackConsultation(
                0,
                component.consultationsService.list[0]
            )
        ).toBe(component.consultationsService.list[0].id);
    });
});
