import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { MeetingsPageRoutingModule } from './meetings-routing.module';
import { MeetingsPage } from './meetings.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        SharedModule,
        MeetingsPageRoutingModule
    ],
    providers: [],
    declarations: [MeetingsPage]
})
export class MeetingsPageModule {}
