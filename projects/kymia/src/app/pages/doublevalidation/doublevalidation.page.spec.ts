import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterModule, Routes } from '@angular/router';
import { IonicModule, NavController, ToastController } from '@ionic/angular';
import { of, throwError } from 'rxjs';
import { MockUser } from 'src/app/mock';
import { StockageService } from 'src/app/services/stockage.service';
import { UserService } from 'src/app/services/user.service';
import { HomePage } from '../home/home.page';
import { LoginComponent } from '../login/login.page';
import { DoublevalidationPage } from './doublevalidation.page';

let userActivated;
const INVALID_CODE = 'Le code de validation est invalide';
const EMAIL_NOT_FOUND =
    "L'adresse e-mail est introuvable. Veuillez réessayer en cliquant sur le lien envoyé par e-mail.";

const routes: Routes = [
    { path: '', component: HomePage },
    { path: 'tabs/home', component: HomePage },
    { path: 'login', component: LoginComponent },
    { path: 'validation', component: DoublevalidationPage }
];
class MockUservice {
    user = MockUser.newUSer;
    resend(email) {
        return of({
            token: MockUser.loggedUser.token
        });
    }
    login(email, password) {
        return of({
            token: MockUser.loggedUser.token
        });
    }
    validation({ email, code }: { email: string; code: string }) {
        userActivated = `L'utilisateur ${email} a été activé avec succès`;
        return of(userActivated);
    }
}
class MockStorageService {
    token = '';
    set(name, value) {
        this[name] = value;
    }
}

describe('KYMIA#86695mfth: should have primary color for color link', () => {
    let component: DoublevalidationPage;
    let fixture: ComponentFixture<DoublevalidationPage>;
    let computeLink: HTMLElement;
    let userServiceSpy: MockUservice = new MockUservice();
    let navControllerSpy: { navigateForward: jest.Mock };
    let routerSpy: { navigate: jest.Mock };
    let toastControllerSpy = {
        create: (args) =>
            Promise.resolve({
                present: () => Promise.resolve()
            }),
        dismiss: () => Promise.resolve()
    };
    let storageSpy: MockStorageService = new MockStorageService();

    beforeEach(waitForAsync(() => {
        routerSpy = { navigate: jest.fn() };
        TestBed.configureTestingModule({
            declarations: [DoublevalidationPage],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot(routes),
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                { provide: UserService, useValue: userServiceSpy },
                { provide: NavController, useValue: navControllerSpy },
                { provide: Router, useValue: routerSpy },
                {
                    provide: StockageService,
                    useValue: storageSpy
                },
                {
                    provide: ToastController,
                    useValue: toastControllerSpy
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DoublevalidationPage);
        component = fixture.componentInstance;
        computeLink = fixture.nativeElement.querySelector(
            '[data-test-id="resend"]'
        );
        fixture.detectChanges();
    }));

    it('should put the link to return the code in blue', () => {
        expect(computeLink.getAttribute('color')).toBe('primary');
    });

    it('should send a notification when the code is successfully returned.', fakeAsync(() => {
        jest.spyOn(toastControllerSpy, 'create');
        component.resendconfirmation();
        tick();
        expect(toastControllerSpy.create).toHaveBeenCalledWith({
            message: 'Code envoyer avec succes',
            duration: 4000,
            color: 'success'
        });
    }));

    it('should receive an error message during submission when the email is not found', fakeAsync(() => {
        localStorage.removeItem('email');
        jest.spyOn(userServiceSpy, 'resend').mockReturnValue(
            throwError({
                status: 404,
                error: {
                    message: EMAIL_NOT_FOUND
                }
            })
        );
        jest.spyOn(component, 'getError');
        component.resendconfirmation();
        tick();
        expect(component.error.getValue()).toBe(EMAIL_NOT_FOUND);
    }));

    it('should not redirect to the home page upon submission if the validation code is empty', fakeAsync(() => {
        component.validationForm.controls.codeConfirm.setValue('');
        component.onSubmit();
        tick();
        expect(window.location.pathname).not.toBe('/tabs/home');
    }));

    it('should receive an error message during submission when the code is not valid', fakeAsync(() => {
        component.validationForm.controls.codeConfirm.setValue('AX4152');
        jest.spyOn(userServiceSpy, 'validation').mockReturnValueOnce(
            throwError({
                status: 404,
                error: {
                    message: INVALID_CODE
                }
            })
        );
        jest.spyOn(component, 'getError');
        component.onSubmit();
        tick();
        expect(component.error.getValue()).toBe(INVALID_CODE);
    }));

    it('should register user', fakeAsync(() => {
        component.validationForm.controls.codeConfirm.setValue('AX4152');
        component.onSubmit();
        tick();
        expect(routerSpy.navigate).toHaveBeenCalledWith(['/register-success']);
    }));
});
