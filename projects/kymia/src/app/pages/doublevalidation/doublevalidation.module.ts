import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DoublevalidationPageRoutingModule } from './doublevalidation-routing.module';
import { DoublevalidationPage } from './doublevalidation.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        DoublevalidationPageRoutingModule,
        ReactiveFormsModule
    ],
    providers: [],
    declarations: [DoublevalidationPage]
})
export class DoublevalidationPageModule {}
