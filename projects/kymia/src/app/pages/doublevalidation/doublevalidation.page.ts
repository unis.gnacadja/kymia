import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { cardMedia } from '@lib/constants';
import { BehaviorSubject } from 'rxjs';
import { StockageService } from '../../services/stockage.service';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-doublevalidation',
    templateUrl: './doublevalidation.page.html',
    styleUrls: ['./doublevalidation.page.scss']
})
export class DoublevalidationPage implements OnInit {
    cardMedia: string = cardMedia;
    validationForm: FormGroup;
    loading: BehaviorSubject<boolean>;
    submit: BehaviorSubject<boolean>;
    error: BehaviorSubject<string>;

    constructor(
        private storage: StockageService,
        public formBuilder: FormBuilder,
        public router: Router,
        public userService: UserService,
        public toastController: ToastController,
        public nav: NavController
    ) {}

    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);
        this.submit = new BehaviorSubject<boolean>(false);
        this.error = new BehaviorSubject<string>(null);
        this.validationForm = this.formBuilder.group({
            email: ['', Validators.required],
            codeConfirm: ['', Validators.required]
        });
    }

    /**
     * Resend confirmation email
     */
    resendconfirmation(): void {
        this.userService.resend(localStorage.email).subscribe(
            (res: any) => {
                this.submit.next(false);
                this.storage.set('token', res.token);
                this.notify();
            },
            (error) => {
                this.getError(error);
            }
        );
    }

    /**
     * Notify user
     */
    async notify(): Promise<void> {
        const toast = await this.toastController.create({
            message: 'Code envoyer avec succes',
            duration: 4000,
            color: 'success'
        });
        toast.present();
    }

    /**
     * Get error
     */
    getError(error): void {
        this.loading.next(false);
        this.submit.next(false);
        this.error.next(error.error.message);
    }

    /**
     * On submit
     */
    onSubmit(): void {
        this.error.next(null);
        const email = localStorage.email;
        if (!this.submit.getValue()) {
            this.submit.next(true);
        }
        if (this.submit.getValue() && !this.loading.getValue()) {
            this.loading.next(true);
            if (this.validationForm.get('codeConfirm').valid) {
                this.userService
                    .validation({
                        email,
                        code: this.validationForm.get('codeConfirm').value
                    })
                    .subscribe(
                        () => {
                            this.loading.next(true);
                            this.submit.next(false);
                            this.router.navigate(['/register-success']);
                            localStorage.clear();
                        },
                        (error) => {
                            this.getError(error);
                        }
                    );
            } else {
                this.loading.next(false);
            }
        }
    }
}
