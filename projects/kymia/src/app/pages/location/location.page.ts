import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Location } from 'src/app/models';
import { LocationService } from 'src/app/services/location/location.service';
import { UserService } from 'src/app/services/user.service';
@Component({
    selector: 'app-location',
    templateUrl: './location.page.html',
    styleUrls: ['./location.page.scss']
})
export class LocationPage implements OnInit {
    items: Location[] = [];
    search: string;
    constructor(
        public toastController: ToastController,
        public userService: UserService,
        public locationService: LocationService
    ) {}

    ngOnInit() {
        this.locationService.getAvailableCities().subscribe((res: any) => {
            res.content.forEach((data) => {
                this.items = [
                    ...this.items,
                    ...data.cities.map((el) => ({
                        id: el.id,
                        city: el.name,
                        country: data.name
                    }))
                ];
            });
        });
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'Paramètre sauvegardé.',
            duration: 2000
        });
        toast.present();
    }

    setCountry(item: Location): void {
        this.userService.updateLocation(item);
        this.presentToast();
    }
}
