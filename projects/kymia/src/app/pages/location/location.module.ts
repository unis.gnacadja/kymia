import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { LocationPageRoutingModule } from './location-routing.module';
import { LocationPage } from './location.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LocationPageRoutingModule,
        SharedModule
    ],
    declarations: [LocationPage]
})
export class LocationPageModule {}
