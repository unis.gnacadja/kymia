import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ToastController } from '@ionic/angular';
import { of } from 'rxjs';
import { MockCities } from 'src/app/mock';
import { Location } from 'src/app/models';
import { LocationService } from 'src/app/services/location/location.service';
import { UserService } from 'src/app/services/user.service';
import { LocationPage } from './location.page';

let expectedCities: Location[] = [];
MockCities.forEach((region) => {
    expectedCities = [
        ...expectedCities,
        ...region.cities.map((city) => ({
            id: city.id,
            city: city.name,
            country: region.name
        }))
    ];
});

class MockLocationService {
    getAvailableCities() {
        return of({
            content: MockCities
        });
    }
}

describe('LocationPage', () => {
    let component: LocationPage;
    let fixture: ComponentFixture<LocationPage>;
    let locationServiceSpy: MockLocationService = new MockLocationService();
    let toastControllerSpy = {
        create: () =>
            Promise.resolve({
                present: () => Promise.resolve()
            })
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [LocationPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                UserService,
                {
                    provide: LocationService,
                    useValue: locationServiceSpy
                },
                {
                    provide: ToastController,
                    useValue: toastControllerSpy
                }
            ],
            imports: [IonicModule.forRoot(), HttpClientTestingModule]
        }).compileComponents();

        fixture = TestBed.createComponent(LocationPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('TA1: should get all cities when initializing', () => {
        expect(component.items).toEqual(expectedCities);
    });

    it('TA2: should update the location based on the selected city', () => {
        component.setCountry(expectedCities[0]);
        expect(component.userService.location.getValue()).toEqual(
            expectedCities[0]
        );
    });

    it('TA3: should receive notification of successful city selection', () => {
        jest.spyOn(toastControllerSpy, 'create');
        component.setCountry(expectedCities[0]);
        expect(toastControllerSpy.create).toHaveBeenCalledWith({
            message: 'Paramètre sauvegardé.',
            duration: 2000
        });
    });
});
