import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import {
    ActivityMock,
    commonServiceMock,
    ConsultationMock,
    consultationServiceMock,
    userServiceMock
} from 'src/app/mock';
import { CommonService } from 'src/app/services/common.service';
import { ConsultationsService } from 'src/app/services/consultations.service';
import { StockageService } from 'src/app/services/stockage.service';
import { UserService } from 'src/app/services/user.service';
import { normalizeConsultation } from 'src/app/utils/normalizer';
import { HomePage } from './home.page';

describe('KYMIA#86695mfth: should have primary color for color link', () => {
    let component: HomePage;
    let fixture: ComponentFixture<HomePage>;

    ConsultationMock.waiting.calendar.availableDate = '23/05/2023';
    ConsultationMock.payed.calendar.availableDate = '23/05/2023';

    const nextConsultations = [
        ConsultationMock.waiting,
        ConsultationMock.payed
    ];

    consultationServiceMock.getPatientSchedule.mockReturnValue(
        of({
            content: [
                ConsultationMock.terminated,
                ConsultationMock.canceled,
                ...nextConsultations
            ]
        })
    );

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [HomePage],
            imports: [
                IonicModule.forRoot(),
                FormsModule,
                ReactiveFormsModule,
                HttpClientTestingModule
            ],
            providers: [
                { provide: UserService, useValue: userServiceMock },
                {
                    provide: StockageService,
                    useValue: { init: () => Promise.resolve() }
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: ConsultationsService,
                    useValue: consultationServiceMock
                },
                {
                    provide: CommonService,
                    useValue: commonServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(HomePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.doctorlist = { list: [] } as any;
    }));

    it('should obtain all patient consultations when ionViewWillEnter', async () => {
        jest.useFakeTimers().setSystemTime(new Date('2023-04-23T16:00:00'));
        await component.ionViewWillEnter();
        expect(component.list).toEqual(
            nextConsultations.map(normalizeConsultation)
        );
    });

    it('should return all activities when ionViewWillEnter', () => {
        expect(component.domains).toEqual(ActivityMock);
    });

    it('should get available doctor when ionViewWillEnter', () => {
        component.ionViewWillEnter();
        expect(component.search).toEqual({
            item: '',
            location: { name: '', longitude: '0', latitude: '0' },
            size: 3
        });
    });

    it('should notify user when an error occurs during activity retrieval', fakeAsync(() => {
        commonServiceMock.getActivities.mockReturnValue(
            throwError({ error: { status: 404 } })
        );
        component.ionViewWillEnter();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should notify user when an error occurs while fetching patient schedule', fakeAsync(() => {
        consultationServiceMock.getPatientSchedule.mockReturnValue(
            throwError({ error: { status: 404 } })
        );
        component.getHistoryService();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));
});
