import { Component, ViewChild } from '@angular/core';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { NotifyService } from '@idermato/src/app/services/common/notify.service';
import { helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { finalize } from 'rxjs/operators';
import { DoctorlistComponent } from 'src/app/components/doctorlist/doctorlist.component';
import { Consultation, Criteria, Domain } from 'src/app/models';
import { CommonService } from 'src/app/services/common.service';
import { StockageService } from 'src/app/services/stockage.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { normalizeConsultation } from 'src/app/utils/normalizer';
import { ConsultationsService } from '../../../app/services/consultations.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage {
    list: Consultation[] = [];
    slideOpts: {
        initialSlide: number;
        speed: number;
        slidesPerView: number;
        autoplay: boolean;
        effect: string;
    } = {
        initialSlide: 0,
        speed: 1000,
        slidesPerView: 1,
        autoplay: true,
        effect: 'fade'
    };
    domains: Domain[];
    search: Criteria;
    messages: Record<string, string> = messages;
    showDoctors: boolean = false;

    currentUrl: string;
    @ViewChild(DoctorlistComponent) doctorlist: DoctorlistComponent;

    constructor(
        private readonly userService: UserService,
        private readonly stockage: StockageService,
        private readonly commonService: CommonService,
        private readonly consultationService: ConsultationsService,
        private readonly loadingService: LoadingService,
        private readonly notifyService: NotifyService
    ) {}

    async ionViewWillEnter(): Promise<void> {
        this.stockage.init();
        this.search = {
            item: '',
            location: { name: '', longitude: '0', latitude: '0' },
            size: 3
        };
        // get user id
        this.detailsfetch();
    }

    /**
     * Fetches user details, including consultation history and activity
     */
    async detailsfetch(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);
        await this.getHistoryService();
        this.getActivity();
        this.showDoctors = this.doctorlist.list.length > 0;
    }

    /**
     * Retrieves the patient's consultation history
     */
    async getHistoryService(): Promise<void> {
        this.consultationService
            .getPatientSchedule(this.userService.user.id)
            .subscribe({
                next: (res: { content: any }) => {
                    // Normalize and store all consultations
                    this.consultationService.list = res.content.map(
                        normalizeConsultation
                    );
                    const hiddenStatus: string[] = [
                        ConsultationStatus.annuled,
                        ConsultationStatus.terminated
                    ];

                    // Filter and sort the list of consultations
                    this.list = this.consultationService.list
                        .filter(
                            (booking) =>
                                !hiddenStatus.includes(booking.status) &&
                                new Date().getTime() <=
                                    new Date(
                                        booking.calendar.availableDate
                                    ).getTime()
                        )
                        .sort(
                            (a, b) =>
                                new Date(a.calendar.availableDate).getTime() -
                                new Date(b.calendar.availableDate).getTime()
                        );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Retrieves user activity data
     */
    async getActivity(): Promise<void> {
        this.commonService
            .getActivities()
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (data: { content: Domain[] }) => {
                    this.domains = this.commonService.domains = data.content;
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }
}
