import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock } from '@lib/mocks';
import { commonServiceMock, SpecialityMock } from 'src/app/mock';
import { CommonService } from 'src/app/services/common.service';
import { SpecialitiesPage } from './specialities.page';

describe('SpecialitiesPage', () => {
    let component: SpecialitiesPage;
    let fixture: ComponentFixture<SpecialitiesPage>;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SpecialitiesPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: CommonService,
                    useValue: commonServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                }
            ],
            imports: [
                IonicModule.forRoot(),
                HttpClientTestingModule,
                RouterTestingModule
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(SpecialitiesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should filter items based on criteria', () => {
        commonServiceMock.domains = [
            {
                ...SpecialityMock,
                fonctionName: 'Pneumologie'
            },
            {
                ...SpecialityMock,
                fonctionName: 'Ophtamologie'
            }
        ];
        const payload = { item: 'pne' };
        component.getCriteria(payload);
        expect(component.filteredItems).toEqual([commonServiceMock.domains[0]]);
    });

    it('should return all specialities when item is empty', () => {
        const payload = { item: '' };
        component.getCriteria(payload);
        expect(component.filteredItems).toEqual(commonServiceMock.domains);
    });

    it('should track specialities by id', () => {
        expect(component.trackSpecialities(0, component.filteredItems[0])).toBe(
            component.filteredItems[0].id
        );
    });
});
