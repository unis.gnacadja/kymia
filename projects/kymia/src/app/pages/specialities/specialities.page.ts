import { Component,OnInit } from '@angular/core';
import { LoadingService } from '@idermato/src/app/services/common/loading.service';
import { cardMedia,helperMessages } from '@lib/constants';
import { finalize } from 'rxjs/operators';
import { Criteria } from 'src/app/models';
import { Domain } from 'src/app/models/domain.model';
import { CommonService } from 'src/app/services/common.service';

@Component({
    selector: 'app-specialities',
    templateUrl: './specialities.page.html',
    styleUrls: ['./specialities.page.scss']
})
export class SpecialitiesPage implements OnInit {
    filteredItems: Domain[];
    searchCriteria: Criteria;
    cardMedia = cardMedia;

    constructor(
        private commonService: CommonService,
        private loadingService: LoadingService
    ) {}

    async ngOnInit(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);
        this.commonService
            .getActivities()
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe((data: { content: Domain[] }) => {
                this.filteredItems = this.commonService.domains = data.content;
            });
    }

    /**
     * Filter items based on criteria
     *
     * @param payload
     */
    getCriteria(payload: Criteria) {
        if (payload.item.length) {
            this.filteredItems = this.commonService.domains.filter((item) =>
                item.fonctionName
                    .toLowerCase()
                    .includes(payload.item.toLowerCase())
            );
        } else {
            this.filteredItems = this.commonService.domains;
        }
    }

    /**
     * Tracking specialities by id
     *
     * @param _index
     * @param item
     * @returns domain id
     */
    trackSpecialities(_index: number, item: Domain) {
        return item.id;
    }
}
