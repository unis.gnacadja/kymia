import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SpecialitiesPageRoutingModule } from './specialities-routing.module';
import { SpecialitiesPage } from './specialities.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SpecialitiesPageRoutingModule,
        Ng2SearchPipeModule,
        SharedModule
    ],
    declarations: [SpecialitiesPage],
    entryComponents: [],
    providers: []
})
export class SpecialitiesPageModule {}
