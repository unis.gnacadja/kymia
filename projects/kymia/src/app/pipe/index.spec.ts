import { ConsultationStatus } from '@lib/enum';
import { environment } from 'src/environments/environment';
import {
    DefaultPicturePipe,
    FilterBy,
    LabelPluralPipe,
    NextDate,
    TimeFormat
} from '.';
import { consultationList, consultationServiceMock } from '../mock';

describe('Pipes', () => {
    it('should return default avatar URL when no value is provided', () => {
        const defaultPicturePipe = new DefaultPicturePipe();
        expect(defaultPicturePipe.transform('')).toBe(environment.user.avatar);
    });

    describe('LabelPluralPipe', () => {
        let labelPluralPipe: LabelPluralPipe;

        beforeAll(() => {
            labelPluralPipe = new LabelPluralPipe();
        });

        it('should add "s" to the label if the number is greater than 1', () => {
            expect(labelPluralPipe.transform(2, 'item')).toBe('2 items');
        });

        it('should not add "s" to the label if the number is 1', () => {
            expect(labelPluralPipe.transform(1, 'item')).toBe('1 item');
        });

        it('should add zero to the label if the number is undefined', () => {
            expect(labelPluralPipe.transform(undefined, 'item')).toBe('0 item');
        });
    });

    it('should transform a date correctly to "d MMM" format', () => {
        const nextDate = new NextDate();
        expect(nextDate.transform('25-12-2024', '-')).toBe('25 Dec');
    });

    it('should format a Date object to "HH:mm"', () => {
        const timeFormat = new TimeFormat();
        const date = new Date('2024-12-25T14:30:00');

        expect(timeFormat.transform(date)).toBe('14:30');
    });

    it('should filter consultations by a specified property', () => {
        const filterBy = new FilterBy();

        expect(
            filterBy.transform(consultationServiceMock.list, 'status', [
                ConsultationStatus.waiting
            ])
        ).toEqual([consultationList.waiting]);
    });
});
