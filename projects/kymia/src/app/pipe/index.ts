import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { Consultation } from '../models';

@Pipe({ name: 'DefaultPicture' })
export class DefaultPicturePipe implements PipeTransform {
    transform(imgUrl: string): string {
        if (!imgUrl) {
            return 'https://ionicframework.com/docs/img/demos/avatar.svg';
        }
    }
}

@Pipe({
    name: 'LabelPlural',
    pure: true
})
export class LabelPluralPipe implements PipeTransform {
    transform(nb: number, label: string): string {
        if (nb > 1) {
            label += 's';
        }
        return (nb ?? 0) + ' ' + label;
    }
}
@Pipe({
    name: 'nextDate',
    pure: true
})
export class NextDate implements PipeTransform {
    transform(value: string, separator: string): string {
        const date = value.split(separator);
        const datePipe = new DatePipe('en-US');
        return datePipe.transform(
            new Date(date[1] + separator + date[0] + separator + date[2]),
            'd MMM'
        );
    }
}

@Pipe({
    name: 'timeFormat',
    pure: true
})
export class TimeFormat implements PipeTransform {
    transform(value: Date): string {
        const hours = ('0' + value.getHours()).slice(-2);
        const minutes = ('0' + value.getMinutes()).slice(-2);
        return `${hours}:${minutes}`;
    }
}

@Pipe({
    name: 'filterBy'
})
export class FilterBy implements PipeTransform {
    transform(
        list: Consultation[],
        property: string,
        values: string[]
    ): Consultation[] {
        return list.filter((consultation) =>
            values.includes(consultation[property])
        );
    }
}
