import { TestBed } from '@angular/core/testing';
import { ActivityMock } from '../mock';
import { ApprovalService } from './approval.service';

describe('ApprovalService', () => {
    let service: ApprovalService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ApprovalService);
    });

    it('should update the approval stage message when updateApprovalMessage is called', (done) => {
        service.updateApprovalMessage(ActivityMock[0].id, ActivityMock[0].name);

        service.currentApprovalStageMessage
            .pipe()
            .subscribe((updatedMessage) => {
                expect(updatedMessage).toEqual({
                    id: ActivityMock[0].id,
                    domainName: ActivityMock[0].name
                });
                done();
            });
    });
});
