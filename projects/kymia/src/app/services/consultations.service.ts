import { HttpClient } from '@angular/common/http';
import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Consultation } from '../models';

@Injectable()
export class ConsultationsService {
    api = environment.baseUrl;
    list: Consultation[] = [];

    constructor(
        public http: HttpClient,

        @Optional() @SkipSelf() sharedService?: ConsultationsService
    ) {
        if (sharedService) {
            throw new Error('AuthenticationService is already loaded');
        }
    }

    registerConsultation(files): Observable<unknown> {
        return this.http.post(`${this.api}/consultations`, files);
    }

    archiveSchedule(request): Observable<unknown> {
        return this.http.put(`${this.api}/consultations/archive`, request);
    }

    getPatientSchedule(id: string): Observable<unknown> {
        return this.http.get(`${this.api}/consultations/patient/${id}`);
    }

    cancelSchedule(
        consultationId: string,
        raison: string
    ): Observable<unknown> {
        return this.http.put(
            `${this.api}/consultations/status/annule/` + consultationId,
            {
                innitiateur: 'PATIENT',
                raison
            }
        );
    }

    setScheduleComments(
        consultationId: string,

        terminateConsultationDto
    ): Observable<unknown> {
        return this.http.put(
            `${this.api}/consultations/review/` + consultationId,

            terminateConsultationDto
        );
    }

    registerSchedulePaiement(id: string, files): Observable<unknown> {
        return this.http.put(
            `${this.api}/consultations/status/payed/` + id,

            files
        );
    }

    getConsultation(consultationId: string): Observable<unknown> {
        return this.http.get(`${this.api}/consultations/` + consultationId);
    }
}
