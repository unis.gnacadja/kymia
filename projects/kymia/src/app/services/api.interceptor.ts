import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    token: string = '';
    constructor(private router: Router, private user: UserService) {}

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(this.addAuthToken(request)).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error && error.status === 401) {
                    this.router.navigate(['/login']);
                }
                return throwError(error);
            })
        );
    }

    addAuthToken(request: HttpRequest<any>) {
        const token = this.user.getAuthToken();

        if (!token) {
            return request;
        }

        return request.clone({
            setHeaders: {
                authorization: `Bearer ${token}`
            }
        });
    }
}
