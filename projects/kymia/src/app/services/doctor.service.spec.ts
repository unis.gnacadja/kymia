import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '../../environments/environment';
import { Dates, DoctorMock } from '../mock';
import { DoctorService } from './doctor.service';

describe('DoctorService', () => {
    let service: DoctorService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DoctorService]
        });

        service = TestBed.inject(DoctorService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should get the doctor informations', () => {
        service.getDoctor(DoctorMock.id).subscribe((response) => {
            expect(response).toEqual(DoctorMock);
        });

        const req = httpMock.expectOne(
            `${environment.baseUrl}/doctors/${DoctorMock.id}`
        );
        req.flush(DoctorMock);
    });

    it('should get all the doctor availabilities', () => {
        service.getDate(DoctorMock.id).subscribe((response) => {
            expect(response).toEqual(Dates);
        });

        const req = httpMock.expectOne(
            `${environment.baseUrl}/calendars/doctor/group/${DoctorMock.id}`
        );
        req.flush(Dates);
    });

    it('should search doctors by criteria', () => {
        service.search({ item: DoctorMock.name }).subscribe((response) => {
            expect(response).toEqual(DoctorMock);
        });

        const req = httpMock.expectOne(`${environment.baseUrl}/doctors/search`);
        req.flush(DoctorMock);
    });

    it('should get the doctor availabilities by year and month', () => {
        const year = 2024;
        const month = 8;

        service
            .getAvailableDates(DoctorMock.id, year, month)
            .subscribe((response) => {
                expect(response).toEqual(Dates);
            });

        const req = httpMock.expectOne(
            `${environment.baseUrl}/calendars/doctor/group/${DoctorMock.id}?year=${year}&month=${month}`
        );
        req.flush(Dates);
    });
});
