import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ActivityMock, PlaceMock } from '../mock';
import { CommonService } from './common.service';

describe('CommonService', () => {
    let service: CommonService;
    let mockHandler: HttpHandler;
    const REQUEST: HttpClient = new HttpClient(mockHandler);
    const mockService: CommonService = new CommonService(REQUEST);

    class CommonServiceStub {
        success: boolean;

        constructor(success: boolean) {
            this.success = success;
        }
        boolean;

        getActivities() {
            mockService.getActivities();
            if (this.success) {
                return of(ActivityMock);
            } else {
                throw new Error('Error');
            }
        }

        searchActivity(term: string) {
            mockService.searchActivity(term);
            if (this.success) {
                if (term == ActivityMock[0].name) {
                    return of(ActivityMock[0]);
                } else {
                    return of([]);
                }
            } else {
                throw new Error('Error');
            }
        }

        reverseLocationMap(position: { longitude: number; latitude: number }) {
            mockService.reverseLocationMap(position);
            if (this.success) {
                return of(PlaceMock);
            } else {
                throw new Error('Error');
            }
        }
    }

    let serviceSuccess: CommonServiceStub = new CommonServiceStub(true);
    let serviceError: CommonServiceStub = new CommonServiceStub(false);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                {
                    provide: CommonService,
                    useClass: CommonServiceStub,
                    useValue: serviceSuccess
                }
            ]
        });
        service = TestBed.inject(CommonService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('activities', () => {
        it('should return all activities', () => {
            service.getActivities().subscribe((data) => {
                expect(data).toEqual(ActivityMock);
            });
        });

        it('should failed to return all activities', () => {
            try {
                serviceError.getActivities();
            } catch {
                expect(service.getActivities).toThrowError();
            }
        });

        it('should return searched activities', () => {
            service.searchActivity(ActivityMock[0].name).subscribe((data) => {
                expect(data).toEqual([]);
            });
        });

        it('should failed to return searched activities', () => {
            try {
                serviceError.searchActivity(ActivityMock[0].name);
            } catch {
                expect(service.searchActivity).toThrowError();
            }
        });

        it('should return empty array', () => {
            service.searchActivity('test').subscribe((data) => {
                expect(data).toEqual([]);
            });
        });
    });

    describe('reverse location map', () => {
        it('should return reverse location map', () => {
            service
                .reverseLocationMap({
                    longitude: 1,
                    latitude: 1
                })
                .subscribe((data) => {
                    expect(data).toEqual([]);
                });
        });

        it('should failed to return reverse location map', () => {
            try {
                serviceError
                    .reverseLocationMap({
                        longitude: 1,
                        latitude: 1
                    })
                    .subscribe((data) => {
                        expect(data).toEqual(PlaceMock);
                    });
            } catch {
                expect(service.reverseLocationMap).toThrowError();
            }
        });
    });
});
