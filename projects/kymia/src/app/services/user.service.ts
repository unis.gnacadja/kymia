import { HttpClient } from '@angular/common/http';
import { Injectable, Optional, SkipSelf } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/types/user';
import { environment } from 'src/environments/environment';
import { Location } from '../models';

@Injectable()
export class UserService {
    api = environment.baseUrl;

    isLogged = false;

    user: User;
    private userSubject = new BehaviorSubject<User>(null);
    user$ = this.userSubject.asObservable();
    token: string = '';
    location = new BehaviorSubject({ id: '', city: '', country: '' });

    constructor(
        public http: HttpClient,

        @Optional() @SkipSelf() sharedService?: UserService
    ) {
        if (sharedService) {
            throw new Error('AuthenticationService is already loaded');
        }
    }

    getUserInfo(): User {
        if (this.user && !this.user.photo) {
            this.user.photo = environment.user.avatar;
        }
        return this.user;
    }

    setUser(infos: User) {
        this.user = infos;
        this.userSubject.next(this.user);
    }
    login({
        email,

        password
    }: {
        email: string;

        password: string;
    }): Observable<unknown> {
        return this.http.post(`${this.api}/users/login`, { email, password });
    }

    updateLocation(item: Location) {
        this.location.next(item);
    }

    updatePhoto(newPhoto: string) {
        this.user.photo = newPhoto;
    }

    getAuthToken() {
        return this.token ?? 'NO_VALID_AUTH_ID';
    }

    resetPassword(email: string): Observable<unknown> {
        return this.http.put(
            `${this.api}/users/password/reset?mail=${encodeURI(email)}`,

            {}
        );
    }

    newpwd(id: string, { oldPassword, newPassword }): Observable<unknown> {
        return this.http.put(`${this.api}/users/password/update/` + id, {
            newPassword,
            oldPassword
        });
    }

    registerUser(user): Observable<unknown> {
        return this.http.post(`${this.api}/patients`, user);
    }

    validation({
        email,

        code
    }: {
        email: string;

        code: string;
    }): Observable<unknown> {
        return this.http.put(`${this.api}/users/signup/validation`, {
            email,

            code
        });
    }

    resend(email: string): Observable<unknown> {
        return this.http.put(`${this.api}/users/validation/resend`, { email });
    }

    getPatient(patientId: string): Observable<unknown> {
        return this.http.get(`${this.api}/patients/` + patientId);
    }

    // update user

    updateProfilUser(id: string, user): Observable<unknown> {
        return this.http.put(`${this.api}/patients/` + id, user);
    }

    //update user profil photo

    updateUserPhoto(id: string, file: any): Observable<unknown> {
        return this.http.put(`${this.api}/users/photo/` + id, file);
    }

    //delete user profil photo

    deleteUserPhoto(id: string): Observable<unknown> {
        return this.http.delete(`${this.api}/users/photo/` + id);
    }

    logOut(): void {
        this.isLogged = false;
        this.token = null;
    }
}
