import { TestBed } from '@angular/core/testing';
import { Storage } from '@ionic/storage-angular';
import { environment } from 'src/environments/environment';
import { StockageService } from './stockage.service';
describe('StockageService', () => {
    let service: StockageService;
    let storageSpy: Storage = new Storage();

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: Storage,
                    useValue: storageSpy
                }
            ]
        });
        service = TestBed.inject(StockageService);
    });

    describe('when the store does not exist', () => {
        it('should return undefined when trying to store a value.', async () => {
            expect(service.set(environment.introKey, true)).toBeUndefined();
        });

        it('should return undefined when trying to retrieve a key.', async () => {
            expect(service.get(environment.introKey)).toBeUndefined();
        });
    });

    describe('when the store exist', () => {
        it('should retrieve stored value with key.', async () => {
            await service.init();
            let key = environment.introKey;
            let value = true;
            service.set(key, value);
            const receivedValue = await service.get(key);
            expect(receivedValue).toBe(value);
        });

        it('should not retrieve a value for a non-existent key.', async () => {
            let key = 'firstname';
            service.get(key);
            const result = await storageSpy.keys();
            expect(result.includes(key)).toBe(false);
        });
    });
});
