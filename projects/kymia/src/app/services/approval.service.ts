import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApprovalService {
    public approvalStageMessage = new BehaviorSubject({
        id: '',
        domainName: ''
    });
    currentApprovalStageMessage = this.approvalStageMessage.asObservable();

    updateApprovalMessage(id: string, domainName: string) {
        this.approvalStageMessage.next({ id, domainName });
    }
}
