import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Doctor } from '../models';

@Injectable()
export class DoctorService {
    api = environment.baseUrl;
    currentDoctorId: string;

    list: Doctor[] = [];
    constructor(public http: HttpClient) {}

    getDoctor(id: string): Observable<unknown> {
        this.currentDoctorId = id;
        return this.http.get(`${this.api}/doctors/` + id);
    }

    getDate(id: string): Observable<unknown> {
        return this.http.get(`${this.api}/calendars/doctor/group/` + id);
    }

    search(criteria: any): Observable<unknown> {
        return this.http.post(`${this.api}/doctors/search`, criteria);
    }

    /**
     * Get doctor availabilities
     *
     * @param doctorId: doctor id
     * @param year: year numbern
     * @param month: month number
     */
    getAvailableDates(
        doctorId: string,
        year: number,
        month: number
    ): Observable<unknown> {
        return this.http.get(
            `${this.api}/calendars/doctor/group/${doctorId}?year=${year}&month=${month}`
        );
    }
}
