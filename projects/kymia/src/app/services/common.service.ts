import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Domain } from '../models';

@Injectable({
    providedIn: 'root'
})
export class CommonService {
    api = environment.baseUrl;
    openStreetapi = environment.openStreetapi;
    domains: Domain[] = [];

    constructor(public http: HttpClient) {}

    getActivities(): Observable<unknown> {
        return this.http.get(`${this.api}/domains`);
    }

    searchActivity(term: string): Observable<unknown> {
        return this.http.get(`${this.api}/domains/functions/${term}`);
    }

    reverseLocationMap(position: {
        longitude: number;
        latitude: number;
    }): Observable<any> {
        return this.http.get(
            `${this.openStreetapi}/reverse?format=json&lat=${position.latitude}&lon=${position.longitude}`
        );
    }
}
