import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { OrderBy } from 'src/app/utils/constants';
import { ConsultationMock, MockUser } from '../mock';
import { ConsultationsService } from './consultations.service';

let mockHandler: HttpHandler;
const REQUEST: HttpClient = new HttpClient(mockHandler);
const DEFAULT_CONSULTATION_SERVICE: ConsultationsService =
    new ConsultationsService(REQUEST);

const NO_FILES_FOUND =
    'Vous devez joindre des images descriptives lors de la prise de rendez-vous pour une consultation dermatologique';
const CAN_NOT_ARCHIVE =
    'Seules les consultations terminées ou annulées peuvent être archivées.';
const CAN_NOT_GET_PATIENT = 'Connexion requise';
const CAN_NOT_CANCEL_MEETING =
    'La consultation ne peut pas être annulé moins de 48 heures avant';
const TERMINATED_CONSULTATION_ONLY =
    'Seules les consultations terminées peuvent être évaluée';
const INCORRECT_STATUS = 'La consultation doit être en attente de payement.';
const INCORRECT_CONSULTATION_ID = "L'id de la consultation est incorrecte.";
class MockConsultationsService {
    success: boolean;

    constructor(success: boolean) {
        this.success = success;
    }

    registerConsultation(formData) {
        DEFAULT_CONSULTATION_SERVICE.registerConsultation(formData);
        if (this.success) {
            return of(ConsultationMock.waiting);
        } else {
            throw new Error(NO_FILES_FOUND);
        }
    }

    archiveSchedule(id) {
        DEFAULT_CONSULTATION_SERVICE.archiveSchedule(id);
        if (this.success) {
            return of({
                message: 'Archivage effectué avec succès'
            });
        } else {
            throw new Error(CAN_NOT_ARCHIVE);
        }
    }

    getPatientSchedule(id: string, orderBy?: string) {
        DEFAULT_CONSULTATION_SERVICE.getPatientSchedule(id);
        if (this.success) {
            return of({
                content: [
                    ConsultationMock.canceled,
                    ConsultationMock.payed,
                    ConsultationMock.terminated,
                    ConsultationMock.waiting
                ]
            });
        } else {
            throw new Error(CAN_NOT_GET_PATIENT);
        }
    }

    cancelSchedule(consultationId: string, raison: string) {
        DEFAULT_CONSULTATION_SERVICE.cancelSchedule(consultationId, raison);
        if (this.success) {
            return of(ConsultationMock.canceled);
        } else {
            throw new Error(CAN_NOT_CANCEL_MEETING);
        }
    }

    setScheduleComments(
        consultationId: string,
        terminateConsultationDto: {
            comment: string;
            note: number;
        }
    ) {
        DEFAULT_CONSULTATION_SERVICE.setScheduleComments(
            consultationId,
            terminateConsultationDto
        );
        if (this.success) {
            return of(ConsultationMock.terminated);
        } else {
            throw new Error(TERMINATED_CONSULTATION_ONLY);
        }
    }

    registerSchedulePaiement(id: string, files) {
        DEFAULT_CONSULTATION_SERVICE.registerSchedulePaiement(id, files);
        if (this.success) {
            return of(ConsultationMock.payed);
        } else {
            throw new Error(INCORRECT_STATUS);
        }
    }

    getConsultation(id: string) {
        DEFAULT_CONSULTATION_SERVICE.getConsultation(id);
        if (this.success) {
            return of(ConsultationMock.payed);
        } else {
            throw new Error(INCORRECT_CONSULTATION_ID);
        }
    }
}

describe('KYMIA#8669eu3gv : test unitaire pour le service consultations', () => {
    let service: ConsultationsService;
    const PATIENT_ID = MockUser.idOnly.id;
    const mockJpg = new File(['1234'], 'test.jpg', { type: 'image/jpeg' });
    const mockPng = new File(['1234'], 'test.png', { type: 'image/png' });
    const mockFileList = [mockJpg, mockPng];

    let serviceSuccess: MockConsultationsService = new MockConsultationsService(
        true
    );
    let serviceError: MockConsultationsService = new MockConsultationsService(
        false
    );

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                {
                    provide: ConsultationsService,
                    useValue: serviceSuccess
                }
            ]
        });
        service = TestBed.inject(ConsultationsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('TA1: should display authenticationService is already loaded', () => {
        try {
            new ConsultationsService(REQUEST, DEFAULT_CONSULTATION_SERVICE);
        } catch (e) {
            expect(e.message).toBe('AuthenticationService is already loaded');
        }
    });

    describe('TA2: Register a consultation', () => {
        it('TA2-1: should register a new consultation ', fakeAsync(() => {
            const formData = new FormData();
            let i = 1;
            mockFileList.forEach((file) => {
                formData.append('file_' + i, file, file.name);
                i++;
            });
            formData.append(
                'consultationJson',
                JSON.stringify({
                    calendarId: '015771f0-bbe4-4b4e-bebc-05527e0dea0b',
                    patientId: MockUser.idOnly,
                    problemDescription: 'Consultation de suivi'
                })
            );
            serviceSuccess.registerConsultation(formData).subscribe((value) => {
                expect(value).toBe(ConsultationMock.waiting);
            });
        }));

        it('TA2-2: should not register a dermatology consultation without files', fakeAsync(() => {
            const formData = new FormData();
            formData.append(
                'consultationJson',
                JSON.stringify({
                    calendarId: '015771f0-bbe4-4b4e-bebc-05527e0dea0b',
                    patientId: MockUser.idOnly,
                    problemDescription: 'Consultation de suivi'
                })
            );
            try {
                serviceError.registerConsultation(formData);
            } catch (e) {
                expect(e.message).toBe(NO_FILES_FOUND);
            }
        }));
    });

    describe('TA3: Archive a consultation ', () => {
        it('TA3-1: should archive a consultation', () => {
            serviceSuccess
                .archiveSchedule(ConsultationMock.canceled.id)
                .subscribe((res: { message: string }) => {
                    expect(res.message).toBe('Archivage effectué avec succès');
                });
        });

        it('TA3-2: should not archive a consultation with invalid id', () => {
            try {
                serviceError.archiveSchedule('asqwq-');
            } catch (e) {
                expect(e.message).toBe(CAN_NOT_ARCHIVE);
            }
        });
    });
    describe('TA4: List of patient consultations', () => {
        it('TA4-1: should return list of patient scheldule ordered by the given term', () => {
            serviceSuccess
                .getPatientSchedule(PATIENT_ID, OrderBy.asc)
                .subscribe((res) => {
                    expect(res).toEqual({
                        content: [
                            ConsultationMock.canceled,
                            ConsultationMock.payed,
                            ConsultationMock.terminated,
                            ConsultationMock.waiting
                        ]
                    });
                });
        });

        it('TA4-2: should return list of patient scheldule ordered by the given term', () => {
            try {
                serviceError.getPatientSchedule(
                    '454541ljnc bjkvq',
                    OrderBy.asc
                );
            } catch (e) {
                expect(e.message).toBe(CAN_NOT_GET_PATIENT);
            }
        });
    });
    describe('TA5: Cancel a consultation ', () => {
        it('TA5-1: should cancel a consultation', () => {
            serviceSuccess
                .cancelSchedule(
                    ConsultationMock.waiting.id,
                    'Urgence familiale'
                )
                .subscribe((res) => {
                    expect(res).toEqual(ConsultationMock.canceled);
                });
        });

        it('TA5-2: should not cancel a consultation', () => {
            let consultationToCancel = ConsultationMock.waiting;
            consultationToCancel.calendar.availableDate = '13/01/1998';
            try {
                serviceError.cancelSchedule(
                    ConsultationMock.waiting.id,
                    'Urgence familiale'
                );
            } catch (e) {
                expect(e.message).toBe(CAN_NOT_CANCEL_MEETING);
            }
        });
    });
    describe('TA6: Give a review to a consultation ', () => {
        it('TA6-1: should give a review to a consultation', () => {
            const terminateConsultationDto = {
                comment:
                    "La consultation s'est bien passé, le docteur a été très courtois.",
                note: 5
            };
            serviceSuccess
                .setScheduleComments(
                    ConsultationMock.terminated.id,
                    terminateConsultationDto
                )
                .subscribe((res) => {
                    expect(res).toEqual(ConsultationMock.terminated);
                });
        });

        it('TA6-2: should not add a review to a consultation', () => {
            const terminateConsultationDto = {
                comment:
                    "La consultation s'est bien passé, le docteur a été très courtois.",
                note: 5
            };
            try {
                serviceError.setScheduleComments(
                    ConsultationMock.waiting.id,
                    terminateConsultationDto
                );
            } catch (e) {
                expect(e.message).toBe(TERMINATED_CONSULTATION_ONLY);
            }
        });
    });
    describe('TA7: Confirm consultation payment ', () => {
        it("TA7-1: should confirm a consultation's payment", () => {
            serviceSuccess
                .registerSchedulePaiement(ConsultationMock.waiting.id, [])
                .subscribe((res) => {
                    expect(res).toEqual(ConsultationMock.payed);
                });
        });

        it("TA7-2: should not confirm a consultation's payment", () => {
            try {
                serviceError.registerSchedulePaiement(
                    ConsultationMock.canceled.id,
                    []
                );
            } catch (e) {
                expect(e.message).toBe(INCORRECT_STATUS);
            }
        });
    });
    describe('TA8: Find consultation', () => {
        it('TA7-1: should find a consultation', () => {
            serviceSuccess
                .getConsultation(ConsultationMock.payed.id)
                .subscribe((res) => {
                    expect(res).toEqual(ConsultationMock.payed);
                });
        });

        it('TA7-2: should return an error when consultation id is icorrect', () => {
            try {
                serviceError.getConsultation('aa');
            } catch (e) {
                expect(e.message).toBe(INCORRECT_CONSULTATION_ID);
            }
        });
    });
});
