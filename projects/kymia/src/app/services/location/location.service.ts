import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LocationService {
    api = environment.standalone;

    constructor(public http: HttpClient) {}

    /**
     * Fetches a list of all countries.
     *
     * @returns Observable containing an array of countries from the API.
     */
    getCountries(): Observable<Injectable> {
        return this.http.get(`${this.api}/countries`);
    }

    /**
     * Fetches cities for a given country by its ID.
     *
     * @param countryId - The ID of the country.
     * @returns Observable containing the list of cities for the specified country.
     */
    searchCity(countryId: string): Observable<Injectable> {
        return this.http.get(`${this.api}/cities/country/${countryId}`);
    }

    /**
     * Fetches the list of cities where doctors are available.
     *
     * @returns Observable containing the list of available cities for doctors.
     */
    getAvailableCities(): Observable<Injectable> {
        return this.http.get(`${this.api}/countries/doctors/available`);
    }
}
