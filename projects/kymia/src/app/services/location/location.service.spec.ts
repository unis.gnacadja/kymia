import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { citiesMock, CountriesMock } from 'src/app/mock';
import { LocationService } from './location.service';

describe('LocationService', () => {
    let service: LocationService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [LocationService]
        });

        service = TestBed.inject(LocationService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should fetch countries', (done) => {
        service.getCountries().subscribe((countries) => {
            expect(countries).toEqual(CountriesMock);
            done();
        });

        const req = httpTestingController.expectOne(`${service.api}/countries`);
        expect(req.request.method).toBe('GET');
        req.flush(CountriesMock);
    });

    it('should fetch cities by country ID', (done) => {
        const countryId = '1';
        const mockCities = [
            { id: '1', name: 'City1' },
            { id: '2', name: 'City2' }
        ];

        service.searchCity(countryId).subscribe((cities) => {
            expect(cities).toEqual(mockCities);
            done();
        });

        const req = httpTestingController.expectOne(
            `${service.api}/cities/country/${countryId}`
        );
        expect(req.request.method).toBe('GET');
        req.flush(mockCities);
    });

    it('should fetch available cities', (done) => {
        service.getAvailableCities().subscribe((cities) => {
            expect(cities).toEqual(citiesMock);
            done();
        });

        const req = httpTestingController.expectOne(
            `${service.api}/countries/doctors/available`
        );
        expect(req.request.method).toBe('GET');
        req.flush(citiesMock);
    });
});
