import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
    providedIn: 'root'
})
export class StockageService {
    private store: Storage;
    constructor(private storage: Storage) {}
    async init(): Promise<void> {
        const storage = await this.storage.create();
        this.store = storage;
    }
    public set(key: string, value: any): void {
        this.store?.set(key, value);
    }
    public get(name: string) {
        return this.store?.get(name);
    }
}
