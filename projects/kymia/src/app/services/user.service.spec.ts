import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DoctorMock, MockUser } from '../mock';
import { Location } from '../models';
import { UserService } from './user.service';

describe('UserService', () => {
    let service: UserService;
    const response = {
        status: 200
    };

    beforeAll(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [UserService]
        });
        service = TestBed.inject(UserService);
        service.user = MockUser.completeProfile;
        service.http.get = jest
            .fn()
            .mockReturnValue(of(MockUser.completeProfile));
        service.http.post = jest
            .fn()
            .mockReturnValue(of(MockUser.completeProfile));
        service.http.put = jest.fn().mockReturnValue(of(response));
        service.http.delete = jest.fn().mockReturnValue(of(response));
    });

    it('should throw an error if a sharedService is provided', () => {
        let mockHttpClient: HttpClient = {
            get: jest.fn()
        } as any as HttpClient;

        const sharedService = new UserService(mockHttpClient);

        expect(() => {
            const newService = new UserService(mockHttpClient, sharedService);
            newService.user = MockUser.completeProfile;
        }).toThrowError('AuthenticationService is already loaded');
    });

    // Service API calls

    it('should log in user', () => {
        service
            .login({
                email: MockUser.completeProfile.email,
                password: MockUser.completeProfile.password
            })
            .subscribe((res) => {
                expect(res).toEqual(MockUser.completeProfile);
            });
    });

    it('should reset user password', () => {
        service
            .resetPassword(MockUser.completeProfile.email)
            .subscribe((res) => {
                expect(res).toEqual(response);
            });
    });

    it('should update user password', () => {
        service
            .newpwd(service.user.id, {
                oldPassword: MockUser.completeProfile.password,
                newPassword: MockUser.completeProfile.password
            })
            .subscribe((res) => {
                expect(res).toEqual(response);
            });
    });

    it('should register user', () => {
        service.registerUser(MockUser.completeProfile).subscribe((res) => {
            expect(res).toEqual(MockUser.completeProfile);
        });
    });

    it('should validate user registration', () => {
        service
            .validation({
                email: MockUser.completeProfile.email,
                code: MockUser.completeProfile.validationPin
            })
            .subscribe((res) => {
                expect(res).toEqual(MockUser.completeProfile);
            });
    });

    it('should resend user validation email', () => {
        service.resend(MockUser.completeProfile.email).subscribe((res) => {
            expect(res).toEqual(response);
        });
    });

    it('should get patient', () => {
        service.getPatient(MockUser.completeProfile.id).subscribe((res) => {
            expect(res).toEqual(MockUser.completeProfile);
        });
    });

    it('should update user profile', () => {
        service
            .updateProfilUser(
                MockUser.completeProfile.id,
                MockUser.completeProfile
            )
            .subscribe((res) => {
                expect(res).toEqual(response);
            });
    });

    it('should update user photo', () => {
        service
            .updateUserPhoto(
                MockUser.completeProfile.id,
                MockUser.completeProfile.photo
            )
            .subscribe((res) => {
                expect(res).toEqual(response);
            });
    });

    it('should delete user photo', () => {
        service
            .deleteUserPhoto(MockUser.completeProfile.id)
            .subscribe((res) => {
                expect(res).toEqual(response);
            });
    });

    // Service methods

    describe('getUserInfo', () => {
        it('should return user info with default avatar when no photo is present', () => {
            service.setUser({
                ...MockUser.completeProfile,
                photo: ''
            });
            const userInfo = service.getUserInfo();
            expect(userInfo.photo).toBe(environment.user.avatar); // Assuming this is the default avatar
        });

        it('should return user info without changing photo if photo exists', () => {
            service.setUser(MockUser.completeProfile);
            const userInfo = service.getUserInfo();
            expect(userInfo.photo).toBe(MockUser.completeProfile.photo);
        });
    });

    it('should update location behavior subject', () => {
        const newLocation: Location = {
            id: DoctorMock.city.id,
            city: DoctorMock.city.name,
            country: DoctorMock.city.country.name
        };
        service.updateLocation(newLocation);
        service.location.subscribe((location) => {
            expect(location).toEqual(newLocation);
        });
    });

    it('should update user photo', () => {
        service.updatePhoto(MockUser.completeProfile.photo);
        expect(service.user.photo).toBe(MockUser.completeProfile.photo);
    });

    describe('getAuthToken', () => {
        it("should return 'NO_VALID_AUTH_ID' when the user is not logged in", () => {
            service.token = null;
            const token = service.getAuthToken();
            expect(token).toBe('NO_VALID_AUTH_ID');
        });

        it('should return the auth token when the user is logged in', () => {
            service.token = MockUser.completeProfile.token;
            const token = service.getAuthToken();
            expect(token).toBe(MockUser.completeProfile.token);
        });
    });

    it('should log out user', () => {
        service.logOut();
        expect(service.isLogged).toBe(false);
        expect(service.token).toBeNull();
    });
});
