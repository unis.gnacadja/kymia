export enum ConsultationStatus {
    terminated = 'TERMINATED',
    confirmByDoctor = 'CONFIRM_BY_DOCTOR',
    payed = 'PAYED',
    annuled = 'ANNULED',
    waiting = 'WAITING',
    waitingDiagnostic = 'WAITING_DIAGNOSTIC'
}

export enum Application {
    idermato = 'idermato',
    kymia = 'kymia'
}
