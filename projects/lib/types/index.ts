export type Media = {
    src: string;
    alt: string;
    title: string;
};
