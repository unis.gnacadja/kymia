import { BehaviorSubject, of } from 'rxjs';

export const loadingServiceMock = {
    present: jest.fn(),
    dismiss: jest.fn()
};

export const mockActivatedRoute = {
    queryParams: of({})
};

export const mockRouter = {
    navigate: jest.fn()
};

export const notifyServiceMock: {
    setPopover: jest.Mock;
    getPopover: jest.Mock;
    popover: BehaviorSubject<any>;
    presentToast: jest.Mock;
    closePopover: jest.Mock;
    closeEvent$: BehaviorSubject<any>;
} = {
    setPopover: jest.fn().mockReturnValue(of({})),
    getPopover: jest.fn().mockReturnValue(of({})),
    popover: new BehaviorSubject<any>({
        isOpen: false
    }),
    presentToast: jest.fn(),
    closePopover: jest.fn(),
    closeEvent$: new BehaviorSubject({})
};
