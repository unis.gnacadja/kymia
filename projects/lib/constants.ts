export const acceptedFileTypes =
    'application/pdf,image/png,image/jpeg,image/jpg,image/webp';
export const cardMedia = 'https://ionicframework.com/docs/img/demos/card-media.png';

export const helperMessages: Record<string, string> = {
    loading: 'chargement.....',
    errorOccurred: 'Une erreur est survenue, veuillez réessayer.',
    next: 'Prochain',
};
