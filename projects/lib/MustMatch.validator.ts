import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

const mustMatch =
    (controlName: string, matchingControlName: string) =>
    (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors['mustMatch']) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    };

const dataPattern = {
    email: /(?=[a-zA-Z])\w+@\w+([.-]?\w+)*(\.\w{2,})+$/,
    password: /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/,
    phone: /^[0-9]{6,16}$/,
    alphaNumeric: /^[a-zA-Z0-9]+$/,
    lettersOnly: /^[a-zA-Z_\-'À-ÖØ-öø-Ÿ\s]+$/,
    text: /^[a-zA-Z0-9' \-_\p{L}\p{M}]+$/u,
    noSpace: /^\s*\S.*$/,
    name: /^(?!\d+$)[a-zA-Z0-9' \-_]+$/,
    numbersOnly: /^[1-9]\d*$/
};

const rangeValidator: (min: number, max: number) => ValidatorFn =
    (min, max) => (control: AbstractControl) => {
        const value = control.value;
        if (value && (value.length < min || value.length > max)) {
            return {
                range: true,
                message: `Nombre de caractères requis ${min} à ${max}`
            };
        }
        return null;
    };

export { mustMatch, dataPattern, rangeValidator };
