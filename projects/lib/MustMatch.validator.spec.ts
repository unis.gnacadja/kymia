import { FormControl, FormGroup } from '@angular/forms';
import { mustMatch, rangeValidator } from './MustMatch.validator';

describe('Validators', () => {
    describe('mustMatch', () => {
        it('should set error when values do not match', () => {
            const formGroup = new FormGroup({
                password: new FormControl('password123'),
                confirmPassword: new FormControl('differentPassword')
            });

            const validator = mustMatch('password', 'confirmPassword');
            validator(formGroup); // Running the validator

            // Confirm that the 'mustMatch' error is set on confirmPassword
            expect(
                formGroup.controls['confirmPassword'].hasError('mustMatch')
            ).toBe(true);
        });

        it('should not set error when values match', () => {
            const formGroup = new FormGroup({
                password: new FormControl('password123'),
                confirmPassword: new FormControl('password123')
            });

            const validator = mustMatch('password', 'confirmPassword');
            validator(formGroup); // Running the validator

            // Confirm that no error is set on confirmPassword
            expect(
                formGroup.controls['confirmPassword'].hasError('mustMatch')
            ).toBe(false);
        });

        it('should not set error if there are existing errors on matchingControl', () => {
            const formGroup = new FormGroup({
                password: new FormControl('password123'),
                confirmPassword: new FormControl('differentPassword', null) // simulate other errors
            });

            formGroup.controls['confirmPassword'].setErrors({
                someOtherError: true
            });

            const validator = mustMatch('password', 'confirmPassword');
            validator(formGroup); // Running the validator

            // confirm that mustMatch error is not set, since there's already another error
            expect(
                formGroup.controls['confirmPassword'].hasError('mustMatch')
            ).toBe(false);
        });
    });

    describe('rangeValidator', () => {
        it('should return range error when value length is out of bounds', () => {
            const control = new FormControl('short');

            const validator = rangeValidator(6, 10);
            const result = validator(control);

            // Expect a range error with a specific message
            expect(result).toEqual({
                range: true,
                message: 'Nombre de caractères requis 6 à 10'
            });
        });

        it('should not return error when value length is within bounds', () => {
            const control = new FormControl('1234567890');

            const validator = rangeValidator(6, 10);
            const result = validator(control);

            // Expect no errors
            expect(result).toBeNull();
        });

        it('should return range error when value length is below the minimum', () => {
            const control = new FormControl('123');

            const validator = rangeValidator(6, 10);
            const result = validator(control);

            expect(result).toEqual({
                range: true,
                message: 'Nombre de caractères requis 6 à 10'
            });
        });
    });
});
