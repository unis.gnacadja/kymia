import * as DoctorMock from '@kymia/src/app/mock/doctor.mock.json';
import { environment } from '@kymia/src/environments/environment';
import { normalizeDoctor } from './normalizer';

describe('normalizeDoctor', () => {
    it('should normalize doctor data with default environment as doctor picture when doctor has no photo', () => {
        const normalizedData = normalizeDoctor({
            ...DoctorMock,
            photo: null
        });

        expect(normalizedData.photo).toBe(environment.user.avatar);
    });

    it('should get doctor photo from the doctor object', () => {
        const normalizedData = normalizeDoctor({
            id: DoctorMock.id,
            nom: DoctorMock.name,
            prenom: DoctorMock.firstname,
            email: DoctorMock.email,
            stats: {
                noteMoyenne: DoctorMock.noteMoyenne,
                nbrCommentaire: DoctorMock.nbrComments
            },
            domain: DoctorMock.domain,
            photo: DoctorMock.photo,
            statutAgent: DoctorMock.status
        });
        expect(normalizedData.photo).toBe(DoctorMock.photo);
    });
});
