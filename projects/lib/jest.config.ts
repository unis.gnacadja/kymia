/* eslint-disable @typescript-eslint/naming-convention */
import type { Config } from 'jest';

const config: Config = {
    displayName: 'Idermato',
    verbose: true,
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/../idermato/setup-jest.ts'],
    globalSetup: 'jest-preset-angular/global-setup',
    rootDir: './',
    roots: ['<rootDir>'],
    modulePaths: ['<rootDir>'],
    moduleNameMapper: {
        '^@idermato/(.*)$': '<rootDir>/../idermato/$1',
        '^@kymia/(.*)$': '<rootDir>/../kymia/$1'
    },
    collectCoverage: true,
    collectCoverageFrom: ['**/*.ts'],
    coveragePathIgnorePatterns: [
        'constants.ts',
        'jest.config.ts',
        'enum.ts',
        'mocks.ts'
    ],
    coverageDirectory: './coverage',
    coverageReporters: ['clover', 'json', 'lcov', ['text', { skipFull: true }]],
    reporters: [
        ['default', { outputDirectory: 'coverage', outputName: 'junit.xml' }]
    ],
    testResultsProcessor: 'jest-sonar-reporter'
};

export default config;
