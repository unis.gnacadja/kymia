import { TestBed } from '@angular/core/testing';
import { PhoneService } from './phone.service'; // Adjust the path as necessary

describe('PhoneService', () => {
    let service: PhoneService;

    beforeAll(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(PhoneService);
    });

    const phoneNumberTests = [
        { input: '+27215551234', expected: '215551234' },
        { input: '+2290167221580', expected: '0167221580' }
    ];

    // Test local phone number extraction
    phoneNumberTests.forEach(({ input, expected }) => {
        it(`should set local number to ${expected}`, () => {
            expect(service.getPhoneNumber(input)).toBe(expected);
        });
    });

    const countryCodeTests = [
        { code: 'EG', expected: '+20' },
        { code: 'CI', expected: '+225' }
    ];

    // Test setting phone country indicator
    countryCodeTests.forEach(({ code, expected }) => {
        it(`should set the phone indicator to ${expected}`, (done) => {
            service.setPhoneCountry(code);
            service.getSelectedPhone().subscribe(() => {
                expect(service.dialCode).toBe(expected);
                done();
            });
        });
    });
});
