import { Injectable } from '@angular/core';
import CountryList from 'country-list-with-dial-code-and-flag';
import { CountryInterface } from 'country-list-with-dial-code-and-flag/dist/types';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PhoneService {
    selectedPhoneCountry: BehaviorSubject<CountryInterface> =
        new BehaviorSubject<CountryInterface>(CountryList.getAll()[0]);
    dialCode: string = '';

    constructor() {}

    /**
     * Get phone number
     *
     * @param phone : phone number
     * @return phone number without phone indicator
     */
    getPhoneNumber(phone: string) {
        let phoneWithoutDialCode = phone;
        for (const country of CountryList.getAll()) {
            if (phone.startsWith(country.dialCode)) {
                this.dialCode = country.dialCode;
                this.selectedPhoneCountry.next(country);
                phoneWithoutDialCode = phone.slice(country.dialCode.length);
            }
        }
        return phoneWithoutDialCode;
    }

    /**
     * Retrieves an observable stream of selectedPhoneCountry properties
     *
     * @returns Observable stream emitting selectedPhoneCountry properties.
     */
    getSelectedPhone(): Observable<CountryInterface> {
        return this.selectedPhoneCountry.asObservable();
    }

    /**
     * Set phone country
     *
     * @param countryCode: dial code
     */
    setPhoneCountry(countryCode: string) {
        const selectedPhone = CountryList.findOneByCountryCode(countryCode);
        if (selectedPhone) {
            this.selectedPhoneCountry.next(selectedPhone);
            this.dialCode = selectedPhone.dialCode;
        }
    }
}
