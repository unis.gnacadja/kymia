import { Doctor } from '../kymia/src/app/models';
import { environment } from '../kymia/src/environments/environment';

/**
 * Normalizes the doctor data.
 *
 * @param doctor - The data to be normalized.
 * @returns The normalized Diary object.
 */
export const normalizeDoctor: (doctor: any) => Doctor = (doctor: any) => ({
    id: doctor.id,
    name: doctor.name ?? doctor.nom,
    firstname: doctor.firstname ?? doctor.prenom,
    email: doctor.email,
    rate: doctor.noteMoyenne ?? doctor.stats.noteMoyenne,
    commentCount: doctor.nbrComments ?? doctor.stats.nbrCommentaire,
    domain: {
        id: doctor.domain.id,
        name: doctor.domain.name,
        icon: doctor.domain.icon,
        description: doctor.domain.description,
        consultationPrice: doctor.domain.consultationPrice,
        fonctionName: doctor.domain.fonctionName
    },
    photo: doctor.photo ?? environment.user.avatar,
    ...(doctor.city && {
        city: {
            id: doctor.city.id,
            name: doctor.city.name,
            longitude: doctor.city.longitude,
            latitude: doctor.city.latitude,
            country: {
                id: doctor.city.country.id,
                name: doctor.city.country.name
            }
        }
    }),
    status: doctor.status ?? doctor.statutAgent,
    experienceYears: doctor.experienceYears,
    consultationPrice: doctor.consultationPrice,
    paymentNumbers: doctor.numeroPaiement,
    rib: doctor.rib,
    noteAndCommentResponses: doctor.noteAndCommentResponses,
    ...(doctor.nextCalendar && {
        nextCalendar: {
            id: doctor.nextCalendar.id,
            availableDate: doctor.nextCalendar.availableDate
                .split('/')
                .reverse()
                .join('/'),
            availableDateStart: doctor.nextCalendar.availableDateStart,
            availableDateEnd: doctor.nextCalendar.availableDateEnd,
            available: doctor.nextCalendar.available
        }
    }),
    phone: doctor.phone
});
