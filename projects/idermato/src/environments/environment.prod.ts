import { environment as kymiaEnvironment } from '@kymia/src/environments/environment.prod';
export const environment = {
    ...kymiaEnvironment,
    introKey: 'intro-idermato-has-seen',
    baseUrl: 'https://api.dev.dermato.v2.rintio.com',
    caseAvatar: '../../assets/case.svg'
};
