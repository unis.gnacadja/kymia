import { ValidatorFn, Validators } from '@angular/forms';
import { dataPattern } from '@lib/MustMatch.validator';
import { TypeError } from '../models/types/typeError';

export const validatorsHelper: Record<string, ValidatorFn[]> = {
    phone: [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern(dataPattern.phone)
    ] as ValidatorFn[],
    email: [
        Validators.required,
        Validators.pattern(dataPattern.email)
    ] as ValidatorFn[]
};

export const requiredField: TypeError = {
    type: 'required',
    description: 'Ce champ est requis'
};

export const invalidEmail: TypeError = {
    type: 'pattern',
    description: "L'email est invalide"
};

export const phoneMinError: TypeError = {
    type: 'minlength',
    description: 'Veuillez entrer au moins 8 chiffres'
};

export const phoneMaxError: TypeError = {
    type: 'maxlength',
    description: 'Ce champ doit avoir au maximum 16 chiffres'
};

export const nameMinError: TypeError = {
    type: 'minlength',
    description: 'Veuillez entrer au moins 2 lettres'
};

export const nameMaxError: TypeError = {
    type: 'maxlength',
    description: 'Veuillez entrer moins de 25 lettres'
};

export const phonePatternError: TypeError = {
    type: 'pattern',
    description: 'Le numéro de téléphone est incorrect'
};

export const lettersOnly: TypeError = {
    type: 'pattern',
    description: 'Ce champ doit comporter uniquement des lettres'
};

export const errorMessage: TypeError = {
    type: 'pattern',
    description: 'Ce champs comporte une erreur'
};

export const lettersNumberOnly: TypeError = {
    type: 'pattern',
    description: 'Ce champ doit comporter des chiffres et des lettres'
};

export const ribRange: TypeError = {
    type: 'range',
    description: 'Ce champ doit avoir au moins 20 et au plus 34 caractères'
};

export const noSpace: TypeError = {
    type: 'pattern',
    description: 'Ce champ est incorrect'
};

export const numbersOnly: TypeError = {
    type: 'pattern',
    description: 'Entrez un nombre superieur à 0'
};
export const errorMessages: Record<string, TypeError> = {
    required: requiredField,
    email: invalidEmail,
    phoneMin: phoneMinError,
    phoneMax: phoneMaxError,
    nameMin: nameMinError,
    nameMax: nameMaxError,
    phone: phonePatternError,
    lettersOnly,
    errorMessage,
    lettersNumberOnly,
    ribRange,
    noSpace,
    numbersOnly
};

export const statusMessages: Record<string, string> = {
    terminated: 'Terminée',
    payed: 'Payée',
    annuled: 'Annulée',
    confirmByDoctor: 'Confirmée',
    waiting: 'En attente de confirmation',
    waitingDiagnostic: 'En attente de diagnostic'
};

export const caseStatusMessage: Record<string, string> = {
    open: 'Ouvert',
    close: 'Fermé'
};

export const messages: Record<string, string> = {
    missingCalendar:
        "Vous devez définir l'agenda de vos consultations pour que vos patients puissent prendre rendez-vous.",
    mustRegisterCalendar:
        'Il semble que vous n’avez pas de rendez-vous prévu. Pensez à mettre à jour votre agenda',
    noCalendarAvailable: 'Vous n’avez aucun rendez-vous à venir',
    noResult: 'Aucun résultat',
    mustConfirmDiary:
        'Vous devez confirmer votre rendez-vous. En cas d’indisponibilité pensez à le transférer à un collègue.',
    diaryAvailable:
        'La téléconsultation sera active 15mn avant l’heure du rendez-vous.',
    checkPhoneEquipments:
        'Vérifiez la qualité audio et vidéo de votre équipement. Une meilleure qualité sonore favorise une bonne communication',
    reassurePatient:
        'Certains patients peuvent être sous stress. Pensez à les rassurer.',
    safeDiary:
        'Cet appel n’est pas enregistré et est chiffré de bout en bout. Il est opéré par notre partenaire zoom',
    askDiagnostic: 'Quel est votre diagnostic sur ce cas ?',
    askTreatment: 'Quel est votre traitement sur ce cas ?',
    askPrescription: 'Votre prescription ?',
    fileNotAccepted: 'Le format n’est pas accepté',
    next: 'Suivant',
    submit: 'Soumettre',
    noAppointment: 'Aucun rendez-vous',
    expectHours: 'Vous devez configurer vos horaires de consultation',
    aiDiagnosticError: 'Une erreur est survenue pendant le diagnostic AI',
    aiDiagnosticInProgress:
        'Un auto diagnostic IA est en cours.Les résultats seront disponibles',
    aiDiagnosticNone: 'Aucun diagnostic IA trouvé',
    commentPublished: 'Votre avis a été publié.',
    error: 'Echec !',
    caseClosed: 'Votre cas est fermé.',
    updateSuccess: 'Vos informations ont été mises à jour !',
    updateFailed: 'Des erreurs ont été rencontrées. Veuillez réessayer',
    updateSuccessTitle: 'Modification Réussie!',
    updateFailedTitle: 'Echec de la mise à jour',
    mustDefinePlanning: 'Veuillez définir une période de planification',
    mustSelectPeriod: 'Veuillez sélectionner une période',
    checkMailBox:
        'Consultez votre boîte mail pour récupérer votre mot de passe.',
    thanks: 'Merci !',
    notifyAvailability: 'Votre patient sera notifié de sa disponibilité !',
    planningCreated: 'Planning créé avec succès !!!',
    commentPosted: 'Votre avis a été publié.',
    incompleteInfo: 'Les informations bancaires sont incomplètes',
    update: 'Modifier',
    validate: 'Valider',
    registerSuccessfully: 'Inscription acceptée !',
    loginInfos: 'Vos informations de connexions seront envoyer par email !',
    emailAlreadyUsed: `l'email est déjà utilisé`,
    planningNotFound: "Vous n'avez pas configuré votre agenda.",
    profileIncomplete:
        "Certaines informations d'identification sont manquantes.",
    missingPaymentInformation:
        "Vous n'avez pas saisi les informations de paiements.",
    reason: 'Dites nous pourquoi !',
    warning: 'Erreur !',
    notifyTransfer:
        'Votre réponse a bien été prise en compte. Nous nous chargeons de notifier au patient votre indisponibilité.',
    diagnosticIa: 'Diagnostique IA (Robot)',
    passwordMessage: `Votre nouveau mot de passe doit être différent des mots de passe utilisés précédemment.`,
    incorrectNewPassword:
        "Le nouveau mot de passe doit être différent de l'ancien mot de passe",
    passwordNotMatch: 'Les champs ne correspondent pas',
    incorrectOldPassword: "L'ancien mot de passe est incorrect"
};

export const iA = 'IA';

export const days = [
    { name: 'Lun', day: 'MONDAY' },
    { name: 'Mar', day: 'TUESDAY' },
    { name: 'Mer', day: 'WEDNESDAY' },
    { name: 'Jeu', day: 'THURSDAY' },
    { name: 'Ven', day: 'FRIDAY' },
    { name: 'Sam', day: 'SATURDAY' },
    { name: 'Dim', day: 'SUNDAY' }
];

export const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];
