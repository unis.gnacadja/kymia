import { ConsultationMock } from '@kymia/src/app/mock';
import * as CaseMock from 'src/app/mock/case.mock.json';
import * as DiaryMock from 'src/app/mock/diary.json';
import * as DoctorMock from 'src/app/mock/doctor.json';
import * as PrescriptionMock from 'src/app/mock/prescription.mock.json';
import { environment } from 'src/environments/environment';
import {
    normalizeCase,
    normalizeComment,
    normalizeDiagnostic,
    normalizeDiary,
    normalizeMedia
} from './normalizer';

describe('normalizeDiary', () => {
    it('should normalize diary data', () => {
        const additionnalData = {
            calendarId: 'expected id',
            hour: 'expected hour',
            diagnostique: 'expected diagnostique',
            transaction: {
                billingOperator: 'Expected billingOperator',
                paymentProof: 'Expected paymentProof',
                transactionDate: 'Expected transactionDate'
            },
            idCase: CaseMock.id
        };

        const data = {
            ...ConsultationMock.terminated,
            ...DiaryMock,
            ...additionnalData,
            ordonnace: PrescriptionMock
        };

        const normalizedData = normalizeDiary(data);

        expect(normalizedData).toEqual({
            ...DiaryMock,
            ...additionnalData,
            ordonnance: PrescriptionMock
        });
    });
});

describe('normalizeComment', () => {
    let normalizedComment = normalizeComment({
        ...CaseMock.listCommentaires[0],
        agent: DoctorMock.agent
    });

    it('should normalize comment', () => {
        expect(normalizedComment.content).toBe(
            CaseMock.listCommentaires[0].contenu
        );
    });

    it('should normalize comment with updated photo value', () => {
        normalizedComment = normalizeComment({
            ...CaseMock.listCommentaires[0],
            agent: {
                ...DoctorMock.agent,
                photo: null
            }
        });
        expect(normalizedComment.agent.photo).toEqual(environment.user.avatar);
    });
});

describe('normalizeCase', () => {
    it('should have prediction diagnostic in diagnostics', () => {
        jest.useFakeTimers().setSystemTime(new Date('2024-12-17T03:24:00'));
        expect(normalizeCase(CaseMock)).toEqual({
            id: CaseMock.id,
            agent: CaseMock.agent,
            description: CaseMock.description,
            comments: CaseMock.listCommentaires.map(normalizeComment),
            medias: CaseMock.listMedia.map(normalizeMedia),
            diagnostics: [
                normalizeDiagnostic(
                    {
                        ...CaseMock,
                        name: CaseMock.hypotheseDiagnostique
                    },
                    CaseMock.id
                ),
                ...CaseMock.listDiagnostique.diagnostiques.map(
                    (diagnostic: any) =>
                        normalizeDiagnostic(diagnostic, CaseMock.id)
                ),
                ...CaseMock.listDiagnostique.predictionIA.map(
                    (diagnostic: any) =>
                        normalizeDiagnostic(diagnostic, CaseMock.id)
                )
            ],
            createdAt: CaseMock.dateCreation,
            updatedAt: CaseMock.dateMiseAJour,
            status: CaseMock.statut
        });
    });
    it('should not have prediction diagnostic in diagnostics', () => {
        const caseWithoutPrediction = {
            ...CaseMock,
            listDiagnostique: {
                diagnostiques: [
                    {
                        ...CaseMock.listDiagnostique.diagnostiques[0],
                        agent: {
                            ...CaseMock.agent,
                            photo: null
                        }
                    }
                ]
            }
        };
        expect(normalizeCase(caseWithoutPrediction)).toEqual({
            id: caseWithoutPrediction.id,
            agent: caseWithoutPrediction.agent,
            description: caseWithoutPrediction.description,
            comments:
                caseWithoutPrediction.listCommentaires.map(normalizeComment),
            medias: caseWithoutPrediction.listMedia.map(normalizeMedia),
            diagnostics: [
                normalizeDiagnostic(
                    {
                        ...CaseMock,
                        name: CaseMock.hypotheseDiagnostique
                    },
                    CaseMock.id
                ),
                ...caseWithoutPrediction.listDiagnostique.diagnostiques.map(
                    (diagnostic: any) =>
                        normalizeDiagnostic(
                            diagnostic,
                            caseWithoutPrediction.id
                        )
                )
            ],
            createdAt: caseWithoutPrediction.dateCreation,
            updatedAt: caseWithoutPrediction.dateMiseAJour,
            status: caseWithoutPrediction.statut
        });
    });
});
