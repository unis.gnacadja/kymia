export enum Color {
    primary = 'primary',
    secondary = 'secondary',
    tertiary = 'tertiary',
    success = 'success',
    warning = 'warning',
    danger = 'danger',
    gray = 'dove-gray',
    medium = 'medium',
    fern = 'fern',
    white = 'white',
    greyLight = 'grey-light'
}

export enum Position {
    start = 'start',
    end = 'end'
}

export enum CaseStatus {
    open = 'OUVERT',
    close = 'FERMER'
}

export enum DiagnosticTabs {
    diagnostic = 'diagnostic',
    avis = 'avis'
}

export enum State {
    failed = 'danger',
    success = 'success',
    warning = 'warning',
    dark = 'dark',
    limeGreenDarker = 'lime-green-darker'
}

export enum ToastPosition {
    top = 'top',
    middle = 'middle',
    bottom = 'bottom'
}

export enum DiaryListMode {
    list = 'list',
    slider = 'slider'
}

export enum IconSize {
    small = 'small',
    large = 'large'
}
