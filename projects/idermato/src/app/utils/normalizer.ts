import { environment } from 'src/environments/environment';
import { Case, Comment, Diagnostic, Media } from '../models/types/case';
import { Diary } from '../models/types/diary';
import { User } from '../models/types/user';
import { iA } from './constants';

/**
 * Normalizes the diary data.
 *
 * @param data - The data to be normalized.
 * @returns The normalized Diary object.
 */
export const normalizeDiary: (data: any) => Diary = (data: any) => ({
    id: data.id,
    calendar: {
        availableDate: data.calendar.availableDate,
        availableDateEnd: data.calendar.availableDateEnd,
        availableDateStart: data.calendar.availableDateStart
    },
    problemDescription: data.problemDescription,
    status: data.status,
    images: data.images,
    ...(data.diagnostique && { diagnostique: data.diagnostique }),
    ...(data.hour && { hour: data.hour }),
    ...(data.patient && { patient: data.patient }),
    ...(data.calendarId && { calendarId: data.calendarId }),
    ...(data.callUrl && { callUrl: data.callUrl }),
    ...(data.ordonnace && { ordonnance: data.ordonnace }),
    ...(data.transaction && {
        transaction: data.transaction
    }),
    ...(data.idCase && { idCase: data.idCase })
});

/**
 * Normalizes the comment data.
 *
 * @param data - The data to be normalized.
 * @returns The normalized Comment object.
 */
export const normalizeComment: (data: any) => Comment = (data: any) => ({
    id: data.id,
    content: data.contenu,
    createdAt: data.dateCreation,
    updatedAt: data.dateMiseAJour,
    ...(data.agent && {
        agent: {
            ...data.agent,
            photo: data.agent.photo ?? environment.user.avatar
        }
    })
});

/**
 * Normalizes the media data.
 *
 * @param data - The data to be normalized.
 * @returns The normalized Media object.
 */
export const normalizeMedia: (data: any) => Media = (data: any) => ({
    id: data.id,
    type: data.type,
    url: data.lien
});

/**
 * Normalizes the user data.
 *
 * @param user - The data to be normalized.
 * @returns The normalized User object.
 */
export const normalizeUser = (user: any): User => ({
    ...user,
    photo: user.photo || environment.user
});

/**
 * Normalizes the case diagnostic data.
 *
 * @param data - The data to be normalized.
 * @returns The normalized Diagnostic object.
 */
export const normalizeDiagnostic: (data: any, caseId: string) => Diagnostic = (
    data: any,
    caseId: string
) => ({
    ...(data.id && { id: data.id }),
    caseId,
    agent: data.agent ? normalizeUser(data.agent) : iA,
    final: data.diagnostiqueFinal || false,
    ...(data.prescription && { prescription: data.prescription }),
    diagnostic: data.diagnostique ?? data.name,
    ...(data.probabality && { probability: data.probabality }),
    ...(data.traitement && { treatment: data.traitement }),
    createdAt: data.dateCreation ?? new Date().toISOString(),
    updatedAt: data.dateMiseAJour ?? new Date().toISOString()
});

/**
 * Normalizes the case data.
 *
 * @param data - The data to be normalized.
 * @returns The normalized Case object.
 */
export const normalizeCase: (data: any) => Case = (data: any) => ({
    id: data.id,
    agent: data.agent,
    description: data.description,
    comments: data.listCommentaires.map(normalizeComment),
    medias: data.listMedia.map(normalizeMedia),
    diagnostics: [
        normalizeDiagnostic(
            {
                ...data,
                name: data.hypotheseDiagnostique
            },
            data.id
        ),
        ...data.listDiagnostique.diagnostiques.map((diagnostic: any) =>
            normalizeDiagnostic(diagnostic, data.id)
        ),
        ...(data.listDiagnostique.predictionIA || []).map((diagnostic: any) =>
            normalizeDiagnostic(diagnostic, data.id)
        )
    ],
    createdAt: data.dateCreation,
    updatedAt: data.dateMiseAJour,
    status: data.statut
});
