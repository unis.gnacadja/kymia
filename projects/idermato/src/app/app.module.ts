import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage-angular';
import { StockageService } from '@kymia/src/app/services/stockage.service';
import { AppLoaderComponent } from 'src/app/components/app-loader/app-loader.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './components/shared/shared.module';
import { CaseService } from './services/case.service';
import { CommentService } from './services/comment.service';
import { NotifyService } from './services/common/notify.service';
import { ConsultationService } from './services/consultation.service';
import { DiagnosticService } from './services/diagnostic.service';
import { PlanningService } from './services/planning.service';
import { UserService } from './services/user.service';

@NgModule({
    declarations: [AppComponent, AppLoaderComponent],
    imports: [
        IonicStorageModule.forRoot({
            name: '__Idermato' /// setting local database name
        }),
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        DatePipe,
        SharedModule
    ],
    providers: [
        UserService,
        ConsultationService,
        CaseService,
        NotifyService,
        CommentService,
        DiagnosticService,
        PlanningService,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        StockageService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
