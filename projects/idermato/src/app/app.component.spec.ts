import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { notifyServiceMock } from '@lib/mocks';
import { AppComponent } from './app.component';
import { NotifyService } from './services/common/notify.service';
import { messages } from './utils/constants';

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppComponent],
            providers: [
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
    });

    it('should assign component props on init when popover is open', () => {
        notifyServiceMock.popover.next({
            isOpen: true,
            title: messages.thanks
        });
        component.ngOnInit();
        expect(component.popoverTitle).toBe(messages.thanks);
    });
});
