import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/types/user';
import { UserService } from 'src/app/services/user.service';
import { Color } from 'src/app/utils/enum';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    environment = environment;
    @Input() title: string = 'Bienvenue';
    @Input() subTitle: string | null = null;
    @Input() color: string = Color.white;
    user: User;
    description: string = '';
    showBackButton: boolean = false;
    @Input() border: boolean = true;

    constructor(public userService: UserService, public router: Router) {}
    ngOnInit(): void {
        this.showBackButton =
            this.router.url.includes('tabs') &&
            this.router.url.split('/').length - 1 === 2;
        this.userService.user$.subscribe(() => {
            this.user = this.userService.getUserInfo();
            this.description =
                this.subTitle ?? `Dr ${this.user.prenom} ${this.user.nom}`;
        });
    }
}
