import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models/types/user';
import { UserService } from 'src/app/services/user.service';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let userServiceMock = {
        user: MockUser.simpleIdermato,
        user$: new BehaviorSubject(MockUser.simpleIdermato),
        setUser(user: User) {
            this.user = user;
            this.user$.next(user);
        },
        getUserInfo() {
            return MockUser.simpleIdermato;
        }
    };
    let router: Router;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [HeaderComponent],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{ provide: UserService, useValue: userServiceMock }]
        }).compileComponents();

        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        router = TestBed.inject(Router);
    }));
    it('TA1:should not have "ion-no-border" class when on a direct tab menu', () => {
        jest.spyOn(router, 'url', 'get').mockReturnValue(
            'idermato.com/tabs/home'
        );
        component.ngOnInit();
        fixture.detectChanges();
        expect(
            fixture.nativeElement.querySelector('ion-header').classList
        ).not.toContain('ion-no-border');
    });

    it('TA2:should display the image when showBackButton is true', () => {
        component.showBackButton = true;
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-img')).not.toBeNull();
    });

    it('TA3:should not display the image when showBackButton is false', () => {
        component.showBackButton = false;
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('app-img')).toBeNull();
    });

    it('TA4:should  not display the back button when showBackButton is false', () => {
        fixture.detectChanges();
        expect(
            fixture.nativeElement.querySelector('ion-back-button')
        ).not.toBeNull();
    });

    it('TA5:should not display the back button when showBackButton is true', () => {
        component.showBackButton = true;
        fixture.detectChanges();
        expect(
            fixture.nativeElement.querySelector('ion-back-button')
        ).toBeNull();
    });

    it('TA6: should set description based on subTitle or default value', () => {
        const customSubTitle = 'Dr Doe John';
        component.subTitle = customSubTitle;
        component.ngOnInit();
        fixture.detectChanges();
        const subTitleElement = fixture.nativeElement.querySelector(
            '[data-test-id="description"]'
        );
        expect(subTitleElement.textContent).toBe(customSubTitle);
    });
});
