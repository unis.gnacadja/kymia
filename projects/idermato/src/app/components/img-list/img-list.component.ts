import { Component, Input } from '@angular/core';
import { cardMedia } from '@lib/constants';
import { Media } from '@lib/types';

@Component({
    selector: 'app-img-list',
    templateUrl: './img-list.component.html',
    styleUrls: ['./img-list.component.scss']
})
export class ImgListComponent {
    @Input() data: Media[] = [];
    @Input() errorSrc: string = cardMedia;
}
