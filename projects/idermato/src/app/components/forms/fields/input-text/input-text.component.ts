import {
    Component,
    EventEmitter,
    forwardRef,
    Input,
    OnInit,
    Output,
    ViewEncapsulation
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    NG_VALUE_ACCESSOR,
    ValidatorFn
} from '@angular/forms';
import { TypeError } from 'src/app/models/types/typeError';
import { Color, IconSize } from 'src/app/utils/enum';

enum InputType {
    text = 'text',
    tel = 'tel',
    textarea = 'textarea'
}

@Component({
    selector: 'app-input-text',
    templateUrl: './input-text.component.html',
    styleUrls: ['./input-text.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputTextComponent),
            multi: true
        }
    ],
    encapsulation: ViewEncapsulation.None
})
export class InputTextComponent implements OnInit {
    @Input() control: FormGroup;
    @Input() hasBorder: boolean = true;
    @Input() name: string;
    @Input() rows: number;
    @Input() iconName: string;
    @Input() inputType: string = InputType.text;
    @Input() placeholder: string = '';
    @Input() isReadonly: boolean = false;
    @Input() errorMessages: TypeError[];
    @Input() validators: ValidatorFn[];
    @Input() actionName: string = '';
    @Input() actionIconColor: string = Color.gray;
    @Input() iconSize: string = IconSize.small;
    @Output() action: EventEmitter<string> = new EventEmitter<string>();

    formBuilder: FormBuilder;
    error: string = '';
    hasError: boolean = false;
    inputTypeOptions: typeof InputType = InputType;

    @Output() keyPress: EventEmitter<KeyboardEvent> =
        new EventEmitter<KeyboardEvent>();

    ngOnInit(): void {
        this.control.controls[this.name].setValidators(this.validators);
    }

    get f(): { [key: string]: FormControl } {
        return this.control.controls as { [key: string]: FormControl };
    }

    /**
     * Validates the field
     */
    validateField(): void {
        const control = this.f[this.name];
        if (control.errors && control.dirty) {
            this.hasError = true;
            this.error = this.getDescriptionByType(
                Object.keys(control.errors)[0]
            );
        } else {
            this.hasError = false;
            this.error = '';
        }
    }

    /**
     * Returns the description of the error by type
     *
     * @param type
     */
    private getDescriptionByType(type: string): string {
        for (const error of this.errorMessages) {
            if (error.type === type) {
                return error.description;
            }
        }
        return '';
    }

    /**
     * Emits the event when the icon is clicked
     */
    onClick(): void {
        this.action.emit();
    }
}
