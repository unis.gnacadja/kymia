import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import {
    FormControl,
    FormGroup,
    NG_VALUE_ACCESSOR,
    ReactiveFormsModule,
    Validators
} from '@angular/forms';
import { By } from '@angular/platform-browser';
import { dataPattern } from '@lib/MustMatch.validator';
import { errorMessages } from 'src/app/utils/constants';
import { InputTextComponent } from './input-text.component';

describe('InputTextComponent', () => {
    let component: InputTextComponent;
    let fixture: ComponentFixture<InputTextComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [InputTextComponent],
            imports: [ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(InputTextComponent);
        fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);
        component = fixture.componentInstance;
        component.control = new FormGroup({
            phone: new FormControl('')
        });
        component.name = 'phone';
        component.validators = [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16),
            Validators.pattern(dataPattern.phone)
        ];
        component.errorMessages = [
            errorMessages.required,
            errorMessages.phoneMin,
            errorMessages.phoneMax,
            errorMessages.phone
        ];
        component.actionName = 'eye-outline';
        component.iconName = 'call-outline';
        component.inputType = 'tel';
        component.placeholder = 'Téléphone';
        fixture.detectChanges();
    }));

    it('should display the input element', () => {
        const inputElement = fixture.debugElement.query(By.css('ion-input'));
        expect(inputElement).toBeDefined();
    });

    describe('Border', () => {
        it("should not apply the 'has-border-danger'class  when there are no error", () => {
            component.control.get('phone').setValue('123456');
            component.validateField();
            fixture.detectChanges();
            const fieldBox = fixture.debugElement.query(
                By.css('ion-row')
            ).nativeElement;
            expect(Array.from(fieldBox.classList)).not.toContain(
                'has-border-danger'
            );
        });

        it("should apply the class 'has-border-danger' when error", fakeAsync(() => {
            component.control.get('phone').setValue('123');
            component.control.get('phone').markAsDirty();
            component.validateField();
            fixture.detectChanges();
            tick();
            const fieldBox = fixture.debugElement.query(
                By.css('ion-row')
            ).nativeElement;
            expect(Array.from(fieldBox.classList)).toContain(
                'has-border-danger'
            );
        }));
    });

    it('should emit keyPress event with the entered value', () => {
        const inputElement = fixture.debugElement.query(By.css('ion-input'));
        const spy = jest.spyOn(component.keyPress, 'emit');
        inputElement.triggerEventHandler('ionChange');
        expect(spy).toHaveBeenCalledWith();
    });

    it('should display error message when the control is dirty and has errors', () => {
        component.control.get('phone').setValue('');
        component.control.get('phone').markAsDirty();
        component.validateField();
        fixture.detectChanges();
        const errorElement = fixture.debugElement.query(
            By.css('[data-test-id="error"]')
        );
        expect(errorElement.nativeElement.textContent.trim()).toBe(
            errorMessages.required.description
        );
    });

    it('should display error message when the control is empty and required', () => {
        component.control.get('phone').setValue('');
        component.control.get('phone').markAsDirty();
        component.validateField();
        fixture.detectChanges();
        const errorElement = fixture.debugElement.query(
            By.css('[data-test-id="error"]')
        );
        expect(errorElement.nativeElement.textContent.trim()).toBe(
            errorMessages.required.description
        );
    });

    it('should display length error message when the control has minlength error', () => {
        component.control.get('phone').setValue('12345');
        component.control.get('phone').markAsDirty();
        component.validateField();
        fixture.detectChanges();
        const errorElement = fixture.debugElement.query(
            By.css('[data-test-id="error"]')
        );
        expect(errorElement.nativeElement.textContent.trim()).toBe(
            errorMessages.phoneMin.description
        );
    });

    it('should display pattern error message when the control has pattern error', () => {
        component.control.get('phone').setValue('12345678+');
        component.control.get('phone').markAsDirty();
        component.validateField();
        fixture.detectChanges();
        const errorElement = fixture.debugElement.query(
            By.css('[data-test-id="error"]')
        );
        expect(errorElement.nativeElement.textContent.trim()).toBe(
            errorMessages.phone.description
        );
    });

    it('should return an empty string when there are no errors', () => {
        component.control.get('phone').setValue('123456');
        component.control.get('phone').markAsDirty();
        component.validateField();
        fixture.detectChanges();
        expect(component.error).toBe('');
    });

    it('should return an empty string when there are errors but not corresponding message', () => {
        component.control.get('phone').setValue('12345678+');
        component.control.get('phone').markAsDirty();
        component.validators = [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(16),
            Validators.pattern(dataPattern.phone)
        ];
        component.errorMessages = [
            errorMessages.required,
            errorMessages.phoneMin,
            errorMessages.phoneMax
        ];
        component.validateField();
        fixture.detectChanges();
        expect(component.error).toBe('');
    });

    it('should emit action event on submit', () => {
        const spy = jest.spyOn(component.action, 'emit');
        component.onClick();
        expect(spy).toHaveBeenCalled();
    });
});
