import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PhoneIndicatorComponent } from './phone-indicator.component';

describe('PhoneIndicatorComponent', () => {
    let component: PhoneIndicatorComponent;
    let fixture: ComponentFixture<PhoneIndicatorComponent>;

    beforeAll(() => {
        TestBed.configureTestingModule({
            declarations: [PhoneIndicatorComponent],
            providers: [],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(PhoneIndicatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should update the selectedCountry when onCountryChange is called', () => {
        component.onCountryChange({
            detail: {
                value: component.countries[0].code
            }
        });
        expect(component.selectedCountry.code).toBe(
            component.countries[0].code
        );
    });

    it('should return the dial_code when trackCountry is called', () => {
        const trackCountry = component.trackCountry(0, component.countries[0]);
        expect(trackCountry).toBe(component.countries[0].code);
    });
});
