import { Component, Input, OnInit } from '@angular/core';
import { PhoneService } from '@lib/services/phone.service';
import CountryList from 'country-list-with-dial-code-and-flag';
import { CountryInterface } from 'country-list-with-dial-code-and-flag/dist/types';

type Country = {
    code: string;
    name: string;
};

@Component({
    selector: 'app-phone-indicator',
    templateUrl: './phone-indicator.component.html',
    styleUrls: ['./phone-indicator.component.scss']
})
export class PhoneIndicatorComponent implements OnInit {
    countries: Country[] = [];
    selectedCountry: CountryInterface;
    @Input() disabled: boolean = false;

    constructor(private readonly phoneService: PhoneService) {}

    ngOnInit(): void {
        this.countries = CountryList.getAll().map(
            (country: CountryInterface) => ({
                code: country.code,
                name: `${country.flag} ${country.dial_code} (${country.name})`
            })
        );
        this.phoneService.getSelectedPhone().subscribe(() => {
            this.selectedCountry =
                this.phoneService.selectedPhoneCountry.getValue();
        });
    }

    /**
     * Update selected country and emit the dial code
     *
     * @param event: any
     */
    onCountryChange(event: any): void {
        this.phoneService.setPhoneCountry(event.detail.value);
    }

    /**
     * Track country
     *
     * @param _index number: _index of the item
     * @param item Country: item to track
     * @returns string: id of the item
     */
    trackCountry(_index: number, item: Country): string {
        return item.code;
    }
}
