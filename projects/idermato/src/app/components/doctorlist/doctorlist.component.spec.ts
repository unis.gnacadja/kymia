import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { doctorServiceMock, normalizedDoctor } from '@kymia/src/app/mock';
import { SearchDoctorPipe } from 'src/app/pipes/doctor.pipe';
import { DoctorlistComponent } from './doctorlist.component';

describe('DoctorlistComponent', () => {
    let component: DoctorlistComponent;
    let fixture: ComponentFixture<DoctorlistComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DoctorlistComponent, SearchDoctorPipe],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            providers: [SearchDoctorPipe]
        }).compileComponents();
        fixture = TestBed.createComponent(DoctorlistComponent);
        component = fixture.componentInstance;
        component.list = [
            {
                ...normalizedDoctor,
                name: 'Expected search term'
            },
            ...doctorServiceMock.list
        ];
        fixture.detectChanges();
    }));

    it('should display the list of praticiens matching the search term', () => {
        component.filterTerm = component.list[0].name;
        component.ngOnChanges();
        fixture.detectChanges();
        expect(component.filteredDoctors).toEqual([component.list[0]]);
    });
});
