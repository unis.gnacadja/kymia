import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output
} from '@angular/core';
import { Doctor } from '@kymia/src/app/models/doctor.model';
import { SearchDoctorPipe } from 'src/app/pipes/doctor.pipe';

@Component({
    selector: 'app-doctorlist',
    templateUrl: './doctorlist.component.html',
    styleUrls: ['./doctorlist.component.scss']
})
export class DoctorlistComponent implements OnChanges {
    @Input() list: Doctor[] = [];
    @Input() loading: boolean = false;
    @Input() filterTerm: string = '';
    filteredDoctors: Doctor[] = [];
    @Output() itemSelect: EventEmitter<Doctor> = new EventEmitter<Doctor>();
    constructor(public searchDoctor: SearchDoctorPipe) {}
    ngOnChanges(): void {
        this.filteredDoctors = this.searchDoctor.transform(
            this.list,
            this.filterTerm
        );
    }
}
