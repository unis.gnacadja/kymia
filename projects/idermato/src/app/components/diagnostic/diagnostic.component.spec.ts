import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { normalizedCaseWithCommentAgent } from 'src/app/tests';
import { iA, messages } from 'src/app/utils/constants';
import { DiagnosticComponent } from './diagnostic.component';

describe('DiagnosticComponent', () => {
    let component: DiagnosticComponent;
    let fixture: ComponentFixture<DiagnosticComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DiagnosticComponent],
            imports: [IonicModule.forRoot()],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(DiagnosticComponent);
        component = fixture.componentInstance;
        component.item = normalizedCaseWithCommentAgent.diagnostics[0];
        fixture.detectChanges();
    }));

    it('should display the correct title for IA', () => {
        component.item.agent = iA;
        component.ngOnInit();
        expect(component.title).toBe(messages.diagnosticIa);
    });
});
