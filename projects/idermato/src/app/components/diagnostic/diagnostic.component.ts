import { Component, Input, OnInit } from '@angular/core';
import { Diagnostic } from 'src/app/models/types/case';
import { iA, messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-diagnostic',
    templateUrl: './diagnostic.component.html',
    styleUrls: ['./diagnostic.component.scss']
})
export class DiagnosticComponent implements OnInit {
    @Input() item: Diagnostic;
    iA: 'IA' = iA;
    defaultImage: string = environment.user.avatar;
    title: string;
    ngOnInit() {
        this.title =
            this.item.agent === iA
                ? messages.diagnosticIa
                : `Dr ${this.item.agent.prenom} ${this.item.agent.nom}`;
    }
}
