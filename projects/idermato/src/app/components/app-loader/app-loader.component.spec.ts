import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppLoaderComponent } from './app-loader.component';

describe('AppLoaderComponent', () => {
    let component: AppLoaderComponent;
    let fixture: ComponentFixture<AppLoaderComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppLoaderComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(AppLoaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
