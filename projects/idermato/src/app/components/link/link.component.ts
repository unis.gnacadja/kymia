import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-link',
    templateUrl: './link.component.html',
    styleUrls: ['./link.component.scss']
})
export class LinkComponent {
    @Input() isActive: boolean = false;
    @Input() link: string;
    @Input() name: string;

    constructor(private router: Router) {}

    isActiveRoute(routePath: string): boolean {
        return this.router.isActive(routePath, false);
    }
}
