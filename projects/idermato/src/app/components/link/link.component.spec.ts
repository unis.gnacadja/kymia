import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LinkComponent } from './link.component';

describe('LinkComponent', () => {
    let component: LinkComponent;
    let fixture: ComponentFixture<LinkComponent>;
    const testLink = '/test';
    const testName = 'Test Link';
    const mockRouter = {
        isActive: (routePath: string, exact: boolean) => routePath === testLink
    };

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LinkComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{ provide: Router, useValue: mockRouter }]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LinkComponent);
        component = fixture.componentInstance;
        component.link = testLink;
        component.name = testName;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('TA1: should display the link name', () => {
        fixture.detectChanges();
        const loginLink = fixture.debugElement.query(
            By.css('[data-test-id="app-link"]')
        ).nativeElement;
        expect(loginLink.textContent.trim()).toBe(testName);
    });

    describe('TA2 : Xhen active', () => {
        beforeAll(() => {
            component.isActive = false;
            fixture.detectChanges();
        });
        it("TA2:-2 should apply the 'active' class", () => {
            fixture.detectChanges();
            const registerLink = fixture.debugElement.query(
                By.css('[data-test-id="app-link"]')
            ).nativeElement;
            expect(Array.from(registerLink.classList)).toContain('active');
        });

        it("TA2- 2 : should not apply the 'active' class when not active", () => {
            const registerLink = fixture.debugElement.query(
                By.css('[data-test-id="app-link"]')
            ).nativeElement;
            expect(Array.from(registerLink.classList)).not.toContain('dark');
        });
    });

    it("TA3- 2 : when route doesn't correspond should not apply the 'active' class", () => {
        component.link = '/fail';
        fixture.detectChanges();
        const registerLink = fixture.debugElement.query(
            By.css('[data-test-id="app-link"]')
        ).nativeElement;
        expect(Array.from(registerLink.classList)).not.toContain('active');
    });
});
