import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
    let component: ButtonComponent;
    let fixture: ComponentFixture<ButtonComponent>;
    describe('TA1 : when button has no data', () => {
        beforeAll(() => {
            TestBed.configureTestingModule({
                declarations: [ButtonComponent],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents();
            fixture = TestBed.createComponent(ButtonComponent);
            component = fixture.componentInstance;
            component.changeDetectorRef.detectChanges();
            fixture.detectChanges();
        });
        it('TA1-1 : should not display the title', () => {
            const element = fixture.nativeElement.querySelector(
                '[data-test-id="title"]'
            );
            expect(element.textContent).toBe('');
        });
        it('TA1-2 : should not display the icon', () => {
            const iconElement = fixture.nativeElement.querySelector(
                '[data-test-id="ion-icon"]'
            );
            expect(iconElement).toBeNull();
        });
        it('TA1-3 : should not display the loading', () => {
            const loadingElement = fixture.nativeElement.querySelector(
                '[data-test-id="loading"]'
            );
            expect(loadingElement).toBeNull();
        });
        it('TA1-4: should emit onClick event when triggerButtonClick is called', fakeAsync(() => {
            jest.spyOn(component.onClick, 'emit');
            component.triggerButtonClick();
            expect(jest.spyOn(component.onClick, 'emit')).toHaveBeenCalled();
        }));
    });

    describe('TA2: when button has not title, icon and loading is true ', () => {
        beforeAll(() => {
            TestBed.configureTestingModule({
                declarations: [ButtonComponent],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents();
            fixture = TestBed.createComponent(ButtonComponent);
            component = fixture.componentInstance;
            component.loading = true;
            component.ngOnChanges();
            fixture.detectChanges();
        });
        it('TA2-1 : should show the loading spinner when loading input is true', () => {
            const loadingElement: HTMLElement =
                fixture.nativeElement.querySelector('[data-test-id="loading"]');
            expect(loadingElement).toBeTruthy();
        });
        it('TA2-2 : should not show the button content when loading input is true', () => {
            const buttonContent: HTMLElement =
                fixture.nativeElement.querySelector('[data-test-id="content"]');
            expect(buttonContent).toBeNull();
        });
    });

    describe('TA3: when button has title, icon and loading is false ', () => {
        beforeAll(() => {
            TestBed.configureTestingModule({
                declarations: [ButtonComponent],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents();
            fixture = TestBed.createComponent(ButtonComponent);
            component = fixture.componentInstance;
            component.title = 'expected title';
            component.icon = 'arrow-forward-outline';
            component.ngOnChanges();
            fixture.detectChanges();
        });
        it('TA3-1: should display the title', () => {
            const element: HTMLElement = fixture.nativeElement.querySelector(
                '[data-test-id="title"]'
            );
            expect(element.textContent).toBe(component.title);
        });
        it('TA3-2 : should display icon value expected', () => {
            const iconElement: HTMLIonIconElement =
                fixture.nativeElement.querySelector(
                    '[data-test-id="ion-icon"]'
                );
            expect(iconElement.name).toEqual('arrow-forward-outline');
        });
    });
});
