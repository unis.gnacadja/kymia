import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    NgZone,
    OnChanges,
    Output
} from '@angular/core';
import { Config, IonButton } from '@ionic/angular';

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
    providers: [Config]
})
export class ButtonComponent extends IonButton implements OnChanges {
    @Input() title: string = '';
    @Input() loading: boolean = false;
    @Input() icon: string = '';
    @Input() iconSize: string = 'size-20';
    @Output() onClick: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        public changeDetectorRef: ChangeDetectorRef,
        public elementRef: ElementRef,
        public ngZone: NgZone
    ) {
        super(changeDetectorRef, elementRef, ngZone);
    }

    ngOnChanges(): void {
        this.changeDetectorRef.detectChanges();
    }

    triggerButtonClick() {
        this.onClick.emit();
    }
}
