import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-action-required',
    templateUrl: './action-required.component.html',
    styleUrls: ['./action-required.component.scss']
})
export class ActionRequiredComponent {
    @Input() message: string = '';
}
