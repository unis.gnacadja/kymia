import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ActionRequiredComponent } from './action-required.component';

describe('ActionRequiredComponent', () => {
    let component: ActionRequiredComponent;
    let fixture: ComponentFixture<ActionRequiredComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ActionRequiredComponent],
            imports: [IonicModule.forRoot()],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(ActionRequiredComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).not.toBeNull();
    });
});
