import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import {
    Camera,
    CameraResultType,
    CameraSource,
    Photo
} from '@capacitor/camera';
import { ModalController } from '@ionic/angular';
import { City, Country } from '@kymia/src/app/models';
import { LocationService } from '@kymia/src/app/services/location/location.service';
import { helperMessages } from '@lib/constants';
import { dataPattern } from '@lib/MustMatch.validator';
import { PhoneService } from '@lib/services/phone.service';
import { finalize } from 'rxjs/operators';
import { TypeError } from 'src/app/models/types/typeError';
import { User } from 'src/app/models/types/user';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import {
    errorMessages,
    messages,
    requiredField,
    validatorsHelper
} from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';
import { CasOptionPhotoComponent } from '../../cas-option-photo/cas-option-photo.component';

const inputColors: {
    simple: string;
    error: string;
    success: string;
} = {
    simple: 'grey',
    error: 'danger',
    success: 'primary'
};

@Component({
    selector: 'app-user-infos',
    templateUrl: './user-infos.component.html',
    styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {
    @Input() isReadOnly: boolean = false;
    @Input() mode?: 'create' | 'update' = 'create';
    @Input() loading: boolean = false;
    @Output() submit: EventEmitter<any> = new EventEmitter<any>();
    @Input() user?: User = {
        nom: '',
        prenom: '',
        numero: '',
        fonction: '',
        pays: '',
        ville: '',
        photo: '',
        email: ''
    };
    image: {
        url: string;
        file?: Photo;
        uid?: string;
    } = { url: environment.user.avatar };
    defaultImage = environment.user.avatar;
    countries: Country[];
    cities: City[];
    userCountry: Country;
    userCity: City;

    registerForm: FormGroup;
    formBuilder: FormBuilder = new FormBuilder();
    errors: Record<string, TypeError> = errorMessages;
    requiredValidator: ValidatorFn = Validators.required;
    validators: Record<string, ValidatorFn[]> = validatorsHelper;
    noSpaceValidators: ValidatorFn = Validators.pattern(dataPattern.noSpace);
    nameValidator: ValidatorFn[] = [
        Validators.pattern(dataPattern.lettersOnly),
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(25),
        Validators.pattern(dataPattern.noSpace)
    ];
    textValidator: ValidatorFn = Validators.pattern(dataPattern.text);

    requiredField: TypeError = requiredField;
    countryColor: string = inputColors.simple;
    townColor: string = inputColors.simple;

    constructor(
        private readonly modal: ModalController,
        private readonly notifyService: NotifyService,
        private readonly loadingService: LoadingService,
        private readonly locationService: LocationService,
        private readonly phoneService: PhoneService
    ) {}

    async ngOnInit(): Promise<void> {
        this.registerForm = this.formBuilder.group(
            {
                name: [''],
                firstName: [''],
                phone: [''],
                email: [''],
                function: [''],
                country: [''],
                town: ['']
            },
            {
                updateOn: 'blur'
            }
        );
        await this.getUserInfo();
    }

    /**
     * Get user informations
     */
    async getUserInfo(): Promise<void> {
        if (this.mode === 'update') {
            this.registerForm.patchValue({
                name: this.user.nom,
                firstName: this.user.prenom,
                phone: this.phoneService.getPhoneNumber(this.user.numero),
                email: this.user.email,
                function: this.user.fonction,
                country: this.user.pays,
                town: this.user.ville
            });
            if (this.user.photo) {
                this.image.url = this.user.photo;
            }
        }
        await this.loadingService.present(helperMessages.loading);

        this.locationService
            .getCountries()
            .pipe(
                finalize(async () => {
                    if (this.mode === 'update') {
                        this.userCountry = this.countries.find(
                            (country) => country.name === this.user.pays
                        );
                        await this.getCities(this.userCountry.id, true);
                    }
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (res: any) => {
                    this.countries = res.content.sort((a, b) =>
                        a.name.localeCompare(b.name)
                    );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Update or register a user
     */
    onSubmit(): void {
        if (this.isReadOnly) {
            this.isReadOnly = false;
        } else if (this.registerForm.valid) {
            const data = {
                nom: this.registerForm.value.name,
                prenom: this.registerForm.value.firstName,
                numero: `${this.phoneService.dialCode}${this.registerForm.value.phone}`,
                email: this.registerForm.value.email,
                fonction: this.registerForm.value.function,
                pays: this.registerForm.value.country,
                ville: this.registerForm.value.town,
                photo: this.image.url
            };
            this.submit.emit({
                user: data,
                ...(this.image.uid && {
                    picture: this.image
                })
            });
            if (this.mode === 'update') {
                this.isReadOnly = true;
            }
        } else {
            this.notifyService.presentToast({
                message: messages.submitError,
                duration: 3000
            });
        }
    }

    /**
     * Open the option selection modal
     */
    async openOptionSelection() {
        const modal = await this.modal.create({
            component: CasOptionPhotoComponent,
            cssClass: 'transparent-modal'
        });
        modal.onDidDismiss().then((res) => {
            if (res.role === 'select') {
                this.takePicture(res.data);
            } else if (res.role === 'remove') {
                this.image.uid = `${new Date().getTime()}.svg`;
                this.image.url = environment.user.avatar;
            }
        });
        return modal.present();
    }

    /**
     * Take a picture
     *
     * @param type: CameraSource: camera source
     */
    async takePicture(type: any) {
        this.image.file = await Camera.getPhoto({
            quality: 90,
            allowEditing: true,
            resultType: CameraResultType.Uri,
            width: 100,
            height: 100,
            source: CameraSource[type]
        });
        this.image.uid = new Date().getTime() + '.' + this.image.file.format;
        this.image.url = this.image.file.webPath;
    }

    /**
     * Get cities of country
     *
     * @param countryId: string: country id
     * @param setUserCity: boolean: set user city
     */
    async getCities(
        countryId: string,
        setUserCity: boolean = false
    ): Promise<void> {
        this.locationService.searchCity(countryId).subscribe({
            next: (res: any) => {
                this.cities = res.content.sort((a, b) =>
                    a.name.localeCompare(b.name)
                );
                if (setUserCity) {
                    this.userCity = this.cities.find(
                        (city) => city.name === this.user.ville
                    );
                }
            }
        });
    }

    /**
     * Selects a country and fetches the cities of the selected country
     *
     * @param country: Country: country to select
     */
    selectCountry(country: Country) {
        this.getCities(country.id);
        this.registerForm.patchValue({
            country: country.name
        });
        this.registerForm.controls.country.markAsDirty();
        this.countryColor = inputColors.success;
    }

    /**
     * Remove the country
     */
    removeCountry() {
        this.registerForm.patchValue({
            country: ''
        });
        this.countryColor = inputColors.error;
    }

    /**
     * Selects a city
     *
     * @param city: City: city to select
     */
    selectCity(city: City): void {
        this.registerForm.patchValue({
            town: city.name
        });
        this.registerForm.controls.town.markAsDirty();
        this.townColor = inputColors.success;
    }

    /**
     * Clears the selected city
     */
    removeCity() {
        this.registerForm.patchValue({
            town: ''
        });
        this.townColor = inputColors.error;
    }
}
