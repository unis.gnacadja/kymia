import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Camera } from '@capacitor/camera';
import { IonicModule, ModalController } from '@ionic/angular';
import {
    CountriesMock,
    Doctors,
    locationServiceMock,
    MockPhoto,
    MockUser
} from '@kymia/src/app/mock';
import { LocationService } from '@kymia/src/app/services/location/location.service';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';
import { CasOptionPhotoComponent } from '../../cas-option-photo/cas-option-photo.component';
import { UserInfosComponent } from './user-infos.component';

const mockedUser = {
    nom: MockUser.simpleIdermato.nom,
    prenom: MockUser.simpleIdermato.prenom,
    numero: MockUser.withoutId.phone,
    fonction: MockUser.simpleIdermato.fonction,
    pays: CountriesMock[0].name,
    ville: Doctors.content[0].city.name,
    photo: MockUser.completeProfile.photo,
    email: MockUser.simpleIdermato.email
};

const modalControllerSpy = {
    create: () =>
        Promise.resolve({
            present: () => Promise.resolve(),
            dismiss: () => Promise.resolve(),
            onDidDismiss: () => Promise.resolve({ role: '', data: '' })
        }),
    dismiss: () => Promise.resolve()
};

jest.mock('@capacitor/camera', () => {
    return {
        Camera: {
            getPhoto: () => MockPhoto.photo
        },
        CameraResultType: {
            Uri: 'uri'
        },
        CameraSource: {
            Prompt: 'PROMPT',
            Camera: 'CAMERA',
            Photos: 'PHOTOS'
        }
    };
});

describe('UserInfosComponent', () => {
    let component: UserInfosComponent;
    let fixture: ComponentFixture<UserInfosComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UserInfosComponent, CasOptionPhotoComponent],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: ModalController,
                    useValue: modalControllerSpy
                },
                {
                    provide: LocationService,
                    useValue: locationServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                { provide: UserService, useValue: {} },
                FormBuilder
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(UserInfosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('when call without default value', () => {
        it('should render default user value', () => {
            expect(component.user).toEqual({
                nom: '',
                prenom: '',
                numero: '',
                fonction: '',
                pays: '',
                ville: '',
                photo: '',
                email: ''
            });
        });
        it('should be in create mode', () => {
            let createCreate: HTMLElement = fixture.nativeElement.querySelector(
                '[data-testid="signup"]'
            );
            expect(createCreate).not.toBeNull();
        });
    });

    it("should display a notification when an error occurs while retrieving the user's location ", async () => {
        locationServiceMock.getCountries.mockReturnValueOnce(
            throwError({ error: { status: 500 } })
        );
        await component.getUserInfo();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });

    describe('On submit', () => {
        beforeEach(() => {
            component.user = mockedUser;
            component.user.photo = environment.user.avatar;
            component.mode = 'update';
        });

        it('should emit submit event with picture when image file is defined', () => {
            component.registerForm.controls.email.setValue(
                MockUser.simpleIdermato.email
            );
            component.registerForm.controls.name.setValue(
                MockUser.simpleIdermato.nom
            );
            component.registerForm.controls.firstName.setValue(
                MockUser.simpleIdermato.prenom
            );
            component.registerForm.controls.phone.setValue(
                MockUser.simpleIdermato.numero
            );
            component.registerForm.controls.function.setValue(
                MockUser.simpleIdermato.fonction
            );
            component.registerForm.controls.country.setValue(
                MockUser.simpleIdermato.pays
            );
            component.registerForm.controls.town.setValue(
                MockUser.simpleIdermato.ville
            );
            fixture.detectChanges();
            component.mode = 'update';
            component.ngOnInit();
            component.image.uid = `${new Date().getTime()}.svg`;
            const emitSpy = jest.spyOn(component.submit, 'emit');
            component.onSubmit();
            expect(emitSpy.mock.calls[0][0]).toHaveProperty(
                'picture',
                component.image
            );
        });

        it('should set isReadOnly to false when isReadOnly is true', () => {
            component.isReadOnly = true;
            fixture.detectChanges();
            component.onSubmit();
            expect(component.isReadOnly).toBe(false);
        });

        it('should display a notification when the form is invalid', async () => {
            component.mode = 'update';
            await component.getUserInfo();
            component.registerForm.setErrors({ email: true });
            fixture.detectChanges();
            component.onSubmit();
            expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
                message: messages.submitError,
                duration: 3000
            });
        });
    });

    describe('When user upload a new photo', () => {
        beforeAll(() => {
            jest.spyOn(modalControllerSpy, 'create').mockReturnValueOnce(
                Promise.resolve({
                    present: () => Promise.resolve(),
                    dismiss: () => Promise.resolve(),
                    onDidDismiss: () =>
                        Promise.resolve({ role: 'select', data: 'Camera' })
                })
            );
        });

        it('should call takePicture', () => {
            const spyTakePicture = jest.spyOn(component, 'takePicture');
            component.takePicture('Photo');
            expect(spyTakePicture).toHaveBeenCalled();
        });

        it('should open camera', fakeAsync(() => {
            jest.spyOn(Camera, 'getPhoto');
            component.openOptionSelection();
            tick();
            expect(Camera.getPhoto).toHaveBeenCalledWith({
                allowEditing: true,
                quality: 90,
                resultType: 'uri',
                source: 'CAMERA',
                width: 100,
                height: 100
            });
        }));
    });

    it('should call removePicture when "remove" is selected when removing photo', async () => {
        jest.spyOn(modalControllerSpy, 'create').mockReturnValueOnce(
            Promise.resolve({
                present: () => Promise.resolve(),
                dismiss: () => Promise.resolve(),
                onDidDismiss: () =>
                    Promise.resolve({ role: 'remove', data: '' })
            })
        );
        await component.openOptionSelection();
        expect(component.image.url).toBe(component.defaultImage);
    });

    it("should update the user's country when a country is selected", () => {
        component.selectCountry(CountriesMock[1]);
        expect(component.registerForm.value.country).toBe(
            CountriesMock[1].name
        );
    });

    it("should remove the user's country when removeCountry is called", () => {
        component.removeCountry();
        expect(component.registerForm.value.country.length).toBe(0);
    });

    it("should update the user's city when a city is selected", () => {
        component.selectCity(Doctors.content[1].city);
        expect(component.registerForm.value.town).toBe(
            Doctors.content[1].city.name
        );
    });

    it('should remove the city when removeCity is called', () => {
        component.removeCity();
        expect(component.registerForm.value.town.length).toBe(0);
    });
});
