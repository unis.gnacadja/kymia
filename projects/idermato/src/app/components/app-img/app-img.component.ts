import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-img',
    templateUrl: './app-img.component.html',
    styleUrls: ['./app-img.component.scss']
})
export class AppImgComponent {
    @Input() src: string;
    @Input() alt: string;
    @Input() title: string;
    @Input() errorSrc: string;

    onImageError() {
        this.src = this.errorSrc;
    }
}
