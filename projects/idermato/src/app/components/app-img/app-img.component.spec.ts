import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppImgComponent } from './app-img.component';

describe('AppImgComponent', () => {
    let component: AppImgComponent;
    let fixture: ComponentFixture<AppImgComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppImgComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AppImgComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should render the image element with the provided src', () => {
        const imgElement: HTMLImageElement =
            fixture.nativeElement.querySelector('[data-test-id="app-img"]');
        expect(imgElement.src).toBe(component.src);
    });

    it('should render the image element with the provided errorsrc when an image error occurs', () => {
        component.onImageError();

        const imgElement: HTMLImageElement =
            fixture.nativeElement.querySelector('[data-test-id="app-img"]');
        expect(imgElement.src).toBe(component.errorSrc);
    });
});
