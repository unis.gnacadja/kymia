import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CardComponent } from '@kymia/src/app/components/card/card.component';
import { NotAvailableServiceComponent } from '@kymia/src/app/components/not-available-service/not-available-service.component';
import { AutocompleteLibModule } from '@kymia/src/app/modules/autocomplete-lib/src/lib/autocomplete-lib.module';
import { NgPipesModule } from 'ngx-pipes';
import { MessagePipe, NormalizeDate } from 'src/app/pipes';
import { SearchDoctorPipe } from 'src/app/pipes/doctor.pipe';
import { LoadingService } from 'src/app/services/common/loading.service';
import { ActionRequiredComponent } from '../action-required/action-required.component';
import { AppImgComponent } from '../app-img/app-img.component';
import { ButtonComponent } from '../button/button.component';
import { CasOptionPhotoComponent } from '../cas-option-photo/cas-option-photo.component';
import { CaseComponent } from '../case/case.component';
import { CaselistComponent } from '../caselist/caselist.component';
import { DiagnosticComponent } from '../diagnostic/diagnostic.component';
import { DiaryComponent } from '../diary/diary.component';
import { DiarylistComponent } from '../diarylist/diarylist.component';
import { DoctorComponent } from '../doctor-item/doctor-item.component';
import { DoctorlistComponent } from '../doctorlist/doctorlist.component';
import { InputTextComponent } from '../forms/fields/input-text/input-text.component';
import { PhoneIndicatorComponent } from '../forms/fields/phone-indicator/phone-indicator.component';
import { HeaderComponent } from '../header/header.component';
import { ImgListComponent } from '../img-list/img-list.component';
import { LinkComponent } from '../link/link.component';
import { NotificationComponent } from '../notification/notification.component';
import { PopupComponent } from '../popup/popup.component';
import { TabsComponent } from '../tabs/tabs.component';
import { TileComponent } from '../tile/tile.component';
import { UserInfosComponent } from '../user/infos/user-infos.component';

@NgModule({
    declarations: [
        LinkComponent,
        InputTextComponent,
        PopupComponent,
        ButtonComponent,
        CardComponent,
        CaseComponent,
        CaselistComponent,
        AppImgComponent,
        NotAvailableServiceComponent,
        DiaryComponent,
        DiarylistComponent,
        HeaderComponent,
        CasOptionPhotoComponent,
        UserInfosComponent,
        ActionRequiredComponent,
        NotificationComponent,
        MessagePipe,
        DoctorComponent,
        DoctorlistComponent,
        TileComponent,
        DiagnosticComponent,
        ImgListComponent,
        SearchDoctorPipe,
        NormalizeDate,
        TabsComponent,
        PhoneIndicatorComponent
    ],
    exports: [
        LinkComponent,
        InputTextComponent,
        PopupComponent,
        ButtonComponent,
        CardComponent,
        CaseComponent,
        CaselistComponent,
        AppImgComponent,
        NotAvailableServiceComponent,
        DiaryComponent,
        DiarylistComponent,
        HeaderComponent,
        CasOptionPhotoComponent,
        UserInfosComponent,
        ActionRequiredComponent,
        NotificationComponent,
        DoctorComponent,
        TileComponent,
        DiagnosticComponent,
        ImgListComponent,
        DoctorlistComponent,
        SearchDoctorPipe,
        NormalizeDate,
        TabsComponent,
        PhoneIndicatorComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        NgPipesModule,
        AutocompleteLibModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [MessagePipe, SearchDoctorPipe, LoadingService]
})
export class SharedModule {}
