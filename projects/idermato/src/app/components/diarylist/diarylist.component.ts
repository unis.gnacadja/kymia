import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonList } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { finalize } from 'rxjs/operators';
import { Diary } from 'src/app/models/types/diary';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { messages } from 'src/app/utils/constants';
import { DiaryListMode, State } from 'src/app/utils/enum';

@Component({
    selector: 'app-diarylist',
    templateUrl: './diarylist.component.html',
    styleUrls: ['./diarylist.component.scss']
})
export class DiarylistComponent {
    @Input() list: Diary[] = [];
    @Input() limit: number = 0;
    @Input() useOptions: boolean = false;
    @Input() mode: DiaryListMode = DiaryListMode.slider;
    listMode: DiaryListMode = DiaryListMode.list;
    waiting: string = ConsultationStatus.waiting;
    slideOpts: {
        initialSlide: number;
        speed: number;
        slidesPerView: number;
        autoplay: boolean;
        effect: string;
    } = {
        initialSlide: 0,
        speed: 1000,
        slidesPerView: 1,
        autoplay: true,
        effect: 'fade'
    };

    @ViewChild('slidingList') slidingList: IonList;

    constructor(
        private router: Router,
        private loadingService: LoadingService,
        private consultationService: ConsultationService,
        private notifyService: NotifyService
    ) {}

    /**
     * Track diary by id
     *
     * @param _index number: _index of the item
     * @param item Diary: item to track
     * @returns string: id of the item
     */
    trackDiary(_index: number, item: Diary): string {
        return item.id;
    }

    /**
     * Transfer an appointment
     *
     * @param diaryId string: id of the appointment
     */
    transfer(diaryId: string): void {
        this.slidingList.closeSlidingItems();
        this.router.navigate(['tabs/diaries/consultation/transfert'], {
            queryParams: { diaryId }
        });
    }

    /**
     * Confirm the consultation
     *
     * @param diaryId string: id of the appointment
     */
    async confirm(diaryId: string): Promise<void> {
        this.slidingList.closeSlidingItems();
        await this.loadingService.present(helperMessages.loading);
        this.consultationService
            .confirm(diaryId)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: async () => {
                    await this.consultationService
                        .getConsultation(diaryId)
                        .subscribe((data: any) => {
                            this.consultationService.setConsultation(data);
                        });
                    this.notifyService.setPopover(
                        true,
                        messages.notifyAvailability,
                        messages.thanks,
                        State.success
                    );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }
}
