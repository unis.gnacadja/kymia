import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { IonicModule, NavController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { consultationServiceMock, diaries, routerMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { DiaryListMode, State } from 'src/app/utils/enum';
import { DiarylistComponent } from './diarylist.component';

describe('DiarylistComponent', () => {
    let component: DiarylistComponent;
    let fixture: ComponentFixture<DiarylistComponent>;

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DiarylistComponent],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: Router,
                    useValue: routerMock
                },
                {
                    provide: NavController,
                    useValue: {}
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: ConsultationService,
                    useValue: consultationServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DiarylistComponent);
        component = fixture.componentInstance;
        component.list = diaries;
        component.mode = DiaryListMode.list;

        fixture.detectChanges();
        component.slidingList = { closeSlidingItems: jest.fn() } as any;
    }));

    it('should track diary by id', () => {
        expect(component.trackDiary(0, component.list[0])).toBe(
            component.list[0].id
        );
    });

    it('should redirect to the transfer page when clicking the transfer button', () => {
        component.transfer(component.list[0].id);
        expect(routerMock.navigate).toHaveBeenCalledWith(
            ['tabs/diaries/consultation/transfert'],
            {
                queryParams: { diaryId: component.list[0].id }
            }
        );
    });

    it('should notify success upon successful appointment confirmation', async () => {
        await component.confirm(component.list[0].id);
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.notifyAvailability,
            messages.thanks,
            State.success
        );
    });

    it('should notify error upon failed appointment confirmation', async () => {
        consultationServiceMock.confirm.mockReturnValue(
            throwError({ error: { message: helperMessages.errorOccurred } })
        );
        await component.confirm('123456789');
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });
});
