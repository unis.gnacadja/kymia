import { Component, Input } from '@angular/core';
import { Application } from '@lib/enum';

type TabItem = {
    name: string;
    icon: string;
    label?: string;
};

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.component.html',
    styleUrls: ['tabs.component.scss']
})
export class TabsComponent {
    @Input() activeTab: string = '';
    @Input() name: string = Application.idermato;

    kymia: string = Application.kymia;

    tabs: Record<string, TabItem[]> = {
        idermato: [
            {
                name: 'home',
                icon: 'home'
            },
            {
                name: 'diaries',
                icon: 'calendar-clear-outline'
            },
            {
                name: 'cases',
                icon: 'medkit-outline'
            },
            {
                name: 'account',
                icon: 'person'
            }
        ],
        kymia: [
            {
                name: 'home',
                icon: 'home-outline',
                label: 'Accueil'
            },
            {
                name: 'meetings',
                icon: 'calendar-outline',
                label: 'Rendez-vous'
            },
            {
                name: 'setting',
                icon: 'settings-outline',
                label: 'Paramètres'
            }
        ]
    };

    /**
     * Track tab by name
     *
     * @param _index number: _index of the item
     * @param item TabItem: item to track
     * @returns string: id of the item
     */
    trackTab(_index: number, item: TabItem): string {
        return item.name;
    }
}
