import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { notifyServiceMock } from '@lib/mocks';
import { NotifyService } from 'src/app/services/common/notify.service';
import { PopupComponent } from './popup.component';

describe('PopupComponent', () => {
    let component: PopupComponent;
    let fixture: ComponentFixture<PopupComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [PopupComponent],
            providers: [
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            imports: [IonicModule.forRoot()],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(PopupComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).not.toBeNull();
    });
});
