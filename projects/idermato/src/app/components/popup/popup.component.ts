import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    Input,
    NgZone
} from '@angular/core';
import { IonPopover } from '@ionic/angular';
import { NotifyService } from 'src/app/services/common/notify.service';

@Component({
    selector: 'app-popup',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.scss']
})
export class PopupComponent extends IonPopover {
    @Input() title: string;
    @Input() content: string;
    @Input() color: string;
    @Input() icon: string;

    constructor(
        public notifyService: NotifyService,
        public changeDetectorRef: ChangeDetectorRef,
        public elementRef: ElementRef,
        public ngZone: NgZone
    ) {
        super(changeDetectorRef, elementRef, ngZone);
    }
}
