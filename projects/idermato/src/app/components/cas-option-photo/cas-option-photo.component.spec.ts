import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';
import { CasOptionPhotoComponent } from './cas-option-photo.component';

describe('CasOptionPhotoComponent', () => {
    let component: CasOptionPhotoComponent;
    let fixture: ComponentFixture<CasOptionPhotoComponent>;
    let modalControllerSpy = {
        dismiss: () => Promise.resolve()
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [CasOptionPhotoComponent],
            imports: [IonicModule.forRoot()],
            providers: [
                {
                    provide: ModalController,
                    useValue: modalControllerSpy
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(CasOptionPhotoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('TA2: should end procedure with "backdrop" parameter to close', () => {
        jest.spyOn(modalControllerSpy, 'dismiss');
        component.closeModal();
        expect(modalControllerSpy.dismiss).toHaveBeenCalledWith(
            null,
            'backdrop'
        );
    });

    it('TA3: should choose "photos" type when gallery is selected and close', () => {
        jest.spyOn(modalControllerSpy, 'dismiss');
        component.actionPicture('Photos', 'select');
        expect(modalControllerSpy.dismiss).toHaveBeenCalledWith(
            null,
            'backdrop'
        );
    });
});
