import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-cas-option-photo',
    templateUrl: './cas-option-photo.component.html',
    styleUrls: ['./cas-option-photo.component.scss']
})
export class CasOptionPhotoComponent {
    tabs = [
        {
            name: 'trash',
            label: 'Supprimer',
            role: 'Delete',
            action: 'remove',
            color: 'has-background-danger'
        },
        {
            name: 'images',
            label: 'Parcourir',
            role: 'Photos',
            action: 'select',
            color: 'has-background-blue'
        },
        {
            name: 'camera',
            label: 'Camera',
            role: 'Camera',
            action: 'select',
            color: 'has-background-green'
        }
    ];
    constructor(public modalController: ModalController) {}

    /**
     * Function to close the modal
     */
    closeModal(): void {
        this.modalController.dismiss(null, 'backdrop');
    }

    /**
     * Function to perform an action with a picture.
     *
     * @param type - The type of the picture.
     * @param action - The action to be performed on the picture.
     */
    async actionPicture(type: string, action: string): Promise<void> {
        this.modalController.dismiss(type, action);
    }
}
