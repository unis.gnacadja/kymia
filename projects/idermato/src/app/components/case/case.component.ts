import { Component, Input, OnInit } from '@angular/core';
import { cardMedia } from '@lib/constants';
import { Case, Diagnostic } from 'src/app/models/types/case';
import { caseStatusMessage, iA } from 'src/app/utils/constants';
import { CaseStatus, DiagnosticTabs } from 'src/app/utils/enum';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-case',
    templateUrl: './case.component.html',
    styleUrls: ['./case.component.scss']
})
export class CaseComponent implements OnInit {
    @Input() case: Case;
    environment = environment;
    caseStatus = CaseStatus;
    caseStatusMessage: Record<string, string> = caseStatusMessage;
    name: string;
    casePicture: string;
    predictionAI: number;
    diagnosticTabs = DiagnosticTabs;

    ngOnInit() {
        this.predictionAI = this.case.diagnostics.filter(
            (diagnostic: Diagnostic) => diagnostic.agent === iA
        ).length;
        this.casePicture =
            this.case.medias.length > 0 ? this.case.medias[0].url : cardMedia;
        this.name = `${this.case.agent.prenom} ${this.case.agent.nom}`;
    }

    stopPropagation(event: Event) {
        event.stopPropagation();
    }
}
