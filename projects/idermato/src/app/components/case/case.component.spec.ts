import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { cardMedia } from '@lib/constants';
import { ShortenPipe } from 'ngx-pipes';
import { MessagePipe } from 'src/app/pipes';
import { CaseService } from 'src/app/services/case.service';
import { caseServiceMock, normalizedCaseWithCommentAgent } from 'src/app/tests';
import { CaseComponent } from './case.component';

describe('CaseComponent', () => {
    let component: CaseComponent;
    let fixture: ComponentFixture<CaseComponent>;
    const event = new MouseEvent('click');
    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [CaseComponent, ShortenPipe, MessagePipe],
            imports: [IonicModule.forRoot()],
            providers: [
                {
                    provide: CaseService,
                    useValue: caseServiceMock
                }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(CaseComponent);
        component = fixture.componentInstance;
        component.case = normalizedCaseWithCommentAgent;
        fixture.detectChanges();
    }));

    it('should have default cardMedia when list media is empty', () => {
        component.case.medias = [];
        component.ngOnInit();
        expect(component.casePicture).toBe(cardMedia);
    });

    it('should call stopPropagation on the event object', () => {
        jest.spyOn(event, 'stopPropagation');
        component.stopPropagation(event);
        expect(event.stopPropagation).toHaveBeenCalled();
    });
});
