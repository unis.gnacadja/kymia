import { Component, Input } from '@angular/core';
import { Doctor } from '@kymia/src/app/models/doctor.model';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-doctor-item',
    templateUrl: './doctor-item.component.html',
    styleUrls: ['./doctor-item.component.scss']
})
export class DoctorComponent {
    @Input() doctor: Doctor;
    @Input() link;
    defaultImage: string = environment.user.avatar;
}
