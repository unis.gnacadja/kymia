import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { normalizedDoctor } from '@kymia/src/app/mock';
import { DoctorComponent } from './doctor-item.component';

describe('DoctorComponent', () => {
    let component: DoctorComponent;
    let fixture: ComponentFixture<DoctorComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DoctorComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        });
        fixture = TestBed.createComponent(DoctorComponent);
        component = fixture.componentInstance;
        component.doctor = normalizedDoctor;
        fixture.detectChanges();
    });

    it('should render the component', () => {
        expect(component).not.toBeNull();
    });
});
