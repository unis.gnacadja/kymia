import {
    Component,
    Input,
    OnChanges,
    OnDestroy,
    SimpleChanges
} from '@angular/core';
import { ConsultationStatus } from '@lib/enum';
import { Diary } from 'src/app/models/types/diary';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { statusMessages } from 'src/app/utils/constants';

@Component({
    selector: 'app-diary',
    templateUrl: './diary.component.html',
    styleUrls: ['./diary.component.scss']
})
export class DiaryComponent implements OnChanges, OnDestroy {
    @Input() diary: Diary;
    consultationDate: Date;
    @Input() showPersonalInfo: boolean = false;
    ConsultationStatus = ConsultationStatus;
    statusMessages: Record<string, string> = statusMessages;
    showVideo: boolean = false;
    available: boolean = false;
    patientAge: string;
    timer: ReturnType<typeof setTimeout>;
    info: string = '';

    constructor(
        private readonly normalizeDate: NormalizeDate,
        private readonly normalizeHour: NormalizeHour
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.diary && changes.diary) {
            this.consultationDate = this.normalizeDate.transform(
                this.diary.calendar.availableDate,
                '-'
            );

            this.showVideoCam();

            if (this.showPersonalInfo) {
                const birthdate = this.normalizeDate.transform(
                    this.diary.patient.dateDeNaissance,
                    '-'
                );

                this.patientAge = (
                    new Date().getFullYear() - birthdate.getFullYear()
                ).toString();

                this.info = this.diary.patient.secteurActivite
                    ? `${this.diary.patient.secteurActivite}, ${this.patientAge} ans`
                    : `${this.patientAge} ans`;
            } else {
                this.info = this.diary.problemDescription;
            }
        }
    }

    /**
     * enable the video cam when the consultation meets the requirements
     */
    showVideoCam(): void {
        const availableDateStart = this.normalizeHour.transform(
            this.consultationDate,
            this.diary.calendar.availableDateStart
        );

        const quarterHourMs = 15 * 60 * 1000;
        const diff = this.timeDiffFromNow(availableDateStart);

        this.available = diff <= quarterHourMs && diff >= 0;

        const eligible = this.diary.status === ConsultationStatus.payed;

        if (!this.available && eligible && diff > quarterHourMs) {
            this.timer = setTimeout(() => {
                this.showVideo =
                    this.timeDiffFromNow(availableDateStart) <= quarterHourMs;
            }, diff);
        }
        this.showVideo = this.available && eligible;
    }

    /**
     * get time difference between a date and now
     *
     * @param date The date to compare
     * @returns the time difference between the date and the current time in millisecond
     */

    timeDiffFromNow(date: Date) {
        return date.getTime() - new Date().getTime();
    }

    ngOnDestroy(): void {
        clearTimeout(this.timer);
    }
}
