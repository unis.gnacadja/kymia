import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NavController } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ConsultationStatus } from '@lib/enum';
import * as DiaryMock from 'src/app/mock/diary.json';
import { MessagePipe, NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { UserService } from 'src/app/services/user.service';
import { DiaryComponent } from './diary.component';

describe('DiaryComponent', () => {
    let component: DiaryComponent;
    let fixture: ComponentFixture<DiaryComponent>;

    let navControllerSpy = {
        navigateForward: jest.fn()
    };
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DiaryComponent, MessagePipe],

            providers: [
                UserService,
                NextDate,
                NormalizeHour,
                NormalizeDate,
                { provide: NavController, useValue: navControllerSpy }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        });

        fixture = TestBed.createComponent(DiaryComponent);
        component = fixture.componentInstance;
        component.diary = DiaryMock;
        fixture.detectChanges();
    });

    it('TA2: should display video consultation icon when diary is confirmed and availableDateStart is 15 minutes before the current date time', () => {
        component.diary.status = ConsultationStatus.payed;
        component.diary.calendar.availableDate = '23-05-2025';
        component.diary.calendar.availableDateStart = '16:12:00';
        jest.useFakeTimers().setSystemTime(new Date('2025-05-23T16:00:00'));
        component.ngOnChanges({
            diary: new SimpleChange(null, component.diary, true)
        });
        fixture.detectChanges();
        const videoConsultationIcon = fixture.nativeElement.querySelector(
            '[data-test-id="video-consultation"]'
        );
        expect(videoConsultationIcon).not.toBeNull();
    });

    it('TA3: should automatically display video consultation icon when time is more than 15 minutes before the current date time', () => {
        component.diary.status = ConsultationStatus.payed;
        component.diary.calendar.availableDate = '23-05-2025';
        component.diary.calendar.availableDateStart = '16:25:00';
        jest.useFakeTimers().setSystemTime(new Date('2025-05-23T16:00:00'));
        component.ngOnChanges({
            diary: new SimpleChange(null, component.diary, true)
        });
        jest.advanceTimersByTime(25 * 60 * 1000);
        fixture.detectChanges();
        const videoConsultationIcon = fixture.nativeElement.querySelector(
            '[data-test-id="video-consultation"]'
        );
        expect(videoConsultationIcon).not.toBeNull();
    });

    it('TA4: should show the right patientAge and patient function when showPersonalInfo is false', () => {
        component.showPersonalInfo = true;
        component.ngOnChanges({
            diary: new SimpleChange(null, component.diary, true)
        });
        expect(component.info).toBe(
            `${component.diary.patient.secteurActivite}, ${component.patientAge} ans`
        );
    });

    it('TA5: should show the right patientAge only when showPersonalInfo is false', () => {
        component.showPersonalInfo = true;
        component.diary.patient.secteurActivite = '';
        component.ngOnChanges({
            diary: new SimpleChange(null, component.diary, true)
        });
        expect(component.info).toBe(`${component.patientAge} ans`);
    });

    it('TA8: should clear timeout when component is destroyed', () => {
        const spy = jest.spyOn(window, 'clearTimeout');
        component.ngOnDestroy();
        expect(spy).toHaveBeenCalled();
    });
});
