import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {
    @Input() loading: boolean = false;
    @Input() icon: string = '';
    @Input() iconColor: string = '';
    @Input() iconSize: string = 'size-52';
    @Input() message: string = '';
}
