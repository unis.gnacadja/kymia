import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { NotificationComponent } from './notification.component';

describe('NotificationComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NotificationComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    });

    it('should display the correct message from projected content', () => {
        @Component({
            template:
                '<app-notification><span message>Projected content</span></app-notification>'
        })
        class TestHostComponent {}

        TestBed.configureTestingModule({
            declarations: [NotificationComponent, TestHostComponent]
        }).compileComponents();

        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        const compiled = fixture.nativeElement;

        // Checking if the projected content is correctly displayed
        expect(compiled.querySelector('span')).not.toBeNull();
    });
});
