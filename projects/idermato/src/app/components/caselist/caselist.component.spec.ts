import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { caseServiceMock } from 'src/app/tests';
import { CaselistComponent } from './caselist.component';

describe('CaselistComponent', () => {
    let component: CaselistComponent;
    let fixture: ComponentFixture<CaselistComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [CaselistComponent],
            imports: [IonicModule.forRoot(), HttpClientTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(CaselistComponent);
        component = fixture.componentInstance;
        component.list = caseServiceMock.list;
        fixture.detectChanges();
    }));

    it('should display all the case items when limit is zero', () => {
        component.limit = 0;
        component.ngOnChanges({
            list: new SimpleChange([], component.list, true)
        });
        fixture.detectChanges();
        const elements = fixture.nativeElement.querySelectorAll(
            '[data-test-id="case-item"]'
        );

        expect(elements.length).toBe(component.list.length);
    });

    it('should display only the desired number of case items', () => {
        component.limit = 3;
        component.ngOnChanges({
            list: new SimpleChange([], component.list, true)
        });
        fixture.detectChanges();
        const elements = fixture.nativeElement.querySelectorAll(
            '[data-test-id="case-item"]'
        );

        expect(elements.length).toBe(component.limit);
    });
});
