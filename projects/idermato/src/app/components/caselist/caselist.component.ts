import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Case } from 'src/app/models/types/case';

@Component({
    selector: 'app-caselist',
    templateUrl: './caselist.component.html',
    styleUrls: ['./caselist.component.scss']
})
export class CaselistComponent implements OnChanges {
    @Input() list: Case[] = [];
    @Input() limit: number = 0;
    filteredList: Case[] = [];

    /**
     * Determines the list to be displayed
     */
    ngOnChanges(changes: SimpleChanges): void {
        if (this.list && changes.list) {
            this.filteredList =
                this.limit > 0 ? this.list.slice(0, this.limit) : this.list;
        }
    }

    trackByFn(_: number, item: Case): string {
        return item.id;
    }
}
