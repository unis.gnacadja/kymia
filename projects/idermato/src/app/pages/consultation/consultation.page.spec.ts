import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IonicModule, IonModal, NavController } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ConsultationStatus } from '@lib/enum';
import {
    loadingServiceMock,
    mockActivatedRoute,
    notifyServiceMock
} from '@lib/mocks';
import { of, throwError } from 'rxjs';
import * as CaseMock from 'src/app/mock/case.mock.json';
import * as DiaryResumeMock from 'src/app/mock/diary-resume.json';
import * as DiaryMock from 'src/app/mock/diary.json';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import {
    caseServiceMock,
    consultationServiceMock,
    diaries,
    planningServiceMock,
    userServiceMock
} from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';
import { ConsultationComponent } from './consultation.page';

@Component({
    selector: 'ionModal',
    template: '',
    providers: [
        {
            provide: IonModal,
            useClass: MockIonModal
        }
    ]
})
class MockIonModal {
    onDidDismiss() {
        jest.fn();
    }
}

describe('ConsultationPage', () => {
    let component: ConsultationComponent;
    let fixture: ComponentFixture<ConsultationComponent>;

    const diary = {
        ...DiaryMock
    };
    const routerSpy = {
        navigate: jest.fn()
    };
    const navControllerSpy = {
        navigateForward: jest.fn()
    };

    const input = document.createElement('input');
    input.type = 'file';
    let file = new File(['test content'], 'test.pdf', {
        type: 'application/pdf'
    });
    Object.defineProperty(input, 'files', { value: [file] });

    let event = new CustomEvent('change');
    Object.defineProperty(event, 'target', { value: input });

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ConsultationComponent, MockIonModal],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: ConsultationService,
                    useValue: consultationServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                {
                    provide: PlanningService,
                    useValue: planningServiceMock
                },
                {
                    provide: CaseService,
                    useValue: caseServiceMock
                },
                FormBuilder,
                NextDate,
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: Router, useValue: routerSpy },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                { provide: NavController, useValue: navControllerSpy },
                NormalizeDate,
                NormalizeHour
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(ConsultationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        const originalModule = jest.requireActual('@ionic/angular');
        component.closemodal = {
            ...originalModule,
            onDidDismiss: jest.fn().mockReturnValue(Promise.resolve())
        };
        component.ngOnInit();
    }));

    it(' should load diary from state', fakeAsync(() => {
        tick();
        expect(component.diary).toEqual(diary);
    }));

    it(' should confirm diary successfully', fakeAsync(() => {
        component.loading = true;
        component.confirm();
        tick();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.notifyAvailability,
            messages.thanks,
            State.success
        );
    }));
    it('should throw error when confirm fails', fakeAsync(() => {
        component.loading = false;
        consultationServiceMock.confirm.mockReturnValue(throwError(Error));
        component.confirm();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should set consultationInfo to diaryAvailable when status is payed', () => {
        component.diary.status = ConsultationStatus.payed;
        component.loadConsultation();
        expect(component.consultationInfo).toEqual(messages.diaryAvailable);
    });

    it('should set consultationInfo to mustConfirmDiary when status is waiting', () => {
        component.diary.status = ConsultationStatus.waiting;
        component.loadConsultation();
        expect(component.consultationInfo).toEqual(messages.mustConfirmDiary);
    });

    it('shoud set consultationInfo to empty when status is not payed or waiting', () => {
        component.diary.status = ConsultationStatus.terminated;
        component.loadConsultation();
        expect(component.consultationInfo).toEqual('');
    });

    it('should authorize cancellation if requested less than 2 days before', () => {
        component.diary.calendar.availableDate = '23-05-2025';
        jest.useFakeTimers().setSystemTime(new Date('2025-05-20'));
        component.diary.status = ConsultationStatus.waiting;
        component.loadConsultation();
        expect(component.isCancellable).toBe(true);
    });

    it('should display the file header when files exists', () => {
        let header = fixture.nativeElement.querySelector(
            '[data-test-id="file-header"]'
        );
        expect(header.textContent.trim()).toEqual('Fichiers');
    });

    it('should redirect to meetings info page when goToMeeting is called up', () => {
        jest.spyOn(component.router, 'navigate');
        component.goToMeeting();
        expect(component.router.navigate).toHaveBeenCalledWith(
            ['/tabs/diaries/consultation/meeting-infos'],
            {
                queryParams: { callUrl: DiaryMock.callUrl }
            }
        );
    });

    it('should assign value to diagnostic and finish consultation', fakeAsync(() => {
        component.submitForm.get('description').setValue('Expected diagnostic');
        component.loading = false;
        component.close();
        tick();
        expect(component.diagnostic).toBe('Expected diagnostic');
    }));

    it('should update consultation status to terminated when consultation is terminated successfully', fakeAsync(() => {
        component.submitForm.get('description').setValue('Expected treatment');
        component.loading = false;
        consultationServiceMock.list[0].status = ConsultationStatus.terminated;
        consultationServiceMock.close.mockReturnValue(of(DiaryResumeMock));
        component.close();
        tick();
        expect(consultationServiceMock.list[0].status).toBe(
            component.consultationService.list[0].status
        );
    }));

    it('should set isOpen to true when openModal is called without a doctor', () => {
        component.openCloseModal(true);
        expect(component.isOpen).toBe(true);
    });

    it('should update consultation by uploading prescription', async () => {
        const mock = {
            ...diaries[0],
            status: ConsultationStatus.terminated
        };
        consultationServiceMock.getConsultation.mockReturnValue(of(mock));

        await component.loadPrescription(event);
        expect(
            component.consultationService.setConsultation
        ).toHaveBeenCalledWith(mock);
    });

    it('should not update consultation if no file is uploaded', async () => {
        consultationServiceMock.list = diaries;
        consultationServiceMock.reportConsultation.mockReturnValue(
            throwError('Error')
        );

        await component.loadPrescription(event);
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });

    it('should not update consultation if file is not accepted', async () => {
        consultationServiceMock.list = diaries;
        consultationServiceMock.reportConsultation.mockReturnValue(
            throwError('Error')
        );

        file = new File(['test content'], 'test.mp4', { type: 'image/mp4' });
        Object.defineProperty(input, 'files', { value: [file] });

        event = new CustomEvent('change');

        Object.defineProperty(event, 'target', { value: input });

        await component.loadPrescription(event);
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
            message: messages.fileNotAccepted,
            duration: 3000
        });
    });

    it('should submit case successfully', async () => {
        routerSpy.navigate.mockClear();
        jest.spyOn(component.router, 'navigate');
        await component.submitCase();
        expect(component.router.navigate).toHaveBeenCalledWith(
            ['/tabs/cases/detail-cas'],
            {
                queryParams: {
                    caseId: CaseMock.id
                }
            }
        );
    });

    it('should throw error when submitCase fails', async () => {
        caseServiceMock.save.mockReturnValue(throwError('Error'));
        await component.submitCase();
        expect(component.notifyService.presentToast).toHaveBeenCalled();
    });
});
