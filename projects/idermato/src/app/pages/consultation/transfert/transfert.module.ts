import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { TransfertPageRoutingModule } from './transfert-routing.module';
import { TransfertComponent } from './transfert.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TransfertPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [TransfertComponent]
})
export class TransfertPageModule {}
