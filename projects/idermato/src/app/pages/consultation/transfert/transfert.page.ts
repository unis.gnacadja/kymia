import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IonModal, NavController } from '@ionic/angular';
import { Doctor } from '@kymia/src/app/models/doctor.model';
import { NextDate } from '@kymia/src/app/pipe';
import { DoctorService } from '@kymia/src/app/services/doctor.service';
import { UserService as transfertService } from '@kymia/src/app/services/user.service';
import { ConsultationStatus } from '@lib/enum';
import { dataPattern } from '@lib/MustMatch.validator';
import { normalizeDoctor } from '@lib/normalizer';
import { finalize } from 'rxjs/operators';
import { Diary } from 'src/app/models/types/diary';
import { TypeError } from 'src/app/models/types/typeError';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { errorMessages, messages } from 'src/app/utils/constants';

@Component({
    selector: 'app-transfert',
    templateUrl: './transfert.page.html',
    styleUrls: ['./transfert.page.scss'],
    providers: [NextDate, DoctorService, transfertService, DatePipe]
})
export class TransfertComponent implements OnInit {
    loadDoctor: boolean = true;
    btnloading: boolean = false;
    list: Doctor[] = [];
    searchForm: FormGroup;
    diary: Diary;
    submitError: boolean = false;
    modalTitle: string = messages.reason;
    modalContent: string;
    reasonForm: FormGroup;
    errorMessages: TypeError[];
    errors: Record<string, TypeError> = errorMessages;
    textValidator: ValidatorFn = Validators.pattern(dataPattern.text);
    requiredValidator: ValidatorFn = Validators.required;
    selectedDoctor: Doctor;
    isOpen: boolean = false;
    @ViewChild(IonModal) cancelmodal: IonModal;

    constructor(
        private readonly consultationService: ConsultationService,
        private readonly navCtrl: NavController,
        private readonly formBuilder: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.searchForm = this.formBuilder.group({
            search: ['']
        });
        this.reasonForm = this.formBuilder.group(
            {
                reason: ['']
            },
            {
                updateOn: 'blur'
            }
        );
        this.activatedRoute.queryParams.subscribe((params) => {
            this.diary = this.consultationService.searchDiary(
                params['diaryId']
            );
            this.getDoctor();
        });
    }

    /**
     * Fetches available doctors for transfer
     */
    getDoctor() {
        this.consultationService
            .getAvailableDoctor(this.diary.id)
            .pipe(
                finalize(() => {
                    this.loadDoctor = false;
                })
            )
            .subscribe({
                next: (availabilities: any[]) => {
                    this.list = availabilities.map((availability: any) =>
                        normalizeDoctor({
                            ...availability.agent,
                            nextCalendar: availability.calendar
                        })
                    );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Opens the modal for transfer or cancellation
     *
     * @param doctor - The selected doctor for transfer, or null for cancellation
     */
    openModal(doctor: Doctor | null = null): void {
        this.selectedDoctor = doctor;
        this.isOpen = true;
    }

    /**
     * Handles form submission for transfer or cancellation
     */
    onSubmit() {
        this.reasonForm
            .get('reason')
            .setValue(this.reasonForm.value.reason.trim());
        if (this.reasonForm.valid) {
            this.submitError = false;
            this.btnloading = true;
            if (this.selectedDoctor) {
                this.transferConsultation();
            } else {
                this.cancelConsultation();
            }
        }
    }

    /**
     * Transfers the consultation to another doctor
     */
    transferConsultation() {
        this.consultationService
            .transfertConsultation(
                this.diary.id,
                this.selectedDoctor.nextCalendar.id,
                this.selectedDoctor.id
            )
            .subscribe({
                next: () => {
                    this.handleResponse(true);
                    const consultationIndex =
                        this.consultationService.list.findIndex(
                            (consultation) => consultation.id === this.diary.id
                        );
                    this.consultationService.list.splice(consultationIndex, 1);
                },
                error: () => {
                    this.handleResponse(false);
                }
            });
    }

    /**
     * Cancels the consultation
     */
    cancelConsultation() {
        this.consultationService
            .canceledConsultation(this.diary.id, this.reasonForm.value.reason)
            .subscribe({
                next: () => {
                    this.handleResponse(true);
                    const consultationIndex =
                        this.consultationService.list.findIndex(
                            (consultation) => consultation.id === this.diary.id
                        );
                    this.consultationService.list[consultationIndex].status =
                        ConsultationStatus.annuled;
                },
                error: () => {
                    this.handleResponse(false);
                }
            });
    }

    /**
     * Handles the response after transfer or cancellation
     *
     * @param success - Indicates whether the operation was successful
     */
    handleResponse(success: boolean) {
        this.btnloading = false;
        if (success) {
            this.diary.id = null;
            this.modalTitle = messages.thanks;
            this.modalContent = messages.notifyTransfer;
            this.reasonForm.get('reason').setValue('');
        } else {
            this.submitError = true;
            this.modalTitle = messages.warning;
            this.modalContent = messages.errorOccured;
        }
    }

    /**
     * Closes the modal and resets related properties
     */
    closeModal(): void {
        this.isOpen = false;
        this.selectedDoctor = null;
        this.modalTitle = messages.reason;
        this.modalContent = '';
    }

    /**
     * Navigates to the diaries page after modal dismissal
     */
    goToDiaries() {
        this.isOpen = false;
        this.cancelmodal.onDidDismiss().then(() => {
            this.navCtrl.navigateRoot('/tabs/diaries');
        });
    }
}
