import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransfertComponent } from './transfert.page';

const routes: Routes = [
    {
        path: '',
        component: TransfertComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransfertPageRoutingModule {}
