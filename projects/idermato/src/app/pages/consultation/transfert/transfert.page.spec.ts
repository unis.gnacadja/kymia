import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IonicModule, IonModal, NavController } from '@ionic/angular';
import { normalizedDoctor } from '@kymia/src/app/mock';
import { mockActivatedRoute, notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import * as DiaryMock from 'src/app/mock/diary.json';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { consultationServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { TransfertComponent } from './transfert.page';

@Component({
    selector: 'ionModal',
    template: '',
    providers: [
        {
            provide: IonModal,
            useClass: MockIonModal
        }
    ]
})
class MockIonModal {
    onDidDismiss() {
        jest.fn();
    }
}

describe('TransfertPage', () => {
    let component: TransfertComponent;
    let fixture: ComponentFixture<TransfertComponent>;
    let navControllerSpy = {
        navigateRoot: jest.fn()
    };
    mockActivatedRoute.queryParams = of({ diaryId: DiaryMock.id });

    beforeAll(() => {
        TestBed.configureTestingModule({
            declarations: [TransfertComponent, MockIonModal],
            imports: [IonicModule.forRoot(), ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: ConsultationService,
                    useValue: consultationServiceMock
                },
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: NavController, useValue: navControllerSpy },

                { provide: NotifyService, useValue: notifyServiceMock }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(TransfertComponent);
        component = fixture.componentInstance;
        const formBuilder = TestBed.inject(FormBuilder);
        component.searchForm = formBuilder.group({
            search: ['']
        });
        fixture.detectChanges();
    });

    it('should display search Input when list length is greater than 10', fakeAsync(() => {
        let searchForm: HTMLElement = fixture.nativeElement.querySelector(
            '[data-test-id="form"]'
        );
        expect(searchForm).not.toBeNull;
    }));

    it('should notify when doctor information cannot be retrieved', fakeAsync(() => {
        consultationServiceMock.getAvailableDoctor.mockReturnValueOnce(
            throwError(messages.error)
        );
        jest.spyOn(consultationServiceMock, 'getAvailableDoctor');
        component.diary = DiaryMock;
        component.getDoctor();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should set selectedDoctor when openModal is called with a doctor', () => {
        component.openModal(normalizedDoctor);
        component.isOpen = true;
        expect(component.selectedDoctor).toEqual(normalizedDoctor);
    });

    it('should set isOpen to true when openModal is called without a doctor', () => {
        component.isOpen = true;
        component.openModal();
        expect(component.isOpen).toBe(true);
    });

    it('should set isOpen to false when openModal is called without or with a doctor', () => {
        component.isOpen = false;
        component.closeModal();
        expect(component.isOpen).toBe(false);
    });

    it('should  call userService.transfertConsultation when selectedDoctor is  defined and form is valid', fakeAsync(() => {
        component.selectedDoctor = normalizedDoctor;
        component.reasonForm.controls.reason.setValue('urgence');
        component.btnloading = false;
        component.loadDoctor = false;
        component.isOpen = true;
        component.modalContent = '';
        component.diary = DiaryMock;
        jest.useFakeTimers();
        component.onSubmit();
        jest.runAllTimers();
        tick();
        expect(component.modalTitle).toEqual('Merci !');
    }));

    it('should  call userService.transfertConsultation when selectedDoctor and something is wrong', fakeAsync(() => {
        component.selectedDoctor = normalizedDoctor;
        component.reasonForm.controls.reason.setValue('Expected reason');
        component.btnloading = false;
        component.isOpen = true;
        component.modalContent = '';
        consultationServiceMock.transfertConsultation.mockReturnValue(
            throwError(messages.error)
        );
        component.onSubmit();
        tick();
        expect(component.modalTitle).toEqual(messages.warning);
    }));
    it('should call userService.canceledConsultation when selectedDoctor is not defined', () => {
        component.selectedDoctor = undefined;
        component.loadDoctor = false;
        component.isOpen = true;
        component.onSubmit();
        expect(component.modalTitle).toEqual(messages.thanks);
    });
    it('should  call userService.canceledConsultation when selectedDoctor and something is wrong', fakeAsync(() => {
        component.reasonForm.controls.reason.setValue('Expected reason');
        component.selectedDoctor = null;
        component.btnloading = false;
        component.isOpen = true;
        component.modalTitle = '';
        component.modalContent = '';
        consultationServiceMock.canceledConsultation.mockReturnValue(
            throwError(messages.error)
        );
        component.onSubmit();
        tick();
        expect(component.modalTitle).toEqual(messages.warning);
    }));
    it('should redirect to the diaries page', fakeAsync(() => {
        jest.spyOn(navControllerSpy, 'navigateRoot');
        component.isOpen = false;
        const originalModule = jest.requireActual('@ionic/angular');
        component.cancelmodal = {
            ...originalModule,
            onDidDismiss: () => Promise.resolve() as unknown as IonModal
        };
        component.goToDiaries();
        tick();
        expect(navControllerSpy.navigateRoot).toHaveBeenCalledWith(
            '/tabs/diaries'
        );
    }));
});
