import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultationComponent } from './consultation.page';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ConsultationComponent
            },
            {
                path: 'transfert',
                loadChildren: () =>
                    import('./transfert/transfert.module').then(
                        (m) => m.TransfertPageModule
                    )
            },
            {
                path: 'meeting-infos',
                loadChildren: () =>
                    import('./meeting-infos/meeting-infos.module').then(
                        (m) => m.MeetingInfosPageModule
                    )
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConsultationPageRoutingModule {}
