import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ShortenPipe } from 'ngx-pipes';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { ConsultationPageRoutingModule } from './consultation-routing.module';
import { ConsultationComponent } from './consultation.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ConsultationPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [NextDate, NormalizeDate, NormalizeHour, ShortenPipe],
    declarations: [ConsultationComponent]
})
export class ConsultationPageModule {}
