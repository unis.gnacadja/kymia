import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { messages } from 'src/app/utils/constants';
import { Color } from 'src/app/utils/enum';

@Component({
    selector: 'app-meeting-infos',
    templateUrl: './meeting-infos.page.html',
    styleUrls: ['./meeting-infos.page.scss']
})
export class MeetingInfosComponent implements OnInit {
    callUrl: string;
    hasStarted: boolean = false;
    list = [
        {
            icon: 'phone-portrait-outline',
            description: messages.checkPhoneEquipments,
            color: 'blue-light'
        },
        {
            icon: 'warning',
            description: messages.reassurePatient,
            color: Color.warning
        },
        {
            icon: 'shield-half-outline',
            description: messages.safeDiary,
            color: Color.primary
        }
    ];
    constructor(
        private activatedRoute: ActivatedRoute,
        private location: Location,
        private platform: Platform
    ) {}

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.callUrl = params['callUrl'];
        });
        this.platform.resume.subscribe(() => {
            if (this.hasStarted) {
                this.location.back();
            }
        });
    }

    startZoom() {
        this.hasStarted = true;
        window.open(this.callUrl, '_system');
    }
}
