import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { MeetingInfosPageRoutingModule } from './meeting-infos-routing.module';
import { MeetingInfosComponent } from './meeting-infos.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MeetingInfosPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [NextDate],
    declarations: [MeetingInfosComponent]
})
export class MeetingInfosPageModule {}
