import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeetingInfosComponent } from './meeting-infos.page';

const routes: Routes = [
    {
        path: '',
        component: MeetingInfosComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MeetingInfosPageRoutingModule {}
