import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { IonicModule, Platform } from '@ionic/angular';
import { mockActivatedRoute } from '@lib/mocks';
import { of } from 'rxjs';
import * as DiaryMock from 'src/app/mock/diary.json';
import { locationMock, platformMock } from 'src/app/tests';
import { MeetingInfosComponent } from './meeting-infos.page';

describe('MeetingInfosComponent', () => {
    let component: MeetingInfosComponent;
    let fixture: ComponentFixture<MeetingInfosComponent>;

    beforeAll(waitForAsync(() => {
        mockActivatedRoute.queryParams = of({ callUrl: DiaryMock.callUrl });

        TestBed.configureTestingModule({
            declarations: [MeetingInfosComponent],
            imports: [IonicModule.forRoot()],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: Location, useValue: locationMock },
                { provide: Platform, useValue: platformMock }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MeetingInfosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('startZoom should set hasStarted to true and call window.open', () => {
        window.open = jest.fn();
        component.startZoom();
        expect(component.hasStarted).toBe(true);
        expect(window.open).toHaveBeenCalledWith(DiaryMock.callUrl, '_system');
    });

    it('window.onfocus should call location.back if hasStarted is true', () => {
        component.hasStarted = true;
        platformMock.resume.toPromise();
        component.ngOnInit();
        expect(locationMock.back).toHaveBeenCalled();
    });
});
