import { Component, OnInit, ViewChild } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IonModal } from '@ionic/angular';
import { acceptedFileTypes, helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { dataPattern } from '@lib/MustMatch.validator';
import { Media } from '@lib/types';
import { finalize } from 'rxjs/operators';
import { DiaryComponent } from 'src/app/components/diary/diary.component';
import { Diary } from 'src/app/models/types/diary';
import { TypeError } from 'src/app/models/types/typeError';
import { NormalizeDate } from 'src/app/pipes';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { errorMessages, messages } from 'src/app/utils/constants';
import { CaseStatus, State } from 'src/app/utils/enum';

@Component({
    selector: 'app-consultation',
    templateUrl: './consultation.page.html',
    styleUrls: ['./consultation.page.scss']
})
export class ConsultationComponent implements OnInit {
    loading: boolean = false;
    diary: Diary;
    isCancellable: boolean = false;
    ConsultationStatus = ConsultationStatus;
    listMedia: Media[] = [];
    consultationInfo: string = '';
    errors: Record<string, TypeError> = errorMessages;
    requiredValidator: ValidatorFn = Validators.required;
    textValidator: ValidatorFn = Validators.pattern(dataPattern.text);
    submitForm: FormGroup;
    consultationForm: FormGroup;
    submitError: boolean = false;
    diagnostic: string = '';
    modalTitle: string = messages.askDiagnostic;
    buttonTitle: string = messages.next;
    isOpen: boolean = false;
    isWaiting: boolean = false;
    showFooter: boolean = false;
    @ViewChild('diaryComponent') diaryComponent: DiaryComponent;
    @ViewChild(IonModal) closemodal: IonModal;
    acceptedFileTypes: string = acceptedFileTypes;

    constructor(
        public router: Router,
        public consultationService: ConsultationService,
        public normalizeDate: NormalizeDate,
        public caseService: CaseService,
        public formBuilder: FormBuilder,
        public activatedRoute: ActivatedRoute,
        public notifyService: NotifyService,
        public loadingService: LoadingService
    ) {}

    ngOnInit(): void {
        this.consultationForm = this.formBuilder.group({
            file: ['']
        });
        this.activatedRoute.queryParams.subscribe((params) => {
            this.diary = this.consultationService.searchDiary(
                params['diaryId']
            );
            this.loadConsultation();
        });
        this.submitError = false;
        this.submitForm = this.formBuilder.group(
            {
                description: ['']
            },
            {
                updateOn: 'blur'
            }
        );
        this.listMedia = this.diary.images.map((media) => ({
            src: media,
            alt: this.diary.problemDescription,
            title: this.diary.problemDescription
        }));
    }

    /**
     * load consultation
     */
    loadConsultation() {
        if (this.diary.status === ConsultationStatus.payed) {
            this.consultationInfo = messages.diaryAvailable;
        } else if (this.diary.status === ConsultationStatus.waiting) {
            this.consultationInfo = messages.mustConfirmDiary;
        } else {
            this.consultationInfo = '';
        }

        this.isWaiting =
            this.diary.status === ConsultationStatus.waitingDiagnostic ||
            this.diary.status === ConsultationStatus.terminated;

        // Authorize the cancellation of the consultation when requested less than 2 days before
        const dateRdv: Date = this.normalizeDate.transform(
            this.diary.calendar.availableDate,
            '-'
        );

        dateRdv.setDate(dateRdv.getDate() - 2);

        this.isCancellable =
            dateRdv > new Date() &&
            this.diary.status === ConsultationStatus.waiting;

        this.showFooter =
            this.isCancellable ||
            this.diary.status === ConsultationStatus.waiting ||
            this.diary.status === ConsultationStatus.waitingDiagnostic ||
            (this.diary.status === ConsultationStatus.terminated &&
                !this.diary.idCase) ||
            this.diary.status === ConsultationStatus.payed;
    }

    /**
     * Confirm the consultation
     */
    async confirm(): Promise<void> {
        this.loading = true;
        this.consultationService
            .confirm(this.diary.id)
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe({
                next: () => {
                    this.notifyUser();
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Finish consultation
     */
    close() {
        this.submitError = false;
        this.submitForm
            .get('description')
            .setValue(this.submitForm.controls['description'].value.trim());
        if (this.submitForm.get('description').valid) {
            if (this.diagnostic) {
                this.loading = true;
                const terminateConsultationDto = {
                    descriptionClinique: this.diary.problemDescription,
                    diagnostique: this.diagnostic,
                    traitement: this.submitForm.get('description').value
                };
                this.consultationService
                    .close(this.diary.id, terminateConsultationDto)
                    .pipe(
                        finalize(() => {
                            this.isOpen = false;
                            this.loading = false;
                        })
                    )
                    .subscribe({
                        next: () => {
                            this.closemodal.onDidDismiss().then(() => {
                                this.submitForm.get('description').setValue('');
                                this.getConsultation();
                            });
                        }
                    });
            } else {
                this.diagnostic = this.submitForm.get('description').value;
                this.submitForm.get('description').setValue('');
                this.modalTitle = messages.askTreatment;
                this.buttonTitle = messages.submit;
            }
        }
    }

    openCloseModal(isOpen: boolean): void {
        this.isOpen = isOpen;
    }

    goToMeeting() {
        this.router.navigate(['/tabs/diaries/consultation/meeting-infos'], {
            queryParams: { callUrl: this.diary.callUrl }
        });
    }

    /**
     * Update the consultation by uploading the prescription
     *
     * @param event Event
     */
    async loadPrescription(event: Event): Promise<void> {
        await this.loadingService.present(helperMessages.loading);

        const input = event.target as HTMLInputElement;

        if (input.files && input.files.length > 0) {
            const file = input.files[0];

            if (this.acceptedFileTypes.includes(file.type)) {
                const formData = new FormData();
                formData.append('ordonnance', input.files[0]);

                this.consultationService
                    .reportConsultation(this.diary.id, formData)
                    .pipe(
                        finalize(() => {
                            this.loadingService.dismiss();
                        })
                    )
                    .subscribe({
                        next: () => {
                            this.notifyUser();
                        },
                        error: () => {
                            this.notifyService.presentToast();
                        }
                    });
            } else {
                this.loadingService.dismiss();
                this.notifyService.presentToast({
                    message: messages.fileNotAccepted,
                    duration: 3000
                });
            }
        }
    }

    /**
     * Get consultation
     */
    async getConsultation() {
        this.consultationService
            .getConsultation(this.diary.id)
            .subscribe((data: any) => {
                this.consultationService.setConsultation(data);
            });
    }

    /**
     * notify user
     */
    notifyUser(): void {
        this.getConsultation();
        this.notifyService.setPopover(
            true,
            messages.notifyAvailability,
            messages.thanks,
            State.success
        );
    }

    /**
     * Submit case
     */
    async submitCase(): Promise<void> {
        let caseId: string;

        try {
            this.loading = true;

            const res = await this.caseService.save(this.diary.id).toPromise();
            caseId = res.id;
        } catch {
            this.notifyService.presentToast();
        }

        if (caseId) {
            await this.caseService
                .updateStatus(caseId, CaseStatus.open)
                .toPromise();
            await this.getConsultation();
            await this.caseService.getAll();
            this.router.navigate(['/tabs/cases/detail-cas'], {
                queryParams: { caseId }
            });
        }

        this.loading = false;
    }
}
