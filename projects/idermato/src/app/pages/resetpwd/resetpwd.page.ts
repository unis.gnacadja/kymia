import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { cardMedia } from '@lib/constants';
import { dataPattern } from '@lib/MustMatch.validator';
import { finalize } from 'rxjs/internal/operators/finalize';
import { TypeError } from 'src/app/models/types/typeError';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService as ResetPasswordService } from 'src/app/services/user.service';
import { errorMessages, messages } from 'src/app/utils/constants';

@Component({
    selector: 'app-resetpwd',
    templateUrl: './resetpwd.page.html',
    styleUrls: ['./resetpwd.page.scss']
})
export class ResetPasswordPage implements OnInit {
    resetPasswordForm: FormGroup;
    submitError: string = '';
    loading: boolean = false;
    errors: Record<string, TypeError> = errorMessages;
    cardMedia = cardMedia;
    requiredValidator: ValidatorFn = Validators.required;
    emailValidators: ValidatorFn[] = [
        Validators.required,
        Validators.pattern(dataPattern.email),
        Validators.pattern(dataPattern.noSpace)
    ];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private notifyService: NotifyService,
        private resetPasswordService: ResetPasswordService
    ) {}

    ngOnInit(): void {
        this.resetPasswordForm = this.formBuilder.group(
            {
                email: ['']
            },
            {
                updateOn: 'blur'
            }
        );
    }

    async onReset() {
        this.resetPasswordForm
            .get('email')
            .setValue(this.resetPasswordForm.value.email.trim());
        if (this.resetPasswordForm.valid) {
            this.submitError = '';
            this.loading = true;
            this.resetPasswordService
                .resetPasswordAgent(this.resetPasswordForm.value.email)
                .pipe(
                    finalize(() => {
                        this.loading = false;
                    })
                )
                .subscribe({
                    next: () => {
                        this.notifyService.setPopover(
                            true,
                            messages.checkMailBox
                        );
                        this.notifyService.closeEvent$.subscribe(() => {
                            this.router.navigate(['/login']);
                        });
                    },
                    error: (error) => {
                        this.submitError = error.error.message;
                    }
                });
        }
    }
}
