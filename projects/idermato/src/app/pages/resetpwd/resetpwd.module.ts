import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { ResetPasswordPageRoutingModule } from './resetpwd-routing.module';
import { ResetPasswordPage } from './resetpwd.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        ResetPasswordPageRoutingModule
    ],
    providers: [],
    declarations: [ResetPasswordPage]
})
export class ResetPasswordPageModule {}
