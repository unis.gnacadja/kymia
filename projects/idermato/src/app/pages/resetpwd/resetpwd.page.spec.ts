import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { IonicModule, NavController } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService as ResetPasswordService } from 'src/app/services/user.service';
import { navControllerMock, routerMock, userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { ResetPasswordPage } from './resetpwd.page';

let user = {
    email: MockUser.simpleIdermato.email
};

describe('ResetPasswordPage', () => {
    let component: ResetPasswordPage;
    let fixture: ComponentFixture<ResetPasswordPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ResetPasswordPage],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: ResetPasswordService,
                    useValue: userServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                { provide: Router, useValue: routerMock },
                {
                    provide: NavController,
                    useValue: navControllerMock
                },
                FormBuilder
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ResetPasswordPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should notify on successful form submission', fakeAsync(() => {
        component.resetPasswordForm.setValue(user);
        component.loading = false;
        component.onReset();
        tick();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.checkMailBox
        );
    }));

    it('should display error message on form submission error', fakeAsync(() => {
        userServiceMock.resetPasswordAgent.mockReturnValue(
            throwError({ error: { message: messages.updateFailed } })
        );
        component.resetPasswordForm.setValue(user);
        component.loading = false;
        component.onReset();
        tick();
        expect(component.submitError).toBe(messages.updateFailed);
    }));

    it('should navigate to the login page when closeEvent$ emits', fakeAsync(() => {
        jest.spyOn(userServiceMock, 'resetPasswordAgent').mockReturnValue(
            of({})
        );
        jest.spyOn(routerMock, 'navigate');
        component.resetPasswordForm.setValue(user);
        component.loading = false;
        component.onReset();
        tick();
        expect(routerMock.navigate).toHaveBeenCalledWith(['/login']);
    }));
});
