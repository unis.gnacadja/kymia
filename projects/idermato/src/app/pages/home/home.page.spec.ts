import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { loadingServiceMock } from '@lib/mocks';
import { of } from 'rxjs';
import * as MockCalendars from 'src/app/mock/calendars.json';
import * as CaseMock from 'src/app/mock/case.mock.json';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import {
    caseServiceMock,
    consultationServiceMock,
    diaries,
    planningServiceMock,
    userServiceMock
} from 'src/app/tests';
import { CaseStatus } from 'src/app/utils/enum';
import { normalizeCase, normalizeDiary } from 'src/app/utils/normalizer';
import { HomePage } from './home.page';

describe('HomePage', () => {
    let component: HomePage;
    let fixture: ComponentFixture<HomePage>;

    userServiceMock.user = MockUser.completeProfile;

    const normalizeCased = normalizeCase(CaseMock);

    let openCasesInService = Array(10).fill({
        ...normalizeCased,
        status: CaseStatus.open
    });

    caseServiceMock.list = [
        {
            ...normalizeCased,
            status: CaseStatus.close
        },
        ...openCasesInService
    ];

    const consultationList = [normalizeDiary(diaries[0])];
    consultationServiceMock.list = [diaries[0]];

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HomePage],
            imports: [IonicModule.forRoot()],
            providers: [
                { provide: UserService, useValue: userServiceMock },
                {
                    provide: ConsultationService,
                    useValue: consultationServiceMock
                },
                { provide: PlanningService, useValue: planningServiceMock },
                { provide: CaseService, useValue: caseServiceMock },
                { provide: LoadingService, useValue: loadingServiceMock }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(HomePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.ionViewWillEnter();
    });

    describe('On init', () => {
        it('should set hasCalendar to true if the user has a calendar', fakeAsync(() => {
            planningServiceMock.searchCalendars.mockReturnValue(
                of([MockCalendars])
            );
            component.ionViewWillEnter();
            tick();
            expect(component.hasCalendar).toBe(true);
        }));

        it('should set hasCalendar to false if the user has no calendar', fakeAsync(() => {
            planningServiceMock.searchCalendars.mockReturnValue(of([]));
            component.ionViewWillEnter();
            tick();
            expect(component.hasCalendar).toBe(false);
        }));

        it('should get consultation list', () => {
            expect(component.consultationService.list).toEqual(
                consultationList
            );
        });

        it('should have only open cases', () => {
            const openCasesInComponent = component.caseList.filter(
                (item) => item.status === CaseStatus.open
            );
            expect(openCasesInComponent).toEqual(openCasesInService);
        });
    });

    it('should only have open cases when returning to the page', () => {
        openCasesInService = Array(9).fill({
            ...normalizeCased,
            status: CaseStatus.open
        });
        caseServiceMock.list = [
            {
                ...normalizeCased,
                status: CaseStatus.close
            },
            ...openCasesInService
        ];
        component.ionViewWillEnter();
        const openCases = component.caseList.filter(
            (item) => item.status === CaseStatus.open
        );
        expect(openCases).toEqual(openCasesInService);
    });
});
