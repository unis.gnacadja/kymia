import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ShortenPipe } from 'ngx-pipes';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { HomePageRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';

@NgModule({
    imports: [
        SharedModule,
        IonicModule,
        CommonModule,
        FormsModule,
        HomePageRoutingModule
    ],
    declarations: [HomePage],
    providers: [NextDate, NormalizeDate, NormalizeHour, ShortenPipe]
})
export class HomePageModule {}
