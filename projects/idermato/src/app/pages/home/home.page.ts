import { Component } from '@angular/core';
import { helperMessages } from '@lib/constants';
import { finalize } from 'rxjs/operators';
import { Case } from 'src/app/models/types/case';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { ConsultationService } from 'src/app/services/consultation.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { CaseStatus } from 'src/app/utils/enum';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage {
    loading = {
        consultation: true,
        calendar: true,
        case: true
    };
    messagesInfos: Record<string, string> = messages;
    caseList: Case[] = [];
    hasCalendar: boolean = false;

    constructor(
        public consultationService: ConsultationService,
        public planningService: PlanningService,
        public userService: UserService,
        public caseService: CaseService,
        private loadingService: LoadingService
    ) {}

    ionViewWillEnter(): void {
        this.detailsFetch();
        this.filterOpenCases();
    }

    /**
     * Verify if the user has a calendar
     * Get the list of diaries and open cases
     */
    async detailsFetch(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);
        this.planningService
            .searchCalendars(this.userService.user.id, 0, 5)
            .pipe(
                finalize(async () => {
                    this.loading.calendar = false;
                    await this.getDiaries();
                })
            )
            .subscribe({
                next: (data: any[]) => {
                    this.hasCalendar = data.length > 0;
                }
            });
    }

    /**
     * Get the list of diaries
     */
    async getDiaries(): Promise<void> {
        this.consultationService
            .searchConsultations(this.userService.user.id, 0, 5)
            .pipe(
                finalize(async () => {
                    this.loading.consultation = false;
                    await this.getOpenCases();
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (data: any[]) => {
                    this.consultationService.setConsultations(data);
                }
            });
    }

    /**
     * get the open cases
     */
    async getOpenCases(): Promise<void> {
        await this.caseService.getAll();
        this.filterOpenCases();
        this.loading.case = false;
    }

    /**
     * Filters a list of cases to only include cases with status 'open'.
     */
    private filterOpenCases(): void {
        this.caseList = this.caseService.list.filter(
            (item) => item.status === CaseStatus.open
        );
    }
}
