import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { NavController } from '@ionic/angular';
import { cardMedia } from '@lib/constants';
import { dataPattern } from '@lib/MustMatch.validator';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { TypeError } from 'src/app/models/types/typeError';
import { User } from 'src/app/models/types/user';
import { UserService } from 'src/app/services/user.service';
import { errorMessages } from '../../utils/constants';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    isServiceAvailable: boolean = false;
    loading: boolean = false;
    submit: BehaviorSubject<boolean>;
    errors: Record<string, TypeError> = errorMessages;
    cardMedia = cardMedia;
    requiredValidator: ValidatorFn = Validators.required;
    emailValidators: ValidatorFn[] = [
        Validators.required,
        Validators.pattern(dataPattern.email)
    ];
    passwordValidators: ValidatorFn[] = [
        Validators.required,
        Validators.pattern(dataPattern.noSpace)
    ];
    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    inputType: string = 'password';

    constructor(
        public userService: UserService,
        private formBuilder: FormBuilder,
        private nav: NavController
    ) {}

    ngOnInit(): void {
        this.submit = new BehaviorSubject<boolean>(false);
        this.loginForm = this.formBuilder.group(
            {
                email: [''],
                password: ['']
            },
            {
                updateOn: 'blur'
            }
        );
    }

    get f(): { [key: string]: AbstractControl } {
        return this.loginForm.controls;
    }

    onSubmit(): void {
        this.isServiceAvailable = false;
        this.error.next(null);
        this.loginForm.controls['email'].setValue(
            this.loginForm.controls['email'].value.trim()
        );
        this.loginForm.controls['password'].setValue(
            this.loginForm.controls['password'].value.trim()
        );
        if (!this.submit.getValue()) {
            this.submit.next(true);
        }
        if (
            this.submit.getValue() &&
            !this.loading &&
            this.loginForm.get('email').valid &&
            this.loginForm.get('password').valid
        ) {
            this.loading = true;
            this.userService
                .login({
                    email: this.loginForm.get('email').value.trim(),
                    password: this.loginForm.get('password').value.trim()
                })
                .pipe(
                    finalize(() => {
                        this.loading = false;
                        this.submit.next(false);
                    })
                )
                .subscribe(
                    (user: User) => {
                        this.userService.isLogged = true;
                        this.userService.setUser(user);
                        this.nav.navigateRoot(
                            this.userService.user.firstConnection
                                ? '/newpwd'
                                : '/tabs'
                        );
                    },

                    (errors) => {
                        if (errors.status === 403) {
                            this.error.next(errors.error.message);
                        } else {
                            this.isServiceAvailable = true;
                        }
                    }
                );
        }
    }
    toggleShowPassword(): void {
        this.inputType = this.inputType === 'text' ? 'password' : 'text';
    }
}
