import { HttpClient, HttpHandler } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { IonicModule, NavController } from '@ionic/angular';
import { StockageService } from '@kymia/src/app/services/stockage.service';
import { of, throwError } from 'rxjs';
import * as MockUser from 'src/app/mock/user.json';
import { UserService } from 'src/app/services/user.service';
import { LoginComponent } from './login.page';

describe('LoginPage', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let userServiceSpy = {
        login() {
            return of(MockUser.loggedUser);
        },
        getUserInfo() {
            return of();
        },
        setUser(value) {
            this.user = value;
        }
    };
    let navControllerSpy = {
        navigateRoot(args) {
            return {};
        }
    };
    let computeLink: HTMLElement;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [IonicModule.forRoot()],
            providers: [
                {
                    provide: StockageService,
                    useValue: {
                        init: () => Promise.resolve(),
                        get: () => Promise.resolve(),
                        set: () => Promise.resolve()
                    }
                },
                HttpClient,
                HttpHandler,
                FormBuilder,
                { provide: UserService, useValue: userServiceSpy },
                {
                    provide: NavController,
                    useValue: navControllerSpy
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        computeLink = fixture.nativeElement.querySelector('#register');
        fixture.detectChanges();
    }));

    it('TA1: should have login form fields for email and password', () => {
        const EXPECTED_KEYS = ['email', 'password'];
        expect(Object.keys(component.f)).toEqual(EXPECTED_KEYS);
    });

    it('TA2: should redirect to the home page', fakeAsync(() => {
        jest.spyOn(navControllerSpy, 'navigateRoot');
        component.loginForm.controls.email.setValue('johndoe@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        tick();
        expect(navControllerSpy.navigateRoot).toHaveBeenCalledWith('/tabs');
    }));

    it('TA3: should display popup when service is unavailable', fakeAsync(() => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                error: { error: 'Server Error' },
                status: 502
            })
        );
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        tick();
        fixture.detectChanges();
        const NotAvailableServiceComponent: HTMLElement =
            fixture.nativeElement.querySelector('ion-popover');
        expect(NotAvailableServiceComponent).toBeDefined();
    }));

    it('TA4: should display email-not-found or password incorrect error message upon submission', fakeAsync(() => {
        const EXPECTED_ERROR = 'Email inconnu ou mot de passe incorrect';
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            throwError({
                error: { message: EXPECTED_ERROR },
                status: 403
            })
        );
        component.loginForm.controls.email.setValue('mack@gmail.com');
        component.loginForm.controls.password.setValue('Pa$$w0rd!');
        component.onSubmit();
        tick();
        expect(component.error.getValue()).toBe(EXPECTED_ERROR);
    }));

    describe('TA5: function toggleShow should display or hide password', () => {
        it('TA5-1: should display the password when it is hidden', () => {
            component.inputType = 'password';
            component.toggleShowPassword();
            expect(component.inputType).toBe('text');
        });

        it('TA5-2: should hide the password when it is displayed', () => {
            component.inputType = 'text';
            component.toggleShowPassword();
            expect(component.inputType).toBe('password');
        });
    });
    it('should navigate to "/newpwd" when firstConnection is true', fakeAsync(() => {
        jest.spyOn(userServiceSpy, 'login').mockReturnValueOnce(
            of({ firstConnection: true, ...MockUser.loggedUser })
        );
        component.loginForm.controls.email.setValue(MockUser.withoutId.email);
        component.loginForm.controls.password.setValue(
            MockUser.withoutId.password
        );
        jest.spyOn(navControllerSpy, 'navigateRoot');
        component.onSubmit();
        tick();
        fixture.detectChanges();
        expect(navControllerSpy.navigateRoot).toHaveBeenCalledWith('/newpwd');
    }));
});
