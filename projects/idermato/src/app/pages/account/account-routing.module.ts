import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountPage } from './account.page';

const routes: Routes = [
    {
        path: '',
        component: AccountPage
    },
    {
        path: 'payment',
        loadChildren: () =>
            import('../payment/payment.module').then((m) => m.PaymentPageModule)
    },
    {
        path: 'agenda',
        loadChildren: () =>
            import('../agenda/agenda.module').then((m) => m.AgendaPageModule)
    },
    {
        path: 'identity',
        loadChildren: () =>
            import('../identity/identity.module').then(
                (m) => m.IdentityPageModule
            )
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountPageRoutingModule {}
