import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { User } from 'src/app/models/types/user';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { environment } from 'src/environments/environment';

type Option = {
    text: string;
    icon: string;
    link: string;
};

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss']
})
export class AccountPage implements OnInit {
    user: User;
    defaultUser: string = environment.user.avatar;
    warnings: string[] = [];
    options: Option[] = [
        {
            icon: 'person-outline',
            text: 'Identité',
            link: '/tabs/account/identity'
        },
        {
            icon: 'calendar-number-outline',
            text: 'Agenda',
            link: '/tabs/account/agenda'
        },
        {
            icon: 'card-outline',
            text: 'Paiement',
            link: '/tabs/account/payment'
        }
    ];

    constructor(
        public navCtrl: NavController,
        public userService: UserService
    ) {}

    ngOnInit(): void {
        this.user = this.userService.getUserInfo();
    }

    ionViewDidEnter() {
        this.user = this.userService.user;
        this.checkWarning();
    }

    checkWarning(): void {
        this.warnings = [];
        if (!this.user.validPlanning) {
            this.warnings.push(messages.planningNotFound);
        }
        if (
            !this.user.compteBanquaire ||
            !this.user.numeroPaiements ||
            this.user.numeroPaiements.length === 0 ||
            !this.user.tarif
        ) {
            this.warnings.push(messages.missingPaymentInformation);
        }
    }

    logout(): void {
        this.userService.logOut();
        this.navCtrl.navigateRoot(['/login']);
    }

    trackBy(_index: number, option: Option) {
        return option.text;
    }
}
