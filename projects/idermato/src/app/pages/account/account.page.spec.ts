import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { IonicModule, NavController } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { UserService } from 'src/app/services/user.service';
import { userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { AccountPage } from './account.page';

class MockNavController {
    navigate(params) {
        return {};
    }
    navigateRoot(params) {
        return {};
    }
}

describe('AccountPage', () => {
    let component: AccountPage;
    let fixture: ComponentFixture<AccountPage>;
    let navControllerSpy: MockNavController = new MockNavController();
    let routerSpy = {
        navigate() {
            return {};
        }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [AccountPage],
            imports: [IonicModule.forRoot()],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: NavController, useValue: navControllerSpy },
                { provide: Router, useValue: routerSpy },
                {
                    provide: UserService,
                    useValue: userServiceMock
                }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(AccountPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    describe('should set warnings', () => {
        it('when planning is not found', () => {
            userServiceMock.user.completeProfile = true;
            userServiceMock.user.validPlanning = false;
            component.ionViewDidEnter();
            fixture.detectChanges();
            const warnings = fixture.nativeElement.querySelector(
                '[data-test-id="warnings"]'
            );
            expect(warnings.textContent.trim()).toBe(messages.planningNotFound);
        });
        it('when payments information are missing', () => {
            userServiceMock.user.numeroPaiements = [];
            userServiceMock.user.validPlanning = true;
            userServiceMock.user.tarif = MockUser.completeIdermato.tarif;
            component.ionViewDidEnter();
            fixture.detectChanges();
            const warnings = fixture.nativeElement.querySelector(
                '[data-test-id="warnings"]'
            );
            expect(warnings.textContent.trim()).toBe(
                messages.missingPaymentInformation
            );
        });
    });

    it('should not render a warnings when there is no warnings', () => {
        userServiceMock.user = MockUser.completeIdermato;
        component.ionViewDidEnter();
        fixture.detectChanges();
        const warningsElement = fixture.nativeElement.querySelector(
            '[data-test-id="warnings"]'
        );
        expect(warningsElement).toBeNull();
    });

    it('should redirect to login page on clicking button disconnection', () => {
        jest.spyOn(component.navCtrl, 'navigateRoot');
        component.logout();
        expect(component.navCtrl.navigateRoot).toHaveBeenCalledWith(['/login']);
    });

    it('should track option by text', () => {
        expect(component.trackBy(0, component.options[0])).toBe(
            component.options[0].text
        );
    });
});
