import { HttpClient, HttpHandler } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { IonicModule, NavController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { navControllerMock, userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { NewPasswordPage } from './newpwd.page';

describe('LoginPage', () => {
    let component: NewPasswordPage;
    let fixture: ComponentFixture<NewPasswordPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [NewPasswordPage],
            imports: [IonicModule.forRoot()],
            providers: [
                HttpClient,
                HttpHandler,
                FormBuilder,
                {
                    provide: NavController,
                    useValue: navControllerMock
                },
                { provide: UserService, useValue: userServiceMock },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(NewPasswordPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should displau an error message when the new password is the same as the old password', () => {
        component.form.patchValue({
            oldPassword: 'oldPassword',
            newPassword: 'oldPassword',
            confirmPassword: 'oldPassword'
        });
        component.onSubmit();
        expect(component.error.value).toBe(messages.incorrectNewPassword);
    });

    it('should display an error message when the passwords do not match', () => {
        component.form.patchValue({
            oldPassword: 'oldPassword',
            newPassword: 'password',
            confirmPassword: 'password2'
        });
        component.onSubmit();
        expect(component.error.value).toBe(messages.passwordNotMatch);
    });

    it('should redirect to the login page when the update is reissued', fakeAsync(() => {
        component.form.patchValue({
            oldPassword: 'oldPassword',
            newPassword: 'password',
            confirmPassword: 'password'
        });
        component.onSubmit();
        tick();
        expect(navControllerMock.navigateRoot).toHaveBeenCalledWith('/login');
    }));

    it('should notify the user when the update fails', fakeAsync(() => {
        userServiceMock.updatePassword.mockReturnValue(
            throwError(helperMessages.errorOccurred)
        );
        component.form.patchValue({
            oldPassword: 'oldPassword',
            newPassword: 'password',
            confirmPassword: 'password'
        });
        component.onSubmit();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    }));

    it('should display an error message when the old password is incorrect', fakeAsync(() => {
        userServiceMock.updatePassword.mockReturnValue(
            throwError({
                error: {
                    message: messages.incorrectOldPassword
                },
                status: 403
            })
        );
        component.form.patchValue({
            oldPassword: 'oldPassword',
            newPassword: 'password',
            confirmPassword: 'password'
        });
        component.onSubmit();
        tick();
        expect(component.error.value).toBe(messages.incorrectOldPassword);
    }));
});
