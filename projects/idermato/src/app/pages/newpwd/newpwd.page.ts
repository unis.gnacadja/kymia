import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { NavController } from '@ionic/angular';
import { cardMedia, helperMessages } from '@lib/constants';
import { dataPattern } from '@lib/MustMatch.validator';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { TypeError } from 'src/app/models/types/typeError';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { errorMessages, messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';

@Component({
    selector: 'app-newpwd',
    templateUrl: './newpwd.page.html',
    styleUrls: ['./newpwd.page.scss']
})
export class NewPasswordPage implements OnInit {
    form: FormGroup;
    isServiceAvailable: boolean = false;
    loading: boolean = false;
    errors: Record<string, TypeError> = errorMessages;
    cardMedia = cardMedia;
    passwordMessage: string = messages.passwordMessage;

    requiredValidator: ValidatorFn = Validators.required;
    passwordValidators: ValidatorFn[] = [
        Validators.required,
        Validators.pattern(dataPattern.noSpace)
    ];
    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(
        private readonly userService: UserService,
        private readonly formBuilder: FormBuilder,
        private readonly loadingService: LoadingService,
        private readonly notifyService: NotifyService,
        private readonly nav: NavController
    ) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group(
            {
                oldPassword: [''],
                newPassword: [''],
                confirmPassword: ['']
            },
            {
                updateOn: 'blur'
            }
        );
    }

    /**
     * Password update
     */
    async onSubmit(): Promise<void> {
        this.error.next(null);
        this.form.patchValue({
            oldPassword: this.form.get('oldPassword').value.trim(),
            newPassword: this.form.get('newPassword').value.trim(),
            confirmPassword: this.form.get('confirmPassword').value.trim()
        });

        if (
            this.form.get('newPassword').value ===
            this.form.get('oldPassword').value
        ) {
            this.error.next(messages.incorrectNewPassword);
        } else if (
            this.form.get('newPassword').value !==
            this.form.get('confirmPassword').value
        ) {
            this.error.next(messages.passwordNotMatch);
        } else if (!this.loading && this.form.valid) {
            await this.loadingService.present(helperMessages.loading);
            this.loading = true;
            this.userService
                .updatePassword(
                    this.form.get('newPassword').value,
                    this.form.get('oldPassword').value
                )
                .pipe(
                    finalize(() => {
                        this.loading = false;
                        this.loadingService.dismiss();
                    })
                )
                .subscribe({
                    next: () => {
                        this.notifyService.presentToast({
                            message: messages.updateSuccessTitle,
                            color: State.success
                        });
                        this.nav.navigateRoot('/login');
                    },
                    error: (error) => {
                        if (error.status === 403) {
                            this.error.next(messages.incorrectOldPassword);
                        } else {
                            this.notifyService.presentToast();
                        }
                    }
                });
        }
    }
}
