import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NewPasswordPageRoutingModule } from './newpwd-routing.module';
import { NewPasswordPage } from './newpwd.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NewPasswordPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [],
    declarations: [NewPasswordPage]
})
export class NewPasswordPageModule {}
