import { HttpClient, HttpHandler } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MockPhoto, MockUser } from '@kymia/src/app/mock';
import { StockageService } from '@kymia/src/app/services/stockage.service';
import { notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';
import { IdentityComponent } from './identity.page';

describe('IdentityComponent', () => {
    let component: IdentityComponent;
    let fixture: ComponentFixture<IdentityComponent>;
    let userServiceSpy = {
        user: {
            id: 0,
            ...MockUser.simpleIdermato
        },
        setUser(data: any) {
            return data;
        },
        getUserInfo() {
            return of(MockUser.simpleIdermato);
        },
        updateAgent(data: any) {
            return of({ ...MockUser.simpleIdermato, ...data });
        },
        setMedia() {
            return {
                toPromise() {
                    return of(MockUser.completeProfile);
                }
            };
        }
    };

    let identity = {
        nom: MockUser.simpleIdermato.nom,
        prenom: MockUser.simpleIdermato.prenom,
        numero: MockUser.simpleIdermato.numero,
        email: MockUser.simpleIdermato.email,
        fonction: MockUser.simpleIdermato.fonction,
        pays: MockUser.simpleIdermato.pays,
        ville: MockUser.simpleIdermato.ville,
        photo: MockUser.completeProfile.photo
    };

    global.fetch = jest.fn(() =>
        Promise.resolve({
            blob: jest.fn(() =>
                Promise.resolve(
                    new File(['1234'], 'file_1.png', {
                        type: 'image/png'
                    })
                )
            )
        })
    ) as jest.Mock;

    let setMediaSpy = jest.spyOn(userServiceSpy, 'setMedia');

    beforeAll(() => {
        TestBed.configureTestingModule({
            declarations: [IdentityComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [IonicModule.forRoot()],
            providers: [
                {
                    provide: StockageService,
                    useValue: {
                        init: () => Promise.resolve(),
                        get: () => Promise.resolve(),
                        set: () => Promise.resolve()
                    }
                },
                HttpClient,
                HttpHandler,
                FormBuilder,
                { provide: UserService, useValue: userServiceSpy }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(IdentityComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    describe('TA1: form submission success', () => {
        it('TA1-1 : global fetch should have be called', fakeAsync(() => {
            component.updateUser({
                user: identity,
                picture: {
                    url: MockPhoto.photo.url,
                    uid: MockPhoto.photo.uid
                }
            });
            expect(global.fetch).toHaveBeenCalledWith(MockPhoto.photo.url);
        }));

        it('TA1-2 : global set media when picture exist', () => {
            expect(setMediaSpy).toHaveBeenCalledWith(expect.any(FormData));
        });
    });
    it('should notify on form submission failure', () => {
        userServiceSpy.setMedia = jest
            .fn()
            .mockReturnValue(throwError({ error: { status: 500 } }));
        component
            .updateUser({
                user: identity,
                picture: {
                    url: MockPhoto.photo.url,
                    uid: MockPhoto.photo.uid
                }
            })
            .then(() => {
                expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
                    true,
                    messages.updateFailed,
                    messages.updateFailedTitle,
                    State.failed
                );
            });
    });
});
