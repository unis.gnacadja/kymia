import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TypeError } from 'src/app/models/types/typeError';
import { User } from 'src/app/models/types/user';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { errorMessages, messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';

@Component({
    selector: 'app-identity',
    templateUrl: './identity.page.html',
    styleUrls: ['./identity.page.scss']
})
export class IdentityComponent implements OnInit {
    user: User;
    errors: Record<string, TypeError> = errorMessages;
    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    isReadOnly: boolean = true;
    loading: boolean = false;

    constructor(
        public userService: UserService,
        private notifyService: NotifyService
    ) {}

    ngOnInit(): void {
        this.user = this.userService.getUserInfo();
    }

    async updateUser(payload: { user: User; picture?: any }) {
        this.loading = true;
        try {
            if (payload.picture) {
                const response = await fetch(payload.picture.url);
                const blob = await response.blob();
                const media = new FormData();
                media.set('file', blob, payload.picture.uid);
                await this.userService.setMedia(media).toPromise();
            }

            const user = {
                nom: payload.user.nom,
                prenom: payload.user.prenom,
                numero: payload.user.numero,
                fonction: payload.user.fonction,
                pays: payload.user.pays,
                ville: payload.user.ville
            };
            this.userService.updateAgent(user).subscribe((newUser: User) => {
                this.userService.setUser(newUser);
                this.notifyService.setPopover(
                    true,
                    messages.updateSuccess,
                    messages.updateSuccessTitle,
                    State.success
                );
            });
        } catch {
            this.notifyService.setPopover(
                true,
                messages.updateFailed,
                messages.updateFailedTitle,
                State.failed
            );
        } finally {
            this.loading = false;
        }
    }
}
