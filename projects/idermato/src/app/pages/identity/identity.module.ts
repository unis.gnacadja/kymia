import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { IdentityPageRoutingModule } from './identity-routing.module';
import { IdentityComponent } from './identity.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        IdentityPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [],
    declarations: [IdentityComponent],
    exports: []
})
export class IdentityPageModule {}
