import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StockageService } from '@kymia/src/app/services/stockage.service';
import { cardMedia } from '@lib/constants';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-intro',

    templateUrl: './intro.page.html',

    styleUrls: ['./intro.page.scss']
})
export class IntroPage {
    loading: boolean = false;
    defaultAvatar: string = environment.user.avatar;
    cardMedia: string = cardMedia;

    constructor(
        private stockage: StockageService,
        public navCtrl: NavController
    ) {}

    async start(): Promise<void> {
        this.loading = true;
        await this.stockage.init();
        this.stockage.set(environment.introKey, true);
        this.loading = false;
        this.navCtrl.navigateRoot('/login');
    }
}
