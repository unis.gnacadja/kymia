import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { NavController } from '@ionic/angular';
import { StockageService } from '@kymia/src/app/services/stockage.service';
import { IntroPage } from './intro.page';

class MockNavController {
    navigateRoot(args) {
        return {};
    }
}

describe('IntroPage', () => {
    let component: IntroPage;
    let fixture: ComponentFixture<IntroPage>;
    let navControllerSpy: MockNavController = new MockNavController();
    let mockStockage = {
        init: () => Promise.resolve(),
        set: jest.fn()
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [IntroPage],
            imports: [],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: StockageService,
                    useValue: mockStockage
                },
                {
                    provide: NavController,
                    useValue: navControllerSpy
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(IntroPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    describe('TA1: on clicking "start" link', () => {
        it('TA1-1: should set loading to true', () => {
            component.start();
            expect(component.loading).toBe(true);
        });
        it('TA1-2: should redirect to register page', fakeAsync(() => {
            jest.spyOn(navControllerSpy, 'navigateRoot');
            component.start();
            tick();
            expect(navControllerSpy.navigateRoot).toHaveBeenCalledWith(
                '/login'
            );
        }));
    });
});
