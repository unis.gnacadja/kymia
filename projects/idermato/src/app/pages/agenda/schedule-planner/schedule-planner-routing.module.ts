import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchedulePlannerPage } from './schedule-planner.page';

const routes: Routes = [
    {
        path: '',
        component: SchedulePlannerPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SchedulePlannerPageRoutingModule {}
