import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/internal/operators/finalize';
import { take } from 'rxjs/operators';
import { User } from 'src/app/models/types/user';
import { NotifyService } from 'src/app/services/common/notify.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import { days, messages } from 'src/app/utils/constants';
import { Color } from 'src/app/utils/enum';

type SelectedDay = {
    name: string;
    selected: boolean;
    day: string;
};
type Duration = {
    label: string;
    value: number;
};
@Component({
    selector: 'app-schedule-planner',
    templateUrl: './schedule-planner.page.html',
    styleUrls: ['./schedule-planner.page.scss'],
    providers: [DatePipe]
})
export class SchedulePlannerPage implements OnInit {
    loading: BehaviorSubject<boolean>;
    today: string;
    startDate: string;
    endDate: any;
    maxYear: number;
    durationOptions: Duration[] = [
        { value: 15, label: '15mn' },
        { value: 20, label: '20mn' },
        { value: 30, label: '30mn' },
        { value: 60, label: '1h' }
    ];
    duration: number;
    pauseOptions: Duration[] = [
        { value: 0, label: '0mn' },
        { value: 10, label: '10mn' },
        { value: 15, label: '15mn' },
        { value: 20, label: '20mn' },
        { value: 30, label: '30mn' },
        { value: 60, label: '1h' }
    ];
    pause: number;
    dayStartHour: string = '00:00';
    dayEndHour: string = '23:59';
    daysOfWeek: SelectedDay[];
    colors: string[];
    selectedColor: string = Color.fern;
    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    radix: number = 10;
    minEndDate: string;
    @ViewChild('endDateInput', { static: false }) endDateInput;

    constructor(
        private readonly modalController: ModalController,
        private readonly userService: UserService,
        public datePipe: DatePipe,
        private readonly planningService: PlanningService,
        private readonly notifyService: NotifyService,
        private readonly router: Router
    ) {}

    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);
        this.maxYear = new Date().getFullYear() + 7;
        this.daysOfWeek = days.map((obj) => ({
            ...obj,
            selected: false
        }));

        this.startDate = this.nextDay(1);
        this.endDate = this.nextDay(2);
        this.today = this.startDate;
        this.minEndDate = this.endDate;
        this.colors = Object.values(Color).filter(
            (color) => color !== Color.white
        );
        this.duration = this.durationOptions[0].value;
        this.pause = this.pauseOptions[0].value;
    }

    /**
     * Calculates the date for the next day
     *
     * @param dayToAdd - The number of days to add to the current date
     * @returns The calculated date as a string in ISO format (YYYY-MM-DD)
     */
    nextDay(dayToAdd: number): string {
        const currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + dayToAdd);
        return currentDate.toISOString().slice(0, 10);
    }

    /**
     * Updates the minimum end date based on the selected start date
     */
    async updateEndDateMin() {
        const selectedDate: Date = new Date(this.startDate);
        selectedDate.setDate(selectedDate.getDate() + 1);
        this.minEndDate = selectedDate.toISOString().slice(0, 10);
        this.endDate = this.minEndDate;
        await this.endDateInput.reset();
    }

    /**
     * Update the selected end date
     */
    updateEndDate() {
        if (!this.endDate) {
            this.endDate = new Date(this.minEndDate);
        }
    }

    /**
     * Checks if a given date is enabled (not in the past)
     *
     * @param dateString - The date to check in string format
     * @returns A boolean indicating whether the date is enabled
     */
    isDateEnabled(dateString: string): boolean {
        return new Date().getTime() <= new Date(dateString).getTime();
    }

    /**
     * Sorts and updates time ranges
     *
     * @param start - The start time property name
     * @param end - The end time property name
     */
    onDateChange(): void {
        const times = [this.dayStartHour, this.dayEndHour];
        times.sort((a, b) => a.localeCompare(b));
        this.dayEndHour = times[0];
        this.dayEndHour = times[1];
    }

    /**
     * Toggles the selection state of a day
     *
     * @param day - The day object to toggle
     */
    toggleDay(day) {
        day.selected = !day.selected;
    }

    /**
     * Selects a color and dismisses the modal
     *
     * @param color - The selected color
     */
    selectColor(color: string) {
        this.selectedColor = color;
        this.modalController.dismiss();
    }

    // ... (previous code remains the same)

    /**
     * Submits the planning data
     */
    async onSubmit(): Promise<void> {
        // Reset any previous error
        this.error.next(null);

        // Validate that start and end dates are provided
        if (!this.startDate.trim() || !this.endDate.trim()) {
            this.error.next(messages.mustDefinePlanning);
        } else {
            // Filter and map selected days
            const selectedDays = this.daysOfWeek
                .filter((day) => day.selected === true)
                .map((day) => day.day);

            // Parse hours time ranges
            const hours = {
                start: this.dayStartHour.split(':'),
                end: this.dayEndHour.split(':')
            };

            // Proceed if at least one day is selected
            if (selectedDays.length > 0) {
                // Set loading state to true
                this.loading.next(true);

                // Format start and end dates to French locale (DD-MM-YYYY)
                const formattedStartDate = new Date(this.startDate)
                    .toLocaleDateString('fr-FR', {
                        day: '2-digit',
                        month: '2-digit',
                        year: 'numeric'
                    })
                    .replace(/\//g, '-');
                const formattedEndDate = new Date(this.endDate)
                    .toLocaleDateString('fr-FR', {
                        day: '2-digit',
                        month: '2-digit',
                        year: 'numeric'
                    })
                    .replace(/\//g, '-');

                // Call planning service to register the planning
                this.planningService
                    .registerPlanning({
                        color: this.selectedColor,
                        dateDebut: formattedStartDate,
                        dateFin: formattedEndDate,
                        days: selectedDays,
                        doctorId: this.userService.user.id,
                        duration: this.duration,
                        pause: this.pause,
                        periods: [
                            {
                                start: {
                                    hour: parseInt(hours.start[0], this.radix),
                                    minutes: parseInt(
                                        hours.start[1],
                                        this.radix
                                    )
                                },
                                end: {
                                    hour: parseInt(hours.end[0], this.radix),
                                    minutes: parseInt(hours.end[1], this.radix)
                                }
                            }
                        ]
                    })
                    .pipe(
                        // Ensure loading state is set to false after operation completes
                        finalize(() => {
                            this.loading.next(false);
                        })
                    )
                    .subscribe({
                        next: () => {
                            // Show success message
                            this.notifyService.setPopover(
                                true,
                                messages.planningCreated
                            );
                            // update user
                            this.userService.getAgent().subscribe({
                                next: (user: User) => {
                                    this.userService.setUser(user);
                                }
                            });
                            // Navigate to agenda page after notification is closed
                            this.notifyService.closeEvent$
                                .pipe(take(1))
                                .subscribe(() => {
                                    this.router.navigate(
                                        ['/tabs/account/agenda'],
                                        { replaceUrl: true }
                                    );
                                });
                        },
                        error: (errors) => {
                            // Set error message if registration fails
                            this.error.next(errors.error.message);
                        }
                    });
            } else {
                // Set error if no days are selected
                this.error.next(messages.mustSelectPeriod);
            }
        }
    }
    /**
     * Track duration by value
     *
     * @param _index number: _index of the item
     * @param item Duration: item to track
     * @returns string: value of the item
     */
    trackDuration(_index: number, item: Duration): number {
        return item.value;
    }
}
