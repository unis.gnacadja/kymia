import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { SchedulePlannerPageRoutingModule } from './schedule-planner-routing.module';
import { SchedulePlannerPage } from './schedule-planner.page';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        SchedulePlannerPageRoutingModule,
        SharedModule
    ],
    declarations: [SchedulePlannerPage]
})
export class SchedulePlannerPageModule {}
