import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import { NotifyService } from 'src/app/services/common/notify.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import {
    planningServiceMock,
    routerMock,
    userServiceMock
} from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { Color } from 'src/app/utils/enum';
import { SchedulePlannerPage } from './schedule-planner.page';

describe('SchedulePlannerPage', () => {
    let component: SchedulePlannerPage;
    let fixture: ComponentFixture<SchedulePlannerPage>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SchedulePlannerPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: ModalController,
                    useValue: {
                        create: jest.fn(),
                        dismiss: jest.fn()
                    }
                },
                { provide: UserService, useValue: userServiceMock },
                { provide: PlanningService, useValue: planningServiceMock },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                {
                    provide: Router,
                    useValue: routerMock
                }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(SchedulePlannerPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('TA1: should return true for a future date', () => {
        const futureDate = new Date(`${new Date().getFullYear() + 5}-01-01`);
        const result = component.isDateEnabled(futureDate.toISOString());
        expect(result).toBe(true);
    });

    it('TA2-1: should update start times in ascending order when onDateChange is called', () => {
        component.dayStartHour = '00:00';
        component.dayEndHour = '23:59';
        component.onDateChange();
        expect(component.dayStartHour).toBe('00:00');
    });

    it('TA3: should toggle the selected property of a day', () => {
        const day = { name: 'Lun', selected: true, day: 'MONDAY' };
        component.toggleDay(day);
        expect(day.selected).toBe(false);
    });

    it('TA4: should set the selectedColor to the given color', () => {
        const color = Color.danger;
        component.selectColor(color);
        expect(component.selectedColor).toBe(color);
    });

    it('TA5: should show an error if datetime is not set', async () => {
        component.onSubmit();
        const errorMessage = fixture.nativeElement.querySelector(
            '[data-test-id="errorMessage"]'
        );
        fixture.detectChanges();
        expect(errorMessage.textContent).toBe(messages.mustSelectPeriod);
    });

    it('TA6: should get the error message when onSubmit is called without selecting a day', async () => {
        component.startDate = '2023-01-01';
        component.endDate = '2023-02-02';
        component.daysOfWeek = component.daysOfWeek.map((item) => ({
            ...item,
            selected: false
        }));
        await component.onSubmit();
        const errorMessage = fixture.nativeElement.querySelector(
            '[data-test-id="errorMessage"]'
        );
        fixture.detectChanges();
        expect(errorMessage.textContent).toBe(messages.mustSelectPeriod);
    });

    it('TA7: should show succes popover whenonSubmit is called with valid data', fakeAsync(() => {
        component.startDate = '2023-01-01';
        component.endDate = '2023-02-02';
        component.daysOfWeek[0].selected = true;
        planningServiceMock.registerPlanning.mockReturnValue(of({}));
        component.onSubmit();
        tick();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.planningCreated
        );
    }));

    it('TA8: should get an error message when onSubmit is called with invalid data', async () => {
        component.startDate = '2023-01-01';
        component.endDate = '2023-02-02';
        component.daysOfWeek[0].selected = true;
        planningServiceMock.registerPlanning.mockReturnValue(
            throwError({ error: { message: helperMessages.errorOccurred } })
        );
        const errorMessage = fixture.nativeElement.querySelector(
            '[data-test-id="errorMessage"]'
        );
        await component.onSubmit();
        fixture.detectChanges();
        expect(errorMessage.textContent).toBe(helperMessages.errorOccurred);
    });

    it('TA9: should update minEndDate when updateEndDateMin is called', () => {
        component.endDateInput = {
            reset: jest.fn()
        };
        component.updateEndDateMin();
        const expectedMinEndDate = component.datePipe.transform(
            component.minEndDate,
            'yyyy-MM-dd'
        );
        expect(component.minEndDate).toBe(expectedMinEndDate);
    });

    it('TA10: should update endDate when endDate is not set', () => {
        component.minEndDate = '2024-05-01T00:00:00.000Z';
        component.endDate = '';
        component.updateEndDate();
        expect(component.endDate.toISOString()).toBe(component.minEndDate);
    });

    it('TA11: should get an error message when onSubmit is called with empty planning period', async () => {
        component.startDate = '';
        component.endDate = '';
        await component.onSubmit();
        const errorMessage = fixture.nativeElement.querySelector(
            '[data-test-id="errorMessage"]'
        );
        fixture.detectChanges();
        expect(errorMessage.textContent).toBe(messages.mustDefinePlanning);
    });

    it('TA12: should not update endDate when endDate is already set', () => {
        component.minEndDate = '2024-05-01';
        component.endDate = '2024-05-02';
        component.updateEndDate();
        expect(component.endDate).toBe(component.endDate);
    });
});
