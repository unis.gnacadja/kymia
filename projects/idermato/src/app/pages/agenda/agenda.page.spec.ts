import { Component, CUSTOM_ELEMENTS_SCHEMA, Input } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { User } from '@kymia/src/app/models';
import { TimeFormat } from '@kymia/src/app/pipe';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { CalendarComponent } from 'ionic2-calendar';
import { of, throwError } from 'rxjs';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import { planningServiceMock, userServiceMock } from 'src/app/tests';
import { AgendaPage } from './agenda.page';

jest.mock('ionic2-calendar', () => ({
    CalendarComponent: class MockNavComponent {
        calendarMode: string;
    }
}));
let navControllerSpy = {
    navigateRoot: jest.fn()
};
@Component({
    selector: 'calendar',
    template: '',
    providers: [
        {
            provide: CalendarComponent,
            useClass: MockCalendarComponent
        }
    ]
})
class MockCalendarComponent {
    @Input() calendarMode: string;
    @Input() currentDate: Date;
    @Input() startHour: string;
    @Input() endHour: string;
    @Input() timeInterval: string;
    @Input() allDayLabel: Date;
    @Input() dateFormatter: string;
    @Input() dayviewNormalEventTemplate: string;
    @Input() eventSource: string;
    slidePrev() {
        jest.fn();
    }
    slideNext() {
        jest.fn();
    }
}
describe('AgendaPage', () => {
    let component: AgendaPage;
    let fixture: ComponentFixture<AgendaPage>;
    let user: User = MockUser.completeProfile;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [AgendaPage, MockCalendarComponent],
            imports: [IonicModule.forRoot()],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                TimeFormat,
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: NavController,
                    useValue: navControllerSpy
                },
                {
                    provide: PlanningService,
                    useValue: planningServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(AgendaPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should display a notification when no schedule has been defined', async () => {
        planningServiceMock.searchCalendars.mockReturnValueOnce(of([]));
        await component.ionViewWillEnter();
        expect(component.showNotification).toBe(true);
    });

    it('should get last planning available date when entering the page', async () => {
        await component.ionViewWillEnter();
        expect(component.lastCalendar).toBe('2024-05-22');
    });

    it('should notify when failed to get diaries', async () => {
        planningServiceMock.searchDiaries.mockReturnValue(
            throwError(new Error())
        );
        await component.ionViewWillEnter();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });

    it('should notify when failed to get calendar', () => {
        notifyServiceMock.presentToast.mockReset();
        planningServiceMock.searchCalendars.mockReturnValue(
            throwError(new Error())
        );
        component.getCalendar();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });

    it('should slide to previous item on back button click', () => {
        const backSpy = jest.spyOn(component.calendarComponent, 'slidePrev');
        const button = fixture.nativeElement.querySelector(
            '[data-test-id="button-back"]'
        );
        button.click();
        expect(backSpy).toHaveBeenCalled();
    });

    it('should slide to next item on next button click', () => {
        const nextSpy = jest.spyOn(component.calendarComponent, 'slideNext');
        const button = fixture.nativeElement.querySelector(
            '[data-test-id="button-next"]'
        );
        button.click();
        expect(nextSpy).toHaveBeenCalled();
    });

    it('should format day view hour column correctly', () => {
        const dateFormatter = component.customDateFormatter;
        const testDate = new Date(2023, 9, 9, 10, 30);
        const formattedHour = dateFormatter.formatDayViewHourColumn(testDate);
        expect(formattedHour).toBe('10:30');
    });

    it('should emit action event on submit', () => {
        const spy = jest.spyOn(component.click, 'emit');
        component.onClick();
        expect(spy).toHaveBeenCalled();
    });

    it(' should update currentDate when onCurrentDateChanged is called', () => {
        const newDate = new Date('2023-11-01');
        component.onCurrentDateChanged(newDate);

        expect(component.calendar.currentDate).toEqual(newDate);
    });

    it('should update currentDate when onDateTimeChange is called', () => {
        const customEvent = new CustomEvent('ionChange', {
            detail: { value: '2023-11-01' }
        });
        component.onDateTimeChange(customEvent);
        expect(component.calendar.currentDate).toEqual(new Date('2023-11-01'));
    });

    it('should format date and time correctly', () => {
        const result = component.formatDateTime('27-05-2023', '14:35:45');
        expect(result).toBe('05 27, 2023 14:35:45');
    });

    it('should return the correct next day', () => {
        const mockDate = new Date(2024, 4, 27);
        jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
        const result = component.nextDay(1);
        expect(result).toBe('28-05-2024');
    });
});
