import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { TimeFormat } from '@kymia/src/app/pipe';
import { helperMessages } from '@lib/constants';
import { CalendarComponent } from 'ionic2-calendar';
import { CalendarMode, IDateFormatter } from 'ionic2-calendar/calendar';
import { finalize } from 'rxjs/operators';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { PlanningService } from 'src/app/services/planning.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';

@Component({
    selector: 'app-agenda',
    templateUrl: './agenda.page.html',
    styleUrls: ['./agenda.page.scss']
})
export class AgendaPage {
    @Output() click: EventEmitter<void> = new EventEmitter<void>();
    calendar: {
        mode: CalendarMode;
        currentDate: Date;
    } = {
        mode: 'day',
        currentDate: new Date()
    };
    @ViewChild(CalendarComponent) calendarComponent: CalendarComponent;
    eventSource: {
        color: string;
        title: string;
        startTime: Date;
        endTime: Date;
        allDay: boolean;
    }[] = [];
    lastCalendar: string;
    showNotification: boolean = false;
    expectHours: string = messages.expectHours;

    constructor(
        private readonly timeFormat: TimeFormat,
        private readonly userService: UserService,
        private readonly planningService: PlanningService,
        private readonly loadingService: LoadingService,
        private readonly notifyService: NotifyService
    ) {}

    async ionViewWillEnter(): Promise<void> {
        this.getDiaries();
    }

    /**
     * Get the calendar
     */
    async getCalendar(): Promise<void> {
        this.planningService
            .searchCalendars(this.userService.user.id, 0, 5)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (data: any[]) => {
                    if (data.length === 0) {
                        this.showNotification = true;
                    } else {
                        this.lastCalendar = data[data.length - 1].availableDate
                            .split('-')
                            .reverse()
                            .join('-');

                        const availableDate = new Date(this.lastCalendar);
                        const maxDate = new Date();

                        // Add 4 months to the current date
                        maxDate.setMonth(maxDate.getMonth() + 4);

                        if (availableDate.getTime() < maxDate.getTime()) {
                            this.showNotification = true;
                        }
                    }
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    nextDay(dayToAdd: number): string {
        const currentDate = new Date(this.calendar.currentDate);
        currentDate.setDate(currentDate.getDate() + dayToAdd);
        return currentDate
            .toLocaleDateString('fr-FR', {
                day: '2-digit',
                month: '2-digit',
                year: 'numeric'
            })
            .replace(/\//g, '-');
    }

    formatDateTime(date: string, time: string): string {
        const dateParts = date.split('-');
        return `${dateParts[1]} ${dateParts[0]}, ${dateParts[2]} ${time}`;
    }

    async getDiaries() {
        await this.loadingService.present(helperMessages.loading);
        const formattedStartDate = this.nextDay(0);

        const formattedEndDate = this.nextDay(7);

        this.planningService
            .searchDiaries(formattedStartDate, formattedEndDate)

            .pipe(
                finalize(() => {
                    this.getCalendar();
                })
            )
            .subscribe({
                next: (calendars: any[]) => {
                    this.eventSource = [];
                    for (const event of calendars) {
                        const start = this.formatDateTime(
                            event.availableDate,
                            event.availableDateStart
                        );
                        const end = this.formatDateTime(
                            event.availableDate,
                            event.availableDateEnd
                        );

                        const endDate = new Date(end);
                        endDate.setMinutes(endDate.getMinutes() + 5);
                        this.eventSource.push({
                            title: messages.noAppointment,
                            startTime: new Date(start),
                            endTime: endDate,
                            allDay: false,
                            color: `has-background-${event.color}`
                        });
                    }
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    back() {
        this.calendarComponent.slidePrev();
        this.getDiaries();
    }

    next() {
        this.calendarComponent.slideNext();
        this.getDiaries();
    }

    onClick(): void {
        this.click.emit();
    }

    onDateTimeChange(event: Event) {
        const customEvent = event as CustomEvent;
        this.calendar.currentDate = new Date(customEvent.detail.value);
        this.getDiaries();
    }

    customDateFormatter: IDateFormatter = {
        formatDayViewHourColumn: (date: Date) => this.timeFormat.transform(date)
    };

    onCurrentDateChanged(newDate: Date) {
        this.calendar.currentDate = newDate;
    }
}
