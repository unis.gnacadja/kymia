import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TimeFormat } from '@kymia/src/app/pipe';
import { CalendarComponent, NgCalendarModule } from 'ionic2-calendar';
import { NormalizeDate } from 'src/app/pipes';
import { SharedModule } from '../../components/shared/shared.module';
import { AgendaPageRoutingModule } from './agenda-routing.module';
import { AgendaPage } from './agenda.page';

@NgModule({
    declarations: [AgendaPage],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AgendaPageRoutingModule,
        NgCalendarModule,
        SharedModule
    ],
    providers: [NormalizeDate, TimeFormat],
    exports: [CalendarComponent]
})
export class AgendaPageModule {}
