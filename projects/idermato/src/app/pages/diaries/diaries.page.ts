import { ChangeDetectorRef, Component, DoCheck, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Diary } from 'src/app/models/types/diary';
import { ConsultationService } from 'src/app/services/consultation.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { DiaryListMode } from 'src/app/utils/enum';

@Component({
    selector: 'app-diaries',
    templateUrl: './diaries.page.html',
    styleUrls: ['./diaries.page.scss']
})
export class DiariesPage implements OnInit, DoCheck {
    filteredList: Diary[] = [];
    searchForm: FormGroup;
    DiaryListMode: typeof DiaryListMode = DiaryListMode;
    loading: boolean = true;
    messagesInfos: Record<string, string> = messages;

    constructor(
        private formBuilder: FormBuilder,
        public userService: UserService,
        public consultationService: ConsultationService,
        public loadingCtrl: LoadingController,
        public cd: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.loading = true;
        this.searchForm = this.formBuilder.group({
            search: []
        });
        this.getConsultations();
    }

    ionDidViewEnter() {
        this.filteredList = this.consultationService.list;
    }

    /**
     * Retrieves the consultations
     */
    getConsultations(): void {
        this.loadingCtrl
            .create({
                message: 'chargement.....'
            })
            .then((loading) => {
                loading.present();

                try {
                    this.consultationService
                        .searchConsultations(this.userService.user.id, 0, 20)
                        .subscribe((data: any[]) => {
                            this.consultationService.setConsultations(data);

                            this.filteredList = this.consultationService.list;

                            this.loading = false;
                            this.loadingCtrl.dismiss();
                        });
                } catch (error) {
                    this.loading = false;
                    this.loadingCtrl.dismiss();
                }
            });
    }

    /**
     * Make a search by patient name or information
     */
    ngDoCheck() {
        if (this.searchForm.value.search) {
            const searchTerm = this.searchForm.value.search.toLowerCase();
            this.filteredList = this.consultationService.list.filter(
                (item) =>
                    item.patient.nom.toLowerCase().includes(searchTerm) ||
                    item.patient.prenom.toLowerCase().includes(searchTerm) ||
                    item.problemDescription.toLowerCase().includes(searchTerm)
            );
        }
    }
}
