import { DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule, LoadingController } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { NextDate } from '@kymia/src/app/pipe';
import { ConsultationService } from 'src/app/services/consultation.service';
import { UserService } from 'src/app/services/user.service';
import {
    consultationServiceMock,
    diaries,
    loadingControllerSpy,
    userServiceMock
} from 'src/app/tests';
import { DiariesPage } from './diaries.page';

describe('DiariesPage', () => {
    let component: DiariesPage;
    let fixture: ComponentFixture<DiariesPage>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DiariesPage],
            imports: [IonicModule.forRoot(), ReactiveFormsModule],
            providers: [
                {
                    provide: UserService,
                    useValue: {
                        ...userServiceMock,
                        user: MockUser.completeProfile
                    }
                },
                {
                    provide: ConsultationService,
                    useValue: consultationServiceMock
                },
                NextDate,
                DatePipe,
                {
                    provide: LoadingController,
                    useValue: loadingControllerSpy
                }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(DiariesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('TA1 should display search Input when the list has more than 9 items', fakeAsync(() => {
        component.ngOnInit();
        fixture.detectChanges();
        let searchInput: HTMLElement = fixture.nativeElement.querySelector(
            '[data-test-id="search"]'
        );
        expect(searchInput).not.toBeNull();
    }));

    it('TA3: should throw error when searchConsultations fails', fakeAsync(() => {
        consultationServiceMock.searchConsultations.mockImplementation(() => {
            throw new Error('Error');
        });

        const spy = jest.spyOn(consultationServiceMock, 'searchConsultations');

        component.ngOnInit();
        tick();
        expect(spy).toThrowError();
    }));

    it('TA4: should assign consultationService.list to filteredList when ionDidViewEnter is called', () => {
        component.ionDidViewEnter();
        expect(component.filteredList).toEqual(diaries);
    });

    it('TA5: should filter the list by patient name', () => {
        component.ngOnInit();
        component.searchForm.setValue({ search: diaries[0].patient.nom });
        component.ngDoCheck();
        expect(component.filteredList).toEqual(diaries);
    });

    it('TA6: should filter the list by patient surname', () => {
        component.ngOnInit();
        component.searchForm.setValue({ search: diaries[0].patient.prenom });
        component.ngDoCheck();
        expect(component.filteredList).toEqual(diaries);
    });

    it('TA7: should filter the list by patient description', () => {
        component.ngOnInit();
        component.searchForm.setValue({
            search: diaries[0].problemDescription
        });
        component.ngDoCheck();
        expect(component.filteredList).toEqual(diaries);
    });
});
