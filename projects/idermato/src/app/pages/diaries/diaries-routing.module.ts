import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiariesPage } from './diaries.page';

const routes: Routes = [
    {
        path: '',
        component: DiariesPage
    },
    {
        path: 'consultation',
        loadChildren: () =>
            import('../consultation/consultation.module').then(
                (m) => m.ConsultationPageModule
            )
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DiariesPageRoutingModule {}
