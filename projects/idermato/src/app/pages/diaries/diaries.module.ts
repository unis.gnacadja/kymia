import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ShortenPipe } from 'ngx-pipes';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { DiariesPageRoutingModule } from './diaries-routing.module';
import { DiariesPage } from './diaries.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        DiariesPageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [DiariesPage],
    providers: [NextDate, NormalizeDate, NormalizeHour, ShortenPipe]
})
export class DiariesPageModule {}
