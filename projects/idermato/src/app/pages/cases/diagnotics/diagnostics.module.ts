import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DiagnoticsPageRoutingModule } from './diagnostics-routing.module';
import { DiagnosticsPage } from './diagnostics.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DiagnoticsPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [DiagnosticsPage]
})
export class DiagnosticsPageModule {}
