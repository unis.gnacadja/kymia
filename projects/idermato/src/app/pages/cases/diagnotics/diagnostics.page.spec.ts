import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IonicModule, IonModal } from '@ionic/angular';
import { DoctorMock } from '@kymia/src/app/mock';
import {
    loadingServiceMock,
    mockActivatedRoute,
    notifyServiceMock
} from '@lib/mocks';
import { of, throwError } from 'rxjs';
import * as CaseMock from 'src/app/mock/case.mock.json';
import { CaseService } from 'src/app/services/case.service';
import { CommentService } from 'src/app/services/comment.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { DiagnosticService } from 'src/app/services/diagnostic.service';
import { UserService } from 'src/app/services/user.service';
import {
    caseServiceMock,
    commentServiceMock,
    diagnosticServiceMock,
    normalizedCaseWithCommentAgent,
    userServiceMock
} from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { CaseStatus, State } from 'src/app/utils/enum';
import { DiagnosticsPage } from './diagnostics.page';

@Component({
    selector: 'ionModal',
    template: '',
    providers: [
        {
            provide: IonModal,
            useClass: MockIonModal
        }
    ]
})
class MockIonModal {
    onDidDismiss() {
        jest.fn();
    }
}

describe('DiagnosticsPage', () => {
    let component: DiagnosticsPage;
    let fixture: ComponentFixture<DiagnosticsPage>;
    mockActivatedRoute.queryParams = of({ caseId: CaseMock.id });
    const normalizedCase = normalizedCaseWithCommentAgent;

    caseServiceMock.list = Array(11).fill({
        ...normalizedCaseWithCommentAgent,
        diagnostics: [
            {
                ...CaseMock.listDiagnostique.diagnostiques[0],
                agent: DoctorMock
            }
        ]
    });

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DiagnosticsPage, MockIonModal],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                {
                    provide: CaseService,
                    useValue: caseServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: DiagnosticService,
                    useValue: diagnosticServiceMock
                },
                FormBuilder,
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                },
                {
                    provide: CommentService,
                    useValue: commentServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(DiagnosticsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
        const originalModule = jest.requireActual('@ionic/angular');
        component.suggestmodal = {
            ...originalModule,
            onDidDismiss: jest.fn().mockReturnValue(Promise.resolve())
        };
    }));

    it('should set showSuggestDiagnostic to true when conditions are met', () => {
        component.case.agent.id = '2';
        component.case.status = CaseStatus.open;
        component.ngOnInit();
        expect(component.showSuggestDiagnostic).toBe(true);
    });

    it('should display the diagnostic tab when the case status is not open', () => {
        component.case.status = CaseStatus.close;
        component.ngOnInit();
        fixture.detectChanges();
        expect(component.selectedTab).toBe(component.diagnosticTabs.diagnostic);
    });

    it('should run an AI diagnostic and call getCase', async () => {
        jest.spyOn(diagnosticServiceMock, 'getPrediction').mockReturnValue(
            of({
                predictions: CaseMock.listDiagnostique.predictionIA
            })
        );
        component.getPrediction().then(() => {
            fixture.detectChanges();
            expect(diagnosticServiceMock.getPrediction).toHaveBeenCalled();
        });
    });

    it('should run an AI diagnostic and display none message when no prediction', fakeAsync(() => {
        jest.spyOn(diagnosticServiceMock, 'getPrediction').mockReturnValue(
            of({
                predictions: []
            })
        );
        component.getPrediction();
        tick();
        expect(component.notifications).toEqual([messages.aiDiagnosticNone]);
    }));

    it('should assign value to diagnostic and suggest diagnostic', fakeAsync(() => {
        component.submitForm.get('description').setValue('Expected diagnostic');
        component.loading = false;
        component.save();
        tick();
        expect(component.diagnostic).toBe('Expected diagnostic');
    }));

    it('should suggest when form is valid and no diagnostic or treatment has been provided', fakeAsync(() => {
        const form = component.formBuilder.group({
            description: ['']
        });
        component.submitForm = form;
        jest.spyOn(diagnosticServiceMock, 'suggest').mockReturnValue(of({}));
        component.save();
        tick();
        expect(diagnosticServiceMock.suggest).not.toHaveBeenCalled();
    }));

    it('should call suggest with correct data', fakeAsync(() => {
        component.isOpen = false;
        component.loading = false;
        component.submitForm
            .get('description')
            .setValue(CaseMock.listDiagnostique.diagnostiques[0].prescription);
        component.diagnostic =
            CaseMock.listDiagnostique.diagnostiques[0].diagnostique;
        component.treatment =
            CaseMock.listDiagnostique.diagnostiques[0].traitement;
        component.case.id = CaseMock.id;
        component.case.agent.id = userServiceMock.user.id;
        caseServiceMock.list = [normalizedCase];
        caseServiceMock.getCases.mockReturnValue(of([normalizedCase]));
        jest.spyOn(diagnosticServiceMock, 'suggest').mockReturnValue(of({}));
        component.save();
        tick();

        expect(diagnosticServiceMock.suggest).toHaveBeenCalledWith({
            idCas: CaseMock.id,
            diagnostique:
                CaseMock.listDiagnostique.diagnostiques[0].diagnostique,
            traitement: CaseMock.listDiagnostique.diagnostiques[0].traitement,
            prescription:
                CaseMock.listDiagnostique.diagnostiques[0].prescription
        });
    }));

    it('should render popover with error message when close case error', fakeAsync(() => {
        diagnosticServiceMock.suggest.mockReturnValue(
            throwError({ status: 404 })
        );
        component.diagnostic =
            CaseMock.listDiagnostique.diagnostiques[0].diagnostique;
        component.treatment =
            CaseMock.listDiagnostique.diagnostiques[0].traitement;
        component.save();
        tick();
        expect(notifyServiceMock.presentToast).toHaveBeenCalledWith();
    }));

    it('should set isOpen to true when button is button is clicked', () => {
        component.openSuggestModal(true);
        expect(component.isOpen).toBe(true);
    });

    it('should throw error when AI diagnostic fails', async () => {
        diagnosticServiceMock.getPrediction.mockReturnValue(
            throwError({ status: 500 })
        );

        component.getPrediction().then(() => {
            expect(notifyServiceMock.presentToast).toHaveBeenCalledWith({
                message: messages.aiDiagnosticError
            });
        });
    });

    it('should render popover with error message when close comment error', async () => {
        commentServiceMock.getAll.mockReturnValue(throwError({ status: 500 }));
        jest.spyOn(commentServiceMock, 'getAll');
        component.getComment().then(() => {
            expect(notifyServiceMock.presentToast).toHaveBeenCalled();
        });
    });

    it('should render popover with success message when the comment send successfully', () => {
        component.saveComment().then(() => {
            expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
                true,
                messages.commentPosted,
                messages.thanks,
                State.success
            );
        });
    });

    it('should render popover with error message when the comment send error', async () => {
        commentServiceMock.save.mockReturnValue(throwError({ status: 500 }));
        jest.spyOn(commentServiceMock, 'save');
        component.saveComment().then(() => {
            expect(notifyServiceMock.presentToast).toHaveBeenCalled();
        });
    });

    it('should trackBy diagnostic by id', () => {
        expect(component.trackByFn(0, component.case.diagnostics[0])).toBe(
            component.case.diagnostics[0].id
        );
    });
});
