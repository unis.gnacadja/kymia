import { Component, OnInit, ViewChild } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IonModal } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { finalize } from 'rxjs/operators';
import { Case, Comment, Diagnostic } from 'src/app/models/types/case';
import { TypeError } from 'src/app/models/types/typeError';
import { User } from 'src/app/models/types/user';
import { CaseService } from 'src/app/services/case.service';
import { CommentService } from 'src/app/services/comment.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { DiagnosticService } from 'src/app/services/diagnostic.service';
import { UserService } from 'src/app/services/user.service';
import { errorMessages, iA, messages } from 'src/app/utils/constants';
import { CaseStatus, DiagnosticTabs, State } from 'src/app/utils/enum';

@Component({
    selector: 'app-diagnostics',
    templateUrl: './diagnostics.page.html',
    styleUrls: ['./diagnostics.page.scss']
})
export class DiagnosticsPage implements OnInit {
    case: Case;
    loading: boolean = false;
    diagnosticTabs = DiagnosticTabs;
    selectedTab: string = DiagnosticTabs.diagnostic;
    comment: string = '';
    comments: Comment[];
    caseStatus = CaseStatus;
    showSuggestDiagnostic: boolean = false;
    requiredMessage: TypeError = errorMessages.required;
    requiredValidator: ValidatorFn = Validators.required;
    submitForm: FormGroup;
    diagnostic: string = '';
    treatment: string = '';
    modalTitle: string = messages.askDiagnostic;
    buttonTitle: string = messages.next;
    isOpen: boolean = false;
    @ViewChild(IonModal) suggestmodal: IonModal;
    notifications: string[] = [];
    canStartDiagnostic: boolean = false;

    constructor(
        public activatedRoute: ActivatedRoute,
        public caseService: CaseService,
        public diagnosticService: DiagnosticService,
        public userService: UserService,
        public formBuilder: FormBuilder,
        public commentService: CommentService,
        public notifyService: NotifyService,
        public loadingService: LoadingService
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.loadCase(params['caseId']);
            if (this.case.status === this.caseStatus.open) {
                this.selectedTab =
                    params['diagnosticTabs'] || DiagnosticTabs.diagnostic;
                this.getComment();
            } else {
                this.selectedTab = this.diagnosticTabs.diagnostic;
            }
        });
        this.submitForm = this.formBuilder.group(
            {
                description: ['']
            },
            {
                updateOn: 'blur'
            }
        );
    }

    resetForm(): void {
        this.modalTitle = messages.askDiagnostic;
        this.buttonTitle = messages.next;
        this.diagnostic = '';
        this.treatment = '';
        this.submitForm.get('description').reset();
    }

    /**
     *   Open and close suggest a diagnostic modal
     *
     * @param isOpen : the state of the modal (opened or closed)
     */
    openSuggestModal(isOpen: boolean): void {
        this.isOpen = isOpen;
        this.resetForm();
    }

    /**
     *  Diagnosis suggestion form validation function
     */
    save(): void {
        if (this.submitForm.get('description').valid) {
            if (!this.diagnostic) {
                this.submitForm
                    .get('description')
                    .setValue(
                        this.submitForm.controls['description'].value.trim()
                    );
                if (this.submitForm.get('description').valid) {
                    this.diagnostic = this.submitForm.get('description').value;
                    this.modalTitle = messages.askTreatment;
                    this.buttonTitle = messages.next;
                    this.submitForm
                        .get('description')
                        .removeValidators(Validators.required);
                }
            } else if (this.modalTitle === messages.askTreatment) {
                this.treatment = this.submitForm.get('description').value;
                this.modalTitle = messages.askPrescription;
                this.buttonTitle = messages.submit;
            } else {
                this.submitSuggestion();
            }
            this.submitForm.get('description').reset();
        }
    }

    /**
     * Calling the diagnostic suggestion service
     */
    private submitSuggestion(): void {
        this.loading = true;
        const formData = {
            idCas: this.case.id,
            diagnostique: this.diagnostic,
            traitement: this.treatment,
            prescription: this.submitForm.get('description').value
        };
        this.diagnosticService
            .suggest(formData)
            .pipe(
                finalize(() => {
                    this.isOpen = false;
                    this.loading = false;
                })
            )
            .subscribe({
                next: () => {
                    this.suggestmodal.onDidDismiss().then(() => {
                        this.resetForm();
                    });
                    this.getCase();
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Retrieves cases from the service and loads a specific case.
     */
    private async getCase(): Promise<void> {
        await this.caseService.getAll();
        this.loadCase(this.case.id);
    }

    /**
     * Loads a specific case object from the case list based on the provided ID.
     */
    private loadCase(caseId) {
        this.case = this.caseService.list.find((item) => item.id === caseId);
        this.shouldShowSuggestDiagnostic();
        this.canStartDiagnostic =
            this.case.diagnostics.filter(
                (diagnostic: Diagnostic) => diagnostic.agent === iA
            ).length === 0 && this.case.status === this.caseStatus.open;
    }

    /**
     * Determines whether the suggest diagnostic should be shown.
     * The suggest diagnostic should be shown if the following conditions are met:
     * - The current user is not the agent assigned to the case.
     * - The case status is open.
     * - None of the diagnostics in the case have been assigned to the current user.
     */
    private shouldShowSuggestDiagnostic(): void {
        this.showSuggestDiagnostic =
            this.case.agent.id !== this.userService.user.id &&
            this.case.status === this.caseStatus.open &&
            this.case.diagnostics.every(
                (diagnostic) =>
                    diagnostic.agent === iA ||
                    (diagnostic.agent as User).id !== this.userService.user.id
            );
    }

    /**
     * Get comments for the case and sort them by date
     */
    async getComment() {
        await this.loadingService.present(helperMessages.loading);
        this.commentService
            .getAll(this.case.id)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: (comments: Comment[]) => {
                    this.comments = comments.slice().reverse();
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Send a new comment for the case
     */
    async saveComment() {
        await this.loadingService.present(helperMessages.loading);

        this.commentService
            .save(this.case.id, this.comment)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: async () => {
                    this.comment = '';
                    this.caseService.getAll();
                    await this.getComment();
                    this.notifyService.setPopover(
                        true,
                        messages.commentPosted,
                        messages.thanks,
                        State.success
                    );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Runs an AI diagnostic for the current case.
     *
     */
    async getPrediction(): Promise<void> {
        if (!this.loading) {
            this.loading = true;
            this.notifications = [messages.aiDiagnosticInProgress];

            this.diagnosticService
                .getPrediction(this.case.id)
                .pipe(
                    finalize(() => {
                        this.notifications.shift();
                        this.loading = false;
                    })
                )
                .subscribe({
                    next: (response: any) => {
                        if (response.predictions.length === 0) {
                            this.notifications.push(messages.aiDiagnosticNone);
                        } else {
                            this.getCase();
                        }
                    },
                    error: () => {
                        this.notifyService.presentToast({
                            message: messages.aiDiagnosticError
                        });
                    }
                });
        }
    }

    trackByFn(_: number, item: Diagnostic | Comment): string {
        return item.id;
    }
}
