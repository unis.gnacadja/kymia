import { Component, DoCheck, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { helperMessages } from '@lib/constants';
import { Case } from 'src/app/models/types/case';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';

@Component({
    selector: 'app-cases',
    templateUrl: './cases.page.html',
    styleUrls: ['./cases.page.scss']
})
export class CasesPage implements OnInit, DoCheck {
    searchForm: FormGroup;
    filteredList: Case[];
    loading: boolean = true;

    constructor(
        private formBuilder: FormBuilder,
        public caseService: CaseService,
        public loadingService: LoadingService,
        public notifyService: NotifyService
    ) {}

    async ngOnInit(): Promise<void> {
        this.searchForm = this.formBuilder.group({
            search: []
        });
        this.getCases();
    }

    ionViewWillEnter(): void {
        this.filteredList = this.caseService.list;
    }

    /**
     * get the cases
     */
    async getCases(): Promise<void> {
        try {
            await this.loadingService.present(helperMessages.loading);

            await this.caseService.getAll();
            this.filteredList = this.caseService.list;
        } catch {
            this.notifyService.presentToast();
        }
        this.loadingService.dismiss();
    }

    /**
     * Make a search by agent name, function or by status or description
     */
    ngDoCheck() {
        if (this.searchForm.value.search) {
            const searchTerm = this.searchForm.value.search.toLowerCase();
            this.filteredList = this.caseService.list.filter(
                (item) =>
                    item.agent.domain?.name
                        .toLowerCase()
                        .includes(searchTerm) ||
                    item.agent.nom.toLowerCase().includes(searchTerm) ||
                    item.agent.prenom.toLowerCase().includes(searchTerm) ||
                    item.description.toLowerCase().includes(searchTerm) ||
                    item.status.toLowerCase().includes(searchTerm)
            );
        }
    }
}
