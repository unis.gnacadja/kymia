import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    waitForAsync
} from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import {
    loadingServiceMock,
    mockActivatedRoute,
    notifyServiceMock
} from '@lib/mocks';
import { of, throwError } from 'rxjs';
import * as CaseMock from 'src/app/mock/case.mock.json';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { caseServiceMock, userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';
import { DetailsCasPage } from './details-cas.page';

describe('DetailsCasPage', () => {
    let component: DetailsCasPage;
    let fixture: ComponentFixture<DetailsCasPage>;

    mockActivatedRoute.queryParams = of({ caseId: CaseMock.id });

    beforeAll(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DetailsCasPage],
            imports: [IonicModule.forRoot()],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                {
                    provide: CaseService,
                    useValue: caseServiceMock
                },
                {
                    provide: UserService,
                    useValue: userServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DetailsCasPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should render popover with success message when close case successfully', fakeAsync(() => {
        caseServiceMock.getCases.mockReturnValue(of([CaseMock]));
        component.close();
        tick();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.caseClosed,
            messages.thanks,
            State.success
        );
    }));

    it('should render popover with error message when close case error', async () => {
        caseServiceMock.updateStatus.mockReturnValue(
            throwError({ status: 500 })
        );
        await component.close();
        expect(notifyServiceMock.presentToast).toHaveBeenCalled();
    });
});
