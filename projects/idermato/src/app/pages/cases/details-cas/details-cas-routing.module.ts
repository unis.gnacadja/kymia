import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsCasPage } from './details-cas.page';

const routes: Routes = [
    {
        path: '',
        component: DetailsCasPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetailsCasPageRoutingModule {}
