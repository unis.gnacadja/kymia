import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { helperMessages } from '@lib/constants';
import { Media } from '@lib/types';
import { finalize } from 'rxjs/operators';
import { Case } from 'src/app/models/types/case';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { CaseStatus, State } from 'src/app/utils/enum';

@Component({
    selector: 'app-details-cas',
    templateUrl: './details-cas.page.html',
    styleUrls: ['./details-cas.page.scss']
})
export class DetailsCasPage implements OnInit {
    case: Case;
    medias: Media[] = [];
    CaseStatus = CaseStatus;

    constructor(
        public activatedRoute: ActivatedRoute,
        public caseService: CaseService,
        public userService: UserService,
        public notifyService: NotifyService,
        public loadingService: LoadingService
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.setCase(params['caseId']);
        });
    }

    /**
     * Close the cas
     */
    async close(): Promise<void> {
        await this.loadingService.present(helperMessages.loading);

        this.caseService
            .updateStatus(this.case.id, CaseStatus.close)
            .pipe(
                finalize(() => {
                    this.loadingService.dismiss();
                })
            )
            .subscribe({
                next: async () => {
                    await this.getCase();
                    this.notifyService.setPopover(
                        true,
                        messages.caseClosed,
                        messages.thanks,
                        State.success
                    );
                },
                error: () => {
                    this.notifyService.presentToast();
                }
            });
    }

    /**
     * Retrieves cases from the service and loads a specific case.
     */
    async getCase(): Promise<void> {
        await this.caseService.getAll();
        this.setCase(this.case.id);
    }

    /**
     * Set the case
     *
     * @param caseId case id
     */
    setCase(caseId) {
        this.case = this.caseService.list.find((item) => item.id === caseId);
        this.medias = this.case.medias.map((media) => ({
            src: media.url,
            alt: this.case.description,
            title: this.case.description
        }));
    }
}
