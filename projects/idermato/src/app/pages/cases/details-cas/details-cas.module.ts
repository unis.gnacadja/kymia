import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DetailsCasPageRoutingModule } from './details-cas-routing.module';
import { DetailsCasPage } from './details-cas.page';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DetailsCasPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [DetailsCasPage]
})
export class DetailsCasPageModule {}
