import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasesPage } from './cases.page';

const routes: Routes = [
    {
        path: '',
        component: CasesPage
    },
    {
        path: 'detail-cas',
        loadChildren: () =>
            import('../cases/details-cas/details-cas.module').then(
                (m) => m.DetailsCasPageModule
            )
    },
    {
        path: 'diagnostics',
        loadChildren: () =>
            import('../cases/diagnotics/diagnostics.module').then(
                (m) => m.DiagnosticsPageModule
            )
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CasesPageRoutingModule {}
