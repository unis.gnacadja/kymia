import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NextDate } from '@kymia/src/app/pipe';
import { ShortenPipe } from 'ngx-pipes';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { NormalizeDate, NormalizeHour } from 'src/app/pipes';
import { CasesPageRoutingModule } from './cases-routing.module';
import { CasesPage } from './cases.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        IonicModule,
        CasesPageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [CasesPage],
    providers: [NextDate, NormalizeDate, NormalizeHour, ShortenPipe]
})
export class CasesPageModule {}
