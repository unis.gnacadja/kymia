import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { loadingServiceMock, notifyServiceMock } from '@lib/mocks';
import { of, throwError } from 'rxjs';
import * as caseMock from 'src/app/mock/case.mock.json';
import { CaseService } from 'src/app/services/case.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { NotifyService } from 'src/app/services/common/notify.service';
import { caseServiceMock, normalizedCaseWithCommentAgent } from 'src/app/tests';
import { CasesPage } from './cases.page';

describe('CasesPage', () => {
    let component: CasesPage;
    let fixture: ComponentFixture<CasesPage>;
    const normalizedCase = normalizedCaseWithCommentAgent;
    const listCaseMock = [...Array(11).fill(caseMock)];

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CasesPage],
            imports: [IonicModule.forRoot(), ReactiveFormsModule],
            providers: [
                {
                    provide: CaseService,
                    useValue: caseServiceMock
                },
                {
                    provide: LoadingService,
                    useValue: loadingServiceMock
                },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(CasesPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should display search Input when the list has more than 9 items', fakeAsync(() => {
        caseServiceMock.getCases.mockReturnValueOnce(of(listCaseMock));
        component.ngOnInit();
        fixture.detectChanges();
        let searchInput: HTMLElement = fixture.nativeElement.querySelector(
            '[data-test-id="search"]'
        );
        expect(searchInput).not.toBeNull();
    }));

    it('should throw error when getCases fails', async () => {
        caseServiceMock.getAll.mockRejectedValueOnce(throwError('Error'));

        component.getCases().then(() => {
            expect(component.notifyService.presentToast).toHaveBeenCalled();
        });
    });

    it('should filter the list by agent domain', () => {
        const expectedResult = {
            ...normalizedCase,
            agent: {
                ...normalizedCase.agent,
                domain: {
                    ...normalizedCase.agent.domain,
                    name: 'Chirugie'
                }
            }
        };
        component.caseService.list = [normalizedCase, expectedResult];
        component.searchForm.setValue({
            search: expectedResult.agent.domain.name
        });
        component.ngDoCheck();
        expect(component.filteredList).toEqual([expectedResult]);
    });

    it('should filter the list by agent name', () => {
        const expectedResult = {
            ...normalizedCase,
            agent: {
                ...normalizedCase.agent,
                nom: 'Benoit'
            }
        };
        component.caseService.list = [normalizedCase, expectedResult];
        component.searchForm.setValue({ search: expectedResult.agent.nom });
        component.ngDoCheck();
        expect(component.filteredList).toEqual([expectedResult]);
    });

    it('should filter the list by agent prenom', () => {
        const expectedResult = {
            ...normalizedCase,
            agent: {
                ...normalizedCase.agent,
                prenom: 'Martin'
            }
        };
        component.caseService.list = [normalizedCase, expectedResult];
        component.searchForm.setValue({ search: expectedResult.agent.prenom });
        component.ngDoCheck();
        expect(component.filteredList).toEqual([expectedResult]);
    });

    it('should filter the list by description', () => {
        const expectedResult = {
            ...normalizedCase,
            description: 'LaDescription'
        };
        component.caseService.list = [normalizedCase, expectedResult];
        component.searchForm.setValue({ search: expectedResult.description });
        component.ngDoCheck();
        expect(component.filteredList).toEqual([expectedResult]);
    });

    it('should filter the list by status', () => {
        const expectedResult = {
            ...normalizedCase,
            status: 'LeStatut'
        };
        component.caseService.list = [normalizedCase, expectedResult];
        component.searchForm.setValue({ search: expectedResult.status });
        component.ngDoCheck();
        expect(component.filteredList).toEqual([expectedResult]);
    });

    it('should initialize filterList with case service list before entering in the page', () => {
        component.ionViewWillEnter();
        expect(component.filteredList).toEqual(component.caseService.list);
    });
});
