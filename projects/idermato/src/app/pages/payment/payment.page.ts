import { Component, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { dataPattern, rangeValidator } from '@lib/MustMatch.validator';
import { PhoneService } from '@lib/services/phone.service';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/internal/operators/finalize';
import { TypeError } from 'src/app/models/types/typeError';
import { User } from 'src/app/models/types/user';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { State } from 'src/app/utils/enum';
import { errorMessages, messages } from '../../utils/constants';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.page.html',
    styleUrls: ['./payment.page.scss']
})
export class PaymentComponent implements OnInit {
    paymentForm: FormGroup;
    loading: BehaviorSubject<boolean>;
    isReadonly: boolean = true;
    error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    errors: Record<string, TypeError> = errorMessages;
    nameValidator: ValidatorFn = Validators.pattern(dataPattern.lettersOnly);
    numberValidator: ValidatorFn = Validators.pattern(dataPattern.numbersOnly);
    ribValidators: ValidatorFn[] = [
        rangeValidator(20, 34),
        Validators.pattern(dataPattern.alphaNumeric)
    ];
    phoneValidators: ValidatorFn[] = [
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern(dataPattern.phone)
    ];
    requiredValidators: ValidatorFn = Validators.required;
    user: User;
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly userService: UserService,
        private readonly notifyService: NotifyService,
        private readonly phoneService: PhoneService
    ) {}

    ngOnInit(): void {
        this.loading = new BehaviorSubject<boolean>(false);
        this.paymentForm = this.formBuilder.group(
            {
                name: [
                    this.userService.user.compteBanquaire
                        ? this.userService.user.compteBanquaire.nomTitulaire
                        : ''
                ],
                rib: [
                    this.userService.user.compteBanquaire
                        ? this.userService.user.compteBanquaire.rib
                        : ''
                ],
                phone: [
                    this.userService.user.numeroPaiements &&
                    this.userService.user.numeroPaiements.length > 0
                        ? this.phoneService.getPhoneNumber(
                              this.userService.user.numeroPaiements[0].numero
                          )
                        : ''
                ],
                tarif: [this.userService.user.tarif]
            },
            {
                updateOn: 'blur'
            }
        );
    }

    canModify(): void {
        this.isReadonly = false;
    }

    async onSubmit(): Promise<void> {
        this.error.next(null);
        if (!this.loading.getValue() && this.paymentForm.valid) {
            if (
                (this.paymentForm.get('name').value ||
                    this.paymentForm.get('rib').value) &&
                (!this.paymentForm.get('name').value ||
                    !this.paymentForm.get('rib').value)
            ) {
                this.error.next(messages.incompleteInfo);
            } else {
                await this.userService
                    .updateAgent({
                        compteBanquaire: {
                            nomTitulaire: this.paymentForm.get('name').value,
                            rib: this.paymentForm.get('rib').value
                        },
                        numeroPaiements: this.paymentForm.get('phone').value
                            ? [
                                  {
                                      numero: `${this.phoneService.dialCode}${
                                          this.paymentForm.get('phone').value
                                      }`
                                  }
                              ]
                            : [],
                        tarif: this.paymentForm.get('tarif').value
                    })
                    .pipe(
                        finalize(() => {
                            this.loading.next(false);
                            this.isReadonly = true;
                        })
                    )
                    .subscribe({
                        next: (user: User) => {
                            this.userService.setUser(user);
                            this.notifyService.setPopover(
                                true,
                                messages.updateSuccess,
                                messages.updateSuccessTitle,
                                State.success
                            );
                            // update user
                            this.userService.getAgent().subscribe({
                                next: (updatedUser: User) => {
                                    this.userService.setUser(updatedUser);
                                }
                            });
                        },
                        error: () => {
                            this.notifyService.setPopover(
                                true,
                                messages.updateFailed,
                                messages.updateFailedTitle,
                                State.failed
                            );
                        }
                    });
            }
        }
    }
}
