import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';
import { PaymentComponent } from './payment.page';

describe('PaymentPage', () => {
    let component: PaymentComponent;
    let fixture: ComponentFixture<PaymentComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [PaymentComponent],
            imports: [IonicModule.forRoot()],
            providers: [
                FormBuilder,
                { provide: UserService, useValue: userServiceMock },
                {
                    provide: NotifyService,
                    useValue: notifyServiceMock
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(PaymentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    describe('TA1: should initialize fields as empty when user data is not available', () => {
        it('TA1-1: should initialize name field as empty', () => {
            const nameField = component.paymentForm.get('name');
            expect(nameField.value).toBe('');
        });

        it('TA1-2: should initialize rib field as empty', () => {
            const ribField = component.paymentForm.get('rib');
            expect(ribField.value).toBe('');
        });

        it('TA1-3: should initialize phone field as empty', () => {
            const phoneField = component.paymentForm.get('phone');
            expect(phoneField.value).toBe('');
        });
    });

    describe('TA2: should initialize fields with user data when available', () => {
        beforeAll(() => {
            userServiceMock.user.compteBanquaire = {
                banque: 'UBA',
                rib: '1234567890',
                nomTitulaire: 'James Doe'
            };
            userServiceMock.user.numeroPaiements = [
                {
                    numero: '66457812'
                }
            ];
        });

        it("TA2-1: should initialize name field with user's bank name when user data is available", () => {
            const nameField = component.paymentForm.get('name');
            expect(nameField.value).toBe('James Doe');
        });

        it("TA2-2: should initialize rib field with user's rib when user data is available", () => {
            const ribField = component.paymentForm.get('rib');
            expect(ribField.value).toBe('1234567890');
        });

        it("TA2-3: should initialize phone field with user's phone when user data is available", () => {
            const phoneField = component.paymentForm.get('phone');
            expect(phoneField.value).toBe('66457812');
        });
    });

    it('TA3: submit button should have title "Modifier" after initialization', () => {
        const buttonElement = fixture.nativeElement.querySelector(
            '[data-test-id="submit-button"]'
        );
        expect(buttonElement.getAttribute('title')).toBe(messages.update);
    });

    it('TA4: submit button should have title "Valider" onclick on "Modifier"', () => {
        component.canModify();
        fixture.detectChanges();
        const buttonElement = fixture.nativeElement.querySelector(
            '[data-test-id="submit-button"]'
        );
        expect(buttonElement.getAttribute('title')).toBe(messages.validate);
    });

    it('TA5: should display error message when either name is empty and rib is not', () => {
        component.isReadonly = false;
        component.paymentForm.get('name').setValue('');
        component.paymentForm.get('rib').setValue('ValidRib');
        component.onSubmit();
        fixture.detectChanges();

        const errorElement = fixture.nativeElement.querySelector(
            '[data-test-id="submit-error"]'
        );
        expect(errorElement.textContent).toContain(messages.incompleteInfo);
    });

    it('TA6: should display error message when either rib is empty and name is not', () => {
        component.isReadonly = false;
        component.paymentForm.get('name').setValue('ValidName');
        component.paymentForm.get('rib').setValue('');
        component.onSubmit();
        fixture.detectChanges();

        const errorElement = fixture.nativeElement.querySelector(
            '[data-test-id="submit-error"]'
        );
        expect(errorElement.textContent).toContain(messages.incompleteInfo);
    });

    it('TA7: should call presentPopover with error message on submit on success', async () => {
        component.isReadonly = false;
        component.paymentForm.controls['name'].setValue('John Doe');
        component.paymentForm.controls['rib'].setValue('012345678901');
        component.paymentForm.controls['phone'].setValue('012345678901');

        await component.onSubmit();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.updateSuccess,
            messages.updateSuccessTitle,
            State.success
        );
    });

    it('TA8: should call userService.updateAgent with empty numeroPaiements when phone is empty', async () => {
        component.isReadonly = false;
        component.paymentForm.controls['name'].setValue(
            userServiceMock.user.compteBanquaire.nomTitulaire
        );
        component.paymentForm.controls['rib'].setValue(
            userServiceMock.user.compteBanquaire.rib
        );
        component.paymentForm.controls['phone'].setValue('');
        component.paymentForm.controls['tarif'].setValue(150);
        await component.onSubmit();
        expect(userServiceMock.updateAgent).toHaveBeenCalledWith({
            compteBanquaire: {
                nomTitulaire: userServiceMock.user.compteBanquaire.nomTitulaire,
                rib: userServiceMock.user.compteBanquaire.rib
            },
            numeroPaiements: [],
            tarif: 150
        });
    });

    it('TA9: should call presentPopover with error message on submit when error occurs', async () => {
        userServiceMock.updateAgent.mockReturnValue(
            throwError({ statut: 500 })
        );
        component.isReadonly = false;
        component.paymentForm.controls['name'].setValue('John Doe');
        component.paymentForm.controls['rib'].setValue('012345678901');
        component.paymentForm.controls['phone'].setValue('012345678901');
        await component.onSubmit();
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.updateFailed,
            messages.updateFailedTitle,
            State.failed
        );
    });
});
