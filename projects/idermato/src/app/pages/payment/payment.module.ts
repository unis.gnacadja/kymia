import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { PaymentPageRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PaymentPageRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [],
    declarations: [PaymentComponent]
})
export class PaymentPageModule {}
