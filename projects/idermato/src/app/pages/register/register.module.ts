import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { RegisterPageRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.page';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RegisterPageRoutingModule
    ],
    providers: [],
    declarations: [RegisterComponent]
})
export class RegisterPageModule {}
