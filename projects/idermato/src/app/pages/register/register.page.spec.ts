import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { IonicModule, NavController } from '@ionic/angular';
import { MockUser } from '@kymia/src/app/mock';
import { helperMessages } from '@lib/constants';
import { notifyServiceMock } from '@lib/mocks';
import { throwError } from 'rxjs';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService as RegisterService } from 'src/app/services/user.service';
import { navControllerMock, routerMock, userServiceMock } from 'src/app/tests';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';
import { RegisterComponent } from './register.page';

let user = {
    nom: MockUser.simpleIdermato.nom,
    prenom: MockUser.simpleIdermato.prenom,
    numero: MockUser.simpleIdermato.numero,
    email: MockUser.simpleIdermato.email,
    fonction: MockUser.simpleIdermato.fonction,
    pays: MockUser.simpleIdermato.pays,
    ville: MockUser.simpleIdermato.ville,
    photo: ''
};

describe('RegisterComponent', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    beforeAll(() => {
        TestBed.configureTestingModule({
            declarations: [RegisterComponent],
            imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: RegisterService, useValue: userServiceMock },
                { provide: NotifyService, useValue: notifyServiceMock },
                { provide: Router, useValue: routerMock },
                {
                    provide: NavController,
                    useValue: navControllerMock
                },
                FormBuilder
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should notify on successful form submission', () => {
        component.register({ user });
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.loginInfos,
            messages.registerSuccessfully,
            State.limeGreenDarker
        );
    });

    it('should navigate to the login page when closeEvent$ emits', fakeAsync(() => {
        jest.spyOn(routerMock, 'navigate');
        component.register({ user });
        tick();
        expect(routerMock.navigate).toHaveBeenCalledWith(['/login']);
    }));

    it('should notify on form submission failure with general error', async () => {
        userServiceMock.preRegisterUser.mockReturnValue(
            throwError({ error: { status: 500 } })
        );
        await component.register({ user });
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            helperMessages.errorOccurred,
            messages.error,
            State.failed
        );
    });

    it('should notify on form submission failure with status 12', async () => {
        userServiceMock.preRegisterUser.mockReturnValue(
            throwError({ error: { status: 12 } })
        );
        await component.register({ user });
        expect(notifyServiceMock.setPopover).toHaveBeenCalledWith(
            true,
            messages.emailAlreadyUsed,
            messages.error,
            State.failed
        );
    });
});
