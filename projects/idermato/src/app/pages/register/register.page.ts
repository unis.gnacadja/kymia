import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { helperMessages } from '@lib/constants';
import { finalize } from 'rxjs/internal/operators/finalize';
import { take } from 'rxjs/operators';
import { User } from 'src/app/models/types/user';
import { NotifyService } from 'src/app/services/common/notify.service';
import { UserService } from 'src/app/services/user.service';
import { messages } from 'src/app/utils/constants';
import { State } from 'src/app/utils/enum';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss']
})
export class RegisterComponent {
    loading: boolean = false;
    isReadonly: boolean = false;

    constructor(
        private userService: UserService,
        private notifyService: NotifyService,
        public router: Router
    ) {}

    async register(payload: { user: User }) {
        this.loading = true;
        this.userService
            .preRegisterUser(payload.user)
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe({
                next: () => {
                    this.notifyService.setPopover(
                        true,
                        messages.loginInfos,
                        messages.registerSuccessfully,
                        State.limeGreenDarker
                    );
                    this.notifyService.closeEvent$
                        .pipe(take(1))
                        .subscribe(() => {
                            this.router.navigate(['/login']);
                        });
                },
                error: (error) => {
                    this.notifyService.setPopover(
                        true,
                        error.error.status === 12
                            ? messages.emailAlreadyUsed
                            : helperMessages.errorOccurred,
                        messages.error,
                        State.failed
                    );
                }
            });
    }
}
