import { MockUser } from '@kymia/src/app/mock';
import { Calendar } from '@kymia/src/app/models';
import { ConsultationStatus } from '@lib/enum';
import { of } from 'rxjs';
import * as CaseMock from 'src/app/mock/case.mock.json';
import * as DiaryMock from 'src/app/mock/diary.json';
import * as DoctorMock from 'src/app/mock/doctor.json';
import * as Plannings from 'src/app/mock/plannings.json';
import * as PrescriptionMock from 'src/app/mock/prescription.mock.json';
import { Case, Diagnostic } from '../models/types/case';
import { Diary } from '../models/types/diary';
import { User } from '../models/types/user';
import { normalizeCase, normalizeDiagnostic } from '../utils/normalizer';

export const diaries = [
    ...Array(10).fill(DiaryMock),
    {
        ...DiaryMock,
        transaction: {
            billingOperator: 'Expected billingOperator',
            paymentProof: 'Expected paymentProof',
            transactionDate: 'Expected transactionDate'
        }
    }
];
export const caseWithCommentAgent = {
    ...CaseMock,
    listCommentaires: [
        {
            ...CaseMock.listCommentaires[0],
            agent: DoctorMock.agent
        }
    ]
};
export const normalizedCaseWithCommentAgent =
    normalizeCase(caseWithCommentAgent);
export const doctorListMock = [
    ...Array(10).fill(DoctorMock),
    {
        agent: {
            ...DoctorMock.agent,
            photo: null
        },
        calendar: DoctorMock.calendar
    }
];

export const loadingControllerSpy = {
    create: jest
        .fn()
        .mockResolvedValue({ present: jest.fn(), dismiss: jest.fn() }),
    dismiss: jest.fn()
};

export const locationMock = {
    back: jest.fn()
};

export const platformMock = {
    resume: of({})
};

export const userServiceMock: {
    user: Partial<User>;
    getUserInfo: () => Partial<User>;
    resetPasswordAgent: jest.Mock;
    preRegisterUser: jest.Mock;
    getAgent: jest.Mock;
    setUser: jest.Mock;
    updateAgent: jest.Mock;
    isLogged: true;
    logOut: jest.Mock;
    updatePassword: jest.Mock;
} = {
    user: MockUser.simpleIdermato,
    getUserInfo: () => MockUser.simpleIdermato,
    preRegisterUser: jest.fn().mockReturnValue(of({})),
    resetPasswordAgent: jest.fn().mockReturnValue(of({})),
    getAgent: jest.fn().mockReturnValue(of(MockUser.completeProfile)),
    setUser: jest.fn(),
    updateAgent: jest.fn().mockReturnValue(of(MockUser.simpleIdermato)),
    logOut: jest.fn(),
    isLogged: true,
    updatePassword: jest.fn().mockReturnValue(of({}))
};

export const routerMock = {
    navigate: jest.fn()
};

export const navControllerMock = {
    navigateRoot: jest.fn(),
    navigate: jest.fn()
};

export const consultationServiceMock: {
    list: Diary[];
    filteredList: Diary[];
    searchConsultations: jest.Mock;
    getAvailableDoctor: jest.Mock;
    getConsultation: jest.Mock;
    confirm: jest.Mock;
    close: jest.Mock;
    reportConsultation: jest.Mock;
    searchDiary: jest.Mock;
    transfertConsultation: jest.Mock;
    canceledConsultation: jest.Mock;
    setConsultation: jest.Mock;
    setConsultations: jest.Mock;
} = {
    list: diaries,
    filteredList: diaries,
    searchConsultations: jest.fn(() => of(diaries)),
    getConsultation: jest.fn().mockReturnValue(
        of({
            ...diaries[0],
            patient: {
                ...diaries[0].patient
            },
            problemDescription: diaries[0].problemDescription
        })
    ),
    confirm: jest.fn(() => of(diaries[0])),
    reportConsultation: jest.fn(() =>
        of({
            ...diaries[0],
            status: ConsultationStatus.terminated,
            ordonnace: PrescriptionMock
        })
    ),
    setConsultation: jest.fn(),
    setConsultations: jest.fn(),
    close: jest.fn(),
    searchDiary: jest.fn().mockReturnValue(DiaryMock),
    transfertConsultation: jest.fn().mockReturnValue(of({})),
    canceledConsultation: jest.fn().mockReturnValue(of({})),
    getAvailableDoctor: jest.fn().mockReturnValue(of(doctorListMock))
};

export const planningServiceMock: {
    registerPlanning: jest.Mock;
    searchCalendars: jest.Mock;
    searchDiaries: jest.Mock;
    list: Calendar[];
} = {
    registerPlanning: jest.fn(),
    searchCalendars: jest.fn(() => of(Plannings)),
    list: [],
    searchDiaries: jest.fn().mockReturnValue(of(Plannings))
};

export const caseServiceMock: {
    getCases: jest.Mock;
    list: Case[];
    save: jest.Mock;
    updateStatus: jest.Mock;
    getAll: jest.Mock;
} = {
    getCases: jest.fn(() => of([normalizedCaseWithCommentAgent])),
    list: Array(11).fill(normalizedCaseWithCommentAgent),
    save: jest.fn(() => of(normalizedCaseWithCommentAgent)),
    updateStatus: jest.fn(() => of(normalizedCaseWithCommentAgent)),
    getAll: jest.fn(() => of().toPromise())
};

export const diagnosticServiceMock: {
    getPrediction: jest.Mock;
    diagnostic: Diagnostic;
    suggest: jest.Mock;
    submitSuggestion: jest.Mock;
} = {
    getPrediction: jest.fn(() =>
        of({ predictions: CaseMock.listDiagnostique.predictionIA })
    ),
    diagnostic: normalizeDiagnostic(
        CaseMock.listDiagnostique.diagnostiques[0],
        CaseMock.id
    ),
    suggest: jest.fn(() => of({})),
    submitSuggestion: jest.fn(() =>
        of({
            idCas: CaseMock.id,
            idAgent: CaseMock.agent.id,
            diagnostique:
                CaseMock.listDiagnostique.diagnostiques[0].diagnostique,
            traitement: CaseMock.listDiagnostique.diagnostiques[0].traitement,
            prescription:
                CaseMock.listDiagnostique.diagnostiques[0].prescription
        })
    )
};
export const commentServiceMock: {
    getAll: jest.Mock;
    save: jest.Mock;
} = {
    getAll: jest.fn().mockReturnValue(of([CaseMock.listCommentaires])),
    save: jest.fn().mockReturnValue(of(CaseMock.listCommentaires[0]))
};
