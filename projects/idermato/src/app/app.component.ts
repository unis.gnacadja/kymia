import { Component, OnInit } from '@angular/core';
import { NotifyService } from './services/common/notify.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    openPopover: boolean = false;
    popoverContent: string = '';
    popoverTitle: string = '';
    popoverIcon: string = '';
    popoverColor: string = '';

    constructor(public notifyService: NotifyService) {}

    ngOnInit(): void {
        this.notifyService.getPopover().subscribe(() => {
            this.openPopover = this.notifyService.popover.getValue().isOpen;
            if (this.openPopover) {
                this.popoverContent =
                    this.notifyService.popover.getValue().content;
                this.popoverTitle = this.notifyService.popover.getValue().title;
                this.popoverColor = this.notifyService.popover.getValue().color;
                this.popoverIcon = this.notifyService.popover.getValue().icon;
            }
        });
    }
}
