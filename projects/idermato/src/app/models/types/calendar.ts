export type Calendar = {
    color: string;
    dateDebut: string;
    dateFin: string;
    days: string[];
    doctorId: string;
    duration: number;
    evening: TimeRange;
    morning: TimeRange;
    nbrOccurences?: number;
    pause: number;
};

type TimeRange = {
    end: {
        hour: number;
        minutes: number;
    };
    start: {
        hour: number;
        minutes: number;
    };
};
