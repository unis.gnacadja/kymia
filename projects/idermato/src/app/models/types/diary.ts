import { User } from './user';

export type InfosCase = {
    descriptionClinique?: string;
    diagnostique?: string;
    evolution?: string;
    explorationParaclinique?: string;
    traitement?: string;
};

export type Diary = InfosCase & {
    id: string;
    calendar: {
        availableDate: string;
        availableDateStart: string;
        availableDateEnd?: string;
    };
    patient?: User;
    problemDescription: string;
    status: string;
    images?: string[];
    calendarId?: string;
    hour?: string;
    callUrl?: string;
    ordonnance?: {
        dateCreation: string;
        id: string;
        internalName: string;
        lien: string;
        originalName: string;
        type: string;
    };
    transaction?: {
        billingOperator: string;
        paymentProof: string;
        transactionDate: string;
    };
    idCase?: string;
};
