import { User } from './user';

export type Case = {
    id: string;
    agent: User;
    medias: Media[];
    description: string;
    comments: Comment[];
    diagnostics: Diagnostic[];
    createdAt: string;
    updatedAt: string;
    status: string;
};

export type Media = {
    id: string;
    type: string;
    url: string;
};

export type Comment = {
    id: string;
    content: string;
    agent?: User;
    createdAt: string;
    updatedAt: string;
};

export type Diagnostic = {
    id?: string;
    caseId: string;
    agent: User | 'IA';
    final: boolean;
    prescription?: string;
    diagnostic: string;
    probability?: number;
    treatment?: string;
    createdAt: string;
    updatedAt: string;
};
