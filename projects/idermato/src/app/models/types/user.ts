import { User as KymiaUser } from '@kymia/src/app/models/types/user';

export type User = Pick<
    KymiaUser,
    'id' | 'completeProfile' | 'photo' | 'email'
> & {
    nom: string;
    prenom: string;
    numero: string;
    fonction?: string;
    secteurActivite?: string;
    pays: string;
    ville: string;
    validPlanning?: boolean;
    compteBanquaire?: PaymentInformations;
    numeroPaiements?: {
        numero: string;
    }[];
    tarif?: number;
    dateDeNaissance?: string;
    firstConnection?: boolean;
    domain?: Domain;
};

type PaymentInformations = {
    banque?: string;
    nomTitulaire?: string;
    rib?: string;
};

type Domain = {
    id: string;
    name: string;
    fonctionName: string;
    description: string;
    icon: string;
};
