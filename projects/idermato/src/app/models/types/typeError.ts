export type TypeError = {
    type: string;
    description: string;
};
