import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'normalizeDate',
    pure: true
})
/**
 * transform the date from JJ-MM-AAAA to a valid Date
 */
export class NormalizeDate implements PipeTransform {
    transform(value: string, separator: string): Date {
        const availableDate = value.split(separator);
        const day = parseInt(availableDate[0], 10);
        const month = parseInt(availableDate[1], 10) - 1;
        const year = parseInt(availableDate[2], 10);
        return new Date(year, month, day, 1);
    }
}

@Pipe({
    name: 'normalizeHour',
    pure: true
})
/**
 * transform the hour from HH:MM to a valid Date
 */
export class NormalizeHour implements PipeTransform {
    transform(date: Date, time: string): Date {
        date.setHours(parseInt(time.split(':')[0], 10));
        date.setMinutes(parseInt(time.split(':')[1], 10));
        return date;
    }
}

@Pipe({
    name: 'message'
})
export class MessagePipe implements PipeTransform {
    transform(
        value: string,
        messages: Record<string, string>,
        list: Record<string, string>
    ) {
        return messages[Object.keys(list).find((key) => list[key] === value)];
    }
}
