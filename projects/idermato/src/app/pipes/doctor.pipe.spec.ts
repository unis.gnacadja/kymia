import { doctorServiceMock } from '@kymia/src/app/mock';
import { SearchDoctorPipe } from './doctor.pipe';

describe('Pipe: SearchDoctorPipe', () => {
    let pipe: SearchDoctorPipe = new SearchDoctorPipe();

    const list = doctorServiceMock.list;
    it('should filter doctors list based on name ', () => {
        const filterTerm = list[0].name;
        expect(pipe.transform(list, filterTerm)[0].name).toEqual(filterTerm);
    });
    it('should filter doctors list based on  firstname ', () => {
        const filterTerm = list[0].firstname;
        expect(pipe.transform(list, filterTerm)[0].firstname).toEqual(
            filterTerm
        );
    });
    it('should filter doctors list based on  domain name ', () => {
        const filterTerm = list[0].domain.name;
        expect(pipe.transform(list, filterTerm)[0].domain.name).toEqual(
            filterTerm
        );
    });
});
