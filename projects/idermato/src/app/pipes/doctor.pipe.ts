import { Pipe, PipeTransform } from '@angular/core';
import { Doctor } from '@kymia/src/app/models';

@Pipe({
    name: 'searchDoctor'
})
export class SearchDoctorPipe implements PipeTransform {
    transform(list: Doctor[], filterTerm: string): Doctor[] {
        return list.filter(
            (item) =>
                item.name.toLowerCase().includes(filterTerm.toLowerCase()) ||
                item.firstname
                    .toLowerCase()
                    .includes(filterTerm.toLowerCase()) ||
                item.domain.name
                    .toLowerCase()
                    .includes(filterTerm.toLowerCase())
        );
    }
}
