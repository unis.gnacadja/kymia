import { TestBed } from '@angular/core/testing';
import { ConsultationStatus } from '@lib/enum';
import { MessagePipe, NormalizeDate, NormalizeHour } from '.';
import { statusMessages } from '../utils/constants';

describe('Pipe: NormalizeDate', () => {
    let pipe: NormalizeDate;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [NormalizeDate]
        });
        pipe = TestBed.inject(NormalizeDate);
    });

    it('should transform the date from JJ-MM-AAAA to a valid Date', () => {
        const input = '01-02-2023';
        const separator = '-';
        const expected = new Date(2023, 1, 1, 1);
        expect(pipe.transform(input, separator)).toEqual(expected);
    });
});

describe('Pipe: NormalizeHour', () => {
    let pipe: NormalizeHour;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [NormalizeHour]
        });
        pipe = TestBed.inject(NormalizeHour);
    });

    it('should transform the hour from HH:MM to a valid Date', () => {
        const input = new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate(),
            0,
            0
        );
        const time = '12:00';
        const expected = new Date(
            input.getFullYear(),
            input.getMonth(),
            input.getDate(),
            12,
            0
        );
        expect(pipe.transform(input, time)).toEqual(expected);
    });
});

describe('Pipe: NormalizeHour', () => {
    let pipe: MessagePipe;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MessagePipe]
        });
        pipe = TestBed.inject(MessagePipe);
    });

    it('should return the right status', () => {
        const result = pipe.transform(
            ConsultationStatus.waiting,
            statusMessages,
            ConsultationStatus
        );
        expect(result).toEqual(statusMessages.waiting);
    });
});
