import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Diagnostic } from '../models/types/case';
import { UserService } from './user.service';

@Injectable()
export class DiagnosticService {
    api = `${environment.baseUrl}/service/diagnostiques`;
    predictionApi = `${environment.baseUrl}/service/predictions`;

    constructor(public http: HttpClient, public userService: UserService) {}

    suggest(formData: Partial<Diagnostic>): Observable<unknown> {
        return this.http.post(
            `${this.api}/intermediaire?userId=${this.userService.user.id}`,
            {
                ...formData,
                idAgent: this.userService.user.id
            }
        );
    }

    /**
     * Get the prediction for a specific case.
     *
     * @param caseId - The ID of the case.
     * @returns An observable that emits the prediction.
     */
    getPrediction(caseId: string): Observable<unknown> {
        return this.http.get(
            `${this.predictionApi}/${caseId}?userId=${this.userService.user.id}`
        );
    }
}
