import { Injectable } from '@angular/core';
import { UserService as KymiaUserService } from '@kymia/src/app/services/user.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/types/user';

@Injectable()
export class UserService extends KymiaUserService {
    isLogged = false;

    token: string = '';
    location = new BehaviorSubject({ id: '', city: '', country: '' });

    agent: string = 'service/agents';

    /**
     * Sends a request to reset the password for the specified agent's email.
     *
     * @param email - The email address of the agent whose password is to be reset.
     * @returns An observable containing the response from the reset password request.
     */
    resetPasswordAgent(email: string): Observable<unknown> {
        return this.http.get(
            `${this.api}/${this.agent}/resetPassword?mail=${email}`
        );
    }

    /**
     * Authenticates an agent using their email and password.
     *
     * @param email - The email address of the agent.
     * @param password - The password of the agent.
     * @returns An observable containing the response from the login request.
     */
    login({
        email,
        password
    }: {
        email: string;

        password: string;
    }): Observable<unknown> {
        return this.http.get(
            `${this.api}/${this.agent}/connexion?login=${email}&pwd=${password}`
        );
    }

    /**
     * Pre-registers a user by sending their information to the server.
     *
     * @param user - The user object containing registration details.
     * @returns An observable containing the response from the pre-registration request.
     */
    preRegisterUser(user: User): Observable<unknown> {
        return this.http.post(
            `${this.api}/${this.agent}/pre-inscription`,
            user
        );
    }

    /**
     * Updates the details of a specified agent.
     *
     * @param agent - The agent object containing the details to be updated, excluding the email.
     * @returns An observable containing the response from the update request.
     */
    updateAgent(agent: Omit<Partial<User>, 'email'>): Observable<unknown> {
        return this.http.put(
            `${this.api}/${this.agent}/partial_upadate/${this.user.id}?userId=${this.user.id}`,
            agent
        );
    }

    /**
     * Fetches the user's information from the server
     *
     * @returns An observable containing the response from the get request.
     */
    getAgent(): Observable<unknown> {
        return this.http.get(
            `${this.api}/${this.agent}/${this.user.id}?userId=${this.user.id}`
        );
    }

    /**
     * Sets media (e.g., profile picture) for the current user.
     *
     * @param formData - The FormData object containing the media files to be uploaded.
     * @returns An observable containing the response from the media upload request.
     */
    setMedia(formData: FormData): Observable<unknown> {
        return this.http.post(
            `${this.api}/service/medias/icon/${this.user.id}?userId=${this.user.id}`,
            formData
        );
    }

    /**
     * Updates the password for the current agent.
     *
     * @param newPassword - The new password to be set.
     * @param oldPassword - The current password for verification.
     * @returns An observable containing the response from the password update request.
     */
    updatePassword(
        newPassword: string,
        oldPassword: string
    ): Observable<unknown> {
        const url = `${this.api}/service/agents/updatePassword/${this.user.id}`;
        const params = `newPassword=${newPassword}&oldPassword=${oldPassword}&userId=${this.user.id}`;

        return this.http.put(`${url}?${params}`, {});
    }
}
