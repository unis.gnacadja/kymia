import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MockUser } from '@kymia/src/app/mock';
import * as MockPlanning from '../mock/planning.json';
import { Planning } from '../models/types/planning';
import { PlanningService } from './planning.service';
import { UserService } from './user.service';

describe('PlanningService', () => {
    let service: PlanningService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [PlanningService, UserService]
        });
        service = TestBed.inject(PlanningService);
        httpMock = TestBed.inject(HttpTestingController);

        service.userService.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should register planning', () => {
        const planning: Planning = MockPlanning.planning;
        const response = {
            message: 'planning registered'
        };
        service.registerPlanning(planning).subscribe((resp) => {
            expect(resp).toEqual(response);
        });
        const request = httpMock.expectOne(
            `${service.api}/service/calendars/all?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });

    it('should search calendars', () => {
        const response = {
            calendars: []
        };

        service.searchCalendars('123', 1, 10).subscribe((resp) => {
            expect(resp).toEqual(response);
        });

        const request = httpMock.expectOne(
            `${service.api}/service/calendars/doctor/123?page=1&userId=123&size=10`
        );
        request.flush(response);
    });

    it('should search diaries', () => {
        const startDay = MockPlanning.planning.dateDebut;
        const endDay = MockPlanning.planning.dateFin;
        const response = {
            message: 'search diaries'
        };
        service.searchDiaries(startDay, endDay).subscribe((resp) => {
            expect(resp).toEqual(response);
        });

        const request = httpMock.expectOne(
            `${service.api}/service/calendars/search?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });
});
