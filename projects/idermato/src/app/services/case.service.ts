import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Case } from '../models/types/case';
import { normalizeCase } from '../utils/normalizer';
import { UserService } from './user.service';

@Injectable()
export class CaseService {
    api = `${environment.baseUrl}/service/cases`;

    list: Case[] = [];

    constructor(public http: HttpClient, public userService: UserService) {}

    getCases(): Observable<unknown> {
        return this.http.get(`${this.api}?userId=${this.userService.user.id}`);
    }

    save(consultationId: string, data = {}): Observable<any> {
        return this.http.post(
            `${this.api}/consultation/${consultationId}?userId=${this.userService.user.id}`,
            data
        );
    }

    updateStatus(caseId: string, status: string): Observable<unknown> {
        return this.http.put(
            `${this.api}/changeStatus/${caseId}/${status}?userId=${this.userService.user.id}`,
            {}
        );
    }

    /**
     * Update the list of cases
     */
    getAll(): Promise<void> {
        return this.getCases()
            .toPromise()
            .then((data: any[]) => {
                this.list = data
                    .map(normalizeCase)
                    .sort(
                        (a: Case, b: Case) =>
                            this.getTimeStamp(b) - this.getTimeStamp(a)
                    );
            });
    }

    /**
     * Gets the timestamp of the creation date of a given case.
     *
     * @param selectedCase - The case object containing the creation date.
     * @returns The timestamp of the creation date.
     */
    getTimeStamp(selectedCase: Case) {
        const dateParts = selectedCase.createdAt.split('-');
        return new Date(
            `${dateParts[1]} ${dateParts[0]}, ${dateParts[2]}`
        ).getTime();
    }
}
