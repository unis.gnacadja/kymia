import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MockUser } from '@kymia/src/app/mock';
import { of } from 'rxjs';
import { messages } from '../utils/constants';
import { UserService } from './user.service';

describe('UserService', () => {
    let service: UserService;

    beforeAll(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [UserService]
        });
        service = TestBed.inject(UserService);
        service.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    it('should reset password for agent', () => {
        service.http.get = jest
            .fn()
            .mockReturnValue(of(MockUser.completeIdermato));

        service
            .resetPasswordAgent(MockUser.simpleIdermato.email)
            .subscribe((res) => {
                expect(res).toEqual(MockUser.completeIdermato);
            });
    });

    it('should log in user', () => {
        service.http.get = jest
            .fn()
            .mockReturnValue(of(MockUser.completeIdermato));

        service
            .login({
                email: MockUser.completeIdermato.email,
                password: MockUser.completeIdermato.password
            })
            .subscribe((res) => {
                expect(res).toEqual(MockUser.completeIdermato);
            });
    });

    it('should pre-register user', () => {
        service.http.post = jest
            .fn()
            .mockReturnValue(of(MockUser.simpleIdermato));

        service.preRegisterUser(MockUser.simpleIdermato).subscribe((res) => {
            expect(res).toEqual(MockUser.simpleIdermato);
        });
    });

    it('should update agent', () => {
        service.http.put = jest
            .fn()
            .mockReturnValue(of(MockUser.completeIdermato));

        service.updateAgent(MockUser.completeIdermato).subscribe((res) => {
            expect(res).toEqual(MockUser.completeIdermato);
        });
    });

    it('should set media', () => {
        const dummyFormData = new FormData();

        const response = {
            message: messages.updateSuccessTitle
        };

        service.http.post = jest.fn().mockReturnValue(of(response));

        service.setMedia(dummyFormData).subscribe((res) => {
            expect(res).toEqual(response);
        });
    });

    it('should get agent', () => {
        service.http.get = jest
            .fn()
            .mockReturnValue(of(MockUser.completeIdermato));

        service.getAgent().subscribe((res) => {
            expect(res).toEqual(MockUser.completeIdermato);
        });
    });

    it('should update password', () => {
        service.http.put = jest
            .fn()
            .mockReturnValue(of(MockUser.completeIdermato));

        // Call the updatePassword method
        service
            .updatePassword(
                MockUser.completeProfile.password,
                MockUser.completeIdermato.password
            )
            .subscribe((res) => {
                expect(res).toEqual(MockUser.completeIdermato);
            });
    });
});
