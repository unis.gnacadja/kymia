import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { normalizeComment } from '../utils/normalizer';
import { UserService } from './user.service';

@Injectable()
export class CommentService {
    api = `${environment.baseUrl}/service/commentaires`;

    constructor(public http: HttpClient, public userService: UserService) {}

    getAll(caseId: string): Observable<unknown> {
        return this.http
            .get(
                `${this.api}/byCas/${caseId}?userId=${this.userService.user.id}`
            )
            .pipe(map((dataList: any[]) => dataList.map(normalizeComment)));
    }

    save(caseId: string, comment: string): Observable<unknown> {
        return this.http.post(
            `${this.api}?userId=${this.userService.user.id}`,
            {
                contenu: comment,
                idCas: caseId,
                idAgent: this.userService.user.id
            }
        );
    }
}
