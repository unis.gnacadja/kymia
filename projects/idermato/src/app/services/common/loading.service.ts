import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class LoadingService {
    constructor(private loadingController: LoadingController) {}

    async present(message: string): Promise<void> {
        const loading = await this.loadingController.create({
            message
        });
        await loading.present();
    }

    async dismiss(): Promise<void> {
        await this.loadingController.dismiss();
    }
}
