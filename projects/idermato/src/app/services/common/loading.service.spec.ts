import { LoadingController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { LoadingService } from './loading.service';

describe('LoadingService', () => {
    let loadingService: LoadingService;
    let loadingController: LoadingController;

    beforeEach(() => {
        loadingController = new LoadingController();
        loadingService = new LoadingService(loadingController);
    });

    it('should present a loading', async () => {
        const presentSpy = jest
            .spyOn(loadingController, 'create')
            .mockResolvedValue({
                present: jest.fn()
            } as any);

        await loadingService.present(helperMessages.loading);

        expect(presentSpy).toHaveBeenCalledWith({
            message: helperMessages.loading
        });
    });

    it('should dismiss a loading', async () => {
        const dismissSpy = jest
            .spyOn(loadingController, 'dismiss')
            .mockResolvedValue(true);

        await loadingService.dismiss();

        expect(dismissSpy).toHaveBeenCalled();
    });
});
