import { TestBed } from '@angular/core/testing';
import { ToastController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { messages } from 'src/app/utils/constants';
import { State, ToastPosition } from '../../utils/enum';
import { NotifyService } from './notify.service';

describe('NotifyService', () => {
    let notifyService: NotifyService;
    let toastController = {
        create: jest.fn().mockResolvedValue({
            present: jest.fn()
        })
    };

    beforeAll(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: ToastController,
                    useValue: toastController
                }
            ]
        });
        notifyService = TestBed.inject(NotifyService);
    });

    it('should present a toast with default options', async () => {
        await notifyService.presentToast();

        expect(toastController.create).toHaveBeenCalledWith({
            message: helperMessages.errorOccurred,
            color: State.failed,
            duration: 1500,
            position: ToastPosition.bottom
        });
    });

    it('should present a toast with custom options', async () => {
        const customOptions = {
            message: 'Custom message',
            color: 'customColor',
            duration: 3000,
            position: ToastPosition.top
        };

        await notifyService.presentToast(customOptions);

        expect(toastController.create).toHaveBeenCalledWith({
            message: customOptions.message,
            color: customOptions.color,
            duration: customOptions.duration,
            position: customOptions.position
        });
    });

    it('should reset popover properties when closing popover', () => {
        notifyService.closePopover();
        expect(notifyService.popover.getValue()).toEqual({
            isOpen: false,
            content: '',
            title: '',
            color: '',
            icon: ''
        });
    });

    it('should update popover properties when updating with new data', (done) => {
        const newProps = {
            isOpen: true,
            content: messages.noAppointment,
            title: messages.thanks,
            icon: 'warning',
            color: State.success
        };

        notifyService.setPopover(
            true,
            messages.noAppointment,
            messages.thanks,
            State.success,
            'warning'
        );

        notifyService.getPopover().subscribe((props) => {
            expect(props).toEqual(newProps);
            done();
        });
    });
});
