import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { helperMessages } from '@lib/constants';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { State, ToastPosition } from '../../utils/enum';

type Popover = {
    isOpen: boolean;
    content?: string;
    title?: string;
    icon?: string;
    color?: string;
};

type ToastOptions = {
    message?: string;
    color?: string;
    position?: ToastPosition;
    duration?: number;
};

const defaultProps: Popover = {
    isOpen: false
};

@Injectable({
    providedIn: 'root'
})
export class NotifyService {
    popover: BehaviorSubject<Popover> = new BehaviorSubject<Popover>(
        defaultProps
    );

    private closeEventSubject = new Subject<void>();
    closeEvent$ = this.closeEventSubject.asObservable();

    constructor(private toastController: ToastController) {}

    /**
     * Presents a toast with the provided options
     *
     * @param options - The options for the toast
     * @returns A promise that resolves when the toast is dismissed
     */
    async presentToast(options?: ToastOptions): Promise<void> {
        const toast = await this.toastController.create({
            message: options?.message ?? helperMessages.errorOccurred,
            color: options?.color ?? State.failed,
            duration: options?.duration ?? 1500,
            position: options?.position ?? ToastPosition.bottom
        });
        await toast.present();
    }

    /**
     * Retrieves an observable stream of popover properties
     *
     * @returns Observable stream emitting popover properties.
     */
    getPopover(): Observable<Popover> {
        return this.popover.asObservable();
    }

    /** Updates the popover properties.
     *
     * @param display: boolean show or hide the popover
     * @param description: string description of the popover
     * @param message: string title of the popover
     * @param titleColor: stribg color of the popover
     * @param iconName: string icon of the popover
     */
    setPopover(
        display: boolean,
        description: string = '',
        message: string = '',
        titleColor: string = '',
        iconName: string = ''
    ): void {
        this.popover.next({
            isOpen: display,
            content: description,
            title: message,
            color: titleColor,
            icon: iconName
        });
    }

    /**
     * Closes the popover
     */
    closePopover(): void {
        this.setPopover(false);
        this.closeEventSubject.next();
    }
}
