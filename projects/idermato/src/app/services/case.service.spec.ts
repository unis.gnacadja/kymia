import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MockUser } from '@kymia/src/app/mock';
import { of } from 'rxjs';
import * as CaseMock from 'src/app/mock/case.mock.json';
import * as DiaryMock from 'src/app/mock/diary.json';
import { CaseStatus } from '../utils/enum';
import { CaseService } from './case.service';
import { UserService } from './user.service';

describe('CaseService', () => {
    let service: CaseService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CaseService, UserService]
        });
        service = TestBed.inject(CaseService);
        httpMock = TestBed.inject(HttpTestingController);

        service.userService.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    describe('Requests', () => {
        afterEach(() => {
            httpMock.verify();
        });

        it('should get cases', () => {
            const response = [CaseMock];
            service.getCases().subscribe((resp) => {
                expect(resp).toEqual(response);
            });

            const request = httpMock.expectOne(
                `${service.api}?userId=${service.userService.user.id}`
            );
            request.flush(response);
        });

        it('should submit a case', () => {
            const consultationId = DiaryMock.id;
            const response = {
                message: 'confirmed consultation'
            };
            service.save(consultationId).subscribe((resp) => {
                expect(resp).toEqual(response);
            });

            const request = httpMock.expectOne(
                `${service.api}/consultation/${consultationId}?userId=${service.userService.user.id}`
            );
            request.flush(response);
        });

        it('should update case status', () => {
            const caseId = DiaryMock.id;
            const status = CaseStatus.open;
            const response = {
                message: 'case successfully updated'
            };
            service.updateStatus(caseId, status).subscribe((resp) => {
                expect(resp).toEqual(response);
            });

            const request = httpMock.expectOne(
                `${service.api}/changeStatus/${caseId}/${status}?userId=${service.userService.user.id}`
            );
            request.flush(response);
        });
    });

    describe('Methods', () => {
        it('should get all cases', async () => {
            const caseMockWithComments = [CaseMock, CaseMock];
            jest.spyOn(service, 'getCases').mockReturnValue(
                of(caseMockWithComments)
            );
            await service.getAll();

            expect(service.list.length).toBeGreaterThan(0);
        });
    });
});
