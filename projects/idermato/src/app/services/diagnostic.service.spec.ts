import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MockUser } from '@kymia/src/app/mock';
import * as CaseMock from 'src/app/mock/case.mock.json';
import { environment } from 'src/environments/environment';
import { DiagnosticService } from './diagnostic.service';
import { UserService } from './user.service';

describe('DiagnosticService', () => {
    let service: DiagnosticService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DiagnosticService, UserService]
        });
        service = TestBed.inject(DiagnosticService);
        httpMock = TestBed.inject(HttpTestingController);
        service.userService.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    afterEach(() => {
        httpMock.verify();
    });

    describe('Requests', () => {
        it('should suggest diagnostic', () => {
            const formData = {
                idCas: CaseMock.listDiagnostique.diagnostiques[0].idCas,
                diagnostique:
                    CaseMock.listDiagnostique.diagnostiques[0].diagnostique,
                traitement:
                    CaseMock.listDiagnostique.diagnostiques[0].traitement,
                prescription:
                    CaseMock.listDiagnostique.diagnostiques[0].prescription
            };
            const response = {
                message: 'suggest diagnostic'
            };
            service.suggest(formData).subscribe((res) => {
                expect(res).toEqual(response);
            });

            const request = httpMock.expectOne(
                `${service.api}/intermediaire?userId=${service.userService.user.id}`
            );
            request.flush(response);
        });

        it('should get prediction', async () => {
            const caseId = '123';

            service.getPrediction(caseId).subscribe((res: any) => {
                expect(res.predictions).toEqual(
                    CaseMock.listDiagnostique.predictionIA
                );
            });

            const req = httpMock.expectOne(
                `${environment.baseUrl}/service/predictions/${caseId}?userId=123`
            );
            expect(req.request.method).toBe('GET');
            req.flush(CaseMock.listDiagnostique.diagnostiques[0]);
        });
    });
});
