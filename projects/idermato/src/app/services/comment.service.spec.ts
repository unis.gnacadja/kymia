import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MockUser } from '@kymia/src/app/mock';
import * as CaseMock from 'src/app/mock/case.mock.json';
import { CommentService } from './comment.service';
import { UserService } from './user.service';

describe('CommentService', () => {
    let service: CommentService;
    let httpMock: HttpTestingController;
    const commentMock = [
        CaseMock.listCommentaires[0],
        CaseMock.listCommentaires[0]
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CommentService, UserService]
        });
        service = TestBed.inject(CommentService);
        httpMock = TestBed.inject(HttpTestingController);

        service.userService.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should get all comments', () => {
        service.getAll(commentMock[0].id).subscribe((resp) => {
            expect(resp).toEqual(commentMock[0]);
        });
        const request = httpMock.expectOne(
            `${service.api}/byCas/${commentMock[0].id}?userId=${service.userService.user.id}`
        );
        request.flush(commentMock[0]);
    });

    it('should send comment', () => {
        const response = {
            message: 'comment sent'
        };
        service
            .save(commentMock[0].id, commentMock[0].contenu)
            .subscribe((resp) => {
                expect(resp).toEqual(response);
            });
        const request = httpMock.expectOne(
            `${service.api}?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });
});
