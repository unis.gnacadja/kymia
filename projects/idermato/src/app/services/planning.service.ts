import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Planning } from '../models/types/planning';
import { UserService } from './user.service';

@Injectable()
export class PlanningService {
    api = environment.baseUrl;

    calendar: string = 'service/calendars';

    constructor(
        public http: HttpClient,

        public userService: UserService
    ) {}

    registerPlanning(planning: Planning): Observable<unknown> {
        return this.http.post(
            `${this.api}/${this.calendar}/all?userId=${this.userService.user.id}`,
            planning
        );
    }

    searchCalendars(
        doctorId: string,
        page: number,
        limit?: number
    ): Observable<unknown> {
        let url = `${this.api}/${this.calendar}/doctor/${doctorId}?page=${page}&userId=${doctorId}`;
        if (limit) {
            url += `&size=${limit}`;
        }
        return this.http.get(url);
    }

    searchDiaries(startDay: string, endDay: string): Observable<unknown> {
        return this.http.post(
            `${this.api}/${this.calendar}/search?userId=${this.userService.user.id}`,
            {
                dateDebut: startDay,
                dateFin: endDay,
                doctorId: this.userService.user.id
            }
        );
    }
}
