import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ConsultationMock, Doctors, MockUser } from '@kymia/src/app/mock';
import * as DiaryResumeMocck from 'src/app/mock/diary-resume.json';
import * as DiaryMock from 'src/app/mock/diary.json';
import * as PrescriptionMock from 'src/app/mock/prescription.mock.json';
import { Diary } from '../models/types/diary';
import { ConsultationService } from './consultation.service';
import { UserService } from './user.service';

describe('ConsultationService', () => {
    let service: ConsultationService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ConsultationService, UserService]
        });
        service = TestBed.inject(ConsultationService);
        httpMock = TestBed.inject(HttpTestingController);

        service.userService.user = {
            ...MockUser.simpleIdermato,
            id: '123'
        };
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should search consultations', () => {
        const response = {
            consultations: []
        };

        service.searchConsultations('123', 1, 10).subscribe((res) => {
            expect(res).toEqual(response);
        });

        const request = httpMock.expectOne(
            `${service.api}/search?page=1&userId=123&size=10`
        );
        request.flush(response);
    });

    it('should get consultation', () => {
        const response = {
            message: 'consultation getted'
        };
        const consultationId = DiaryMock.id;

        service.getConsultation(consultationId).subscribe((res) => {
            expect(res).toEqual(response);
        });
        const request = httpMock.expectOne(
            `${service.api}/${consultationId}?userId=${service.userService.user.id}`
        );
        expect(request.request.method).toBe('GET');
        request.flush(response);
    });

    it('should confirm consultation', () => {
        const consultationId = DiaryMock.id;
        const response = {
            message: 'confirmed consultation'
        };
        service.confirm(consultationId).subscribe((res) => {
            expect(res).toEqual(response);
        });

        const request = httpMock.expectOne(
            `${service.api}/status/validate/${consultationId}?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });
    it('should get available doctor', () => {
        const response = {
            message: 'Doctor getted'
        };
        const consultationId = DiaryMock.id;
        service.getAvailableDoctor(consultationId).subscribe((res) => {
            expect(res).toEqual(response);
        });
        const request = httpMock.expectOne(
            `${service.api}/transfert/available/${consultationId}?userId=${service.userService.user.id}`
        );
        expect(request.request.method).toBe('GET');
        request.flush(response);
    });
    it('should transfert consultation', () => {
        const consultationId = DiaryMock.id;
        const newCalendarId = Doctors.content[0].nextCalendar.id;
        const doctorId = Doctors.content[0].id;

        const response = {
            message: 'transfered consultation'
        };
        service
            .transfertConsultation(consultationId, newCalendarId, doctorId)
            .subscribe((res) => {
                expect(res).toEqual(response);
            });

        const request = httpMock.expectOne(
            `${service.api}/transfert/${consultationId}/${newCalendarId}?userId=${doctorId}`
        );
        request.flush(response);
    });
    it('should cancel consultation', () => {
        const consultationId = DiaryMock.id;
        const reason = 'cas urgent';
        const response = {
            message: 'canceled consultation'
        };
        service
            .canceledConsultation(consultationId, reason)
            .subscribe((res) => {
                expect(res).toEqual(response);
            });

        const request = httpMock.expectOne(
            `${service.api}/status/annule/${consultationId}?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });
    it('should update consultation', () => {
        const consultationId = DiaryMock.id;
        const data: FormData = new FormData();
        data.append('ordonnance', new Blob());

        const response: Diary = {
            ...ConsultationMock.terminated,
            ordonnance: PrescriptionMock
        };
        service.reportConsultation(consultationId, data).subscribe((resp) => {
            expect(resp).toEqual(response);
        });

        const request = httpMock.expectOne(
            `${service.api}/reports/${consultationId}?id=${consultationId}&userId=${service.userService.user.id}`
        );
        request.flush(response);
    });

    it('should finish consultation', () => {
        const consultationId = DiaryMock.id;
        const terminateConsultationDto = DiaryResumeMocck;
        const response = {
            message: 'finish consultation'
        };
        service
            .close(consultationId, terminateConsultationDto)
            .subscribe((res) => {
                expect(res).toEqual(response);
            });

        const request = httpMock.expectOne(
            `${service.api}/status/terminated/${consultationId}?userId=${service.userService.user.id}`
        );
        request.flush(response);
    });

    it('should update a consultation with the provided data', () => {
        const data = {
            ...ConsultationMock.terminated,
            id: '123'
        };
        service.list = [data];

        const updated = {
            ...data,
            callUrl: DiaryMock.callUrl
        };

        service.setConsultation(updated);

        expect(service.list[0]).toEqual({
            ...service.list[0],
            callUrl: DiaryMock.callUrl
        });
    });

    it('should update consultation list with the provided data list', () => {
        const data = {
            ...ConsultationMock.terminated,
            id: '123'
        };

        service.list = [data];

        const updated = [
            {
                ...data,
                problemDescription: 'expected problem description'
            }
        ];

        service.setConsultations(updated);

        expect([service.list[0]]).toEqual([
            {
                ...service.list[0],
                problemDescription: 'expected problem description'
            }
        ]);
    });

    it('should get Diary through id', () => {
        const expectedDiary = {
            ...DiaryMock,
            id: 'New Expected Diary Id'
        };
        service.list = [DiaryMock, expectedDiary];
        expect(service.searchDiary(expectedDiary.id)).toEqual(expectedDiary);
    });
});
