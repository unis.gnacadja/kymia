import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Diary, InfosCase } from '../models/types/diary';
import { normalizeDiary } from '../utils/normalizer';
import { UserService } from './user.service';

@Injectable()
export class ConsultationService {
    list: Diary[] = [];
    api = `${environment.baseUrl}/service/consultations`;

    constructor(public http: HttpClient, public userService: UserService) {}

    searchConsultations(
        doctorId: string,
        page: number,
        limit?: number
    ): Observable<unknown> {
        let url = `${this.api}/search?page=${page}&userId=${doctorId}`;
        if (limit) {
            url += `&size=${limit}`;
        }
        return this.http.post(url, {
            doctorId
        });
    }

    getConsultation(id: string): Observable<unknown> {
        return this.http.get(
            `${this.api}/${id}?userId=${this.userService.user.id}`
        );
    }

    confirm(id: string): Observable<unknown> {
        return this.http.put(
            `${this.api}/status/validate/${id}?userId=${this.userService.user.id}`,
            id
        );
    }

    getAvailableDoctor(id: string): Observable<unknown> {
        return this.http.get(
            `${this.api}/transfert/available/${id}?userId=${this.userService.user.id}`
        );
    }
    transfertConsultation(
        id: string,
        newCalendarId: any,
        doctorId: any
    ): Observable<unknown> {
        return this.http.put(
            `${this.api}/transfert/${id}/${newCalendarId}?userId=${doctorId}`,
            id
        );
    }
    canceledConsultation(id: string, reason: string): Observable<unknown> {
        return this.http.put(
            `${this.api}/status/annule/${id}?userId=${this.userService.user.id}`,
            { raison: reason }
        );
    }

    reportConsultation(id: string, data): Observable<unknown> {
        return this.http.put(
            `${this.api}/reports/${id}?id=${id}&userId=${this.userService.user.id}`,

            data
        );
    }

    close(
        id: string,
        terminateConsultationDto: InfosCase
    ): Observable<unknown> {
        return this.http.put(
            `${this.api}/status/terminated/${id}?userId=${this.userService.user.id}`,
            terminateConsultationDto
        );
    }

    /**
     * Updates a consultation in the local list based on the provided data
     *
     * @param data - The consultation data to update
     */
    setConsultation(data: any) {
        const index = this.list.findIndex(
            (consultation) => consultation.id === data.id
        );
        const diary = normalizeDiary(data);
        Object.keys(this.list[index]).forEach((key) => {
            delete this.list[index][key];
        });
        Object.keys(diary).forEach((key) => {
            this.list[index][key] = diary[key];
        });
    }

    /**
     * Get Diary through id
     *
     * @param id - The Diary id
     */
    searchDiary(id: string) {
        const index = this.list.findIndex((item) => item.id === id);
        return this.list[index];
    }

    /**
     * Updates consultation list
     *
     * @param data - The consultation list data
     */
    setConsultations(data: any[]) {
        this.list = data.map((item: any) => normalizeDiary(item));
    }
}
