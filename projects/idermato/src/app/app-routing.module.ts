import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@kymia/src/app/guards/auth.guard';
import { IntroGuard } from '@kymia/src/app/guards/intro.guard';

registerLocaleData(localeFr);
const routes: Routes = [
    {
        path: 'tabs',
        children: [
            {
                path: 'home',
                loadChildren: () =>
                    import('../app/pages/home/home.module').then(
                        (m) => m.HomePageModule
                    )
            },
            {
                path: 'diaries',
                loadChildren: () =>
                    import('../app/pages/diaries/diaries.module').then(
                        (m) => m.DiariesPageModule
                    )
            },
            {
                path: 'cases',
                loadChildren: () =>
                    import('../app/pages/cases/cases.module').then(
                        (m) => m.CasesPageModule
                    )
            },
            {
                path: 'account',
                loadChildren: () =>
                    import('../app/pages/account/account.module').then(
                        (m) => m.AccountPageModule
                    )
            },
            {
                path: '',
                redirectTo: '/tabs/home',
                pathMatch: 'full'
            }
        ],
        canLoad: [IntroGuard, AuthGuard],
        canActivate: [AuthGuard]
    },
    {
        path: 'intro',
        loadChildren: () =>
            import('../app/pages/intro/intro.module').then(
                (m) => m.IntroPageModule
            )
    },
    {
        path: '',
        redirectTo: 'intro',
        pathMatch: 'full'
    },
    {
        path: 'register',
        loadChildren: () =>
            import('./pages/register/register.module').then(
                (m) => m.RegisterPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'forgot-password',
        loadChildren: () =>
            import('./pages/resetpwd/resetpwd.module').then(
                (m) => m.ResetPasswordPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'login',
        loadChildren: () =>
            import('../app/pages/login/login.module').then(
                (m) => m.LoginPageModule
            ),
        canLoad: [IntroGuard]
    },
    {
        path: 'newpwd',
        loadChildren: () =>
            import('../app/pages/newpwd/newpwd.module').then(
                (m) => m.NewPasswordPageModule
            )
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'fr-FR' }],
    exports: [RouterModule]
})
export class AppRoutingModule {}
