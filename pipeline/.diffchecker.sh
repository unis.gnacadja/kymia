#!/bin/sh

git --version
git fetch origin main test
git checkout ${CI_COMMIT_REF_NAME}
git branch 

CURRENT_BRANCH=${CI_COMMIT_REF_NAME}

MODIFY_FILES=""

exclusion="(\.enum\.ts environnement\.ts)"

if [ "$CURRENT_BRANCH" = "test" ];
then 
    MODIFY_FILES=$(git diff origin/main HEAD  --name-only | grep "\.ts$" | grep -vE "$(echo ${exclusion} | tr ' ' '|')$")    
else
    MODIFY_FILES=$(git diff origin/test HEAD  --name-only | grep "\.ts$" | grep -vE "$(echo ${exclusion} | tr ' ' '|')$")    
fi


if [ -z "$MODIFY_FILES" ]; 
then
    echo "NO TEST FOUND"
    exit 7
else
    echo "NEW SUITS TEST FOUND FOR CURRENT BRANCH"
    echo $MODIFY_FILES 
    SPEC_FILES=$(echo "$MODIFY_FILES" | grep '\.spec\.ts$')
    TS_FILES=$(echo "$MODIFY_FILES" | grep -v '\.spec\.ts$') 
    CONVERT_TS_FILES_TO_SPEC_FILES=$(echo "$TS_FILES" | sed "s/\.ts/\.spec\.ts/g")
    ALL_FILES=$(echo ${CONVERT_TS_FILES_TO_SPEC_FILES} ${SPEC_FILES} | sed 's/ /\n/g' | sort -u | tr '\n' ' ')

    echo $ALL_FILES > result_test.txt
fi

