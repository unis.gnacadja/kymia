import { defineConfig, devices } from '@playwright/test';

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
// import dotenv from 'dotenv';
// dotenv.config({ path: path.resolve(__dirname, '.env') });

/**
 * See https://playwright.dev/docs/test-configuration.
 */

const testUrl = {
    kymia: 'http://localhost:4000',
    idermato: 'http://localhost:5000'
};

export default defineConfig({
    testDir: './e2e',
    testMatch: '**/*.spec.ts',
    timeout: 60 * 1000,
    expect: {
        timeout: 5000,
        toHaveScreenshot: {
            stylePath: './e2e/screenshoot.css',
            maxDiffPixels: 10
        },
        toMatchSnapshot: {
            // An acceptable ratio of pixels that are different to the
            // total amount of pixels, between 0 and 1.
            maxDiffPixelRatio: 0.1
        }
    },
    /* Run tests in files in parallel */
    fullyParallel: true,
    /* Fail the build on CI if you accidentally left test.only in the source code. */
    forbidOnly: !!process.env.CI,
    /* Retry on CI only */
    retries: process.env.CI ? 2 : 0,
    /* Opt out of parallel tests on CI. */
    workers: process.env.CI ? 2 : undefined,
    /* Reporter to use. See https://playwright.dev/docs/test-reporters */
    reporter: 'html',
    /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
    use: {
        /* Base URL to use in actions like `await page.goto('/')`. */
        baseURL: 'http://localhost',
        /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
        trace: 'on-first-retry',
        headless: true,
        //      storageState: 'state.json',
        colorScheme: 'light'
    },

    /* Configure projects for major browsers */
    projects: [
        //kymia
        {
            name: 'Desktop-kymia',
            testDir: 'e2e/kymia',
            use: { ...devices['Desktop Chrome'], baseURL: testUrl.kymia }
        },
        {
            name: 'Mobile-Kymia',
            testDir: 'e2e/kymia',
            use: {
                ...devices['Galaxy S9+'],
                viewport: { width: 414, height: 897 },
                baseURL: testUrl.kymia
            }
        },

        // Idermato
        {
            name: 'Desktop-Idermato',
            testDir: 'e2e/idermato',
            use: { ...devices['Desktop Chrome'], baseURL: testUrl.idermato }
        },
        {
            name: 'Mobile-Idermato',
            testDir: 'e2e/idermato',
            use: {
                ...devices['Galaxy S9+'],
                viewport: { width: 414, height: 897 },
                baseURL: testUrl.idermato
            }
        }
    ],

    /* Run your local dev server before starting the tests */
    webServer: [
        {
            command: 'yarn start kymia --port 4000 --no-open',
            port: 4000,
            reuseExistingServer: !process.env.CI,
            timeout: 120 * 1000
        },
        {
            command: 'yarn start idermato --port 5000 --no-open',
            port: 5000,
            reuseExistingServer: !process.env.CI,
            timeout: 120 * 1000
        }
    ]
});
