import * as citiesMock from '@kymia/src/app/mock/cities.json';
import * as CountriesMock from '@kymia/src/app/mock/countries.mock.json';
import * as MockUser from '@kymia/src/app/mock/user.json';
import { statusMessages } from '@kymia/src/app/utils/constants';
import { expect, test } from '@playwright/test';
import { IdermatoPage } from 'e2e/models/idermato.po';

let app: IdermatoPage;

test.use({
    timezoneId: 'UTC'
});

test.beforeEach(async ({ page }) => {
    app = new IdermatoPage(page);
});

test.describe('When lauching idermato', () => {
    test('should display intro page on first use', async () => {
        await app.page.goto('/');
        await app.page.waitForURL('/intro', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/intro');

        await expect(app.page).toHaveScreenshot();
    });

    test('should redirect to login when click on start button', async () => {
        await app.goToLogin();
        await app.page.waitForURL('/login', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/login');
        await expect(app.page).toHaveScreenshot();
    });
});

test.describe('when access login', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
    });

    test('should show the registration page when the sign-up link is clicked', async () => {
        await app.gotoRegister();

        expect(app.page).toHaveURL('/register');
        await expect(app.page).toHaveScreenshot();
    });

    test('should display newpwd page after login for the first time', async () => {
        await app.signIn(MockUser.simpleIdermato.email);
        await app.page.waitForURL('/newpwd', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/newpwd');
        await expect(app.page).toHaveScreenshot();
    });

    test('should display home without consultation after login', async () => {
        await app.signIn(MockUser.newUSer.email);
        await app.page.waitForURL('/tabs/home', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/tabs/home');

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display home page with consultation after login', async () => {
        app.mockTimer('2024-03-05T09:36:00.000Z');
        await app.signIn();
        await app.page.waitForURL('/tabs/home', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/tabs/home');

        await app.page.waitForSelector('[data-testid="diary"]');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test.describe('Login form validation', () => {
        test('should display error when submitting empty form', async () => {
            await app.signIn('  ', '  ');
            await app.page.waitForSelector('[data-test-id="error"]');
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should display error for invalid credentials', async () => {
            await app.signIn(MockUser.withoutId.email);
            await expect(app.page.getByTestId('submit-error')).toBeVisible();
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });
});

test.describe('when access register', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
        await app.gotoRegister();
    });
    async function fillRegisterForm(
        lastname: string,
        firstName: string,
        phone: number,
        email: string,
        fonction: string
    ) {
        // Fill register form
        await app.page.locator('[data-testid="lastname"] input').fill(lastname);
        await app.page
            .locator('[data-testid="firstname"] input')
            .fill(firstName);
        await app.page
            .locator('[data-testid="phone"] [data-testid="input"] input')
            .fill(phone.toString());
        await app.page.locator('[data-testid="email"] input').fill(email);
        await app.page.locator('[data-testid="fonction"] input').fill(fonction);

        // click on country field
        const countryField = app.page.getByTestId('country-field');
        await countryField.click();
        // select country
        const countryOption = app.page
            .locator('[data-testid="country-item"]')
            .filter({ hasText: CountriesMock[0].name });
        await countryOption.click();

        // click on city field
        const cityField = app.page.getByTestId('city-field');
        await cityField.click();
        // select city
        const townOption = app.page
            .locator('[data-testid="city-item"]')
            .filter({ hasText: citiesMock[0].cities[0].name });
        townOption.click();
    }

    test('should display a success message when registration is successful', async () => {
        await fillRegisterForm(
            MockUser.simpleIdermato.nom,
            MockUser.simpleIdermato.prenom,
            12345678,
            MockUser.simpleIdermato.email,
            MockUser.simpleIdermato.fonction
        );

        const submitButton = app.page.getByTestId('signup');
        await submitButton.click();
        await app.page.waitForSelector('ion-popover');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error when register form is invalid', async () => {
        await fillRegisterForm('f', 'l', 15652, 'johnmail.com', ' ');

        // Close or delete country selected
        const countryCloseButton = app.page
            .getByTestId('country-field')
            .locator('.material-icons')
            .filter({ hasText: 'close' });
        await countryCloseButton.click();
        // click outside of the country field
        await app.page.locator('.autocomplete-overlay').click();

        //delete country selected
        const cityCloseButton = app.page
            .getByTestId('city-field')
            .locator('.material-icons')
            .filter({ hasText: 'close' });
        await cityCloseButton.click();
        // click outside of the country field
        await app.page.locator('.autocomplete-overlay').click();

        await app.page.locator('[data-testid="signup"]').isDisabled();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display a list of countries when you click on the country field', async () => {
        const countryField = app.page.getByTestId('country-field');
        await countryField.click();

        await app.page
            .locator('[data-testid="country-item"]')
            .last()
            .scrollIntoViewIfNeeded();
        await expect(app.page).toHaveScreenshot();
    });

    test('should display a list of cities when you click on the city field', async () => {
        // Select a country
        const countryField = app.page.getByTestId('country-field');
        await countryField.click();
        await app.page
            .locator('[data-testid="country-item"]')
            .getByText(CountriesMock[0].name)
            .click();

        // Get list of cities
        const cityField = app.page.getByTestId('city-field');
        await cityField.click();

        await app.page
            .locator('[data-testid="city-item"]')
            .last()
            .scrollIntoViewIfNeeded();

        await expect(app.page).toHaveScreenshot();
    });
});

test.describe('when access newpwd page', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
        await app.signIn(MockUser.simpleIdermato.email);
        await app.page.waitForURL('/newpwd', {
            waitUntil: 'load'
        });
    });

    async function updatePassword(
        oldPassword: string,
        newPassword: string,
        confirmPassword: string
    ) {
        await app.page
            .locator('[data-testid="old-password"] input')
            .fill(oldPassword);
        await app.page
            .locator('[data-testid="new-password"] input')
            .fill(newPassword);
        await app.page
            .locator('[data-testid="confirm-password"] input')
            .fill(confirmPassword);
        await app.page.getByTestId('update-password').click();
    }

    test('should display error messages when fields are empty', async () => {
        await updatePassword(' ', ' ', ' ');
        await expect(app.page).toHaveScreenshot();
    });

    test('should displau an error message when the new password is the same as the old password', async () => {
        await updatePassword(
            MockUser.completeIdermato.password,
            MockUser.completeIdermato.password,
            MockUser.completeIdermato.password
        );
        await expect(app.page.getByTestId('error')).toBeVisible();
        await expect(app.page).toHaveScreenshot();
    });

    test('should display error message when the passwords are not matching', async () => {
        await updatePassword(
            MockUser.completeIdermato.password,
            '1234567',
            MockUser.completeIdermato.password
        );
        await expect(app.page.getByTestId('error')).toBeVisible();
        await expect(app.page).toHaveScreenshot();
    });

    test('should display error message when the old password is incorrect', async () => {
        await updatePassword(
            MockUser.newUSer.name,
            MockUser.completeIdermato.password,
            MockUser.completeIdermato.password
        );
        await expect(app.page.getByTestId('error')).toBeVisible();
        await expect(app.page).toHaveScreenshot();
    });

    test('should notify user when the request fails', async () => {
        await updatePassword(
            MockUser.withoutId.name,
            MockUser.completeProfile.password,
            MockUser.completeProfile.password
        );
        await app.waitForNotification();
        await expect(app.page).toHaveScreenshot();
    });

    test('should notify user when the request succeeds', async () => {
        await updatePassword(
            '123456',
            MockUser.completeProfile.password,
            MockUser.completeProfile.password
        );
        await app.page.locator('ion-toast').screenshot();
        await expect(app.page).toHaveScreenshot();
    });
});

test.describe('when access home', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
        app.mockTimer('2024-03-05T09:36:00.000Z');
    });

    test('should display the appointment page when the appointment button is clicked.', async () => {
        await app.signIn(MockUser.newUSer.email);
        await app.goToAppointment();

        expect(app.page).toHaveURL('/tabs/diaries');

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display the appointment page with appointment when the appointment button is clicked.', async () => {
        await app.signIn();
        await app.goToAppointment();

        expect(app.page).toHaveURL('/tabs/diaries');

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display the account page when the account tab is clicked', async () => {
        await app.signIn();
        await app.selectAccountTab();
        await app.page.waitForURL(/\/tabs\/account/, {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL(/\/tabs\/account/);
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access account', () => {
    test.beforeEach(async () => {
        await app.goToAccount();
    });

    test('should display payment page when the payment tab is clicked', async () => {
        app.mockTimer('2024-10-29T00:00:00.000Z');
        const paymentOption = app.page.locator(
            '[data-test-id="list-options"] ion-item:nth-of-type(3)'
        );
        await paymentOption.click();

        expect(app.page).toHaveURL('/tabs/account/payment');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access calendar', () => {
    test.describe('When no schedule has been defined', () => {
        test('should display a request to add a schedule', async () => {
            app.mockTimer('2024-11-22T00:00:00.000Z');
            await app.goToAccount(MockUser.newUSer.email);
            await app.goToAgenda();

            await app.page.waitForSelector(
                '[data-testid="agenda-notification"]'
            );
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });

    test.describe('When the last planning date is less than 4 months away', () => {
        test('should display the schedule end date', async () => {
            app.mockTimer('2031-09-30T00:00:00.000Z');
            await app.goToAccount();

            await app.goToAgenda();

            await app.page.waitForSelector(
                '[data-testid="agenda-notification"]'
            );
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });
});

test.describe('when access appointment', () => {
    test.beforeEach(async () => {
        app.mockTimer('2024-03-01T09:36:00.000Z');
        await app.goToHome();
        await app.goToAppointment();
    });

    test('should filter appointment by patient name', async () => {
        expect(app.page).toHaveURL('/tabs/diaries');
        await app.page
            .locator('[data-test-id="search"] input')
            .fill(MockUser.completeProfile.name);

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display the prescription download link for completed consultation', async () => {
        await app.selectAppointment(statusMessages['terminated']);
        expect(app.page).toHaveURL(/\/consultation/);

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display confirm, cancel, and transfer buttons when clicking on a consultation awaiting confirmation', async () => {
        await app.selectAppointment(statusMessages['waiting'], 0);
        expect(app.page).toHaveURL(/\/consultation/);

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display the case submission page when clicking on a completed consultation', async () => {
        await app.selectAppointment(statusMessages['terminated'], 0);
        expect(app.page).toHaveURL(/\/consultation/);

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access consultation', () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.goToAppointment();
    });

    test('should download the prescription for completed consultation', async () => {
        await app.selectAppointment(statusMessages['terminated']);
        const downloadPromise = app.page.waitForEvent('download');

        app.page.click('[data-testid="download-prescription"]');

        const download = await downloadPromise;

        // Ensure that the download event was triggered
        expect(download).toBeDefined();
    });

    test('should have a success message when the order has been correctly submitted', async () => {
        await app.selectAppointment(statusMessages['terminated'], 0);
        expect(app.page).toHaveURL(/\/consultation/);

        const addButton = app.page.getByTestId('add-file');
        await addButton.click();

        // select file
        await app.selectFile('case.jpeg');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when order submission fails', async () => {
        await app.selectAppointment(statusMessages['terminated'], 0);
        expect(app.page).toHaveURL(/\/consultation/);

        const addButton = app.page.getByTestId('add-file');
        await addButton.click();

        await app.selectFile('image.svg');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});
