import * as ConsultationMock from '@kymia/src/app/mock/consultation.mock.json';
import * as MockUser from '@kymia/src/app/mock/user.json';
import { messages, statusMessages } from '@kymia/src/app/utils/constants';
import { expect, test } from '@playwright/test';
import { KymiaPage } from 'e2e/models/kymia.po';
let app: KymiaPage;
test.beforeEach(async ({ page }) => {
    app = new KymiaPage(page);
});

test.use({
    timezoneId: 'UTC'
});

test.describe('When lauching kymia', () => {
    test('should display intro page on first use', async () => {
        await app.page.goto('/');
        await app.page.waitForURL('/intro', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/intro');

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should redirect to login when click on start button', async () => {
        await app.goToLogin();
        await app.page.waitForURL('/login', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/login');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access login', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
    });

    test('should display home page after login', async () => {
        await app.signIn();

        // Compare screenshot
        await app.page.waitForURL('/tabs/home', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/tabs/home');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should show the registration page when the sign-up link is clicked', async () => {
        await app.gotoRegister();
        // Wait for the URL to change to the register page
        expect(app.page).toHaveURL('/register');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test.describe('Login form validation', () => {
        async function fillLoginForm(email: string, password: string) {
            await app.page.locator('[data-testid="email"] input').fill(email);
            await app.page
                .locator('[data-testid="password"] input')
                .fill(password);
        }
        test('should display error when submitting empty form', async () => {
            await app.page.getByTestId('login').click();
            await expect(
                app.page.getByTestId('required-password')
            ).toBeVisible();
            await expect(app.page.getByTestId('invalid-email')).toBeVisible();
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should display error for invalid credentials', async () => {
            await fillLoginForm('john@gmail.com', 'wrongpassword');
            await app.page.getByTestId('login').click();
            await expect(
                app.page.getByText(messages['errorAuth'])
            ).toBeVisible();
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });

    test('should display forgot password page on click on forgot password link', async () => {
        await app.goToPwdforget();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access register', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
        await app.gotoRegister();
    });
    async function fillRegisterForm(
        lastname: string,
        firstname: string,
        phone: number,
        email: string,
        password: string
    ) {
        await app.page.locator('[data-testid="lastname"] input').fill(lastname);
        await app.page
            .locator('[data-testid="firstname"] input')
            .fill(firstname);
        await app.page
            .locator('[data-testid="phone"] input')
            .fill(phone.toString());
        await app.page
            .locator('[data-testid="register-email"] input')
            .fill(email);
        await app.page
            .locator('[data-testid="register-password"] input')
            .fill(password);
    }

    test.describe('when register is successful', () => {
        test.beforeEach(async () => {
            await fillRegisterForm(
                MockUser.completeProfile.name,
                MockUser.completeProfile.firstname,
                122338544,
                MockUser.newUSer.email,
                MockUser.completeProfile.password
            );
            const acceptTerms = '[data-testid="accept-terms"]';

            await app.page.locator(acceptTerms).click();

            const submitButton = app.page.getByTestId('signup');
            await submitButton.click();
            await app.page.waitForURL('/validation', {
                waitUntil: 'load'
            });
        });
        test('should redirect to validation page after regisration', async () => {
            // Compare screenshot
            expect(app.page).toHaveURL('/validation');
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should redirect to register success page after double validation', async () => {
            await app.page
                .locator('[data-testid="code-confirm"] input')
                .fill('123456');
            const validationButton = app.page.getByTestId('validation');
            await validationButton.click();

            await app.page.waitForURL('/register-success', {
                waitUntil: 'load'
            });
            expect(app.page).toHaveURL('/register-success');
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });

    test('should display an error when register form is invalid', async () => {
        await fillRegisterForm('f', 'l', 15652, 'johnmail.com', 'ri34oddfe');
        await expect(
            app.page.getByTestId('password-special-character')
        ).toBeVisible();
        await app.page.getByTestId('signup').click();
        await app.page.locator('[data-testid="signup"]').isDisabled();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access home', () => {
    test('should display the search page with instructions only on click on the search input', async () => {
        await app.goToHome();
        await app.goToSearch();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display a message when no practitioner is available', async () => {
        await app.goToLogin();
        await app.page.route('*/**/doctors/search', async (route) => {
            await route.fulfill({
                json: {
                    content: []
                }
            });
        });

        await app.signIn();
        await app.page.getByTestId('no-practitioners-available').isVisible();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display specialities page on click on seemore link', async () => {
        await app.goToHome();
        // Go to specialities page
        const cta = app.page.getByTestId('see-more-specialities');
        await cta.click();

        await app.hideLoader();
        await app.page.waitForURL('/specialities', {
            waitUntil: 'load'
        });
        await app.page.waitForSelector('[data-testid="speciality"]');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display next consultation when an appointment has been made', async () => {
        await app.goToHome();
        app.mockTimer('2022-02-05T00:00:00.000Z');
        await app.page.waitForSelector('app-appointment');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test.describe('when the practitioner is available', () => {
        test.beforeEach(async () => {
            await app.goToHome();
        });
        test('should display detail doctor page on click on doctor name', async () => {
            // Go to doctor detail
            await app.selectDoctor();

            await app.page.waitForURL(/\/search\/detail-doctor/, {
                waitUntil: 'load'
            });
            expect(app.page).toHaveURL(/\/search\/detail-doctor/);
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should display doctor schedule on click on taking button', async () => {
            // Go to doctor schedule
            await app.selectAppointment();

            await app.page.waitForURL(/\/taking/, {
                waitUntil: 'load'
            });
            expect(app.page).toHaveURL(/\/taking/);
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });

    test('should display the next consultations on click on meetings tab', async () => {
        await app.goToHome();
        app.mockTimer('2023-02-10T08:50:00.000Z');
        await app.goToMeetings();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe("when access meeting's page", () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.goToMeetings();
    });

    test('should display completed appointments when you click on the completed option', async () => {
        const completedOption = app.page.locator(
            '[data-testid="meeting-options"] ion-segment-button:nth-of-type(2)'
        );
        await completedOption.click();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display cancelled appointments when you click on the cancelled option', async () => {
        const cancelledOption = app.page.locator(
            '[data-testid="meeting-options"] ion-segment-button:nth-of-type(3)'
        );
        await cancelledOption.click();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
    test('should display the consultation details page on clicking on the consultation', async () => {
        await app.selectConsultation(statusMessages['confirmByDoctor']);
        expect(app.page).toHaveURL(/\/consultation/);
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('When access detail consultation page', () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.goToMeetings();
    });

    test.describe('When proof of payment has not yet been submitted', () => {
        test.beforeEach(async () => {
            await app.selectConsultation(statusMessages['confirmByDoctor']);
            const payButton = app.page.getByTestId('pay-now');
            await payButton.click();
        });

        async function submitProof() {
            await app.selectFile('proof.jpeg');
            await app.page.getByTestId('submit-payment-proof').click();
        }

        test('should display the payment page when the pay now button is clicked', async () => {
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should display a success message when the payment proof is submitted', async () => {
            await submitProof();

            // Wait for navigation to the consultation page
            await app.page.waitForURL(/\/consultation/);

            // Wait for notification and take a screenshot
            await app.waitForNotification();
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });

        test('should notify when an error occurs when sending proof of payment', async () => {
            await app.page.route(
                '*/**/consultations/status/payed/**',
                async (route) => {
                    await route.fulfill({
                        status: 500,
                        json: {
                            error: messages['sendError']
                        }
                    });
                }
            );
            await submitProof();

            // Wait for notification and take a screenshot
            await app.waitForNotification();
            await expect(app.page).toHaveScreenshot({ fullPage: true });
        });
    });
});

test.describe('when access doctor schedule', () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.selectDoctor();
    });

    test('should display doctor schedule on click on seemore button', async () => {
        // Open doctor schedule
        const seemoreButton = app.page.getByTestId('seemore');
        await seemoreButton.click();
        await app.page.waitForSelector('[data-testid="doctor-schedule"]');

        // Compare screenshot
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access pwdforget', () => {
    test.beforeEach(async () => {
        await app.goToLogin();
        await app.goToPwdforget();
    });

    test('should send a confirmation code to an email', async () => {
        await app.page
            .locator('[data-testid="forgot-email"] input')
            .fill('eganhoui@rintio.com');

        await app.page.getByTestId('submit').click();

        await app.page.waitForURL('/newpwdsuccess', {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL('/newpwdsuccess');
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when the e-mail address does not exist', async () => {
        await app.page
            .locator('[data-testid="forgot-email"] input')
            .fill('rintio@rintio.com');

        // Click on submit button
        await app.page.getByTestId('submit').click();

        // Error message
        await expect(app.page.getByTestId('error-message')).toBeVisible();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display error message when email is empty', async () => {
        await app.page.locator('[data-testid="forgot-email"] input').fill('');

        await app.page.getByTestId('submit').click();

        await expect(app.page.getByTestId('required-email')).toBeVisible();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('When access appointment taking', () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.selectAppointment();
    });

    test('should display a success message when the user succeeds in booking an appointment', async () => {
        const button = app.page.getByTestId('change-description');
        await button.click();

        await app.page
            .locator('[data-testid="description"] textarea')
            .fill(ConsultationMock.waiting.problemDescription);

        const addButton = app.page.getByTestId('add-file');
        await addButton.click();

        // select file
        await app.selectFile('case.jpeg');

        // button submit
        const btnSubmit = app.page.getByTestId('submit-appointment');
        await btnSubmit.click();

        await app.page.waitForURL(/\/consultation/, {
            waitUntil: 'load'
        });
        expect(app.page).toHaveURL(/\/consultation/);

        await app.waitForNotification();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when the symptom field is empty', async () => {
        // button submit
        const btnSubmit = app.page.getByTestId('submit-appointment');
        await btnSubmit.click();

        await app.waitForNotification();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when no file has been submitted', async () => {
        const button = app.page.getByTestId('change-description');
        await button.click();

        await app.page
            .locator('[data-testid="description"] textarea')
            .fill(ConsultationMock.waiting.problemDescription);
        // button submit
        const btnSubmit = app.page.getByTestId('submit-appointment');
        await btnSubmit.click();

        await app.waitForNotification();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when the api request has failed', async () => {
        const button = app.page.getByTestId('change-description');
        await button.click();

        await app.page
            .locator('[data-testid="description"] textarea')
            .fill(ConsultationMock.waiting.problemDescription);

        const addButton = app.page.getByTestId('add-file');
        await addButton.click();

        // select file
        await app.selectFile('case.jpeg');

        // button submit

        await app.page.route('*/**/consultations', async (route) => {
            await route.fulfill({
                status: 500,
                json: {}
            });
        });

        const btnSubmit = app.page.getByTestId('submit-appointment');
        await btnSubmit.click();

        await app.waitForNotification();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display an error message when the file added is incorrect', async () => {
        const button = app.page.getByTestId('change-description');
        await button.click();

        await app.page
            .locator('[data-testid="description"] textarea')
            .fill(ConsultationMock.waiting.problemDescription);

        const addButton = app.page.getByTestId('add-file');
        await addButton.click();

        //select file
        await app.selectFile('image.svg');

        await app.waitForNotification();

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});

test.describe('when access search page', () => {
    test.beforeEach(async () => {
        await app.goToHome();
        await app.goToSearch();
    });

    async function searchPractioners() {
        const searchButton = app.page.locator(
            '[data-testid="search-page"] [data-testid="search-button"]'
        );
        await searchButton.click();
    }

    test('should display the available practitioners when the search button is clicked', async () => {
        await searchPractioners();
        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });

    test('should display doctor schedule when the see more button is clicked', async () => {
        await searchPractioners();

        // Open doctor schedule
        const seemoreButton = app.page.locator(
            '[data-testid="search-page"] [data-testid="more-availability"]'
        );
        await seemoreButton.click();
        await app.page.waitForSelector('[data-testid="doctor-schedule"]');

        await expect(app.page).toHaveScreenshot({ fullPage: true });
    });
});
