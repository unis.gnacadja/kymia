import * as CaseMock from '@idermato/src/app/mock/case.mock.json';
import * as DiaryMock from '@idermato/src/app/mock/diary.json';
import * as Planning from '@idermato/src/app/mock/plannings.json';
import * as PrescriptionMock from '@idermato/src/app/mock/prescription.mock.json';
import * as ConsultationMock from '@kymia/src/app/mock/consultation.mock.json';
import * as MockUser from '@kymia/src/app/mock/user.json';
import { messages } from '@kymia/src/app/utils/constants';
import { helperMessages } from '@lib/constants';
import { ConsultationStatus } from '@lib/enum';
import { type Page } from '@playwright/test';
import { AppPage } from './app.po';

const countriesMock = require('@kymia/src/app/mock/countries.mock.json');
const citiesMock = require('@kymia/src/app/mock/cities.json');

const diariesMock = [
    {
        ...DiaryMock,
        id: '1',
        status: ConsultationStatus.payed
    },
    {
        ...DiaryMock,
        id: '2',
        status: ConsultationStatus.waiting
    },
    {
        ...DiaryMock,
        id: '3',
        status: ConsultationStatus.confirmByDoctor
    },
    {
        ...DiaryMock,
        id: '4',
        patient: {
            ...DiaryMock.patient,
            nom: MockUser.completeProfile.name,
            prenom: MockUser.completeIdermato.firstname
        },
        status: ConsultationStatus.annuled
    },
    {
        ...DiaryMock,
        id: '5',
        status: ConsultationStatus.terminated,
        diagnostique: ConsultationMock.terminated.diagnostique,
        transaction: ConsultationMock.terminated.transaction
    },
    ...Array(7).fill({
        ...DiaryMock,
        id: '6',
        status: ConsultationStatus.terminated,
        diagnostique: ConsultationMock.terminated.diagnostique,
        transaction: ConsultationMock.terminated.transaction,
        ordonnace: PrescriptionMock.lien
    })
];

export class IdermatoPage extends AppPage {
    constructor(page: Page) {
        super(page);
        this.setupMocks();
    }

    private async setupMocks() {
        await this.page.route(
            '**/connexion?login=**&pwd=**',
            async (route, request) => {
                const url = new URL(request.url());
                const login = url.searchParams.get('login');
                if (login === MockUser.withoutId.email) {
                    await route.fulfill({
                        status: 403,
                        json: {
                            message: helperMessages['errorOccurred']
                        }
                    });
                } else {
                    await route.fulfill({
                        status: 200,
                        json: {
                            ...MockUser.completeIdermato,
                            id:
                                login === MockUser.newUSer.email
                                    ? MockUser.newUSer.id
                                    : MockUser.completeIdermato.id,
                            firstConnection:
                                login === MockUser.simpleIdermato.email
                        }
                    });
                }
            }
        );

        await this.page.route('*/**/pre-inscription', async (route) => {
            await route.fulfill({
                status: 200,
                json: {
                    content: MockUser.completeProfile
                }
            });
        });

        await this.page.route(
            '**/updatePassword/**?newPassword=**&oldPassword=**&userId=**',
            async (route, request) => {
                const url = new URL(request.url());
                const oldPassword = url.searchParams.get('oldPassword');

                if (oldPassword === MockUser.withoutId.name) {
                    await route.fulfill({
                        status: 500,
                        json: {}
                    });
                } else if (oldPassword === MockUser.newUSer.name) {
                    await route.fulfill({
                        status: 403,
                        json: {}
                    });
                } else {
                    await route.fulfill({
                        status: 200,
                        json: {}
                    });
                }
            }
        );

        await this.page.route(
            '*/**/consultations/search?page=*&userId=*&size=*',
            async (route) => {
                await route.fulfill({
                    status: 200,
                    json: [DiaryMock]
                });
            }
        );

        await this.page.route('*/**/cases?userId=*', async (route) => {
            await route.fulfill({
                json: [CaseMock]
            });
        });

        await this.page.route('*/**/countries', async (route) => {
            await route.fulfill({
                json: {
                    content: countriesMock
                }
            });
        });

        await this.page.route('*/**/cities/country/**', async (route) => {
            await route.fulfill({
                json: {
                    content: citiesMock[0].cities
                }
            });
        });

        await this.page.route(
            '*/**/calendars/doctor/*?page=*&userId=*&size=*',
            async (route, request) => {
                const url = new URL(request.url());
                const userId = url.searchParams.get('userId');
                await route.fulfill({
                    status: 200,
                    json:
                        userId === MockUser.completeIdermato.id
                            ? [
                                  {
                                      ...Planning[0],
                                      availableDate: '30-12-2031'
                                  }
                              ]
                            : []
                });
            }
        );

        await this.page.route(
            '*/**/consultations/search?page=*&userId=*&size=*',
            async (route, request) => {
                const url = new URL(request.url());
                const userId = url.searchParams.get('userId');
                await route.fulfill({
                    json:
                        userId === MockUser.completeIdermato.id
                            ? diariesMock
                            : []
                });
            }
        );

        await this.page.route('*/**/reports/*?id=*&userId=*', async (route) => {
            await route.fulfill({
                json: {
                    messages: messages['fileAccepted']
                }
            });
        });

        await this.page.route('*/**/cases?userId=*', async (route, request) => {
            const url = new URL(request.url());
            const userId = url.searchParams.get('userId');
            await route.fulfill({
                json: userId === MockUser.completeIdermato.id ? [CaseMock] : []
            });
        });

        await this.page.route(
            '*/**/calendars/search?userId=*',
            async (route) => {
                await route.fulfill({
                    json: [
                        {
                            ...Planning[0]
                        }
                    ]
                });
            }
        );
    }

    async goToAppointment() {
        const appointmentTab = this.page.locator(
            '[data-testid="tabs"] ion-item:nth-of-type(2)'
        );
        await appointmentTab.click();
        await this.page.waitForURL('/tabs/diaries', {
            waitUntil: 'load'
        });
        await this.hideLoader();
    }

    async selectAccountTab() {
        const accountTab = this.page.locator(
            '[data-testid="tabs"] ion-item:nth-of-type(4)'
        );
        await accountTab.click();
        await this.page.waitForURL('/tabs/account');
    }

    async goToAgenda() {
        const agendaMenu = this.page.locator(
            '[data-test-id="list-options"] ion-item:nth-of-type(2)'
        );
        await agendaMenu.click();
        await this.page.waitForURL('/tabs/account/agenda');
    }

    async goToAccount(
        email: string = MockUser.completeProfile.email,
        password: string = MockUser.completeProfile.password
    ) {
        await this.goToHome(email, password);
        await this.selectAccountTab();
    }

    async selectAppointment(text: string, index: number = 1) {
        const selectedDiary = this.page
            .locator('app-diaries')
            .getByTestId('diary-status')
            .getByText(text)
            .nth(index);

        await selectedDiary.click();
        await this.page.waitForURL(/\/consultation/, {
            waitUntil: 'load'
        });
    }
}
