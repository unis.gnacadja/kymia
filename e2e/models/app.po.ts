import * as MockUser from '@kymia/src/app/mock/user.json';
import { type Page } from '@playwright/test';

const placeholderImage = require('@kymia/src/app/mock/image.mock.json');
export class AppPage {
    page: Page;
    placeholderImage: string = placeholderImage;
    constructor(page: Page) {
        this.page = page;
        this.defaultMock();
    }

    mockTimer(time: string) {
        this.page.clock.setFixedTime(new Date(time));
    }

    async defaultMock() {
        this.page.route(
            /^(?!.*assets\/).*\.?(png|jpg|jpeg|gif|webp)$/i,
            async (route) => {
                route.fulfill({
                    contentType: 'image/png',
                    body: Buffer.from(placeholderImage, 'base64')
                });
            }
        );
    }

    async hideLoader() {
        await this.page.evaluate(() => {
            const loadingElements = document.querySelectorAll('ion-loading');
            loadingElements.forEach((el) => (el.style.display = 'none'));
        });
    }

    async goToLogin() {
        await this.page.goto('/');
        const startButton = this.page.getByTestId('start');
        await startButton.click();
        await this.page.waitForURL('/login', {
            waitUntil: 'load'
        });
    }

    async gotoRegister() {
        const registerLink = this.page.getByTestId('register');
        await registerLink.click();
        await this.page.waitForURL('/register', {
            waitUntil: 'load'
        });
    }

    async signIn(
        email: string = MockUser.completeProfile.email,
        password: string = MockUser.completeProfile.password
    ) {
        await this.page
            .locator('[data-testid="password"] input')
            .fill(password);
        await this.page.locator('[data-testid="email"] input').fill(email);
        const loginButton = this.page.getByTestId('login');
        await loginButton.click();
    }

    async waitForNotification() {
        await this.page.waitForSelector('ion-toast');
    }

    async goToHome(
        email: string = MockUser.completeProfile.email,
        password: string = MockUser.completeProfile.password
    ) {
        await this.goToLogin();
        await this.signIn(email, password);
    }

    async selectFile(name: string) {
        const fs = require('fs');
        const path = require('path');

        const buffer = Buffer.from(this.placeholderImage, 'base64');

        // Create a temporary file
        const tempFilePath = path.join(__dirname, name);
        fs.writeFileSync(tempFilePath, buffer);

        // Set the file input with the temporary file
        await this.page
            .locator('input[type="file"]')
            .setInputFiles(tempFilePath);

        fs.unlinkSync(tempFilePath);
    }
}
