import * as ConsultationMock from '@kymia/src/app/mock/consultation.mock.json';
import * as Dates from '@kymia/src/app/mock/dates.json';
import * as DoctorMock from '@kymia/src/app/mock/doctor.mock.json';
import * as MockUser from '@kymia/src/app/mock/user.json';
import { messages } from '@kymia/src/app/utils/constants';
import { type Page } from '@playwright/test';
import { AppPage } from './app.po';

const calendar = {
    ...Dates[0].calendars[0],
    doctor: DoctorMock
};

const consultation: Record<string, any> = {
    waiting: {
        ...ConsultationMock.waiting,
        calendar: {
            ...calendar
        }
    },
    confirmed: {
        ...ConsultationMock.confirmed,
        calendar: {
            ...calendar
        }
    },
    canceled: {
        ...ConsultationMock.canceled,
        calendar: {
            ...calendar
        }
    },
    payed: {
        ...ConsultationMock.payed,
        calendar: {
            ...calendar
        }
    },
    terminated: {
        ...ConsultationMock.terminated,
        calendar: {
            ...calendar
        }
    }
};

const activities = require('@kymia/src/app/mock/activity.mock.json');
export class KymiaPage extends AppPage {
    constructor(page: Page) {
        super(page);
        this.setupMocks();
    }

    private async setupMocks() {
        await this.page.route('*/**/users/login', async (route) => {
            const postData = await route.request().postData();
            const requestBody = JSON.parse(postData || '');
            if (
                postData &&
                requestBody.email === MockUser.completeProfile.email &&
                requestBody.password === MockUser.completeProfile.password
            ) {
                await route.fulfill({
                    status: 200,
                    json: {
                        token: MockUser.loggedUser.token
                    }
                });
            } else {
                await route.fulfill({
                    status: 403,
                    json: {
                        error: messages['sendError']
                    }
                });
            }
        });

        await this.page.route('*/**/patients', async (route) => {
            const postData = await route.request().postData();
            const requestBody = JSON.parse(postData || '');
            if (
                postData &&
                requestBody.email === MockUser.completeProfile.email
            ) {
                await route.fulfill({
                    status: 403,
                    json: {
                        message: messages['existMail']
                    }
                });
            } else {
                await route.fulfill({
                    status: 200,
                    json: {
                        content: MockUser.completeProfile
                    }
                });
            }
        });

        await this.page.route('*/**/users/signup/validation', async (route) => {
            await route.fulfill({
                json: {
                    code: '123456'
                }
            });
        });

        await this.page.route('*/**/domains', async (route) => {
            await route.fulfill({
                json: {
                    content: activities
                }
            });
        });

        await this.page.route(
            '*/**/consultations/patient/**',
            async (route) => {
                await route.fulfill({
                    json: {
                        content: Object.values(consultation)
                    }
                });
            }
        );

        await this.page.route('*/**/doctors/search', async (route) => {
            await route.fulfill({
                json: {
                    content: [
                        {
                            ...DoctorMock,
                            nextCalendar: Dates[0].calendars[0]
                        }
                    ]
                }
            });
        });

        await this.page.route(
            '*/**/calendars/doctor/group/**',
            async (route) => {
                await route.fulfill({
                    json: {
                        content: [Dates[0], Dates[1], Dates[2]]
                    }
                });
            }
        );

        await this.page.route('*/**/users/password/reset*', async (route) => {
            const email = new URL(route.request().url()).searchParams.get(
                'mail'
            );

            if (email === MockUser.completeProfile.email) {
                await route.fulfill({
                    json: {
                        message: 'Password reset link sent successfully!'
                    }
                });
            } else {
                await route.fulfill({
                    status: 400,
                    json: {
                        message: `L'utilisateur avec l'email ${email} est inconnu`
                    }
                });
            }
        });

        await this.page.route('*/**/consultations', async (route) => {
            const postData = await route.request().postData();

            if (postData) {
                await route.fulfill({
                    status: 200,
                    json: {
                        ...ConsultationMock.waiting
                    }
                });
            }
        });

        await this.page.route(
            '*/**/consultations/status/payed/**',
            async (route) => {
                consultation['confirmed'].status =
                    ConsultationMock.payed.status;
                consultation['confirmed'].transaction =
                    ConsultationMock.payed.transaction;

                await route.fulfill({
                    status: 200,
                    json: {
                        ...ConsultationMock.payed
                    }
                });
            }
        );
    }

    async goToPwdforget() {
        const forgotLink = this.page.getByTestId('forgot-password');
        await forgotLink.click();
        await this.page.waitForURL('/pwdforget', {
            waitUntil: 'load'
        });
    }

    async goToSearch() {
        const searchInput = this.page.getByTestId('search-input');
        await searchInput.click();

        await this.page.waitForURL('/search', {
            waitUntil: 'load'
        });
    }

    async selectDoctor() {
        this.mockTimer('2022-02-05T00:00:00.000Z');
        const doctorName = this.page.getByTestId('doctor-name');
        await doctorName.click();

        await this.page.waitForURL(/\/search\/detail-doctor/, {
            waitUntil: 'load'
        });
        await this.page.waitForSelector(
            '[data-testid="appointment-notification"]'
        );
    }

    async selectAppointment() {
        const buttonAppointment = this.page.getByTestId('taking');
        await buttonAppointment.click();

        await this.page.waitForURL(/\/taking/, {
            waitUntil: 'load'
        });
    }

    async goToMeetings() {
        const meetingTab = this.page.locator(
            '[data-testid="tabs"] ion-item:nth-of-type(2)'
        );
        await meetingTab.click();
        await this.hideLoader();
        await this.page.waitForURL('/tabs/meetings');
    }

    async selectConsultation(text: string) {
        const selectedconsultation = this.page
            .getByTestId('meeting-status')
            .getByText(text)
            .first();
        await selectedconsultation.click();
        await this.page.waitForURL(/\/consultation/, {
            waitUntil: 'load'
        });
    }
}
